\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amssymb,amsfonts,textcomp}
\usepackage{color}
\usepackage{longtable}
\usepackage{hyperref}
\hypersetup{colorlinks=true, linkcolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue}
\usepackage{graphicx}
\usepackage{epsfig}
\setlength\tabcolsep{1mm}
\renewcommand\arraystretch{1.3}
\begin{document}

\title{ProStack and Omero}
\author{Konstantin Kozlov\\
  Department of Computational biology,\\
  Center for Advanced Studies, St.Petersburg State Polytechnical University,\\
  St.Petersburg,\\
  Russian Federation,\\
  Polytechnicheskaya ul., 29\\
  \texttt{kozlov@spbcas.ru}}
\date {\today}
\maketitle


\begin{abstract}

This guide describes the modules of the ProStack image processing platform that make it possible to communicate with OMERO on the GNU/Linux operating system. The guide is targeted at the scientists that store experimental images in OMERO and need to process those images with complex scenarios. 

It is assumed that OMERO and ProStack software are properly installed and run in production mode at user's site, the user has some images in OMERO and is able to use OMERO client programms. Installation and configuration procedures are not described here.

It is assumed that a reader is able to run scripts from a command line, set environment variables up and understand the basics of the client-server software architecture. These terms and actions are not explained here.

The authors are not affiliated in any way with OMERO Team nor any of the latter is affiliated with ProStack Team. The solution described here was developed using publicly available information and source code.

\end{abstract}
\newpage
\setcounter{tocdepth}{2}
\renewcommand\contentsname{Table of Contents}
\tableofcontents
\listoffigures
%\listoftables
\newpage
\section{Team}
Konstantin N. Kozlov(kozlov@spbcas.ru), Andrei S. Pisarev(pisarev@spbcas.ru).

The leader of the project Maria G. Samsonova(samson@spbcas.ru).

\section{License}

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHAN\-TABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place {}- Suite 330, Boston, MA 02111{}-1307, USA.

\href{http://www.fsf.org/licensing/licenses/gpl.html}{http://www.fsf.org/licensing/licenses/gpl.html}

\section{System requirements}

The modules to communicate with OMERO are currently included in the following ProStack distributions starting from version 2.5.1:
\begin{itemize}
\item RPM for Fedora 13 and later -- prostack-omero*.rpm, prostack-omero-browser-*.rpm;
\item binary bundles for GNU/Linux;
\item binary bundles for Mac OS X.
\end{itemize}

Though both ProStack and OMERO work on Microsoft Windows the modules described here were not tested on this platform. This means that they may or may not work there. If a reader is eager to test the software he may contact the authors for further information and explanation of known problems.

The OMERO software is written in Java and requires Java 1.5. It is sufficient if standard OMERO clients run on your machine. The ProStack modules were developed in Java and tested with OMERO platform v4.1 and the distribution includes Java libraries from OMERO clients distribution.

\section{Terms, Abbreviations and Notation}

\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{r1.eps} \caption{A Workflow is a set of Nodes and Connections}
\label{seq:refFigure0}
\end{figure*}

Workflow {--} a complex scenario or pipeline of operations on some data (see Figure~\ref{seq:refFigure0}). User specifies only inputs. The output of one operation is automatically fed to downstream operations. A workflow can be represented as a Directed Acyclic Graph (DAG). Another synonym is Workspace. 

WM {--} workflow module. The operation that is seen by a software as one command and is displayed to the user as a rectangle. It can be also called as Node or operator.

Each WM has several or zero Input and Output Ports. Input Ports are drawn as small rectangles on the left hand side of a Node and Output Ports {--} on the right hand side.

WM's are connected with Connections that are displayed as lines in the GUI. Each Input Port can have one and only one Connection. Each Output Port can have any or zero number of Connections.

Consequently, a Workflow is a set of Nodes and Connections. It is stored as a text file in AP format. This file can be edited manually or graphically with the \textbf{Graphical scenario builder}.

In the following sections the 
\textsc{Left click} denotes an event when the user presses and releases the left button of a mouse. 
\textsc{Middle click} corresponds to the middle button and
\textsc{Right click} corresponds to the right button. If there are only two buttons on the mouse, the event of pressing the middle one is usually emulated by the simultaneous pressing of both left and right buttons. The emulation is done on the system level and thus is outside the scope of this manual. The
\textsc{Shift+middle click} denotes an event that happens when the user presses and releases the middle button of a mouse while holding the \textsc{Shift} key pressed. The
\textsc{Ctrl+middle click} denotes an event that happens when the user presses and releases the middle button of a mouse while holding the \textsc{Ctrl} key pressed.

On Mac OS X with the mouse pointer that has only one button \textsc{Left click}, \textsc{Right click} and \textsc{Middle click} is emulated by pressing the modifier key on the keyboard and the mouse button simultaneously. As a result, the \textsc{Left click} corresponds to the event when the user presses and releases the mouse button while holding the modifier key pressed and the \textsc{Ctrl+middle click} denotes an event that happens when the user presses and releases the button of a mouse while holding the modifier and the \textsc{Ctrl} keys pressed. Consult the manual for your OS installation for the correct key combinations.

\section{List of modules}

There are four ProStack modules for interaction with OMERO: file, omero\_exporter, omero\_exporter\_i and omero\_importer. The first three are used to get the image data from the OMERO server and the last one uploads the processing results. The user credentials are specified for each of the module manually and verified on each connection. The user should be aware that the credentials are saved in the scenario in clear text.

\subsection{file - Browse the OMERO repository}
\label{op:omero_file}
If non of the operators is selected in the main window the \textsc{Middle click} in the scenario builder inserts the node 'file'. If ProStack modules for OMERO are correctly installed the options dialog for 'file' node contains the button 'Network' in the bottom-right corner.

\subsection{omero\_exporter - Get image from OMERO server}
\label{op:omero_exporter}
This operator downloads the requested image from OMERO server. It uses Java OMERO Client API for communication and saves the image in OME-TIFF format. The path to the image from OMERO server root, i.e. the Project, Dataset and Image names should be provided by the node linked to the input port of omero\_exporter. The node 'file' provides such functionality.

\noindent
\textsc{INPUT}\\
\texttt{1 image:.tif}\\
\textsc{OUTPUT}\\
\texttt{1 image:.tif}\\
\textsc{PARAMETERS}\\
\texttt{Host} -- the hostname of the OMERO server,\\
\texttt{Port} -- the port number that OMERO server is listening on (usually - 4063), \\
\texttt{Login} -- user name,\\
\texttt{Password}

\subsection{omero\_exporter\_i - Get selected image from OMERO server}
\label{op:omero_exporter_i}
This operator downloads the requested image from OMERO server. It uses Java OMERO Client API for communication and saves the image in OME-TIFF format. The path to the image from OMERO server root, i.e. the Project, Dataset and Image names are the options of this operator and need to be defined manually.

\noindent
\textsc{OUTPUT}\\
\texttt{1 image:.tif}\\
\textsc{PARAMETERS}\\
\texttt{Host} -- the hostname of the OMERO server,\\
\texttt{Port} -- the port number that OMERO server is listening on (usually - 4063), \\
\texttt{Login} -- user name,\\
\texttt{Password},\\
\texttt{Project},\\
\texttt{Dataset},\\
\texttt{Image}

\subsection{omero\_importer - Import an image to OMERO server}
\label{op:omero_importer}
This operator uploads an input image to OMERO server. It uses command line importer from OMERO Clients suite.

\noindent
\textsc{INPUT}\\
\texttt{1 image:.tif}\\
\textsc{PARAMETERS}\\
\texttt{Host} -- the hostname of the OMERO server,\\
\texttt{Port} -- the port number that OMERO server is listening on (usually - 4063), \\
\texttt{Login} -- user name,\\
\texttt{Password},\\
\texttt{Dataset} -- the numeric id of the dataset,\\
\texttt{Image}

\section{Step-by-step how-to}

\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{compl.eps} \caption{A Workflow for processing images from OMERO}
\label{fig:compl}
\end{figure*}

\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{fileadd.eps} \caption{The 'file' node is inserted in the scenario builder}
\label{fig:fileadded}
\end{figure*}

\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{networkseen.eps} \caption{The button 'Network' is in the bottom-left corner}
\label{fig:networkseen}
\end{figure*}


\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{credbros.eps} \caption{Type the hostname and port number together with user's credentials}
\label{fig:credbros}
\end{figure*}

\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{selectedinbros.eps} \caption{Navigate to the needed image in the browser and press 'Select'}
\label{fig:selectinbros}
\end{figure*}

\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{expopts.eps} \caption{The user's credentials are to be provided to the omero\_exporter module}
\label{fig:expopts}
\end{figure*}

\begin{figure*}[!h]
\centering\includegraphics[scale=0.5]{impopts.eps} \caption{The settings for omero\_importer module}
\label{fig:inpopts}
\end{figure*}

We will start with the new workspace and build a scenario shown in~\ref{fig:compl} that fetches the 3D image from OMERO server, computes the maximal projection and stores the resulting image back to the server. The image represents the nuclei in Drosophila embryo.

First of all, let's insert a 'file' node to the new workflow. If non of the operators is selected in the main window the \textsc{Middle click} in the scenario builder inserts the 'file' node~\ref{fig:fileadded}. Open options dialog for 'file' node. If ProStack modules for OMERO are properly installed the dialog contains the button 'Network'~\ref{fig:networkseen} in the bottom-left corner. Press this button to connect to OMERO server, type the hostname and port number together with user's credentials~\ref{fig:credbros}. Navigate to the needed image in the browser and press 'Select'~\ref{fig:selectinbros}.

Insert omero\_exporter module to the scenario and open the options dialog~\ref{fig:expopts}. The hostname and the port number are to be provided together with user's credentials. The maximum projection is computed by the 'max3d' operator that is to be inserted next.

The module omero\_importer is used to insert the resulting image into the dataset~\ref{fig:inpopts}.

\bibliographystyle{plain}
\bibliography{parus}


\end{document}
