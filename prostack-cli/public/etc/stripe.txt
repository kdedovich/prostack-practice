NAME
stripe - Excision of horizontal stripe. 

PROGRAM NAME
plp8stripe

DESCRIPTION 
This operator reserves the quantitative information and information about localization obtained  by solo operator from some horizontal stripe. The stripe is set in percentage on a vertical axis.
USAGE
plp8stripe <low value in per cent on a vertical axis,high value in per cent on a vertical axis> <input text file name> <output text file name>

ProStack command
prostak 

REQUIRED ARGUMENTS 
