This operator reduces the intensity of salt and pepper noise in an
image using the Crimmins complementary hulling
algorithm~cite{crimmins85. This algorithm smoothes the image by
reducing the magnitude of locally inconsistent pixels, as well as by
increasing the magnitude of pixels in the neighbourhood surrounding
a spike. The spike is defined here as a pixel whose value is
different from its surroundings by more than 2 intensity levels.
Increasing number of iterations of the algorithm can introduce an effect 
of blurring of the image. In the ultimate case all sharp gradients will be 
smoothed down to a magnitude of 2 intensity levels.


INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Iterations - number of sweeps.
