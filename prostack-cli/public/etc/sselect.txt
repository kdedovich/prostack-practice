NThis operator generates a binary image by thresholding the input image. 
Pixel values greater than the cutoff value are set to 255 in the output image. 
Pixel values less or equal to the cutoff are set to 0. The cutoff value can be 
specified by a user or calculated by Otsu method, which chooses the threshold 
to minimize the intraclass variance of  black and white pixels. 
Second output gives the actual threshold used.


INPUT
1 image:.tif
OUTPUT
1 image:.tif
2 text:.txt
PARAMETERS
Threshold level - the threshold value,
Method - plain or otsu,
Process? - do actual transformation or not.
