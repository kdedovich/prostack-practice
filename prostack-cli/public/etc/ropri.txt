This operator calculates the list of points in polar coordinates that 
determines the inner and outer borders of the round strip. The center 
of the polar coordinates is calculated from the first input. The second 
input is the list of blobs with polar coordinates of centroids that 
can be calculated by the blo2pol operator. 
The output is sorted with respect to the polar angle.


INPUT
1 image:.tif
2 text:.txt
OUTPUT
1 text:.txt
