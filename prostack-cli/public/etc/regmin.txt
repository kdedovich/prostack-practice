This operator produces the image of the areas of local intensity minima in the input image.


INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Minimal number of pixels
Maximal number of pixels
Maximal number of blobs
Connectivity - 4 or 8.
