This operator does successive dilations of the input image by the
structural element supplied as the second input. The structural
element can be calculated by the strel
operator.


INPUT
1 image:.tif
2 text:.txt
OUTPUT
1 image:.tif
PARAMETERS
Repetitions - number of sweeps.
