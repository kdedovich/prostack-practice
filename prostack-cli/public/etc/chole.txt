This operator fills the regions of "off" pixels surrounded by "on"
pixels using a structural element. Unclosed contours are erased. The
structural element can be calculated by the strel
(see~ref{op:strel) operator.


INPUT
1 image:.tif
2 text:.txt - structural element
OUTPUT
1 image:.tif
