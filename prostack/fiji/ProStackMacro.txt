name = getArgument();
if (name=="") exit ("No argument!");
argv=split(name,"@");
argc=lengthOf(argv);
// macro,arg,input,output;
hasArg=0;
hasInput=0;
hasOutput=0;
arg="";
inputname="";
outputname="";
if ( argc > 0 ) {
	macroname=argv[0];
}
if ( argc > 1 ) {
	if ( argv[1] != "-" ) {
		arg=argv[1];
		hasArg=1;
	}
}
if ( argc > 2 ) {
	if ( argv[2] != "-" ) {
		inputname=argv[2];
		hasInput=1;
	}
}
if ( argc > 3 ) {
	if ( argv[3] != "-" ) {
		outputname=argv[3];
		hasOutput=1;
	}
}
print("Argc:"+argc);
print("Macro:"+macroname);
print("Arg:"+arg);
print("Input:"+inputname);
print("Output:"+outputname);
if ( hasInput > 0 ) open(inputname);
if ( hasArg > 0 )
	runMacro(macroname,arg);
else
	runMacro(macroname);
if ( hasOutput > 0 ) {
	selectImage(1);
	saveAs("tif",outputname);
}

