name = getArgument();
if (name=="") exit ("No argument!");
argv=split(name,"@");
argc=lengthOf(argv);
// macro,input,output;
hasInput=0;
hasOutput=0;
inputname="";
outputname="";
if ( argc > 0 ) {
	macroname=argv[0];
}
if ( argc > 1 ) {
	if ( argv[1] != "-" ) {
		inputname=argv[1];
		hasInput=1;
	}
}
if ( argc > 2 ) {
	if ( argv[2] != "-" ) {
		outputname=argv[2];
		hasOutput=1;
	}
}
print("Argc:"+argc);
print("Macro:"+macroname);
print("Input:"+inputname);
print("Output:"+outputname);
if ( hasInput > 0 ) open(inputname);
runMacro(macroname);
if ( hasOutput > 0 ) {
	selectImage(1);
	saveAs("tif",outputname);
}

