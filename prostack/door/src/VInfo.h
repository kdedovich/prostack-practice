/***************************************************************************
 *            VInfo.h
 *
 *  Wed Feb 15 12:21:11 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _VINFO_H
#define _VINFO_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct VInfo {
	int n;
	int *array;
} VInfo;

VInfo*readVInfo(FILE*f);
void deleteVInfo(VInfo*info);
void vinfoSave(VInfo*in, FILE*f);
void vinfoPrint(VInfo*in, char*str);

#ifdef __cplusplus
}
#endif

#endif /* _VINFO_H */
