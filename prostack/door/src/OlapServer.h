/***************************************************************************
 *            OlapServer.h
 *
 *  Tue Feb 14 15:07:02 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _OLAPSERVER_H
#define _OLAPSERVER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <glib.h>

typedef struct OlapServer {
	char*hname;
	int port;
	int ID;
	char*nicname;
	char*address;
	char*proto;
	char*login;
	char*password;
	int is_default;
} OlapServer;

void setErrorOlapServer(void (*ptr)(char*msg));
void setLoggerOlapServer(void (*ptr)(char*msg));
void deleteOlapServer(OlapServer*oServer);
OlapServer*dupOlapServer(OlapServer*o);
OlapServer*newOlapServer(char*host, int port, int ID);
OlapServer*olapserverParse(GKeyFile*gkf, char*grp, int ID);
void olapserverPrint(OlapServer*os, GKeyFile*gkf);
OlapServer*newOlapServerV();
OlapServer*olapserver_from_string(char*str);

#ifdef __cplusplus
}
#endif

#endif /* _OLAPSERVER_H */
