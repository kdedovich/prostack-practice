/***************************************************************************
 *            ListOfNodes.c
 *
 *  Wed Feb 15 11:08:40 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <Node.h>
#include <ListOfNodes.h>
#include <Connection.h>

static void (*logger)(char*msg);
static void (*error)(char*msg);
static short isSetError = 0;
static short isSetLogger = 0;

void setErrorLon(void (*ptr)(char*msg))
{
	isSetError = 1;
	error = ptr;
}


void add2ListOfNodes(ListOfNodes *list, Node*f)
{
	ListOfNodes *l1, *l2;
	l1 = list;
	l2 = list;
	while ( l1 ) {
		l2 = l1;
		l1 = l1->next;
	}
	l1 = newListOfNodes();
	l2->next = l1;
	l1->prev = l2;
	l1->node = f;
	l1->next = (ListOfNodes*)NULL;
}

ListOfNodes *listOfNodesInitWithNode(Node*ne)
{
	ListOfNodes *list;
	list = newListOfNodes();
	list->next = (ListOfNodes*)NULL;
	list->prev = (ListOfNodes*)NULL;
	list->node = ne;
	return list;
}

ListOfNodes *newListOfNodes()
{
	ListOfNodes *list;
	list = (ListOfNodes *)malloc(sizeof(ListOfNodes));
	return list;
}

void installConnection(ListOfNodes *lon, Connection*conn)
{
	ListOfNodes *l1, *l2;
	if ( conn->sourcePortID < 1 || conn->destPortID < 1 ) {
		error("source or dest port for connection is less then 1");
		return;
	}
	l1 = lon;
	while ( l1 && l1->node->ID != conn->sourceNodeID )
		l1= l1->next;
	if ( !l1 && isSetError ) {
		error("no source node for connection");
		return;
	}
	if ( l1->node->nOutConns < conn->sourcePortID && isSetError ) {
		error("no source port for connection");
		return;
	}
	l1->node->outConn[conn->sourcePortID - 1].channel = (Connection**)realloc(l1->node->outConn[conn->sourcePortID - 1].channel, (l1->node->outConn[conn->sourcePortID - 1].nConn + 1) * sizeof(Connection*));
	l1->node->outConn[conn->sourcePortID - 1].channel[l1->node->outConn[conn->sourcePortID - 1].nConn] = conn;
	l1->node->outConn[conn->sourcePortID - 1].nConn++;
	l1->node->terminal = 0;
	l2 = lon;
	while ( l2 && l2->node->ID != conn->destNodeID )
		l2= l2->next;
	if ( !l2 && isSetError ) {
		error("no dest node for connection");
		return;
	}
	if ( l2->node->nInConns < conn->destPortID && isSetError ) {
		error("no dest port for connection");
		return;
	}
	l2->node->inConn[conn->destPortID - 1].channel = (Connection**)realloc(l2->node->inConn[conn->destPortID - 1].channel, (l2->node->inConn[conn->destPortID - 1].nConn + 1) * sizeof(Connection*));
	l2->node->inConn[conn->destPortID - 1].channel[l2->node->inConn[conn->destPortID - 1].nConn] = conn;
	l2->node->inConn[conn->destPortID - 1].nConn++;
	g_mutex_lock (&(l1->node->statusMutex));
	if ( l1->node->status == completed ) {
		g_mutex_lock (&(l2->node->statusMutex));
		g_mutex_lock (&(conn->connMutex));
		conn->host = (char*)calloc( strlen(l1->node->oServer->hname) + 1, sizeof(char));
		conn->host = strcpy(conn->host, l1->node->oServer->hname);
		conn->proto = g_strdup(l1->node->oServer->proto);
		conn->address = g_strdup(l1->node->oServer->address);
		if ( l1->node->type == pam ) {
			char *ty;
			if ( l1->node->outConn[conn->sourcePortID - 1].type == any && l1->node->nInConns > 0 ) {
				char**ext = g_strsplit(l1->node->inConn[0].channel[l1->node->inConn[0].index]->file, ".", MAXTOKENS);
				int next = g_strv_length(ext);
				ty = g_strdup(ext[next-1]);
				g_strfreev(ext);
			} else {
				ty = type2string2(l1->node->outConn[conn->sourcePortID - 1].type);
			}
			conn->file = g_strdup_printf("%s.out.%d.%s", l1->node->uid, conn->sourcePortID - 1, ty);
			free(ty);
			conn->ptype = path2type2(conn->file);
		} else if ( l1->node->type == ins ) {
			conn->file = g_path_get_basename(l1->node->uid);
		} else if ( l1->node->type == file ) {
			conn->file = g_path_get_basename(l1->node->file);
		}
		conn->port = l1->node->oServer->port;
		conn->flag = 1;
		g_mutex_unlock (&(l1->node->statusMutex));
		g_mutex_unlock (&(conn->connMutex));
		g_mutex_unlock (&(l2->node->statusMutex));
	} else {
		g_mutex_unlock (&(l1->node->statusMutex));
	}
}


void uninstallConnection(ListOfNodes *lon, Connection*conn)
{
	ListOfNodes *l1, *l2;
	Node*ne2;
	int h, i;
	if ( conn->sourcePortID < 1 || conn->destPortID < 1 ) {
		error("source or dest port for connection is less then 1");
		return;
	}
	l1 = lon;
	while ( l1 && l1->node->ID != conn->sourceNodeID )
		l1= l1->next;
	if ( !l1 && isSetError ) {
		error("no source node for connection");
		return;
	}
	if ( l1->node->nOutConns < conn->sourcePortID && isSetError ) {
		error("no source port for connection");
		return;
	}
	ne2 = l1->node;
	h = conn->sourcePortID - 1;
	if ( ne2->outConn[h].nConn > 0 ) {
		for ( i = 0; i < ne2->outConn[h].nConn - 1; i++ ) {
			if ( ne2->outConn[h].channel[i] == conn ) {
				ne2->outConn[h].channel[i] = ne2->outConn[h].channel[ne2->outConn[h].nConn - 1];
				break;
			}
		}
		ne2->outConn[h].nConn--;
		ne2->outConn[h].channel = (Connection**)realloc(ne2->outConn[h].channel, ne2->outConn[h].nConn * sizeof(Connection*) );
	}
	nodeCheckTerminal(ne2);
	l2 = lon;
	while ( l2 && l2->node->ID != conn->destNodeID )
		l2= l2->next;
	if ( !l2 && isSetError ) {
		error("no dest node for connection");
		return;
	}
	if ( l2->node->nInConns < conn->destPortID && isSetError ) {
		error("no dest port for connection");
		return;
	}
	ne2 = l2->node;
	h = conn->destPortID - 1;
	if ( ne2->inConn[h].nConn > 0 ) {
		for ( i = 0; i < ne2->inConn[h].nConn - 1; i++ ) {
			if ( ne2->inConn[h].channel[i] == conn ) {
				ne2->inConn[h].channel[i] = ne2->inConn[h].channel[ne2->inConn[h].nConn - 1];
				break;
			}
		}
		ne2->inConn[h].nConn--;
		if ( ne2->inConn[h].nConn == 0 ) {
			ne2->inConn[h].channel[0] = (Connection*)NULL;
			free(ne2->inConn[h].channel);
			ne2->inConn[h].channel = (Connection**)NULL;
		} else {
			ne2->inConn[h].channel = (Connection**)realloc(ne2->outConn[h].channel, ne2->outConn[h].nConn * sizeof(Connection*) );
		}
	}
	g_mutex_lock (&(ne2->statusMutex));
	if ( ne2->status != waiting ) {
		g_mutex_unlock (&(ne2->statusMutex));
		listOfNodesPropagateStatus(lon, ne2, waiting);
	} else {
		g_mutex_unlock (&(ne2->statusMutex));
	}
}

Node*nodeByID(ListOfNodes*lon, int ID)
{
	ListOfNodes*l1;
	char *buf;
	l1= lon;
	while ( l1 && l1->node->ID != ID )
		l1= l1->next;
	if ( !l1 && isSetError ) {
		buf = (char*)calloc(MAX_RECORD, sizeof(char));
		sprintf(buf, "no node with ID %d", ID);
		error(buf);
		free(buf);
		return NULL;
	}
	return l1->node;
}

int hasNodeByID(ListOfNodes*lon, int ID)
{
	ListOfNodes*l1;
	for ( l1= lon; l1; l1 = l1->next )
		if ( l1->node->ID == ID )
			return 1;
	return 0;
}


void deleteListOfNodes(ListOfNodes*lon)
{
	ListOfNodes*curr, *temp;
	curr = lon; 
	while ( curr) {
		temp = curr;
		curr = curr->next;
		free(temp);
	}
}

int maxIDListOfNodes(ListOfNodes *lon)
{
	ListOfNodes *curr;
	int ID;
	ID = 0;
	for(curr = lon; curr; curr = curr->next) {
		if ( curr->node->ID > ID ) {
			ID = curr->node->ID;
		}
	}
	return ID;
}

int maxMacroIDListOfNodes(ListOfNodes*lon)
{
	ListOfNodes *curr;
	int ID;
	ID = 0;
	for(curr = lon; curr; curr = curr->next) {
		if ( curr->node->type == macro && curr->node->info->array[5] > ID ) {
			ID = curr->node->info->array[5];
		}
	}
	return ID;
}

Node *macro_node_by_number(ListOfNodes *lon, int MACRO_NUMBER)
{
	Node *ne;
	ListOfNodes*curr;
	ne = (Node*)NULL;
	for(curr = lon; curr; curr = curr->next) {
		if ( curr->node->type == macro && curr->node->info->array[5] == MACRO_NUMBER ) {
			ne = curr->node;
			break;
		}
	}
	return ne;
}

int macroNodes(ListOfNodes*lon)
{
	ListOfNodes *curr;
	int N;
	N = 0;
	for(curr = lon; curr; curr = curr->next) {
		if ( curr->node->type == macro ) {
			N++;
		}
	}
	return N;
}
