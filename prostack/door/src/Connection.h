/***************************************************************************
 *            Connection.h
 *
 *  Wed Feb 15 12:40:35 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _CONNECTION_H
#define _CONNECTION_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif
#ifndef MAXTOKENS
#define MAXTOKENS 256
#endif

typedef enum PortType {
	any,
	txt,
	tif,
	png,
	bmp,
	jpg,
	gif,
	asc,
	zip,
	dat,
	lqr,
	csv,
	pdf,
	unk
} PortType;

typedef struct Connection {
	int sourceNodeID;
	int sourcePortID;
	int destNodeID;
	int destPortID;
	char*file;
	char*host;
	char*proto;
	char*address;
	int port;
	short flag;
	PortType ptype;
	PortType meta_ptype;
	GMutex connMutex;
} Connection;

Connection*newConnection(int sourceNodeID, int sourcePortID, int destNodeID, int destPortID);
void deleteConnection(Connection*conn);
Connection*dupConnection(Connection*p);
int connection_equal(Connection*conn1, Connection*conn2);

#ifdef __cplusplus
}
#endif

#endif /* _CONNECTION_H */
