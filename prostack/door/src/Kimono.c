/***************************************************************************
 *            Kimono.c
 *
 *  Wed Feb 20 11:37:26 2008
 *  Copyright  2008  kozlov
 *  <kozlov@freya>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include <sqlite3.h>
#include <time.h>
#include <sys/time.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Kimono.h>
#define DB_FILE "door/kimono-db.db"
#define JAVA_DIR "prostack/java"
#define DEFLANG "en_US.UTF-8"
#define MAX_TOKENS 256

#define KIMONO_ERROR g_spawn_kimono_error_quark ()
GQuark g_spawn_kimono_error_quark (void)
{
	return g_quark_from_static_string ("kimono-error-quark");
}

typedef enum
{
	KIMONO_ERROR_DUMP, /* Couldn't install new method */
	KIMONO_ERROR_RESTORE /* Couldn't install new method */
} KimonoError;

static char*user;
static char*lang;
static char*config_home; /* /home/user/.bambu */
static char*db_name;
static int default_port_num = 0;
static sqlite3 *conn;
static GMutex sq_mutex;
static char*db;
static GMainContext *gmaincont;
static GMainLoop *gmainloop;
static GCond gcond;
static GMutex gmutex;
static char*my_table;
static char*temp;
static gboolean loopisrunning;
static gint thread_pool_size = -1;
static char*prostack_java_dir;
static GThread *gmainthread;

static GOptionEntry entries[] =
{
	{ "database", 0, 0, G_OPTION_ARG_STRING, &db_name, "database", "db" },
	{ "language", 0, 0, G_OPTION_ARG_STRING, &lang, "language", "lang" },
	{ "default_port_num", 0, 0, G_OPTION_ARG_INT, &default_port_num, "port_num", "port" },
	{ "conf_dir", 0, 0, G_OPTION_ARG_STRING, &config_home, "runtime conf", "conf" },
	{ "user", 0, 0, G_OPTION_ARG_STRING, &user, "user", "user" },
	{ "thread_pool_size", 0, 0, G_OPTION_ARG_INT, &thread_pool_size, "thread pool size", "thread pool size" },
	{ NULL }
};

static int callback(void *data, int argc, char **argv, char **azColName){
	int i;
	int *n;
	n = (int *)data;
	(*n)++;
	return 0;
}

gint kim_get_thread_pool_size()
{
	return thread_pool_size;
}

char*door_get_config_home()
{
	return config_home;
}

char*door_get_db_name()
{
	return db_name;
}

char*door_get_user()
{
	return user;
}

GOptionGroup *kimono_get_option_group()
{
	GOptionGroup *gog;
	gog = g_option_group_new("kimono", "options for database", "specify database and user", NULL, NULL);
	g_option_group_add_entries(gog, (const GOptionEntry *)entries);
	return gog;
}

int busy_cb(void*data, int count)
{
#ifdef G_OS_WIN32
	g_debug("Oops count = %d\n", count);
	if ( count > 40 ) {
		g_error("SQLite DB BUSY %d times!\natKimono.c:107", count);
	}
	g_usleep(10 * G_USEC_PER_SEC);
	return count - 50;
#else
	g_debug("Oops count = %d\n", count);
	g_usleep(10 * G_USEC_PER_SEC);
	return count - 50;
#endif
}

void kimono_init(int*argc, char***argv, GError **err)
{
	int res, i, j;
	char *scmd, *cmdl, *buf;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	char *zErrMsg = 0;
	int clength;
	int user_db_exists;
	GError *gerror = NULL;
	GOptionContext *context;
	gsize size;
	default_port_num = door_init_config_port();
	if ( argc ) {
		context = g_option_context_new ("");
		g_option_context_add_group(context, kimono_get_option_group());
		g_option_context_set_ignore_unknown_options(context, TRUE);
		if (!g_option_context_parse (context, argc, argv, &gerror)) {
			g_error ("option parsing failed: %s\n", gerror->message);
		}
		g_option_context_free (context);
	}
	if ( !config_home )
		config_home = g_build_filename (g_get_home_dir(), ".bambu", NULL);
	if ( !db_name )
		db_name = g_strdup("kimono-db.db");
	if ( !lang ) {
		gchar *env_lang;
		env_lang = (gchar*)g_getenv("LC_ALL");
		if ( !env_lang )
			env_lang = (gchar*)g_getenv("LC_CTYPE");
		if ( !env_lang )
			env_lang = (gchar*)g_getenv("LANG");
		if ( env_lang )
			lang = g_strdup(env_lang);
		else
			lang = g_strdup(DEFLANG);
	}
	if ( !user )
		user = g_strdup(g_get_user_name());
	buf = g_strdup_printf("%s.%s", db_name, lang);
	db = g_build_filename (config_home, buf, NULL);
	g_free(buf);
	user_db_exists = 0;
	if ( !g_file_test((const gchar *)db, G_FILE_TEST_EXISTS) ) {
		GError*gerror = NULL;
		buf = g_strdup_printf("%s.%s", subst_data_dir(DB_FILE), lang);
		if ( !g_file_get_contents(buf, &scmd, &size, &gerror) ) {
			if (gerror) {
				g_debug("Can't initialize %s: %s\n at Kimono.c:159", buf, gerror->message);
				g_error_free(gerror);
				gerror = NULL;
			} else {
				g_debug("Can't initialize: %s\n at Kimono.c:163", buf);
			}
			g_free(buf);
			g_free(lang);
			g_free(db);
			lang = g_strdup(DEFLANG);
			buf = g_strdup_printf("%s.%s", db_name, lang);
			db = g_build_filename (config_home, buf, NULL);
			g_free(buf);
			buf = g_strdup_printf("%s.%s", subst_data_dir(DB_FILE), lang);
			if ( !g_file_get_contents(buf, &scmd, &size, &gerror) ) {
				g_error("Can't initialize %s: %s\n at Kimono.c:174", buf, gerror->message);
			}
			g_free(buf);
		}
		clength = (int)size;
		if ( gerror ) {
			g_error_free(gerror);
			gerror = NULL;
		}
		cmdl = g_build_filename (config_home, NULL);
		if ( g_mkdir_with_parents((const gchar *)cmdl, S_IRUSR|S_IWUSR|S_IXUSR) )
			g_error("Can't initialize: %s\n at Kimono.c:185", cmdl);
		if ( !g_file_test((const gchar *)db, G_FILE_TEST_EXISTS) ) {
			if ( !g_file_set_contents(db, scmd, size, &gerror) ) {
				g_error("Can't initialize: %s\n at Kimono.c:188", gerror->message);
			}
		} else {
			user_db_exists = 1;
		}
		if ( gerror ) {
			g_error_free(gerror);
			gerror = NULL;
		}
		g_free(scmd);
		g_free(cmdl);
	} else {
		user_db_exists = 1;
	}
/* init main context */
	g_mutex_init(&gmutex);
	g_cond_init(&gcond);
	loopisrunning = 1;
	gmaincont = g_main_context_new();
	gmainloop = g_main_loop_new (gmaincont, FALSE);
	g_mutex_init(&sq_mutex);
	if ( user_db_exists == 1 ) {
		char*pdb = g_strdup_printf("%s.%s", subst_data_dir(DB_FILE), lang);
		char*p_version, *version;
		res = sqlite3_open(pdb, &conn);
		if ( res != SQLITE_OK ) {
			g_debug("Can't open %s database: %s\n", pdb, sqlite3_errmsg(conn));
			sqlite3_close(conn);
			g_free(pdb);
			pdb = g_strdup_printf("%s.%s", subst_data_dir(DB_FILE), DEFLANG);
			res = sqlite3_open(pdb, &conn);
			if ( res != SQLITE_OK ) {
				g_debug("Can't open %s database: %s\n", pdb, sqlite3_errmsg(conn));
				sqlite3_close(conn);
				exit(1);
			}
		}
		res = sqlite3_busy_handler(conn, busy_cb, NULL);
		if( res != SQLITE_OK ) {
			g_debug("SQL error: can't register busy handler\n");
			fflush(stdout);
			exit(-1);
		}
		p_version = kimono_db_get_pak_version("main");
		sqlite3_close(conn);
		res = sqlite3_open(db, &conn);
		if ( res != SQLITE_OK ) {
			g_debug("Can't open %s database: %s\n", db, sqlite3_errmsg(conn));
			sqlite3_close(conn);
			exit(1);
		}
		res = sqlite3_busy_handler(conn, busy_cb, NULL);
		if( res != SQLITE_OK ) {
			g_debug("SQL error: can't register busy handler\n");
			fflush(stdout);
			exit(-1);
		}
		version = kimono_db_get_pak_version("main");
		sqlite3_close(conn);
		if ( kimono_db_pak_version_newer(p_version, version) > 0 ) {
			g_debug("Database %s is older (version %s) than %s (version %s)\n and will be updated", db, version, pdb, p_version);
			if ( !g_file_get_contents(pdb, &scmd, &size, &gerror) ) {
				g_error("Can't initialize %s: %s\n at Kimono.c:246", buf, gerror->message);
			}
			if ( !g_file_set_contents(db, scmd, size, &gerror) ) {
				g_error("Can't initialize: %s\n at Kimono.c:249", gerror->message);
			}
			g_free(scmd);
/* restore user's pams */
			res = sqlite3_open(db, &conn);
			if ( res != SQLITE_OK ) {
				g_debug("Can't open %s database: %s\n", db, sqlite3_errmsg(conn));
				sqlite3_close(conn);
				exit(1);
			}
			res = sqlite3_busy_handler(conn, busy_cb, NULL);
			if( res != SQLITE_OK ) {
				g_debug("SQL error: can't register busy handler\n");
				fflush(stdout);
				exit(-1);
			}
			res = kimono_restore_command (&gerror);
			if ( res != 0 ) {
				g_error(gerror->message);
			}
			sqlite3_close(conn);
		} else {
			g_debug("Database %s is compatible (version %s) with %s (version %s)", db, version, pdb, p_version);
		}
		g_free(pdb);
		if ( version )
			g_free(version);
		if ( p_version )
			g_free(p_version);
	}
	cmdl = door_init_config_path();
	if ( !g_file_test((const gchar *)cmdl, G_FILE_TEST_EXISTS) ) {
		if ( g_mkdir_with_parents((const gchar *)cmdl, S_IRUSR|S_IWUSR|S_IXUSR) ) {
			g_error("Can't initialize: %s\n at Kimono.c:264", cmdl);
		}
	}
	g_free(cmdl);
	g_mutex_lock(&(sq_mutex));
	res = sqlite3_open(db, &conn);
	if ( res != SQLITE_OK ) {
		g_debug("Can't open database: %s\n", sqlite3_errmsg(conn));
		sqlite3_close(conn);
		exit(1);
	}
	res = sqlite3_busy_handler(conn, busy_cb, NULL);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: can't register busy handler\n");
		fflush(stdout);
		exit(-1);
	}
	temp = g_build_filename (config_home, "AXXXXXX", NULL);
	res = g_mkstemp(temp);
	if ( res != -1 ) {
		cmdl = g_strjoinv(" ", (*argv));
		i = strlen(cmdl);
		j = write(res, cmdl, i);
		g_free(cmdl);
		close(res);
	} else {
		g_error("Can't find temp filename!\n");
	}
	my_table = g_path_get_basename(temp);
	scmd = g_strdup_printf("CREATE TABLE %s (ID integer PRIMARY KEY, Owner varchar(80), Type varchar(80) DEFAULT 'file', Operator references Operators (Name), NodeID integer);", my_table);
	res = sqlite3_get_table(conn, scmd, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	g_mutex_unlock(&(sq_mutex));
	if( res != SQLITE_OK ) {
		g_debug("SQL error can't create 'my_table': %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		exit(-1);
	}
	g_free(scmd);
	prostack_java_dir = g_strdup(subst_data_dir (JAVA_DIR));
}

void kimono_shutdown()
{
	char *scmd;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	char *zErrMsg = 0;
	int res;
	scmd = g_strdup_printf("DROP TABLE %s;", my_table);
	g_mutex_lock(&(sq_mutex));
	res = sqlite3_get_table(conn, scmd, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	g_mutex_unlock(&(sq_mutex));
	if( res != SQLITE_OK ) {
		g_debug("SQL error can't drop 'my_table': %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		exit(-1);
	}
	g_free(db);
	g_free(user);
	g_free(config_home);
	g_free(db_name);
	g_free(lang);
	g_mutex_clear(&sq_mutex);
	sqlite3_close(conn);
	if ( !loopisrunning ) {
		g_mutex_clear(&gmutex);
		g_cond_clear(&gcond);
	} else {
		g_main_context_unref(gmaincont);
		g_mutex_clear(&gmutex);
		g_cond_clear(&gcond);
	}
	g_unlink(temp);
	g_free(my_table);
	g_free(temp);
	g_free(prostack_java_dir);
}

GSource* kimono_child_watch_add(GPid pid, GSourceFunc func, gpointer data)
{
	GSource*gsrc;
	guint gsrcid;
	gboolean result = TRUE;
/*	result = g_main_context_is_owner(gmaincont);
	if ( result ) {
		result = g_main_context_acquire(gmaincont);
	} else {
		g_mutex_lock(&(gmutex));
		result = g_main_context_wait(gmaincont, &gcond, &gmutex);
		g_mutex_unlock(&(gmutex));
	}*/
	gsrcid = -1;
/*	if ( result ) {*/
		gsrc = g_child_watch_source_new (pid);
		gsrcid = g_source_attach (gsrc, gmaincont);
		g_source_set_callback (gsrc, func, data, (GDestroyNotify) NULL);
/*		g_main_context_release(gmaincont);
		g_mutex_lock(&gmutex);
		g_cond_signal(&gcond);
		g_mutex_unlock(&gmutex);
	}*/
	return gsrc;
}

void kimono_context_run()
{
    g_main_loop_run (gmainloop);
}

void kimono_context_stop()
{
	g_main_loop_quit (gmainloop);
}

gboolean kimono_main_context_iteration()
{
	gboolean result = TRUE;
/*	result = g_main_context_is_owner(gmaincont);
	if ( result ) {
		result = g_main_context_acquire(gmaincont);
	} else {
		g_mutex_lock(&gmutex);
		result = g_main_context_wait(gmaincont, &gcond, &gmutex);
		g_mutex_unlock(&gmutex);
	}
	if ( result ) {*/
		result = g_main_context_iteration(gmaincont, FALSE);
/*		g_main_context_release(gmaincont);*/
/*		g_mutex_lock(&gmutex);
		g_cond_signal(&gcond);
		g_mutex_unlock(&gmutex);*/
/*	}*/
	return result;
}

void kimono_main_context_wakeup()
{
	gboolean result = TRUE;
	result = g_main_context_is_owner(gmaincont);
	if ( result ) {
		result = g_main_context_acquire(gmaincont);
	} else {
		g_mutex_lock(&gmutex);
		result = g_main_context_wait(gmaincont, &gcond, &gmutex);
		g_mutex_unlock(&gmutex);
	}
	if ( result ) {
		g_main_context_wakeup (gmaincont);
		g_main_context_release(gmaincont);
		g_mutex_lock(&gmutex);
		g_cond_signal(&gcond);
		g_mutex_unlock(&gmutex);
	}
}

char*kimono_get_ins_exec(char*ins_name, int node_id, int*ins_type, char**ins_id)
{
	char*ins_exec;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	ins_exec = NULL;
//	pcommand = g_string_new("SELECT executable FROM Operators WHERE name = ");
//	g_string_append_printf(pcommand, "'%s';SELECT type FROM Operators WHERE name = '%s';", pam_name, pam_name);
	pcommand = g_string_new("");
	g_string_append_printf(pcommand, "INSERT INTO %s (owner, operator, nodeid) VALUES ('%s','%s', %d); SELECT max(ID) FROM %s;", my_table, user, ins_name, node_id, my_table);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
/*	ins_exec = g_strdup(sq_result[1]);
	*ins_type = atoi(sq_result[2]);
	*ins_id = g_strdup_printf("%s%s", my_table, sq_result[3]);*/
	ins_exec = g_strdup("ins");
	*ins_type = 0;
	*ins_id = g_strdup_printf("%s%s", my_table, sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return ins_exec;
}


char*kimono_get_pam_exec(char*pam_name, int node_id, int*pam_type, char**pam_id, int use_metaname, char**pam_metaname)
{
	char*pam_exec;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	pam_exec = NULL;
	pcommand = g_string_new("SELECT DISTINCT name,executable,type FROM Operators ");
	if (use_metaname == 1) {
		g_string_append_printf(pcommand, "WHERE metaname = ( SELECT metaname FROM Operators WHERE name = '%s' ) ORDER BY metaweight DESC LIMIT 1;", pam_name);
	} else {
		g_string_append_printf(pcommand, "WHERE name = '%s';", pam_name);
	}
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK  || sq_rows < 1 || sq_columns < 3 ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	*pam_metaname = g_strdup(sq_result[3]);
	pam_exec = g_strdup(sq_result[4]);
	*pam_type = atoi(sq_result[5]);
	sqlite3_free_table(sq_result);
	pcommand = g_string_assign(pcommand, "");
	g_string_append_printf(pcommand, "INSERT INTO %s (owner, operator, nodeid) VALUES ('%s','%s', %d); SELECT max(ID) FROM %s;", my_table, user, pam_name, node_id, my_table);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK  || sq_rows < 1 || sq_columns < 1 ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	*pam_id = g_strdup_printf("%s%s", my_table, sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	if ( (*pam_type) == 3 ) {
		char*pam_ap, *pam_main_class, *pam_cmd, *pam_pl;
		char*buf;
		char**cont;
		int i;
		GError*gerror = NULL;
		pam_main_class = g_strdup(pam_exec);
		g_free(pam_exec);
		pcommand = g_string_new("SELECT apfile FROM Operators WHERE name = ");
		g_string_append_printf(pcommand, "'%s';", pam_name);
		res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
		if( res != SQLITE_OK ) {
			g_debug("SQL error: %s\n", zErrMsg);
			fflush(stdout);
			sqlite3_free(zErrMsg);
			g_string_free(pcommand, TRUE);
			g_mutex_unlock(&sq_mutex);
			g_free(pam_main_class);
			return NULL;
		}
#ifdef G_OS_WIN32
		pam_cmd = g_strdup("javaw.exe");
#else
		pam_cmd = g_strdup("java");
#endif
		cont = g_strsplit(sq_result[1], ":", MAX_TOKENS);
		if ( cont != NULL ) {
			for ( i = 0; cont[i]; i++ ) {
				buf = cont[i];
				cont[i] = g_build_filename(prostack_java_dir, buf, NULL);
				g_free(buf);
			}
			pam_ap = g_strjoinv(":", cont);
			g_strfreev (cont);
		} else {
			pam_ap = g_strdup(prostack_java_dir);
		}
		pam_exec = g_strdup_printf("%s -cp %s %s", pam_cmd, pam_ap, pam_main_class);
		g_free(pam_cmd);
		g_free(pam_ap);
		g_free(pam_main_class);
		sqlite3_free_table(sq_result);
		g_string_free(pcommand, TRUE);
	}
	g_mutex_unlock(&sq_mutex);
	return pam_exec;
}

void kimono_free_resource(char*uid)
{
	GDir*gdir;
	char*buf;
	GError *gerror = NULL;
	buf = g_build_filename (config_home, "temp", NULL);
	gdir = g_dir_open(buf, 0, &gerror);
	if ( !gdir ) {
		if ( gerror ) {
			g_debug("File error: %s\n", gerror->message);
			fflush(stdout);
			g_error_free(gerror);
		}
		return;
	}
	while ( buf = (char*)g_dir_read_name(gdir) ) {
		if ( g_str_has_prefix(buf, uid) )
			g_remove(buf);
	}
	g_dir_close(gdir);
}

char*kimono_get_pam_info(char*pam_name, char*pam_info_type)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT ");
	g_string_append_printf(pcommand, "%s FROM Operators WHERE name = '%s';", pam_info_type, pam_name);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK || sq_rows < 1 || sq_columns < 1 ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = g_strdup(sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_wksp_info(char*wksp_name, char*wksp_info_type)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT ");
	g_string_append_printf(pcommand, "%s FROM WkspLib WHERE name = '%s';", wksp_info_type, wksp_name);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = g_strdup(sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_data_info(char*data_name, char*data_info_type)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT ");
	g_string_append_printf(pcommand, "%s FROM DataLib WHERE name = '%s';", data_info_type, data_name);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = g_strdup(sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_concepts(char*label)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT name,synonym FROM ");
	pcommand = g_string_append(pcommand, label);
	pcommand = g_string_append(pcommand, ";");
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		info = g_strdup("N/A");
		return info;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_wksptypes(char*label)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT tag,name,synonym FROM ");
	pcommand = g_string_append(pcommand, label);
	pcommand = g_string_append(pcommand, ";");
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		info = g_strdup("N/A");
		return info;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_methods(char*label)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	if ( strcmp(label, "Methods") ) {
		pcommand = g_string_new("SELECT DISTINCT name,implementor FROM Operators WHERE ");
		pcommand = g_string_append(pcommand, label);
		pcommand = g_string_append(pcommand, " = 1;");
	} else {
		pcommand = g_string_new("SELECT DISTINCT name,implementor FROM Operators;");
	}
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_wksps(char*label)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	if ( strcmp(label, "WkspLib") ) {
		pcommand = g_string_new("SELECT DISTINCT tag,name,implementor FROM WkspLib WHERE ");
		pcommand = g_string_append(pcommand, label);
		pcommand = g_string_append(pcommand, " = 1;");
	} else {
		pcommand = g_string_new("SELECT DISTINCT tag,name,implementor FROM WkspLib;");
	}
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_datas(char*label)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	if ( strcmp(label, "DataLib") ) {
		pcommand = g_string_new("SELECT DISTINCT tag,name,owner,source,message,usi,login,password FROM DataLib WHERE ");
		pcommand = g_string_append(pcommand, label);
		pcommand = g_string_append(pcommand, " = 1;");
	} else {
		pcommand = g_string_new("SELECT DISTINCT tag,name,owner,source,message,usi,login,password FROM DataLib;");
	}
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_datatypes(char*label)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT tag,name,synonym FROM ");
	pcommand = g_string_append(pcommand, label);
	pcommand = g_string_append(pcommand, ";");
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		info = g_strdup("N/A");
		return info;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_wkspjobs(char*label, char*id, char*file)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT DISTINCT ");
	if ( id ) {
		pcommand = g_string_append(pcommand, id);
	} else {
		pcommand = g_string_append(pcommand, "name,inputs,outputs,message,wksplibtag,jobstatus,nodestatus,error,log");
	}
	pcommand = g_string_append(pcommand," FROM WkspJobs WHERE");
	g_string_append_printf(pcommand, " owner = '%s'", user);
	if ( label ) {
		g_string_append_printf(pcommand, " AND name = '%s'", label);
	}
	pcommand = g_string_append(pcommand, ";");
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_wkspjobs_with_status(int status)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT DISTINCT name,inputs,outputs,wksplibtag FROM WkspJobs WHERE jobstatus = ");
	g_string_append_printf(pcommand, "%d;", status);
	pcommand = g_string_append(pcommand, ";");
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_update_wkspjobs_status(char*job_name, char*value)
{
	char*info;
	int res, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("UPDATE WkspJobs SET jobstatus = ");
	g_string_append_printf(pcommand, "%s WHERE owner = '%s' AND name = '%s';", value, user, job_name);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	info = g_strdup("OK");
	return info;
}

char*kimono_update_wkspjobs_nodes(char*job_name, int node_id, char*status)
{
	char*info, **info_entries, *node_name, *node_entry, *fake;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, found, n_info, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT DISTINCT nodestatus FROM WkspJobs");
	g_string_append_printf(pcommand, " WHERE owner = '%s' AND name = '%s';", user, job_name);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK || sq_rows < 1 || sq_columns < 1 ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = g_strdup(sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	info_entries = g_strsplit(info, "\n", MAX_TOKENS);
	if ( !info_entries )
		return NULL;
	n_info = g_strv_length(info_entries);
	node_name = g_strdup_printf("%d", node_id);
	node_entry = g_strdup_printf("%d=%s", node_id, status);
	found = 0;
	for ( i = 0; i < n_info; i++ ) {
		if ( info_entries[i] && g_str_has_prefix(info_entries[i], node_name) ) {
			found = 1;
			fake = info_entries[i];
			info_entries[i] = node_entry;
			g_free(fake);
		}
	}
	if ( found == 0 ) {
		info_entries = (char**)realloc(info_entries, ( n_info + 2 ) * sizeof(char*) );
		info_entries[ n_info ] = node_entry;
		info_entries[ n_info + 1 ] = NULL;
	}
	g_free(info);
	g_free(node_name);
	info = g_strjoinv("\n", info_entries);
	g_free(node_entry);
	pcommand = g_string_new("UPDATE WkspJobs SET nodestatus = ");
	g_string_append_printf(pcommand, "'%s' WHERE owner = '%s' AND name = '%s';", info, user, job_name);
	g_free(info);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	info = g_strdup("OK");
	return info;
}

char*sqlite3_table_to_string(char**sq_result, int sq_rows, int sq_columns)
{
	int i, j;
	char*info;
	if ( sq_result !=  NULL && sq_rows > 0 && sq_columns > 0 ) {
		info = (char*)calloc(sq_rows * sq_columns * 80, sizeof(char));
		info = strcpy(info, "");
		for ( i = 1; i <= sq_rows; i++ ) {
			j = 0;
			info = strcat(info, sq_result[i * sq_columns + j]);
			for ( j = 1; j < sq_columns; j++ ) {
				info = strcat(info, "|");
				info = strcat(info, sq_result[i * sq_columns + j]);
			}
			info = strcat(info, "\n");
		}
	} else {
		info = (char*)calloc(80, sizeof(char));
		info = strcpy(info, "");
	}
	return info;
}


static int callback_get(void *data, int argc, char **argv, char **azColName){
	int i;
	GList**list;
	list = (GList**)data;
/*	for( i = 0; i < argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}*/
	(*list) = g_list_append((*list), g_strdelimit(g_strdup(argv[0] ? argv[0] : "NULL"), " ", '_'));
/*	printf("get: %-15s\n", argv[0] ? argv[0] : "NULL");
	printf("\n");*/
	return 0;
}


char*kimono_get_search(char*search, char*concept, char*implementor)
{
	char*info, **search_entries;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j, kounter, n_search, n_concepts;
	char *zErrMsg = 0;
	GString*pcommand;
	GList*oper = NULL;
	if ( search ) search = g_strdelimit(search, "*_", '%');
	if ( concept ) concept = g_strdelimit(concept, "*_", '%');
	if ( implementor ) implementor = g_strdelimit(implementor, "*_", '%');
	g_mutex_lock(&sq_mutex);
	info = NULL;
	search_entries = g_strsplit(search, "+", MAX_TOKENS);
	kounter = 0;
	n_search = g_strv_length(search_entries);
	pcommand = g_string_new("");
	if ( !concept || strlen(concept) < 2 ) {
		pcommand = g_string_assign(pcommand, "SELECT DISTINCT concept,level FROM Substitutions WHERE ");
		for ( kounter = 0; kounter < n_search - 1; kounter++) {
			g_string_append_printf(pcommand, " phrase LIKE '%s' OR", search_entries[kounter]);
		}
		g_string_append_printf(pcommand, " phrase LIKE '%s';", search_entries[n_search - 1]);
		res = sqlite3_exec(conn, pcommand->str, callback_get, &oper, &zErrMsg);
		if( res != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		n_concepts = g_list_length(oper);
	} else if ( g_strrstr((const gchar *)concept, "%") ) {
		pcommand = g_string_assign(pcommand, "SELECT DISTINCT concept,level FROM Substitutions WHERE ");
		g_string_append_printf(pcommand, " concept LIKE '%s';", concept);
		res = sqlite3_exec(conn, pcommand->str, callback_get, &oper, &zErrMsg);
		if( res != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		n_concepts = g_list_length(oper);
	} else {
		n_concepts = 1;
		oper = g_list_append(oper, g_strdup(concept));
	}
	pcommand = g_string_assign(pcommand, "SELECT DISTINCT name,implementor FROM Operators WHERE (");
	for (i = 0; i < n_concepts; i++) {
		char*t;
		t = (char*)g_list_nth_data(oper, i);
		pcommand = g_string_append(pcommand, t);
		pcommand = g_string_append(pcommand, "= 1 OR ");
		free(t);
	}
	g_list_free(oper);
	for ( kounter = 0; kounter < n_search - 1; kounter++) {
		g_string_append_printf(pcommand, " name LIKE '%s' OR", search_entries[kounter]);
	}
	g_string_append_printf(pcommand, " name LIKE '%s')", search_entries[n_search - 1]);
	if ( implementor && strlen(implementor) > 0 ) {
		g_string_append_printf(pcommand, " AND implementor LIKE '%s'", implementor);
	}
	pcommand = g_string_append(pcommand, ";");
	g_strfreev(search_entries);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_screens()
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	int i, j, kounter, n_search, n_concepts;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT name,tag,extension,executable,isdefault,titleoption FROM Screens;");
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = sqlite3_table_to_string(sq_result, sq_rows, sq_columns);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char* kimono_put_method(char*mname, char* spath, char*uri, char*contents)
{
	GError*gerror = NULL;
	int res;
	int i, j, kounter, n_search, n_descr, ntuples, n_files;
	char *zErrMsg = 0;
	GString*pcommand;
	char**files_entries, **search_entries, **descr_entries;
	char*info, *curr, *in, *req, *buf, **marg;
	gsize size;
	int type;
	gchar*Name, *Executable, *uidescr, *message, *inputs, *outputs, *ostring;
	files_entries = g_strsplit(uri, "=", MAX_TOKENS);
	if ( !files_entries || !files_entries[0] ) {
		info = g_strdup_printf("No executable name in put method");
		return info;
	}
	n_files = g_strv_length(files_entries);
	if ( n_files != 4 ) {
		g_strfreev( files_entries );
		info = g_strdup_printf("Number of entries %d not equals 4", n_files);
		return info;
	}
	if ( ( info = kimono_db_get_pak_version(user) ) == NULL ) {
		if ( ( info = kimono_db_get_pak_version("main") ) == NULL ) {
			info = g_strdup_printf("Can't get version for main package");
			return info;
		}
		pcommand = g_string_new("INSERT INTO Packages (name, version) VALUES (");
		g_string_append_printf(pcommand, "'%s','%s');", user, info);
		g_free(info);
		g_mutex_lock(&sq_mutex);
		ntuples = 0;
		res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
		if( res != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			info = g_strdup(zErrMsg);
			sqlite3_free(zErrMsg);
		}
		g_mutex_unlock(&sq_mutex);
		g_string_free(pcommand, TRUE);
		if( res != SQLITE_OK ) {
			return info;
		}
	} else {
		g_free(info);
	}
	type = atoi(files_entries[1]);
	search_entries = g_strsplit(spath, ",", MAX_TOKENS);
	if ( !search_entries ) {
		info = g_strdup_printf("Search path not defined");
		return info;
	}
	descr_entries = g_strsplit(contents, "\r\n$section\r\n", MAX_TOKENS);
	if ( !descr_entries ) {
		info = g_strdup_printf("Wrong format of description");
		return info;
	}
	n_descr = g_strv_length(descr_entries);
	pcommand = g_string_new("INSERT INTO Operators (name, implementor, executable, type");
	for ( kounter = 0; search_entries[kounter]; kounter++) {
		g_string_append_printf(pcommand, ", %s", search_entries[kounter]);
		n_search = kounter;
	}
	n_search++;
	if ( type == 1 ) { /* Just command */
		if ( n_descr != 4 ) {
			info = g_strdup_printf("Wrong format of description %d for type %d", n_descr, type);
			return info;
		}
		pcommand = g_string_append(pcommand, ", inputs, outputs, uidescription, message, metaname, metaweight");
	} else if ( type == 2 ) { /* ProStack Scenario */
		if ( n_descr != 6 ) {
			info = g_strdup_printf("Wrong format of description %d for type %d", n_descr, type);
			return info;
		}
		pcommand = g_string_append(pcommand, ", inputs, outputs, plfile, uidescription, message, apfile, metaname, metaweight");
	} else if ( type == 3 ) { /* Java Class */
		if ( n_descr != 5 ) {
			info = g_strdup_printf("Wrong format of description %d for type %d", n_descr, type);
			return info;
		}
		pcommand = g_string_append(pcommand, ", inputs, outputs, uidescription, message, apfile, metaname, metaweight");
	} else if ( type == 4 ) { /* Fiji Macro */
		if ( n_descr != 4 ) {
			info = g_strdup_printf("Wrong format of description %d for type %d", n_descr, type);
			return info;
		}
		pcommand = g_string_append(pcommand, ", inputs, outputs, uidescription, message, metaname, metaweight");
	}
	pcommand = g_string_append(pcommand, ") VALUES (");
	g_string_append_printf(pcommand, "'%s',", mname);
	g_string_append_printf(pcommand, "'%s',", user);
	g_string_append_printf(pcommand, "'%s',", files_entries[0]);
	g_string_append_printf(pcommand, "%d", type);
	for ( kounter = 0; search_entries[kounter]; kounter++) {
		g_string_append_printf(pcommand, ",%d", 1);
	}
	g_string_append_printf(pcommand, ",'%s',", files_entries[2]);
	g_string_append_printf(pcommand, "'%s'", files_entries[3]);
	for ( kounter = 0; descr_entries[kounter]; kounter++) {
		g_string_append_printf(pcommand, ",'%s'", descr_entries[kounter]);
	}
	pcommand = g_string_append(pcommand, ");");
	g_mutex_lock(&sq_mutex);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		info = g_strdup(zErrMsg);
		sqlite3_free(zErrMsg);
	} else {
		info = g_strdup("OK");
		g_print("OK");
		kimono_dump_command(pcommand->str, mname, 0);
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

void kimono_dump_command(char*command, char*mname, int delete)
{
	char*dump_file, *buf;
	int size;
	GError*gerror = NULL;
	buf = g_strdup_printf("%s.sqlt3", mname);
	dump_file = g_build_filename (config_home, buf, NULL);
	g_free(buf);
	if ( delete != 0 ) {
		if ( g_file_test (dump_file, G_FILE_TEST_EXISTS) ) {
			g_unlink (dump_file);
		}
		return;
	}
	size = strlen(command);
	if ( !g_file_set_contents(dump_file, command, (gssize)size, &gerror) ) {
		g_debug("Can't dump command! %s", gerror->message);
		g_error_free (gerror);
	}
}

int kimono_restore_command(GError**err)
{
	char*dump_file, *info;
	gchar*buf;
	gsize size;
	GError*gerror = NULL;
	GDir *dirptr;
	gchar*dir_entry, *fn;
	g_return_val_if_fail (err == NULL || *err == NULL, 1);
	dirptr = g_dir_open(config_home, 0, NULL);
	if ( !dirptr ) {
		if ( gerror != NULL ) {
			g_propagate_error (err, gerror);
			return 1;
		}
		g_set_error (err,
                   KIMONO_ERROR,                 /* error domain */
                   KIMONO_ERROR_RESTORE,            /* error code */
                   "Can't open %s", /* error message format string */
                   config_home);
		return 1;
	}
	if ( ( info = kimono_db_get_pak_version(user) ) == NULL ) {
		GString*pcommand;
		int ntuples;
		char *zErrMsg = 0;
		int res;
		if ( ( info = kimono_db_get_pak_version("main") ) == NULL ) {
			g_set_error (err,
                   KIMONO_ERROR,                 /* error domain */
                   KIMONO_ERROR_RESTORE,            /* error code */
                   "Can't get version of package %s", /* error message format string */
                   "main");
			return 1;
		}
		pcommand = g_string_new("INSERT INTO Packages (name, version) VALUES (");
		g_string_append_printf(pcommand, "'%s','%s');", user, info);
		g_free(info);
		g_mutex_lock(&sq_mutex);
		ntuples = 0;
		res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
		if( res != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			g_set_error (err,
                   KIMONO_ERROR,                 /* error domain */
                   KIMONO_ERROR_RESTORE,            /* error code */
                   "Error: %s", /* error message format string */
                   zErrMsg);
			sqlite3_free(zErrMsg);
		}
		g_mutex_unlock(&sq_mutex);
		g_string_free(pcommand, TRUE);
		if( res != SQLITE_OK )
			return 1;
	} else {
		g_free(info);
	}
	while ( ( dir_entry = (gchar*)g_dir_read_name(dirptr) ) ) {
		if ( g_str_has_suffix((const gchar*)dir_entry, "sqlt3") ) {
			fn = g_build_filename ( config_home, dir_entry, NULL);
			if ( !g_file_get_contents(fn, &buf, &size, &gerror) ) {
				if ( gerror != NULL ) {
					g_propagate_error (err, gerror);
					g_dir_close(dirptr);
					return 1;
				}
				g_set_error (err,
                   KIMONO_ERROR,                 /* error domain */
                   KIMONO_ERROR_RESTORE,            /* error code */
                   "Can't get %s", /* error message format string */
                   dir_entry);
				g_dir_close(dirptr);
				return 1;
			} else {
				int res;
				int ntuples = 0;
				char *zErrMsg = 0;
				g_mutex_lock(&sq_mutex);
				res = sqlite3_exec(conn, buf, callback, &ntuples, &zErrMsg);
				if( res != SQLITE_OK ) {
					g_set_error (err,
    	               KIMONO_ERROR,                 /* error domain */
    	               KIMONO_ERROR_RESTORE,            /* error code */
    	               "SQL error: %s", /* error message format string */
    	               zErrMsg);
					sqlite3_free(zErrMsg);
				}
				g_mutex_unlock(&sq_mutex);
			}
			g_free(fn);
		}
	}
	g_dir_close(dirptr);
	return 0;
}

char* kimono_put_wksps(char*mname, char*spath, char*uri, char*buf)
{
	GError*gerror = NULL;
	int res;
	int i, j, kounter, n_search, n_concepts, ntuples, n, n_descr, n_files;
	char *zErrMsg = 0;
	GString*pcommand;
	char**search_entries, **descr_entries;
	char**files_entries;
	char*info, *curr, *in, *req, **marg;
	search_entries = g_strsplit(spath, ",", MAX_TOKENS);
	descr_entries = g_strsplit(buf, "\r\n$section\r\n", MAX_TOKENS);
	if ( !descr_entries )
		return NULL;
	n_descr = g_strv_length(descr_entries);
	if ( n_descr != 2 )
		return NULL;
	files_entries = g_strsplit(uri, "=", MAX_TOKENS);
	n_files = g_strv_length(files_entries);
	if ( n_files != 2 )
		return NULL;
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("INSERT INTO WkspLib (tag, name, implementor, type, inputs, outputs, message, apfile");
	for ( kounter = 0; search_entries[kounter]; kounter++) {
		g_string_append_printf(pcommand, ", %s", search_entries[kounter]);
		n_search = kounter;
	}
	n_search++;
	pcommand = g_string_append(pcommand, ") VALUES (");
	g_string_append_printf(pcommand, "'%s',", search_entries[0]);
	g_string_append_printf(pcommand, "'%s',", mname);
	g_string_append_printf(pcommand, "'%s',", user);
	g_string_append_printf(pcommand, "'%d',", 2);
	g_string_append_printf(pcommand, "'%s',", files_entries[0]);
	g_string_append_printf(pcommand, "'%s',", files_entries[1]);
	g_string_append_printf(pcommand, "'%s',", descr_entries[0]);
	g_string_append_printf(pcommand, "'%s'", descr_entries[1]);
	for ( kounter = 0; search_entries[kounter]; kounter++) {
		g_string_append_printf(pcommand, ",%d", 1);
	}
	pcommand = g_string_append(pcommand, ");");
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		info = g_strdup(zErrMsg);
		sqlite3_free(zErrMsg);
	} else {
		info = g_strdup("OK");
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	g_strfreev(descr_entries);
	g_strfreev(search_entries);
	g_strfreev(files_entries);
	return info;
}

char* kimono_put_datas(char*mname, char*spath, char*uri)
{
	GError*gerror = NULL;
	int res;
	int i, j, kounter, n_search, n_concepts, ntuples, n, n_descr;
	char *zErrMsg = 0;
	GString*pcommand;
	char**search_entries, **descr_entries;
	char*info, *curr, *in, *req, *buf, **marg;
	search_entries = g_strsplit(spath, ",", MAX_TOKENS);
	descr_entries = g_strsplit(uri, "$section", MAX_TOKENS);
	if ( !descr_entries )
		return NULL;
	n_descr = g_strv_length(descr_entries);
	if ( n_descr < 1 )
		return NULL;
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("INSERT INTO DataLib (name, owner, uri");
	if ( strlen(descr_entries[1]) > 0 )
		pcommand = g_string_append(pcommand, ", source");
	if ( strlen(descr_entries[2]) > 0 )
		pcommand = g_string_append(pcommand, ", message");
	if ( strlen(descr_entries[3]) > 0 )
		pcommand = g_string_append(pcommand, ", login");
	if ( strlen(descr_entries[4]) > 0 )
		pcommand = g_string_append(pcommand, ", password");
	for ( kounter = 0; search_entries[kounter]; kounter++) {
		g_string_append_printf(pcommand, ", %s", search_entries[kounter]);
		n_search = kounter;
	}
	n_search++;
	pcommand = g_string_append(pcommand, ") VALUES (");
	g_string_append_printf(pcommand, "'%s',", mname);
	g_string_append_printf(pcommand, "'%s',", user);
	g_string_append_printf(pcommand, "'%s',", descr_entries[0]);
	if ( strlen(descr_entries[1]) > 0 )
		g_string_append_printf(pcommand, "'%s',", descr_entries[1]);
	if ( strlen(descr_entries[2]) > 0 )
		g_string_append_printf(pcommand, "'%s',", descr_entries[2]);
	if ( strlen(descr_entries[3]) > 0 )
		g_string_append_printf(pcommand, "'%s'", descr_entries[3]);
	if ( strlen(descr_entries[4]) > 0 )
		g_string_append_printf(pcommand, "'%s'", descr_entries[4]);
	for ( kounter = 0; search_entries[kounter]; kounter++) {
		g_string_append_printf(pcommand, ",%d", 1);
	}
	pcommand = g_string_append(pcommand, ");");
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		info = g_strdup(zErrMsg);
		sqlite3_free(zErrMsg);
	} else {
		info = g_strdup("OK");
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	g_strfreev(descr_entries);
	g_strfreev(search_entries);
	return info;
}

int kimono_run_wkspjob(char*name, char*user, char*inputs, char*outputs, char*wksplibtag)
{
	char*cmd;
	char**margv;
	GPid child_pid;
	GError *gerror = NULL;
	int flaggs;
	GString *command;
	int argcp;
	cmd = g_strdup_printf("komet --database=%s --conf_dir=%s -j", db_name, config_home);
	command = g_string_new((const gchar*)cmd);
	command = g_string_append_c(command, ' ');
	g_free(cmd);
	command = g_string_append(command, name);
	command = g_string_append_c(command, ' ');
	command = g_string_append(command, user);
	command = g_string_append_c(command, ' ');
	command = g_string_append(command, inputs);
	command = g_string_append_c(command, ' ');
	command = g_string_append(command, outputs);
	command = g_string_append_c(command, ' ');
	command = g_string_append(command, wksplibtag);
	command = g_string_append_c(command, ' ');
	flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL;
g_print("Cmd kimono_run_wkspjob: %s\n", command->str);
fflush(stdout);
	if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
		if ( gerror ) {
			g_debug("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
			g_error_free(gerror);
			return -1;
		}
	}
	if ( !g_spawn_async (NULL, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, &child_pid, &gerror) ) {
		if ( gerror ) {
			g_debug("g_spawn_async failed for %s\nwith %s", command->str, gerror->message);
			g_error_free(gerror);
			g_strfreev(margv);
			return -1;
		}
	}
	g_string_free(command, TRUE);
	g_strfreev(margv);
	return 0;
}

char* kimono_put_wkspsjobs(char*mname, char*files, char*msg, char*buf)
{
	GError*gerror = NULL;
	int res;
	int n_files, ntuples, n;
	char *zErrMsg = 0;
	GString*pcommand;
	char**files_entries;
	char*info, *curr, *in, *req, **marg;
	files_entries = g_strsplit(files, "=", MAX_TOKENS);
	n_files = g_strv_length(files_entries);
	if ( n_files != 2 )
		return NULL;
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("INSERT INTO WkspJobs (name, owner, inputs, outputs, wksplibtag");
	if ( buf ) {
		pcommand = g_string_append(pcommand, ",message");
	}
	pcommand = g_string_append(pcommand, ") VALUES (");
	g_string_append_printf(pcommand, "'%s',", mname);
	g_string_append_printf(pcommand, "'%s',", user);
	g_string_append_printf(pcommand, "'%s',", files_entries[0]);
	g_string_append_printf(pcommand, "'%s',", files_entries[1]);
	g_string_append_printf(pcommand, "'%s'", msg);
	if ( buf ) {
		g_string_append_printf(pcommand, ",'%s'", buf);
	}
	pcommand = g_string_append(pcommand, ");");
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		info = g_strdup(zErrMsg);
		sqlite3_free(zErrMsg);
	} else {
		res = kimono_run_wkspjob(mname, user, files_entries[0], files_entries[1], msg);
		if ( res == 0 ) {
			info = g_strdup("OK");
		} else {
			info = g_strdup("Error");
		}
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	g_strfreev(files_entries);
	return info;
}

char* kimono_delete_method(char*mname)
{
	int res;
	int i, j, kounter, n_search, n_concepts, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
	char*info, *req;
	req = kimono_get_pam_info (mname, "implementor");
	if ( strcmp(req, user) ) {
		info = g_strdup_printf("Operator from %s can't be deleted!", req);
		g_free(req);
		return info;
	}
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("DELETE FROM Operators WHERE name = ");
	g_string_append_printf(pcommand, " '%s' AND implementor = '%s';", mname, user);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		info = g_strdup(zErrMsg);
		sqlite3_free(zErrMsg);
	} else {
		info = g_strdup("OK");
		kimono_dump_command(pcommand->str, mname, 1);
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char* kimono_delete_wksps(char*mname)
{
	int res;
	int i, j, kounter, n_search, n_concepts, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
	char*info, *req;
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("DELETE FROM WkspLib WHERE name = ");
	g_string_append_printf(pcommand, " '%s' AND implementor = '%s';", mname, user);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		info = g_strdup(zErrMsg);
		sqlite3_free(zErrMsg);
	} else {
		info = g_strdup("OK");
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char* kimono_delete_datas(char*mname)
{
	int res;
	int i, j, kounter, n_search, n_concepts, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
	char*info, *req;
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("DELETE FROM DataLib WHERE name = ");
	g_string_append_printf(pcommand, " '%s' AND owner = '%s';", mname, user);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		info = g_strdup(zErrMsg);
		sqlite3_free(zErrMsg);
	} else {
		info = g_strdup("OK");
	}
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char* kimono_delete_wkspjobs(char*mname, char*cmd)
{
	int res;
	int i, j, kounter, n_search, n_concepts, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
	char*info, *req;
	g_mutex_lock(&sq_mutex);
	if ( !strcmp(cmd, "delete") ) {
		pcommand = g_string_new("DELETE FROM WkspJobs WHERE name = ");
		g_string_append_printf(pcommand, " '%s' AND owner = '%s';", mname, user);
		ntuples = 0;
		res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
		if( res != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			info = g_strdup(zErrMsg);
			sqlite3_free(zErrMsg);
		} else {
			info = g_strdup("OK");
		}
		g_string_free(pcommand, TRUE);
	} else if ( !strcmp(cmd, "stop") ) {
		pcommand = g_string_new("SELECT PID FROM WkspJobs WHERE name = ");
		g_string_append_printf(pcommand, " '%s' AND owner = '%s';", mname, user);
		ntuples = 0;
		res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
		if( res != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			info = g_strdup(zErrMsg);
			sqlite3_free(zErrMsg);
		} else {
			info = g_strdup("OK");
		}
		g_string_free(pcommand, TRUE);
	} else if ( !strcmp(cmd, "abort") ) {
		pcommand = g_string_new("DELETE FROM WkspJobs WHERE name = ");
		g_string_append_printf(pcommand, " '%s' AND owner = '%s';", mname, user);
		ntuples = 0;
		res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
		if( res != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			info = g_strdup(zErrMsg);
			sqlite3_free(zErrMsg);
		} else {
			info = g_strdup("OK");
		}
		g_string_free(pcommand, TRUE);
	}
	g_mutex_unlock(&sq_mutex);
	return info;
}

gboolean kimono_launch_default_for_uri(const char *uri, GError**gerror)
{
	char**tokens;
	char*extension;
	char*exec_name;
	char*titleoption;
	char*target;
	int ntk;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	gboolean result;
	GPid child_pid;
	GSpawnFlags flaggs;
	char*dname;
	tokens = g_strsplit(uri, ".", MAX_TOKENS);
	ntk = g_strv_length(tokens);
	extension = g_strdup(tokens[ntk - 1]);
	g_strfreev(tokens);
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("SELECT executable,titleoption FROM Screens WHERE extension = ");
	g_string_append_printf(pcommand, "'%s' AND isdefault = 1;", extension);
	g_free(extension);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return FALSE;
	}
	if ( sq_rows < 1 || sq_columns != 2 ) {
		sqlite3_free_table(sq_result);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return FALSE;
	}
	exec_name = g_strdup(sq_result[2]);
	titleoption = g_strdup(sq_result[3]);
	sqlite3_free_table(sq_result);
	g_mutex_unlock(&sq_mutex);
	pcommand = g_string_assign(pcommand, exec_name);
	target = g_shell_quote(uri);
	if ( strcmp(titleoption, "N/A") && strlen(titleoption) > 0 ) {
		g_string_append_printf(pcommand, " %s %s", titleoption, target);
	} else {
		g_string_append_printf(pcommand, " %s", target);
	}
	g_string_append_printf(pcommand, " %s", target);
	g_free(target);
	flaggs = (GSpawnFlags)(G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL);
	if ( !g_shell_parse_argv(pcommand->str, &ntk, &tokens, gerror) ) {
		if ( gerror ) {
			g_debug("g_shell_parse failed for %s\nwith %s", pcommand->str, (*gerror)->message);
			return -1;
		}
	}
	result = g_spawn_async (NULL, tokens, NULL, flaggs, NULL, NULL, &child_pid, gerror);
	g_string_free(pcommand, TRUE);
	g_strfreev(tokens);
	return result;
}

void kimono_update_screen_default(char*dname, char*dext, gboolean setting)
{
	int res, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("UPDATE Screens SET IsDefault = ");
	g_string_append_printf(pcommand, "%d WHERE uid =  '%s_%s';", (int)setting, dext, dname);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
}

void kimono_put_screen(char*cmd)
{
	char**info;
	int res;
	int i, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
//	pthread_mutex_lock (&sq_mutex);
//	sqlite3_mutex_enter(sq_mutex);
	g_mutex_lock(&sq_mutex);
	info = g_strsplit(cmd, "|", 5);
	pcommand = g_string_new("INSERT INTO Screens (uid,name,tag,extension,executable,titleoption) VALUES (");
	g_string_append_printf(pcommand, "'%s_%s'", info[2], info[0]);
	for ( i = 0; info[i]; i++) {
		g_string_append_printf(pcommand, ",'%s'", info[i]);
	}
	pcommand = g_string_append(pcommand, ");");
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	g_string_free(pcommand, TRUE);
//	pthread_mutex_unlock (&sq_mutex);
//	sqlite3_mutex_leave(sq_mutex);
	g_mutex_unlock(&sq_mutex);
}

void kimono_delete_screen(char*dname, char*dext)
{
	int res, ntuples;
	char *zErrMsg = 0;
	GString*pcommand;
//	pthread_mutex_lock (&sq_mutex);
//	sqlite3_mutex_enter(sq_mutex);
	g_mutex_lock(&sq_mutex);
	pcommand = g_string_new("DELETE FROM Screens WHERE uid = ");
	g_string_append_printf(pcommand, " '%s_%s';", dext, dname);
	ntuples = 0;
	res = sqlite3_exec(conn, pcommand->str, callback, &ntuples, &zErrMsg);
	if( res != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	g_string_free(pcommand, TRUE);
//	pthread_mutex_unlock (&sq_mutex);
//	sqlite3_mutex_leave(sq_mutex);
	g_mutex_unlock(&sq_mutex);
}

char*kimono_strcheck(char*str, char*needle, char*replacement)
{
	int i, str_len, needle_len, rep_len, j, k, n;
	char*res;
	if ( !str || !needle || !replacement)
		return NULL;
	str_len = strlen(str);
	needle_len = strlen(needle);
	rep_len = strlen(replacement);
	if ( !str_len || !needle_len || !rep_len)
		return NULL;
	n = 0;
	res = (char*)NULL;
	for ( i = 0; i < str_len; i++ ) {
		k = 0;
		if ( i < str_len - needle_len + 1 ) {
			for ( j = 0; j < needle_len; j++ )
				if ( str[i + j] == needle[j] )
					k++;
		}
		if ( k == needle_len ) {
//		if ( k == 1 ) {
			res = (char*)realloc( res, ( n + rep_len ) * sizeof(char) );
			for ( j = 0; j < rep_len; j++ ) {
				res[ n + j ] = replacement[j];
			}
			i += needle_len - 1;
			n += rep_len;
		} else {
			res = (char*)realloc( res, ( n + 1 ) * sizeof(char) );
			res[n] = str[i];
			n++;
		}
	}
	res = (char*)realloc( res, ( n + 1 ) * sizeof(char) );
	res[n] = '\0';
	return res;
}


char*door_get_config_path(char*hname, int port, char*proto)
{
	char*config_path, *buf;
	buf = door_get_config_string(hname, port, proto);
	config_path = g_build_filename (config_home, "temp", buf, NULL);
	g_free(buf);
	return config_path;
}

char*door_init_config_path()
{
	char*config_path, *buf, *hname;
	hname = g_strdup("localhost");
	config_path = door_get_config_path(hname, default_port_num, "door");
	return config_path;
}

int door_init_config_port()
{
#ifdef G_OS_WIN32
	int local_port = 7778;
#else /* !G_OS_WIN32 */
	int local_port;
	local_port = 7000 + (getuid() % 1000);
#endif /* G_OS_WIN32 */
	return local_port;
}

int door_get_config_port()
{
	return default_port_num;
}

char*door_get_config_string(char*hname, int port, char*proto)
{
	char*config_string;
#ifdef G_OS_WIN32
	char *separ = "%3F";
#else /* !G_OS_WIN32 */
	char *separ = ":";
#endif /* G_OS_WIN32 */
	if ( strcmp(proto, "door") ) {
		if ( port > 0 ) {
			config_string = g_strdup_printf("%s%s%d", hname, separ, port);
		} else {
			config_string = g_strdup_printf("%s", hname);
		}
	} else {
		if ( default_port_num > 0 ) {
			config_string = g_strdup_printf("%s%s%d", hname, separ, default_port_num);
		} else {
			config_string = g_strdup_printf("%s", hname);
		}
	}
	return config_string;
}

/*
    * Mac OS X: _NSGetExecutablePath() (man 3 dyld)
    * Linux: readlink /proc/self/exe
    * Solaris: getexecname()
    * FreeBSD: sysctl CTL_KERN KERN_PROC KERN_PROC_PATHNAME -1
    * BSD with procfs: readlink /proc/curproc/file
    * Windows: GetModuleFileName() with hModule = NULL
*/

gchar*subst_data_dir(gchar*ORIGIN)
{
	static gchar *src_path;
	gchar*data_basename;
	char*runtime_install_prefix;
#ifdef G_OS_WIN32
	runtime_install_prefix = g_win32_get_package_installation_directory_of_module(NULL);
#else
	int ret = -1, ret_size = MAX_RECORD;
	char *buf;
	runtime_install_prefix = (char*)calloc(ret_size, sizeof(char));
#ifdef G_OS_DARWIN
	ret = _NSGetExecutablePath(runtime_install_prefix, &ret_size);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. On Mac OS X the size may be = %d", ret_size);
	runtime_install_prefix[ret_size - 1] = '\0';
#else
	ret = readlink("/proc/self/exe", runtime_install_prefix, ret_size - 1);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. The returned size may be = %d", ret);
	runtime_install_prefix[ret] = '\0';
#endif
	buf = g_path_get_dirname(runtime_install_prefix);
	g_free(runtime_install_prefix);
	runtime_install_prefix = g_path_get_dirname(buf);
#endif
	if ( runtime_install_prefix == NULL ) {
		return ORIGIN;
	}
	if ( src_path != NULL )
		g_free(src_path);
	if ( PACKAGE_DATA_DIR != NULL ) {
		data_basename = g_path_get_basename(PACKAGE_DATA_DIR);
		src_path = g_build_filename(runtime_install_prefix, data_basename, ORIGIN, NULL);
		g_free(data_basename);
	} else {
		src_path = g_strdup(ORIGIN);
	}
	g_strdelimit(src_path, "/", G_DIR_SEPARATOR);
	return src_path;
}

int kimono_db_pak_version_newer(char*p_version, char*version)
{
	int res = 0;
	int v_1, v_2, v_3;
	int p_v_1, p_v_2, p_v_3;
	if ( !p_version && !version ) {
		return 0;
	} else if ( !p_version ) {
		return -1;
	} else if ( !version ) {
		return 1;
	}
	sscanf(p_version, "%d.%d.%d", &p_v_1, &p_v_2, &p_v_3);
	sscanf(version, "%d.%d.%d", &v_1, &v_2, &v_3);
	if ( p_v_1 < v_1 ) {
		return -1;
	} else if ( p_v_1 == v_1 ) {
		if ( p_v_2 < v_2 ) {
			return -1;
		} else if ( p_v_2 == v_2 ) {
			if ( p_v_3 < v_3 ) {
				return -1;
			} else if ( p_v_3 == v_3 ) {
				return 0;
			} else {
				return 1;
			}
		} else {
			return 1;
		}
	} else {
		return 1;
	}
	return res;
}

char*kimono_db_get_pak_version(char*pak)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT ");
	g_string_append_printf(pcommand, "Version FROM Packages WHERE name = '%s';", pak);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK || sq_rows < 1 || sq_columns < 1 ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = g_strdup(sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_package_info(char*pak_name, char*pak_info_type)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT ");
	g_string_append_printf(pcommand, "%s FROM Packages WHERE name = '%s';", pak_info_type, pak_name);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK || sq_rows < 1 || sq_columns < 1 ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = g_strdup(sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

char*kimono_get_compat(char*name, char*version, char*column)
{
	char*info;
	char**sq_result;
	int sq_rows;
	int sq_columns;
	int res;
	char *zErrMsg = 0;
	GString*pcommand;
	g_mutex_lock(&sq_mutex);
	info = NULL;
	pcommand = g_string_new("SELECT ");
	g_string_append_printf(pcommand, "%s FROM Compat WHERE name = '%s' AND version = '%s';", column, name, version);
	res = sqlite3_get_table(conn, pcommand->str, &sq_result, &sq_rows, &sq_columns, &zErrMsg);
	if( res != SQLITE_OK || sq_rows < 1 || sq_columns < 1 ) {
		g_debug("SQL error: %s\n", zErrMsg);
		fflush(stdout);
		sqlite3_free(zErrMsg);
		g_string_free(pcommand, TRUE);
		g_mutex_unlock(&sq_mutex);
		return NULL;
	}
	info = g_strdup(sq_result[1]);
	sqlite3_free_table(sq_result);
	g_string_free(pcommand, TRUE);
	g_mutex_unlock(&sq_mutex);
	return info;
}

double kimono_get_monotonic_time ()
{
	double time = 0;
#ifndef G_OS_WIN32
	struct timespec tp;
	if ( 0 == clock_gettime(CLOCK_MONOTONIC, &tp) ) {
		time = 1e-3 * (double)(tp.tv_nsec) + 1e6 * (double)(tp.tv_sec);
	} else {
		g_debug("error in kimono_get_monotonic_time");
	}
#endif
	return time;
}

double kimono_get_real_time ()
{
	double time = 0;
#ifndef G_OS_WIN32
	struct timespec tp;
	if ( 0 == clock_gettime(CLOCK_THREAD_CPUTIME_ID, &tp) ) {
		time = 1e-3 * (double)(tp.tv_nsec) + 1e6 * (double)(tp.tv_sec);
	} else {
		g_debug("error in kimono_get_real_time");
	}
#endif
	return time;
}

double kimono_get_time_of_day ()
{
	double time = 0;
#ifndef G_OS_WIN32
	struct timeval tv;
	if ( 0 == gettimeofday(&tv, NULL) ) {
		time = (double)(tv.tv_usec) + 1e6 * (double)(tv.tv_sec);
	} else {
		g_debug("error in kimono_get_time_of_day");
	}
#endif
	return time;
}

