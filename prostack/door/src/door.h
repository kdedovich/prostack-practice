
#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif

#ifndef _DOOR_H
#define _DOOR_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <Workspace.h>
#include <VInfo.h>
#include <Node.h>
#include <OlapPAM.h>
#include <OlapServer.h>
#include <Port.h>
#include <Connection.h>
#include <ListOfNodes.h>

#ifdef __cplusplus
}
#endif

#endif /* _VINFO_H */
