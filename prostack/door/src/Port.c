/***************************************************************************
 *            Port.c
 *
 *  Wed Feb 15 12:41:54 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <ctype.h>
#include <Connection.h>
#include <Port.h>

Port* parsePorts(char*respond, int *nConns)
{
	int n;
	int k;
	char c;
	char *buffer;
	int strlenskip;
	char *skip;
	char *format;
	int strlenin;
	Port*p;
	p = (Port*)NULL;
	*nConns = 0;
	strlenin = strlen(respond);
	if ( strlenin == 0 ) {
		p = (Port*)calloc(1, sizeof(Port));
		*nConns = 1;
		p[0].label = (char*)calloc(6, sizeof(char));
		p[0].label = strcpy(p[0].label, "image");
		p[0].type = tif;
		return p;
	}
/*fprintf(stdout,"s = %d : %s ;\n", strlenin, respond);
fflush(stdout);*/
	n = sscanf(respond, "%c", &c);
	if ( c == '0' ) {
		*nConns = 0;
		return p;
	}
	strlenin--;
	strlenskip = 4;
	skip = (char*)calloc(strlenskip, sizeof(char));
	skip = strcpy(skip, "%*c");
	format = (char*)calloc(strlenskip + 2, sizeof(char));
	format = strcpy(format, skip);
	format = strcat(format, "%c");
	while( strlenin != 0 && n != 0 && c != '\r' && c != '\n' ) {
		while ( strlenin != 0 && n != 0 && isdigit((int)c) ) {
			n = sscanf(respond, format, &c);
			strlenin--;
			skip = (char*)realloc(skip, (strlenskip + 4) * sizeof(char));
			skip = strcat(skip, "%*c");
			strlenskip += 4;
			format = (char*)realloc( format, (strlenskip + 2) * sizeof(char));
			format = strcpy(format, skip);
			format = strcat(format, "%c");
		}
		while ( strlenin != 0 && n != 0 && isspace((int)c) ) {
			n = sscanf(respond, format, &c);
			strlenin--;
			skip = (char*)realloc(skip, (strlenskip + 4) * sizeof(char));
			skip = strcat(skip, "%*c");
			strlenskip += 4;
			format = (char*)realloc( format, (strlenskip + 2) * sizeof(char));
			format = strcpy(format, skip);
			format = strcat(format, "%c");
		}
		buffer = (char*)NULL;
		k = 0;
		while ( strlenin != 0 && n != 0 && isalpha((int)c) ) {
			buffer = (char*)realloc(buffer, (k + 1) * sizeof(char));
			buffer[k] = c;
			k++;
			n = sscanf(respond, format, &c);
			strlenin--;
			skip = (char*)realloc(skip, (strlenskip + 4) * sizeof(char));
			skip = strcat(skip, "%*c");
			strlenskip += 4;
			format = (char*)realloc( format, (strlenskip + 2) * sizeof(char));
			format = strcpy(format, skip);
			format = strcat(format, "%c");
		}
		buffer = (char*)realloc(buffer, (k + 1) * sizeof(char));
		buffer[k] = '\0';
		k++;
		p = (Port*)realloc(p, ( (*nConns) + 1 ) * sizeof(Port));
		p[(*nConns)].label = (char*)calloc(k, sizeof(char));
		p[(*nConns)].label = strcpy(p[(*nConns)].label, buffer);
		free(buffer);
		n = sscanf(respond, format, &c);
		strlenin--;
		skip = (char*)realloc(skip, (strlenskip + 4) * sizeof(char));
		skip = strcat(skip, "%*c");
		strlenskip += 4;
		format = (char*)realloc( format, (strlenskip + 2) * sizeof(char));
		format = strcpy(format, skip);
		format = strcat(format, "%c");
		buffer = (char*)NULL;
		k = 0;
		while ( strlenin != 0 && n != 0 && ( isalpha((int)c) || c == '.' ) ) {
			buffer = (char*)realloc(buffer, (k + 1) * sizeof(char));
			buffer[k] = c;
			k++;
			n = sscanf(respond, format, &c);
			strlenin--;
			skip = (char*)realloc(skip, (strlenskip + 4) * sizeof(char));
			skip = strcat(skip, "%*c");
			strlenskip += 4;
			format = (char*)realloc( format, (strlenskip + 2) * sizeof(char));
			format = strcpy(format, skip);
			format = strcat(format, "%c");
		}
		buffer = (char*)realloc(buffer, (k + 1) * sizeof(char));
		buffer[k] = '\0';
		k++;
		p[(*nConns)].type = string2type(buffer);
		(*nConns)++;
		free(buffer);
		while ( strlenin != 0 && n != 0 && !isdigit((int)c) ) {
			n = sscanf(respond, format, &c);
			strlenin--;
			skip = (char*)realloc(skip, (strlenskip + 4) * sizeof(char));
			skip = strcat(skip, "%*c");
			strlenskip += 4;
			format = (char*)realloc( format, (strlenskip + 2) * sizeof(char));
			format = strcpy(format, skip);
			format = strcat(format, "%c");
		}
	}
	free(format);
	free(skip);
	return p;
}

PortType string2type(char*buffer)
{
	if ( !strcmp(buffer, ".tif") )
		return tif;
	else if ( !strcmp(buffer, ".txt") )
		return txt;
	else if ( !strcmp(buffer, ".bmp") )
		return bmp;
	else if ( !strcmp(buffer, ".zip") )
		return zip;
	else if ( !strcmp(buffer, ".asc") )
		return asc;
	else if ( !strcmp(buffer, ".jpg") )
		return jpg;
	else if ( !strcmp(buffer, ".any") )
		return any;
	else if ( !strcmp(buffer, ".png") )
		return png;
	else if ( !strcmp(buffer, ".pdf") )
		return pdf;
	else if ( !strcmp(buffer, ".csv") )
		return csv;
}

char*type2string(PortType type)
{
	char * out;
	out = (char*)calloc(5, sizeof(char));
	switch (type) {
		case tif:
			out = strcpy(out, ".tif");
		break;
		case txt:
			out = strcpy(out, ".txt");
		break;
		case bmp:
			out = strcpy(out, ".bmp");
		break;
		case zip:
			out = strcpy(out, ".zip");
		break;
		case asc:
			out = strcpy(out, ".asc");
		break;
		case jpg:
			out = strcpy(out, ".jpg");
		break;
		case any:
			out = strcpy(out, ".any");
		break;
		case png:
			out = strcpy(out, ".png");
		break;
		case pdf:
			out = strcpy(out, ".pdf");
		break;
		case csv:
			out = strcpy(out, ".csv");
		break;
	}
	return out;
}


Port* portsParse(char*response, int *nConns)
{
	int n;
	int k;
	char c;
	char *buffer;
	int strlenskip;
	char *skip;
	char *format;
	int strlenin;
	char**tokens;
	Port*p;
	p = (Port*)NULL;
	*nConns = 0;
	tokens = g_strsplit(response, ",", 255);
	*nConns = g_strv_length(tokens);
	if ( *nConns > 0 ) {
		p = (Port*)calloc(*nConns, sizeof(Port));
		for ( k = 0; k < *nConns; k++) {
			p[k].meta_type = string2type2(tokens[k]);
		}
		g_strfreev(tokens);
	}
	return p;
}

void ports_set_type(Port*p, char*ports_string)
{
	char**tokens;
	int nConns, k;
	tokens = g_strsplit(ports_string, ",", 255);
	nConns = g_strv_length(tokens);
	for ( k = 0; k < nConns; k++) {
		p[k].type = string2type2(tokens[k]);
	}
	g_strfreev(tokens);
}

PortType path2type2(char*path)
{
	PortType type;
	char**ext = g_strsplit(path, ".", MAXTOKENS);
	int next = g_strv_length(ext);
	type = string2type2(ext[next-1]);
	g_strfreev(ext);
	return type;
}

char*path2string2(char*path)
{
	char*buf;
	char**ext = g_strsplit(path, ".", MAXTOKENS);
	int next = g_strv_length(ext);
	buf = g_strdup(ext[next-1]);
	g_strfreev(ext);
	return buf;
}

PortType string2type2(char*buffer)
{
	if ( !strcmp(buffer, "tif") )
		return tif;
	else if ( !strcmp(buffer, "txt") )
		return txt;
	else if ( !strcmp(buffer, "bmp") )
		return bmp;
	else if ( !strcmp(buffer, "zip") )
		return zip;
	else if ( !strcmp(buffer, "asc") )
		return asc;
	else if ( !strcmp(buffer, "jpg") )
		return jpg;
	else if ( !strcmp(buffer, "any") )
		return any;
	else if ( !strcmp(buffer, "png") )
		return png;
	else if ( !strcmp(buffer, "gif") )
		return gif;
	else if ( !strcmp(buffer, "lqr") )
		return lqr;
	else if ( !strcmp(buffer, "unk") )
		return unk;
	else if ( !strcmp(buffer, "dat") )
		return dat;
	else if ( !strcmp(buffer, "csv") )
		return csv;
	else if ( !strcmp(buffer, "pdf") )
		return pdf;
}

char*type2string2(PortType type)
{
	char *out = (char*)NULL;
	switch (type) {
		case tif:
			out = g_strdup("tif");
		break;
		case txt:
			out = g_strdup("txt");
		break;
		case bmp:
			out = g_strdup("bmp");
		break;
		case zip:
			out = g_strdup("zip");
		break;
		case asc:
			out = g_strdup("asc");
		break;
		case jpg:
			out = g_strdup("jpg");
		break;
		case gif:
			out = g_strdup("gif");
		break;
		case any:
			out = g_strdup("any");
		break;
		case png:
			out = g_strdup("png");
		break;
		case lqr:
			out = g_strdup("lqr");
		break;
		case dat:
			out = g_strdup("dat");
		break;
		case csv:
			out = g_strdup("csv");
		break;
		case pdf:
			out = g_strdup("pdf");
		break;
		default:
			out = g_strdup("unk");
		break;
	}
	return out;
}

