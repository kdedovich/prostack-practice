/***************************************************************************
 *            ListOfNodes.h
 *
 *  Wed Feb 15 11:08:50 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _LISTOFNODES_H
#define _LISTOFNODES_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Node.h>
#include <Connection.h>

#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif

typedef struct ListOfNodes {
	Node *node;
	struct ListOfNodes *next;
	struct ListOfNodes *prev;
} ListOfNodes;

ListOfNodes *newListOfNodes();
ListOfNodes *initFromFileToListOfNodes(FILE*f);
void addFromFileToListOfNodes(ListOfNodes *list, FILE*f);
void readConnectionsFromFile(ListOfNodes *lon, FILE*f);
void installConnection(ListOfNodes *lon, Connection*conn);
void setErrorLon(void (*ptr)(char*msg));
void add2ListOfNodes(ListOfNodes *list, Node*f);
ListOfNodes *listOfNodesInitWithNode(Node*ne);
void uninstallConnection(ListOfNodes *lon, Connection*conn);
Node*nodeByID(ListOfNodes*lon, int ID);
void deleteListOfNodes(ListOfNodes*lon);
int hasNodeByID(ListOfNodes*lon, int ID);
int maxIDListOfNodes(ListOfNodes *lon);
void listOfNodesPropagateStatus(ListOfNodes *lon, Node*ne, NodeStatus status);
int maxMacroIDListOfNodes(ListOfNodes*lon);
Node *macro_node_by_number(ListOfNodes *lon, int MACRO_NUMBER);
int macroNodes(ListOfNodes*lon);

#ifdef __cplusplus
}
#endif

#endif /* _LISTOFNODES_H */
