/***************************************************************************
 *            Node.c
 *
 *  Tue Feb 14 09:48:51 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <Port.h>
#include <Connection.h>
#include <OlapPAM.h>
#include <Node.h>
#include <VInfo.h>
#include <Kimono.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <libsoup/soup.h>

#define HTTP_OK 200

static void (*NotifyNode)(Node*ne, NodeStatus status);
static void (*logger)(char*msg);
static void (*error)(char*msg);
static short isSetError = 0;
static short isSetLogger = 0;
static void (*outerNode)(Node*ne);
static short outerIsSet = 0;

OlapServer*os_init_default()
{
	OlapServer*os;
	os = newOlapServerV();
	os->hname = strcpy(os->hname, "localhost");
	os->port = door_get_config_port();
	os->nicname = g_strdup("default");
	os->proto = g_strdup("door");
	os->address = g_strdup("/");
	os->is_default = 1;
	os->ID = 0;
	return os;
}

char*node_status_to_string(NodeStatus status)
{
	char*str;
	switch (status) {
		case running:
			str = g_strdup("running");
		break;
		case waiting:
			str = g_strdup("waiting");
		break;
		case completed:
			str = g_strdup("completed");
		break;
		case failed:
			str = g_strdup("failed");
		break;
		case terminated:
			str = g_strdup("terminated");
		break;
	}
	return str;
}

void setOuter(void (*ptr)(Node*ne))
{
	outerNode = ptr;
	outerIsSet = 1;
}

void setErrorNode(void (*ptr)(char*msg))
{
	isSetError = 1;
	error = ptr;
}

void setLoggerNode(void (*ptr)(char*msg))
{
	isSetLogger = 1;
	logger = ptr;
}

void setNotifyNode(void (*ptr)(Node*ne, NodeStatus status))
{
	NotifyNode = ptr;
}

Node *node_new (int preallocate)
{
	Node *ne;
	ne = (Node*)malloc(sizeof(Node));
	if ( preallocate == 1 ) {
		ne->label = (char*)calloc(MAX_RECORD, sizeof(char));
		ne->id = (char*)calloc(1, sizeof(char));
		ne->id = strcpy(ne->id, "\0");
		ne->file = (char*)calloc(1, sizeof(char));
		ne->file = strcpy(ne->file, "\0");
	} else {
		ne->label = (char*)NULL;
		ne->id = (char*)NULL;
		ne->file = (char*)NULL;
	}
	ne->uid = (char*)NULL;
	ne->buf = (char*)NULL;
	ne->version = (char*)NULL;
	ne->parent_name = (char*)NULL;
	ne->mode = silent;
	ne->terminal = 1;
	ne->status = waiting;
	ne->nInConns = 0;
	ne->nOutConns = 0;
	ne->timeDelay = 60;
	g_mutex_init(&(ne->statusMutex));
	g_mutex_init(&(ne->condMutex));
	g_cond_init(&(ne->statusCond));
	ne->use_metaname = 0;
	ne->vip = 0;
	return ne;
}

Node *newNode()
{
	Node *ne;
	ne = node_new (1);
	return ne;
}

Node *dupNode(Node *node)
{
	Node *ne;
	int i;
	ne = node_new (0);
	ne->label = g_strdup(node->label);
	ne->id = g_strdup(node->id);
	ne->file = g_strdup(node->file);
	if ( node->version != NULL ) {
		ne->version = g_strdup(node->version);
	}
	ne->mode = node->mode;
	ne->terminal = 1;
	ne->status = waiting;
	ne->nInConns = node->nInConns;
	ne->inConn = (Port*)calloc(node->nInConns, sizeof(Port));
	for ( i = 0; i < ne->nInConns; i++) {
		ne->inConn[i].ID = node->inConn[i].ID;
		ne->inConn[i].isInput = node->inConn[i].isInput;
		ne->inConn[i].nConn = 0;
		ne->inConn[i].channel = (Connection**)NULL;
		if ( node->inConn[i].label ) {
			ne->inConn[i].label = (char*)calloc(1 + strlen(node->inConn[i].label), sizeof(char));
			ne->inConn[i].label = strcpy(ne->inConn[i].label, node->inConn[i].label);
		}
		ne->inConn[i].type = node->inConn[i].type;
	}
	ne->nOutConns = node->nOutConns;
	ne->outConn = (Port*)calloc(node->nOutConns, sizeof(Port));
	for ( i = 0; i < ne->nOutConns; i++) {
		ne->outConn[i].ID = node->outConn[i].ID;
		ne->outConn[i].isInput = node->outConn[i].isInput;
		ne->outConn[i].nConn = 0;
		ne->outConn[i].channel = (Connection**)NULL;
		if ( node->outConn[i].label ) {
			ne->outConn[i].label = (char*)calloc(1 + strlen(node->outConn[i].label), sizeof(char));
			ne->outConn[i].label = strcpy(ne->outConn[i].label, node->outConn[i].label);
		}
		ne->outConn[i].type = node->outConn[i].type;
	}
	ne->oServer = dupOlapServer(node->oServer);
	ne->type = node->type;
	if ( ne->type == pam )
		ne->pam = dupOlapPAM(node->pam);
	ne->info = (VInfo*)malloc(sizeof(VInfo));
	ne->info->n = node->info->n;
	ne->info->array = (int*)calloc(ne->info->n, sizeof(int));
	for(i = 0; i < ne->info->n; i++) {
		ne->info->array[i] = node->info->array[i];
	}
	ne->timeDelay = node->timeDelay;
	g_mutex_init(&(ne->statusMutex));
	g_mutex_init(&(ne->condMutex));
	g_cond_init(&(ne->statusCond));
	return ne;
}

Node*proxy_sys_node(char*mode)
{
	Node*sproxy;
	sproxy = newNode();
	sproxy->type = sys;
	sproxy->uid = g_strdup(mode);
	return sproxy;
}

void proxy_sys_node_set_mode(Node*sproxy, char*mode)
{
	sproxy->type = sys;
	if ( sproxy->uid ) {
		sproxy->uid = (char*)realloc( sproxy->uid, MAX_RECORD *sizeof(char));
		sproxy->uid = strcpy(sproxy->uid, mode);
	} else {
		sproxy->uid = g_strdup(mode);
	}
}

void proxy_sys_node_del(Node*sproxy)
{
	g_mutex_clear(&(sproxy->statusMutex));
	g_mutex_clear(&(sproxy->condMutex));
	g_cond_clear(&(sproxy->statusCond));
	g_free(sproxy->uid);
	g_free(sproxy->label);
	if (sproxy->file) g_free(sproxy->file);
	if (sproxy->parent_name) g_free(sproxy->parent_name);
	g_free(sproxy);
}

void setNodePorts(Node*op)
{
	int i;
	int res;
	Node*sproxy;
	sproxy = proxy_sys_node("pam");
	sproxy->label = strcpy(sproxy->label, op->pam->name);
	sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
	sproxy->oServer = op->oServer;
	sproxy->file = strcpy(sproxy->file, "inputs");
	res = door_call("GET", sproxy);
	if ( res == 0 ) {
		op->inConn = portsParse(sproxy->buf, &op->nInConns);
		for ( i = 0; i < op->nInConns; i++) {
			op->inConn[i].ID = i + 1;
			op->inConn[i].isInput = 1;
			op->inConn[i].nConn = 0;
			op->inConn[i].channel = (Connection**)NULL;
		}
		free(sproxy->buf);
	} else {
		op->inConn = 0;
	}
	sproxy->buf = NULL;
	sproxy->file = strcpy(sproxy->file, "outputs");
	res = door_call("GET", sproxy);
	if ( res == 0 ) {
		op->outConn = portsParse(sproxy->buf, &op->nOutConns);
		for ( i = 0; i < op->nOutConns; i++) {
			op->outConn[i].ID = i + 1;
			op->outConn[i].isInput = 0;
			op->outConn[i].nConn = 0;
			op->outConn[i].channel = (Connection**)NULL;
		}
		free(sproxy->buf);
	} else {
		op->outConn = 0;
	}
	proxy_sys_node_del(sproxy);
}

void node_macro_set_in_ports(Node*n)
{
/*n->file="i=12.3;23.6;32.1;#node.#port;o=#node.#port;..."*/
	int i;
	char c;
	n->nInConns = 0;
	n->inConn = (Port*)NULL;
	i = 0;
	c = n->file[i];
	while ( c != '\0' && !isdigit((int)c) && c != 'o' ) {
		i++;
		c = n->file[i];
	}
	while ( c != '\0' && c != 'o' ) {
		if ( c == '.' ) {
			n->inConn = (Port*)realloc( n->inConn, ( n->nInConns + 1 ) * sizeof(Port));
			n->inConn[n->nInConns].ID = n->nInConns + 1;
			n->inConn[n->nInConns].label = (char*)calloc(6, sizeof(char));
			n->inConn[n->nInConns].label = strcpy(n->inConn[n->nInConns].label, "image");
			n->inConn[n->nInConns].type = tif;
			n->inConn[n->nInConns].nConn = 0;
			n->inConn[n->nInConns].channel = (Connection**)NULL;
			n->nInConns++;
		}
		i++;
		c = n->file[i];
	}
}

void node_macro_set_out_ports(Node*n)
{
	int i;
	char c;
	n->nOutConns = 0;
	n->outConn = (Port*)NULL;
	i = 0;
	c = n->file[i];
	while ( c != '\0' && !isdigit((int)c) && c != 'o' ) {
		i++;
		c = n->file[i];
	}
	while ( c != '\0' && c != 'o' ) {
		i++;
		c = n->file[i];
	}
	while ( c != '\0' && !isdigit((int)c) ) {
		i++;
		c = n->file[i];
	}
	while ( c != '\0' ) {
		if ( c == '.' ) {
			n->outConn = (Port*)realloc( n->outConn, ( n->nOutConns + 1 ) * sizeof(Port));
			n->outConn[n->nOutConns].ID = n->nOutConns + 1;
			n->outConn[n->nOutConns].label = (char*)calloc(6, sizeof(char));
			n->outConn[n->nOutConns].label = strcpy(n->outConn[n->nOutConns].label, "image");
			n->outConn[n->nOutConns].type = tif;
			n->outConn[n->nOutConns].nConn = 0;
			n->outConn[n->nOutConns].channel = (Connection**)NULL;
			n->nOutConns++;
		}
		i++;
		c = n->file[i];
	}
}

void deleteNode(Node*ne)
{
	g_mutex_clear(&(ne->statusMutex));
	g_mutex_clear(&(ne->condMutex));
	g_cond_clear(&(ne->statusCond));
	deleteVInfo(ne->info);
	deleteOlapServer(ne->oServer);
	if ( ne->type == pam )
		deleteOlapPAM(ne->pam);
	free(ne->file);
	free(ne->label);
	free(ne->id);
	if ( ne->uid )
		free(ne->uid);
	if ( ne->buf )
		free(ne->buf);
	if ( ne->version )
		free(ne->version);
	free(ne);
}

short isReady(Node *ne, int *flag_wait)
{
	int i;
	*flag_wait = 1;
	g_mutex_lock(&(ne->statusMutex));
	if ( ne->type == macro ) {
		*flag_wait = 0;
		g_mutex_unlock(&(ne->statusMutex));
		return 0;
	}
	if ( ne->status != waiting ) {
		*flag_wait = 0;
		g_mutex_unlock(&(ne->statusMutex));
		return 0;
	}
	for ( i = 0; i < ne->nInConns; i++ ) {
		int sta, j;
		*flag_wait = 0;
		if ( ne->inConn[i].nConn == 0 ) {
			*flag_wait = 0;
			g_mutex_unlock(&(ne->statusMutex));
			return 0;
		}
		*flag_wait = 1;
		sta = 0;
		for ( j = 0; j < ne->inConn[i].nConn; j++ ) {
			g_mutex_lock(&(ne->inConn[i].channel[j]->connMutex));
			if ( ne->inConn[i].channel[j]->flag == 1 ) {
				sta = 1;
				ne->inConn[i].index = j;
				g_mutex_unlock(&(ne->inConn[i].channel[j]->connMutex));
				break;
			}
			g_mutex_unlock(&(ne->inConn[i].channel[j]->connMutex));
		}
		if ( sta == 0 ) {
			g_mutex_unlock(&(ne->statusMutex));
			return 0;
		}
	}
	g_mutex_unlock(&(ne->statusMutex));
	return 1;
}

void runNodeFile(Node *ne)
{
	int k, j;
	for( k = 0; k < ne->nOutConns; k++ ) {
		for( j = 0; j < ne->outConn[k].nConn; j++ ) {
			g_mutex_lock(&(ne->outConn[k].channel[j]->connMutex));
			ne->outConn[k].channel[j]->host = (char*)calloc( strlen(ne->oServer->hname) + 1, sizeof(char));
			ne->outConn[k].channel[j]->host = strcpy(ne->outConn[k].channel[j]->host, ne->oServer->hname);
			ne->outConn[k].channel[j]->port = ne->oServer->port;
			ne->outConn[k].channel[j]->file = (char*)calloc( strlen(ne->file) + 1, sizeof(char));
			ne->outConn[k].channel[j]->file = strcpy(ne->outConn[k].channel[j]->file, ne->file);
			ne->outConn[k].channel[j]->proto = g_strdup(ne->oServer->proto);
			ne->outConn[k].channel[j]->address = g_strdup(ne->oServer->address);
			ne->outConn[k].channel[j]->flag = 1;
			g_mutex_unlock(&(ne->outConn[k].channel[j]->connMutex));
		}
	}
	NotifyNode(ne, completed);
}

void runNodeDisplay(Node *ne)
{
	if ( outerIsSet )
		outerNode(ne);
	NotifyNode(ne, completed);
}

int copy_big_files(char*from, char*to)
{
	GError*gerror = NULL;
	GFile*ch_from, *ch_to;
	ch_from = g_file_new_for_commandline_arg(from);
//g_fprintf(stderr, "uri:%s\n%s\nscheme:%s\n", g_file_get_uri(ch_from), g_file_get_basename(ch_from), g_file_get_uri_scheme(ch_from));
	ch_to = g_file_new_for_commandline_arg(to);
#ifdef G_OS_WIN32
	if ( g_file_query_exists(ch_to, NULL) ) {
		if ( !g_file_delete(ch_to, NULL, &gerror) ) {
			g_debug("Can't delete existing: %s", gerror->message);
			g_error_free(gerror);
			g_object_unref(ch_from);
			g_object_unref(ch_to);
			return 1;
		}
	}
#endif
	if ( !g_file_copy(ch_from, ch_to, G_FILE_COPY_OVERWRITE, (GCancellable*)NULL, (GFileProgressCallback)NULL, (gpointer)NULL, &gerror) ) {
		if ( gerror ) {
			g_debug("Error: %s", gerror->message);
			if ( g_error_matches(gerror, G_IO_ERROR, G_IO_ERROR_NOT_MOUNTED) ) {
				char**margv,*standard_error;
				char*arg_1;
				char*arg_2;
				int port;
				GString*command;
				int flaggs;
				SoupURI* uri;
				gint exit_status;
				command = g_string_new("curl");
				if ( !g_access(from, F_OK) ) {
					arg_1 = g_strdup(from);
					uri = soup_uri_new(to);
					g_string_append(command, " --upload-file");
				} else {
					arg_1 = g_strdup(to);
					uri = soup_uri_new(from);
					g_string_append(command, " -o");
				}
				port = uri->port;
				if ( !strcmp(uri->scheme, "dav") ) {
					soup_uri_set_scheme(uri, "http");
					soup_uri_set_port(uri, port);
				}
				arg_2 = soup_uri_to_string(uri, FALSE);
				g_string_append_printf(command, " %s %s", arg_1, arg_2);
				margv = g_strsplit(command->str, " ", MAXTOKENS);
				g_string_free(command, TRUE);
				g_error_free(gerror);
				gerror = NULL;
				flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL;
				if ( !g_spawn_sync(g_get_current_dir(), margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, NULL, &standard_error, &exit_status, &gerror) ) {
					g_debug("Error: ");
					if ( gerror ) {
						g_debug(gerror->message);
						g_error_free(gerror);
					}
				}
				g_fprintf(stderr, standard_error);
				g_strfreev(margv);
				if (standard_error)
					g_free(standard_error);
				if ( exit_status == 7 ) {
					if ( !strcmp(uri->scheme, "http") ) {
						soup_uri_set_scheme(uri, "https");
					}
					g_string_append_printf(command, " %s %s", arg_1, arg_2);
					margv = g_strsplit(command->str, " ", MAXTOKENS);
					g_string_free(command, TRUE);
					gerror = NULL;
					if ( !g_spawn_sync(g_get_current_dir(), margv, NULL, flaggs, NULL, NULL, NULL, &standard_error, &exit_status, &gerror) ) {
						g_debug("Error: ");
						if ( gerror ) {
							g_debug(gerror->message);
							g_error_free(gerror);
						}
					}
					g_fprintf(stderr, standard_error);
					g_strfreev(margv);
					if (standard_error)
						g_free(standard_error);
				}
				soup_uri_free(uri);
				g_free(arg_1);
				g_free(arg_2);
				if ( exit_status == 0 ) {
					g_object_unref(ch_from);
					g_object_unref(ch_to);
					return 0;
				}
			} else {
				g_error_free(gerror);
			}
		}
		g_object_unref(ch_from);
		g_object_unref(ch_to);
		return 1;
	}
	g_object_unref(ch_from);
	g_object_unref(ch_to);
	return 0;
}

int copy_big_files_giochannel(char*from, char*to)
{
	GError*gerror = NULL;
	GIOChannel*ch_from, *ch_to;
	GIOStatus gstatus;
	char*buf;
	int count;
	gsize size, nbytes;
	if ( !(ch_from = g_io_channel_new_file(from, "r", &gerror)) ) {
		g_debug("Error: %s", gerror->message);
		g_io_channel_shutdown(ch_from, TRUE, &gerror);
		g_io_channel_unref(ch_from);
		g_error_free(gerror);
		return 1;
	}
	if ( !(ch_to = g_io_channel_new_file(to, "w", &gerror)) ) {
		g_debug("Error: %s", gerror->message);
		g_io_channel_shutdown(ch_from, TRUE, &gerror);
		g_io_channel_shutdown(ch_to, TRUE, &gerror);
		g_io_channel_unref(ch_from);
		g_io_channel_unref(ch_to);
		g_error_free(gerror);
		return 1;
	}
	g_io_channel_set_encoding(ch_from, NULL,&gerror);
	g_io_channel_set_encoding(ch_to, NULL,&gerror);
	if ( gerror ) {
		g_debug("Error: %s", gerror->message);
		g_io_channel_shutdown(ch_from, TRUE, &gerror);
		g_io_channel_shutdown(ch_to, TRUE, &gerror);
		g_io_channel_unref(ch_from);
		g_io_channel_unref(ch_to);
		g_error_free(gerror);
		return 1;
	}
	count = 1024;
	buf = (char*)calloc(count, sizeof(char));
	bzero(buf, count);
/*	g_io_channel_read_chars(ch_from, buf, count - 1, &size, &gerror);
fprintf(stdout,"Started copying\n");
fflush(stdout);
	g_io_channel_write_chars(ch_to, buf, size, &nbytes, &gerror);*/
	while ( ( g_io_channel_read_chars(ch_from, buf, count, &size, &gerror) == G_IO_STATUS_NORMAL ) && ( g_io_channel_write_chars(ch_to, buf, size, &nbytes, &gerror) == G_IO_STATUS_NORMAL ) ) {
		bzero(buf, count);
	}
	free(buf);
	if ( gerror ) {
		g_debug("Error:", gerror->message);
		g_io_channel_flush(ch_from, &gerror);
		g_io_channel_shutdown(ch_from, TRUE, &gerror);
		g_io_channel_shutdown(ch_to, TRUE, &gerror);
		g_io_channel_unref(ch_from);
		g_io_channel_unref(ch_to);
		g_error_free(gerror);
		return 1;
	}
	g_io_channel_flush(ch_from, &gerror);
	g_io_channel_shutdown(ch_from, TRUE, &gerror);
	g_io_channel_shutdown(ch_to, TRUE, &gerror);
	g_io_channel_unref(ch_from);
	g_io_channel_unref(ch_to);
	if ( gerror ) {
		g_debug("Error:", gerror->message);
		g_error_free(gerror);
		return 1;
	}
	return 0;
}

void nodeCleanOutPut(Node*ne)
{
	int j, k;
	if ( !ne->terminal ) {
		for( k = 0; k < ne->nOutConns; k++ ) {
			for( j = 0; j < ne->outConn[k].nConn; j++ ) {
				g_mutex_lock(&(ne->outConn[k].channel[j]->connMutex));
				if ( ne->outConn[k].channel[j]->flag == 1 ) {
					free(ne->outConn[k].channel[j]->host);
					ne->outConn[k].channel[j]->host = (char*)NULL;
					free(ne->outConn[k].channel[j]->file);
					ne->outConn[k].channel[j]->file = (char*)NULL;
					free(ne->outConn[k].channel[j]->proto);
					ne->outConn[k].channel[j]->proto = (char*)NULL;
					free(ne->outConn[k].channel[j]->address);
					ne->outConn[k].channel[j]->address = (char*)NULL;
					ne->outConn[k].channel[j]->flag = 0;
				}
				g_mutex_unlock(&(ne->outConn[k].channel[j]->connMutex));
			}
		}
	}
	if ( ne->uid != NULL ) {
		free(ne->uid);
		ne->uid = (char*)NULL;
	}
}

void nodeCheckTerminal(Node*ne)
{
	int k;
	ne->terminal = 1;
	for( k = 0; k < ne->nOutConns; k++ )
		if ( ne->outConn[k].nConn > 0 )
			ne->terminal = 0;
}

UiDef*getUiDef(Node*ne)
{
	Node*sproxy;
	UiDef*ud;
	sproxy = proxy_sys_node("pam");
	sproxy->label = strcpy(sproxy->label, ne->pam->name);
	sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
	sproxy->oServer = ne->oServer;
	sproxy->file = strcpy(sproxy->file, "uidescription");
	door_call("GET", sproxy);
	ud = parseUiDef(sproxy->buf);
	free(sproxy->buf);
	proxy_sys_node_del(sproxy);
	return ud;
}

UiDef*parseUiDef2(char*respond)
{
	UiDef*ud;
	int j;
	int i, k;
	char c;
	char*buffer;
	int l;
	int lr;
		fprintf(stderr,"\nres %s",respond);
		fflush(stderr);
	lr = strlen(respond);
	i = 0;
	c = respond[i];
	buffer = str2ud(respond, i, lr, &l);
		fprintf(stderr,"\n%d %s",l,buffer);
		fflush(stderr);
	i += l+1;
	if ( l == 0 ) {
		free(buffer);
		if ( isSetError ) {
			return NULL;
		} else {
			return NULL;
		}
	}
	ud = (UiDef*)malloc(sizeof(UiDef));
	ud->length = atoi(buffer);
	free(buffer);
	ud->opts = (OptionString*)calloc(ud->length, sizeof(OptionString));
	for( j = 0; j < ud->length; j++ ) {
		c = respond[i];
		while ( !isalnum((int)c) ) {
			i++;
			c = respond[i];
		}
		ud->opts[j].label = str2ud(respond, i, lr, &l);
		i += l + 1;
		ud->opts[j].type = str2ud(respond, i, lr, &l);
		i += l + 1;
		if ( !strcmp(ud->opts[j].type, "choice") ) {
			buffer = str2ud(respond, i, lr, &l);
			ud->opts[j].lval = atoi(buffer);
			free(buffer);
			i += l + 1;
			ud->opts[j].value = (char**)calloc(ud->opts[j].lval, sizeof(char*));
			for ( k = 0; k < ud->opts[j].lval; k++) {
				ud->opts[j].value[k] = str2ud(respond, i, lr, &l);
				i += l + 1;
			}
		} else if ( !strcmp(ud->opts[j].type, "bool") ) {
			ud->opts[j].lval = 2;
			ud->opts[j].value = (char**)calloc(ud->opts[j].lval, sizeof(char*));
			for ( k = 0; k < ud->opts[j].lval; k++) {
				ud->opts[j].value[k] = str2ud(respond, i, lr, &l);
				i += l + 1;
			}
		} else {
			ud->opts[j].value = (char**)calloc(1, sizeof(char*));
			ud->opts[j].lval = 1;
			ud->opts[j].value[0] = str2ud(respond, i, lr, &l);
			i += l + 1;
		}
	}
	c = respond[i];
	while ( !isalnum((int)c) && c != '$' && c != '-' && c != '"' && c != '\'' && c != '[' ) {
		i++;
		c = respond[i];
	}
	ud->format = str2ud(respond, i, lr, &l);
	return ud;
}

UiDef*parseUiDef(char*respond)
{
	UiDef*ud;
	int j;
	int i, k;
	char c;
	char**buffer;
	int l;
	int lr;
	if ( !respond || !strcmp(respond, "N/A") )
		return NULL;
	buffer = g_strsplit_set(respond, ";\n", MAXTOKENS);
	if ( !buffer || !buffer[0] )
		return NULL;
/*	for( j = 0; j < g_strv_length(buffer); j++ ) {
		fprintf(stderr,"\n%d %s", j, buffer[j]);
		fflush(stderr);
	}*/
	i = 0;
	ud = (UiDef*)malloc(sizeof(UiDef));
	ud->length = atoi(buffer[i]);
	i++;
	while ( buffer[i] && strlen(buffer[i]) == 0 )
		i++;
	ud->opts = (OptionString*)calloc(ud->length, sizeof(OptionString));
	for( j = 0; j < ud->length; j++ ) {
		ud->opts[j].label = g_strdup(buffer[i]);
		i++;
		while ( buffer[i] && strlen(buffer[i]) == 0 )
			i++;
		ud->opts[j].type = g_strdup(buffer[i]);
		i++;
		while ( buffer[i] && strlen(buffer[i]) == 0 )
			i++;
		if ( !strcmp(ud->opts[j].type, "choice") ) {
			ud->opts[j].lval = atoi(buffer[i]);
			i++;
			while ( buffer[i] && strlen(buffer[i]) == 0 )
				i++;
			ud->opts[j].value = (char**)calloc(ud->opts[j].lval, sizeof(char*));
			for ( k = 0; k < ud->opts[j].lval; k++) {
				ud->opts[j].value[k] = g_strdup(buffer[i]);
				i++;
				while ( buffer[i] && strlen(buffer[i]) == 0 )
					i++;
			}
		} else if ( !strcmp(ud->opts[j].type, "bool") ) {
			ud->opts[j].lval = 2;
			ud->opts[j].value = (char**)calloc(ud->opts[j].lval, sizeof(char*));
			for ( k = 0; k < ud->opts[j].lval; k++) {
				ud->opts[j].value[k] = g_strdup(buffer[i]);
				i++;
				while ( buffer[i] && strlen(buffer[i]) == 0 )
					i++;
			}
		} else {
			ud->opts[j].value = (char**)calloc(1, sizeof(char*));
			ud->opts[j].lval = 1;
			ud->opts[j].value[0] = g_strdup(buffer[i]);
			i++;
			while ( buffer[i] && strlen(buffer[i]) == 0 )
				i++;
		}
	}
	while ( buffer[i] && strlen(buffer[i]) == 0 )
		i++;
	ud->format = g_strdup(buffer[i]);
	g_strfreev(buffer);
	return ud;
}

void deleteUiDef(UiDef*ud)
{
	int j, k;
	for( j = 0; j < ud->length; j++ ) {
		free(ud->opts[j].label);
		free(ud->opts[j].type);
		for ( k = 0; k < ud->opts[j].lval; k++)
			free(ud->opts[j].value[k]);
		free(ud->opts[j].value);
	}
	free(ud->opts);
	free(ud->format);
	free(ud);
}

char*str2ud(char *respond, int start, int end, int*len)
{
	int i, l;
	char*buffer, c;
	i = start;
	c = respond[i];
	buffer = (char*)NULL;
	l = 0;
	while ( i < end && c != ';' ) {
		buffer = (char*)realloc(buffer, (l + 1) * sizeof(char));
		buffer[l] = c;
		l++;
		i++;
		c = respond[i];
	}
	buffer = (char*)realloc(buffer, (l + 1) * sizeof(char));
	buffer[l] = '\0';
//	l++;
	*len = l;
	return buffer;
}

char*str2ud2(char *respond, int start, int end, int*len)
{
	int i, l;
	char*buffer, c;
	i = start;
	c = respond[i];
	buffer = (char*)NULL;
	l = 0;
	while ( i < end && c != ',' ) {
		buffer = (char*)realloc(buffer, (l + 1) * sizeof(char));
		buffer[l] = c;
		l++;
		i++;
		c = respond[i];
	}
	buffer = (char*)realloc(buffer, (l + 1) * sizeof(char));
	buffer[l] = '\0';
//	l++;
	*len = l;
	return buffer;
}


void instrsubst(char*string, char*pattern, char*sample)
{
	int pl;
	int sl;
	int tl;
	int al;
	int i;
	int k;
	char*tail;

	pl = strlen(pattern);
	sl = strlen(sample);
	tl = strlen(string);
	for ( i = 0; i < tl; i++ ) {
		if ( i + pl > tl ) {
			break;
		} else if ( !strncmp( &string[i], pattern, pl ) ) {
			tail = strdup(&string[i + pl]);
			al = strlen(tail);
			for ( k = 0; k < pl; k++ ) {
				string[i + k] = ' ';
			}
			for ( k = 0; k < sl; k++ ) {
				string[i + k] = sample[k];
			}
			for ( k = 0; k < al; k++ ) {
				string[i + sl + k] = tail[k];
			}
			string[i + sl + al] = '\0';
			free(tail);
			break;
		}
	}
}


char*outstrsubst(char*string, char*format, char*pattern)
{
	int pl;
	int sl;
	int tl;
	int al;
	int i;
	int k;
	int l;
	int j;
	char*sample;
	pl = strlen(pattern);
	tl = strlen(format);
	al = strlen(string);
	sl = 0;
	sample = (char*)NULL;
	i = 0;
	k = 0;
	l = 1;
	while ( i < tl && k < al ) {
		while ( i < tl && k < al && string[k] == format[i] ) {
			i++;
			k++;
		}
		if ( !strncmp( &format[i], pattern, pl ) ) {
			i += pl;
			while ( k < al && string[k] != format[i] ) {
				sample = (char*)realloc(sample, (sl + 1) * sizeof(char));
				sample[sl] = string[k];
				sl++;
				k++;
			}
			if ( sl > 0 ) {
				sample = (char*)realloc(sample, (sl + 1) * sizeof(char));
				sample[sl] = '\0';
			}
			return sample;
		} else {
			if ( l < 10 ) {
				j = 2;
			} else if ( l < 100 ) {
				j = 3;
			}
			i += j;
			while ( k < al && string[k] != format[i] ) {
				k++;
			}
			l++;
		}
	}
	return sample;
}

char*strdup_replace(char*str, char*needle, char*replacement)
{
	int i, str_len, needle_len, rep_len, j, k, n;
	char*res;
	if ( !str || !needle || !replacement)
		return NULL;
	str_len = strlen(str);
	needle_len = strlen(needle);
	rep_len = strlen(replacement);
	if ( !str_len || !needle_len || !rep_len)
		return NULL;
	n = 0;
	res = (char*)NULL;
	for ( i = 0; i < str_len; i++ ) {
		k = 0;
		if ( i < str_len - needle_len + 1 ) {
			for ( j = 0; j < needle_len; j++ )
				if ( str[i + j] == needle[j] )
					k++;
		}
		if ( k == needle_len ) {
//		if ( k == 1 ) {
			res = (char*)realloc( res, ( n + rep_len ) * sizeof(char) );
			for ( j = 0; j < rep_len; j++ ) {
				res[ n + j ] = replacement[j];
			}
			i += needle_len - 1;
			n += rep_len;
		} else {
			res = (char*)realloc( res, ( n + 1 ) * sizeof(char) );
			res[n] = str[i];
			n++;
		}
	}
	res = (char*)realloc( res, ( n + 1 ) * sizeof(char) );
	res[n] = '\0';
	return res;
}

int node_macro_get_outport_number(Node*node, int nodeID, int portID)
{
	int i;
	int port;
	char *fmat, *skip;
	int nID;
	int pID;
	fmat = (char*)calloc(MAX_RECORD, sizeof(char));
	skip = (char*)calloc(MAX_RECORD, sizeof(char));
	skip = strcpy(skip, "i=");
	for (i = 0; i < node->nInConns; i++ ) {
		skip = strcat(skip,"%*d.%*d;");
	}
	skip = strcat(skip, "o=");
	port = -1;
	for (i = 0; i < node->nOutConns; i++ ) {
		fmat = strcpy(fmat, skip);
		fmat = strcat(fmat, "%d.%d;");
		sscanf(node->file, fmat, &nID, &pID);
		if ( nID == nodeID && portID == pID ) {
			port = i;
			break;
		}
		skip = strcat(skip,"%*d.%*d;");
	}
	free(fmat);
	free(skip);
	return port;
}


void node_macro_get_outport(Node*node, int port, int*nodeID, int*portID)
{
	int i;
	char *fmat;
	if ( port > node->nOutConns - 1 ) {
		*nodeID = -1;
		*portID = -1;
		return;
	}
	fmat = (char*)calloc(MAX_RECORD, sizeof(char));
	fmat = strcpy(fmat, "i=");
	for (i = 0; i < node->nInConns; i++ ) {
		fmat = strcat(fmat,"%*d.%*d;");
	}
	fmat = strcat(fmat, "o=");
	for (i = 0; i < port; i++ ) {
		fmat = strcat(fmat,"%*d.%*d;");
	}
	fmat = strcat(fmat, "%d.%d;");
	sscanf(node->file, fmat, nodeID, portID);
	free(fmat);
}

void node_macro_get_inport(Node*node, int port, int*nodeID, int*portID)
{
	int i;
	char *fmat;
	if ( port > node->nInConns - 1 ) {
		*nodeID = -1;
		*portID = -1;
		return;
	}
	fmat = (char*)calloc(MAX_RECORD, sizeof(char));
	fmat = strcpy(fmat, "i=");
	for (i = 0; i < port; i++ ) {
		fmat = strcat(fmat,"%*d.%*d;");
	}
	fmat = strcat(fmat, "%d.%d;");
	sscanf(node->file, fmat, nodeID, portID);
	free(fmat);
}


void nodePrint(Node*ne, GKeyFile*gkf)
{
	GError*gerror = NULL;
	GString*buf;
	int i;
	char*ty;
	char*grp;
#ifdef G_OS_WIN32
	char *buf_id, *buf_file;
#endif
	grp = g_strdup_printf("Node:%d", ne->ID);
	g_key_file_set_integer_list(gkf, grp, "info", ne->info->array, ne->info->n);
	buf = g_string_new("");
#ifdef G_OS_WIN32
	buf_file = g_strescape(ne->file, NULL);
	buf_id = g_strescape(ne->id, NULL);
	g_key_file_set_value(gkf, grp, "id", buf_id);
#else
	g_key_file_set_value(gkf, grp, "id", ne->id);
#endif
	switch ( ne->type ) {
		case pam:
			g_key_file_set_value(gkf, grp, "type", "PAM");
			for ( i = 0; i < ne->nOutConns - 1; i++) {
				ty = type2string(ne->outConn[i].type);
				g_string_append_printf(buf, "%s,", ty);
				free(ty);
			}
			i = ne->nOutConns - 1;
			if ( i > -1 ) {
				ty = type2string(ne->outConn[i].type);
				g_string_append_printf(buf, "%s", ty);
				free(ty);
			}
			g_key_file_set_value(gkf, grp, "file", buf->str);
			g_key_file_set_value(gkf, grp, "name", ne->pam->name);
			g_key_file_set_value(gkf, grp, "version", ne->version);
			g_key_file_set_integer(gkf, grp, "use_metaname", ne->use_metaname);
		break;
		case file:
			g_key_file_set_value(gkf, grp, "type", "FILE");
#ifdef G_OS_WIN32
			g_key_file_set_value(gkf, grp, "file", buf_file);
#else
			g_key_file_set_value(gkf, grp, "file", ne->file);
#endif
		break;
		case ins:
			g_key_file_set_value(gkf, grp, "type", "INS");
#ifdef G_OS_WIN32
			g_key_file_set_value(gkf, grp, "file", buf_file);
#else
			g_key_file_set_value(gkf, grp, "file", ne->file);
#endif
		break;
		case ous:
			g_key_file_set_value(gkf, grp, "type", "OUS");
#ifdef G_OS_WIN32
			g_key_file_set_value(gkf, grp, "file", buf_file);
#else
			g_key_file_set_value(gkf, grp, "file", ne->file);
#endif
		break;
		case macro:
			g_key_file_set_value(gkf, grp, "type", "MACRO");
#ifdef G_OS_WIN32
			g_key_file_set_value(gkf, grp, "file", buf_file);
#else
			g_key_file_set_value(gkf, grp, "file", ne->file);
#endif
		break;
		case display:
			g_key_file_set_value(gkf, grp, "type", "DISPLAY");
			g_key_file_set_value(gkf, grp, "name", "display");
		break;
	}
	g_key_file_set_value(gkf, grp, "server", ne->oServer->nicname);
	olapserverPrint(ne->oServer, gkf);
	g_key_file_set_integer(gkf, grp, "delay", ne->timeDelay);
	g_key_file_set_integer(gkf, grp, "vip", ne->vip);
	g_key_file_set_value(gkf, grp, "label", ne->label);
	g_string_free(buf, TRUE);
#ifdef G_OS_WIN32
	g_free(buf_id);
	g_free(buf_file);
#endif
	g_free(grp);
	return;
}


Node*nodeParse(GKeyFile*gkf, char*grp)
{
	GError*gerror = NULL;
	int i, j;
	Node*ne;
	char*str;
	char*buf;
	char**title;
	gsize size;
	ne = node_new (1);
	ne->info = (VInfo*)malloc(sizeof(VInfo));
	title = g_strsplit(grp, ":", 2);
	if ( title == NULL ) {
		g_error("node_parse no title error:%s for %s\nat Node.c:1128 ", grp, gerror->message);
	}
	if ( title[1] == NULL ) {
		g_error("node_parse no title[1] error:%s for %s\nat Node.c:1131 ", grp, gerror->message);
	}
	ne->ID = atoi(title[1]);
	g_strfreev(title);
	str = g_key_file_get_string(gkf, (const gchar *)grp, "type", &gerror);
	if ( gerror ) {
		g_debug("node_parse error:%s", gerror->message);
		g_error_free(gerror);
		return ne;
	}
	if ( !strcmp(str,"PAM") ) {
		ne->type = pam;
	} else if ( !strcmp(str,"DISPLAY") ) {
		ne->type = display;
	} else if ( !strcmp(str,"IM") ) {
		ne->type = display;
	} else if ( !strcmp(str,"FILE") ) {
		ne->type = file;
	} else if ( !strcmp(str,"INS") ) {
		ne->type = ins;
	} else if ( !strcmp(str,"OUS") ) {
		ne->type = ous;
	} else if ( !strcmp(str,"MACRO") ) {
		ne->type = macro;
	} else {
		if (isSetError)
			error("unknown pam node");
	}
	ne->info->array = g_key_file_get_integer_list(gkf, (const gchar *)grp, "info", &size, &gerror);
	ne->info->n = (int)size;
	if ( gerror ) {
		g_debug("node_parse error:%s", gerror->message);
		g_error_free(gerror);
		return ne;
	}
	str = g_key_file_get_string(gkf, (const gchar *)grp, "id", &gerror);
	if ( gerror ) {
		g_debug("node_parse error:%s", gerror->message);
		g_error_free(gerror);
		return ne;
	}
	ne->id = str;
	if ( ne->type != display ) {
		str = g_key_file_get_string(gkf, (const gchar *)grp, "file", &gerror);
		if ( gerror ) {
			g_debug("node_parse error:%s", gerror->message);
			g_error_free(gerror);
			return ne;
		}
		ne->file = str;
	}
	if ( ne->type == pam ) {
		if ( ( str = g_key_file_get_string(gkf, (const gchar *)grp, "use_metaname", &gerror) ) != NULL ) {
			ne->use_metaname = atoi(str);
			g_free(str);
		} else {
			if ( gerror ) {
				g_debug("node_parse error:%s", gerror->message);
				g_error_free(gerror);
				gerror = NULL;
			}
		}
	}
	str = g_key_file_get_string(gkf, (const gchar *)grp, "server", &gerror);
	ne->oServer = olapserverParse(gkf, str, ne->ID);
	g_free(str);
	str = g_key_file_get_string(gkf, (const gchar *)grp, "delay", &gerror);
	if ( gerror ) {
		g_debug("node_parse error:%s", gerror->message);
		g_error_free(gerror);
		return ne;
	}
	ne->timeDelay = atoi(str);
	g_free(str);
	str = g_key_file_get_string(gkf, (const gchar *)grp, "label", &gerror);
	if ( gerror ) {
		g_debug("node_parse error:%s", gerror->message);
		g_error_free(gerror);
		return ne;
	}
	ne->label = strcpy(ne->label, str);
	g_free(str);
	switch ( ne->type ) {
		case pam:
			str = g_key_file_get_string(gkf, (const gchar *)grp, "name", &gerror);
			if ( gerror ) {
				g_debug("node_parse error:%s", gerror->message);
				g_error_free(gerror);
				return ne;
			}
			ne->pam = newOlapPAM(str);
			g_free(str);
			str = NULL;
			setNodePorts(ne);
			str = g_key_file_get_string(gkf, (const gchar *)grp, "version", &gerror);
			if ( gerror ) {
				g_debug("node_parse error:%s", gerror->message);
				g_error_free(gerror);
				gerror = NULL;
				ne->version = g_strdup("0.0.0");
			} else {
				ne->version = g_strdup(str);
				g_free(str);
			}
			node_check_version(ne);
		break;
		case file:
			ne->nOutConns = 1;
			ne->outConn = (Port*)calloc(1, sizeof(Port));
			ne->outConn[0].ID = 1;
			ne->outConn[0].label = (char*)calloc(6, sizeof(char));
			ne->outConn[0].label = strcpy(ne->outConn[0].label, "image");
			ne->outConn[0].type = tif;
			ne->outConn[0].nConn = 0;
			ne->outConn[0].channel = (Connection**)NULL;
		break;
		case ins:
			ne->nOutConns = 1;
			ne->outConn = (Port*)calloc(1, sizeof(Port));
			ne->outConn[0].ID = 1;
			ne->outConn[0].label = (char*)calloc(6, sizeof(char));
			ne->outConn[0].label = strcpy(ne->outConn[0].label, "image");
			ne->outConn[0].type = tif;
			ne->outConn[0].nConn = 0;
			ne->outConn[0].channel = (Connection**)NULL;
		break;
		case ous:
			ne->nInConns = 1;
			ne->inConn = (Port*)calloc(1, sizeof(Port));
			ne->inConn[0].ID = 1;
			ne->inConn[0].label = (char*)calloc(6, sizeof(char));
			ne->inConn[0].label = strcpy(ne->inConn[0].label, "image");
			ne->inConn[0].type = tif;
			ne->inConn[0].nConn = 0;
			ne->inConn[0].channel = (Connection**)NULL;
		break;
		case macro:
			node_macro_set_in_ports(ne);
			node_macro_set_out_ports(ne);
		break;
		case display:
			ne->nInConns = 1;
			ne->inConn = (Port*)calloc(1, sizeof(Port));
			ne->inConn[0].ID = 1;
			ne->inConn[0].label = (char*)calloc(6, sizeof(char));
			ne->inConn[0].label = strcpy(ne->inConn[0].label, "image");
			ne->inConn[0].type = tif;
			ne->inConn[0].nConn = 0;
			ne->inConn[0].channel = (Connection**)NULL;
		break;
	}
	str = g_key_file_get_string(gkf, (const gchar *)grp, "vip", &gerror);
	if ( gerror ) {
		g_debug("node_parse error:%s", gerror->message);
		g_error_free(gerror);
		gerror = NULL;
		ne->vip = 0;
	} else {
		ne->vip = atoi(str);
		g_free(str);
	}
	return ne;
}

void outPortPrint(Node*ne, int nport, int nconn, GKeyFile*gkf)
{
	GError*gerror = NULL;
	char*grp;
	char*str;
	char*key;
	gsize size;
	grp = g_strdup_printf("Connections");
	str = g_strdup_printf("%d.%d", ne->outConn[nport].channel[nconn]->destNodeID, ne->outConn[nport].channel[nconn]->destPortID);
	key = g_strdup_printf("%d.%d", ne->outConn[nport].channel[nconn]->sourceNodeID, ne->outConn[nport].channel[nconn]->sourcePortID);
	if ( g_key_file_has_key(gkf, grp, key, &gerror) ) {
		char**list;
		int len;
		list = g_key_file_get_string_list(gkf, grp, key, &size, &gerror);
		len = (int)size;
		list = (char**)realloc(list, (len + 2) * sizeof(char*));
		list[len] = g_strdup(str);
		list[len + 1] = NULL;
		g_key_file_set_string_list(gkf, grp, key, list, len + 1);
		g_strfreev(list);
	} else {
		g_key_file_set_value(gkf, grp, key, str);
	}
	g_free(grp);
	g_free(str);
	g_free(key);
}

Connection**outPortParse(GKeyFile*gkf, char*key, int *n)
{
	GError*gerror;
	Connection**conn;
	int sourceNodeID;
	int sourcePortID;
	int destNodeID;
	int destPortID;
	char**list;
	int len;
	int j;
	gsize size;
	sscanf(key, "%d.%d", &sourceNodeID, &sourcePortID);
	list = g_key_file_get_string_list(gkf, "Connections", key, &size, &gerror);
	len = (int)size;
	*n = len;
	conn = (Connection**)calloc(len, sizeof(Connection*));
	for(j = 0; j < len; j++) {
		sscanf(list[j], "%d.%d", &destNodeID, &destPortID);
		conn[j] = newConnection(sourceNodeID, sourcePortID, destNodeID, destPortID);
	}
	return conn;
}

void disp(Node*ne)
{
	char*cmd, *buf;
	char**margv;
	char *olapfs;
	char *file;
	GPid child_pid;
	GError *gerror = NULL;
	int flaggs;
	int argcp;
	buf = door_get_config_path(ne->oServer->hname, ne->oServer->port, ne->oServer->proto);
	flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL;
/*	flaggs |= G_SPAWN_FILE_AND_ARGV_ZERO;
	flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;*/
	file = g_path_get_basename(ne->inConn[0].channel[ne->inConn[0].index]->file);
	olapfs = g_build_filename (buf, file, NULL);
	g_free(buf);
	buf = g_shell_quote(olapfs);
	g_free(olapfs);
	g_free(file);
	g_mutex_lock(&(ne->inConn[0].channel[ne->inConn[0].index]->connMutex));
	cmd = g_strdup_printf(ne->id, ne->label, buf);
	g_free(buf);
	g_mutex_unlock(&(ne->inConn[0].channel[ne->inConn[0].index]->connMutex));
//	margv = g_strsplit(cmd, " ", MAXTOKENS);
	if ( !g_shell_parse_argv(cmd, &argcp, &margv, &gerror) ) {
		if ( gerror ) {
			g_debug("g_shell_parse failed for %s\nwith %s", cmd, gerror->message);
			g_error_free(gerror);
			g_free(cmd);
			return;
		}
	}
	if ( !g_spawn_async (g_get_home_dir(), margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, &child_pid, &gerror) ) {
		if ( gerror ) {
			g_debug("g_spawn_async failed with %s\nas %s", cmd, gerror->message);
			g_error_free(gerror);
		}
	} else {
		ne->pid = child_pid;
	}
	g_free(cmd);
	g_strfreev(margv);
}

int door_call(char*method, Node*node)
{
	int res;
	char*proto;
	res = -1;
	proto = g_strdup(node->oServer->proto);
	if ( !strcmp(proto, "door") ) {
		if ( !strcmp(method, "GET") ) {
			res = door_get(node);
		} else if ( !strcmp(method, "PUT") ) {
			res = door_put(node);
		} else if ( !strcmp(method, "UPDATE") ) {
			res = door_update(node);
		} else if ( !strcmp(method, "DELETE") ) {
			res = door_delete(node);
		}
	} else if ( !strcmp(proto, "rest") ) {
		if ( !strcmp(method, "GET") ) {
			res = rest_get(node);
		} else if ( !strcmp(method, "PUT") ) {
			res = rest_put(node);
		} else if ( !strcmp(method, "UPDATE") ) {
			res = rest_update(node);
		} else if ( !strcmp(method, "DELETE") ) {
			res = rest_delete(node);
		}
	} else if ( !strcmp(proto, "rests") ) {
		if ( !strcmp(method, "GET") ) {
			res = rests_get(node);
		} else if ( !strcmp(method, "PUT") ) {
			res = rest_put(node);
		} else if ( !strcmp(method, "UPDATE") ) {
			res = rest_update(node);
		} else if ( !strcmp(method, "DELETE") ) {
			res = rest_delete(node);
		}
	}
	return res;
}

int door_get(Node*node)
{
	int res;
	res = -1;
	switch( node->type ) {
		case pam:
			res = door_get_pam(node);
		break;
		case ins:
			res = door_get_ins(node);
		break;
		case ous:
			res = door_get_ous(node);
		break;
		case file:
			res = door_get_file(node);
		break;
		case display:
			res = door_get_display(node);
		break;
		case sys:
			res = door_get_sys(node);
		break;
	}
	return res;
}

int door_put(Node*node)
{
	int res;
	res = -1;
	switch( node->type ) {
		case pam:

		break;
		case ins:

		break;
		case ous:

		break;
		case file:

		break;
		case display:

		break;
		case sys:
			res = door_put_sys(node);
		break;
	}
	return res;
}

int door_update(Node*node)
{
	return -1;
}

int door_delete(Node*node)
{
	int res;
	res = -1;
	if ( !node->uid && node->type != display )
		return res;
	switch( node->type ) {
		case pam:
			res = door_delete_pam(node);
		break;
		case ins:
			res = door_delete_ins(node);
		break;
		case ous:

		break;
		case file:

		break;
		case display:
			res = door_delete_display(node);
		break;
		case sys:
			res = door_delete_sys(node);
		break;
	}
	return res;
}

void nodeRmDisp(Node*node)
{
	if ( outerIsSet && node->status == completed ) {
		gnc_gpid_kill(node->pid);
	}
}

void gnc_gpid_kill(GPid pid)
{
#ifdef G_OS_WIN32
	if (!TerminateProcess((HANDLE) pid, 0)) {
		gchar *msg = g_win32_error_message(GetLastError());
		g_debug("Could not kill child process: %s", msg ? msg : "(null)");
		g_free(msg);
	}
	g_spawn_close_pid(pid);
#else /* !G_OS_WIN32 */
	if (kill(pid, 9)) {
		g_debug("Could not kill child process: %s", g_strerror(errno));
	}
	g_spawn_close_pid(pid);
#endif /* G_OS_WIN32 */
}

int node_pam_check_outputs(Node*node)
{
	int res, i;
	char *buf, *olapfs, *file;
	res = 1;
	olapfs = door_get_config_path(node->oServer->hname, node->oServer->port, node->oServer->proto);
	g_mutex_lock (&(node->statusMutex));
	for ( i = 0; i < node->nOutConns; i++ ) {
		file = node_get_out_conn_file(node, i, node->uid);
		buf = g_build_filename (olapfs, file, NULL);
		g_free(file);
		if ( !g_file_test((const gchar*)buf, G_FILE_TEST_EXISTS) ) {
			res = 0;
			g_free(buf);
			break;
		}
		g_free(buf);
	}
	g_mutex_unlock (&(node->statusMutex));
	g_free(olapfs);
	return res;
}
/*
void gsrc_pam_func_1(GPid pid, gint status, gpointer data)
{
	Node*node;
	node = (Node*)data;
	NotifyNode(node, completed);
	g_spawn_close_pid(pid);
}*/

void gsrc_pam_func(GPid pid, gint status, gpointer data)
{
	Node*node;
	int check;
	node = (Node*)data;
	check = node_pam_check_outputs(node);
	if ( check == 1 ) {
		node_fill_out_conn_flag(node, 1);
		NotifyNode(node, completed);
	} else if ( check == 0 ) {
		NotifyNode(node, failed);
		nodeCleanOutPut (node);
	}
	g_spawn_close_pid(pid);
}

void node_fill_out_conn_flag(Node*node, int val_flag)
{
	int i, j;
	for( i = 0; i < node->nOutConns; i++ ) {
		for( j = 0; j < node->outConn[i].nConn; j++ ) {
			g_mutex_lock (&(node->outConn[i].channel[j]->connMutex));
			node->outConn[i].channel[j]->flag = val_flag;
			node->outConn[i].channel[j]->ptype = path2type2(node->outConn[i].channel[j]->file);
			g_mutex_unlock (&(node->outConn[i].channel[j]->connMutex));
		}
	}
}


GString*node_fill_out_conn_to_cmd(Node*node, char*pam_id, int val_flag, GString*command, gchar*olapfs, char cepar)
{
	int i, j;
	char*buf, *file, *cmd;
	i = 0;
	file = node_get_out_conn_file(node, i, pam_id);
	buf = g_build_filename (olapfs, file, NULL);
	cmd = g_shell_quote(buf);
	g_free(buf);
	command = g_string_append(command, cmd);
	g_free(cmd);
	if ( !node->terminal ) {
		for( j = 0; j < node->outConn[i].nConn; j++ ) {
			g_mutex_lock (&(node->outConn[i].channel[j]->connMutex));
			node->outConn[i].channel[j]->host = g_strdup(node->oServer->hname);
			node->outConn[i].channel[j]->file = g_strdup(file);
			node->outConn[i].channel[j]->proto = g_strdup(node->oServer->proto);
			node->outConn[i].channel[j]->address = g_strdup(node->oServer->address);
			node->outConn[i].channel[j]->port = node->oServer->port;
			node->outConn[i].channel[j]->flag = val_flag;
			node->outConn[i].channel[j]->ptype = unk;
			g_mutex_unlock (&(node->outConn[i].channel[j]->connMutex));
		}
	}
	g_free(file);
	for( i = 1; i < node->nOutConns; i++ ) {
		file = node_get_out_conn_file(node, i, pam_id);
		buf = g_build_filename (olapfs, file, NULL);
		cmd = g_shell_quote(buf);
		g_free(buf);
		command = g_string_append_c(command, cepar);
		command = g_string_append(command, cmd);
		g_free(cmd);
		if ( !node->terminal ) {
			for( j = 0; j < node->outConn[i].nConn; j++ ) {
				g_mutex_lock (&(node->outConn[i].channel[j]->connMutex));
				node->outConn[i].channel[j]->host = g_strdup(node->oServer->hname);
				node->outConn[i].channel[j]->file = g_strdup(file);
				node->outConn[i].channel[j]->proto = g_strdup(node->oServer->proto);
				node->outConn[i].channel[j]->address = g_strdup(node->oServer->address);
				node->outConn[i].channel[j]->port = node->oServer->port;
				node->outConn[i].channel[j]->flag = val_flag;
				node->outConn[i].channel[j]->ptype = unk;
				g_mutex_unlock (&(node->outConn[i].channel[j]->connMutex));
			}
		}
		g_free(file);
	}
	return command;
}

char door_pam_get_scepar ( int pam_type )
{
	char scepar = ' ';
	switch ( pam_type ) {
		case 0:
			scepar = ' ';
		break;
		case 1:
			scepar = ' ';
		break;
		case 2:
			scepar = ' ';
		break;
		case 3:
			scepar = ' ';
		break;
		case 4:
			scepar = '@';
		break;
		default:
			scepar = ' ';
	}
	return scepar;
}

char door_pam_get_cepar (int pam_type)
{
	char cepar = ' ';
	switch ( pam_type ) {
		case 0:
			cepar = ' ';
		break;
		case 1:
			cepar = ',';
		break;
		case 2:
			cepar = ',';
		break;
		case 3:
			cepar = ',';
		break;
		case 4:
			cepar = ',';
		break;
		default:
			cepar = ',';
	}
	return cepar;
}

gchar*door_pam_conversion_helper(gchar*pam_cmd, Node*node, int *data_need_conversion_flag, int *data_conversion_impossible, char cepar, char scepar)
{
	GString *command;
	GString*conv_list_index, *conv_list_input, *conv_list_output;
	char*buftype, *olapfs, *buf, *cmd;
	int i;
	command = g_string_new((const gchar*)pam_cmd);
	conv_list_index = g_string_new("");
	conv_list_input = g_string_new("");
	conv_list_output = g_string_new("");
	i = 0;
	g_mutex_lock(&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
	olapfs = door_get_config_path(node->inConn[i].channel[ node->inConn[i].index ]->host, node->inConn[i].channel[ node->inConn[i].index ]->port, node->inConn[i].channel[ node->inConn[i].index ]->proto);
	buf = g_build_filename (olapfs, node->inConn[i].channel[ node->inConn[i].index ]->file, NULL);
	g_free(olapfs);
	cmd = g_shell_quote(buf);
	command = g_string_append(command, cmd);
	g_free(buf);
	g_free(cmd);
	if ( node->inConn[i].channel[ node->inConn[i].index ]->ptype != node->inConn[i].type ) {
		(*data_need_conversion_flag) = 1;
		g_string_append_printf(conv_list_index, "%d", i);
		buftype = type2string2(node->inConn[i].channel[ node->inConn[i].index ]->ptype);
		g_string_append_printf(conv_list_input, "%s", buftype);
		g_free(buftype);
		buftype = type2string2(node->inConn[i].type);
		g_string_append_printf(conv_list_output, "%s", buftype);
		g_free(buftype);
		if ( node->inConn[i].type == lqr ) {
			(*data_conversion_impossible) = 1;
		}
	}
	g_mutex_unlock(&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
	for( i = 1; i < node->nInConns; i++ ) {
		g_mutex_lock(&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
		command = g_string_append_c(command, cepar);
		olapfs = door_get_config_path(node->inConn[i].channel[ node->inConn[i].index ]->host, node->inConn[i].channel[ node->inConn[i].index ]->port, node->inConn[i].channel[ node->inConn[i].index ]->proto);
		buf = g_build_filename (olapfs, node->inConn[i].channel[ node->inConn[i].index ]->file, NULL);
		g_free(olapfs);
		cmd = g_shell_quote(buf);
		command = g_string_append(command, cmd);
		g_free(buf);
		g_free(cmd);
		if ( node->inConn[i].channel[ node->inConn[i].index ]->ptype != node->inConn[i].type ) {
			if ( (*data_need_conversion_flag) == 1 ) {
				g_string_append_printf(conv_list_index, ",%d", i);
			} else {
				g_string_append_printf(conv_list_index, "%d", i);
			}
			buftype = type2string2(node->inConn[i].channel[ node->inConn[i].index ]->ptype);
			if ( (*data_need_conversion_flag) == 1 ) {
				g_string_append_printf(conv_list_input, ",%s", buftype);
			} else {
				g_string_append_printf(conv_list_input, "%s", buftype);
			}
			g_free(buftype);
			buftype = type2string2(node->inConn[i].type);
			if ( (*data_need_conversion_flag) == 1 ) {
				g_string_append_printf(conv_list_output, ",%s", buftype);
			} else {
				g_string_append_printf(conv_list_output, "%s", buftype);
			}
			g_free(buftype);
			(*data_need_conversion_flag) = 1;
			if ( node->inConn[i].type == lqr ) {
				(*data_conversion_impossible) = 1;
			}
		}
		g_mutex_unlock(&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
	}
	command = g_string_append_c(command, scepar);
	if ( (*data_need_conversion_flag) == 1 ) {
		buftype = g_strdup_printf("door_convert_type --door-convert-input-types %s --door-convert-output-types %s --door-convert-indices %s -- ", conv_list_input->str, conv_list_output->str, conv_list_index->str);
		command = g_string_prepend(command, buftype);
		g_free(buftype);
		g_string_free ( conv_list_index, TRUE);
		g_string_free ( conv_list_input, TRUE);
		g_string_free ( conv_list_output, TRUE);
/*		data_need_conversion_flag = 0;*/
	}
	buftype = command->str;
	g_string_free ( command, FALSE);
	return buftype;
}

int door_get_pam(Node*node)
{
	char*cmd;
	char**margv;
	char *olapfs;
	char *file;
	char* buf;
	GPid child_pid;
	GError *gerror = NULL;
	int flaggs;
	int d, pam_type, i, n, res, j;
	char *pam_id;
	GString *command;
	GSource *gsource;
	guint gsrcid;
	char cepar, scepar;
	NodeStatus node_status;
	int r;
	int fd;
	int argcp;
	int data_need_conversion_flag = 0;
	int data_conversion_impossible = 0;
	char*input_ports_string, *output_ports_string;
	char*pam_metaname;
	pam_id = NULL;
	input_ports_string = NULL;
	output_ports_string = NULL;
	pam_metaname = NULL;
	cmd = kimono_get_pam_exec(node->pam->name, node->ID, &pam_type, &pam_id, node->use_metaname, &pam_metaname);
	if ( !cmd ) {
		NotifyNode(node, failed);
		if (isSetError) {
			buf = g_strdup_printf("Unknown pam");
			error(buf);
			g_free(buf);
		}
		return -1;
	}
	output_ports_string = kimono_get_pam_info(pam_metaname, "outputs");
	if ( output_ports_string != NULL ) {
		ports_set_type(node->outConn, output_ports_string);
		g_free(output_ports_string);
		output_ports_string = NULL;
	}
	input_ports_string = kimono_get_pam_info(pam_metaname, "inputs");
	if ( input_ports_string != NULL ) {
		ports_set_type(node->inConn, input_ports_string);
		g_free(input_ports_string);
		input_ports_string = NULL;
	}
	scepar = door_pam_get_scepar ( pam_type );
	command = g_string_new((const gchar*)cmd);
	command = g_string_append_c(command, scepar);
	g_free(cmd);
	if ( node->id ) {
		buf = g_uri_unescape_string((const char *)node->id, (const char*)NULL);
		if ( buf ) {
			buf = g_strstrip(buf);
			if ( strlen(buf) > 0 ) {
				command = g_string_append(command, buf);
				command = g_string_append_c(command, scepar);
			}
			g_free(buf);
		}
	}
	cepar = door_pam_get_cepar ( pam_type );
// [user[:pass]@]{http[s]|isms|door}://hostname[:port]/some/path/tempfile.ext
// /home/user/.bambu/temp/hostname:port/tempfile.ext
// /some/path/ == address
	if ( node->nInConns > 0 ) {
		buf = door_pam_conversion_helper(command->str, node, &data_need_conversion_flag, &data_conversion_impossible, cepar, scepar);
		command = g_string_assign(command, buf);
		g_free(buf);
	}
	if ( data_need_conversion_flag == 1 && data_conversion_impossible == 1 ) {
/* clean stuff */
		if ( pam_id != NULL ) {
			g_free( pam_id );
			pam_id = NULL;
		}
		if ( input_ports_string != NULL ) {
			g_free( input_ports_string );
			input_ports_string = NULL;
		}
		if ( output_ports_string != NULL ) {
			g_free( output_ports_string );
			output_ports_string = NULL;
		}
		if ( pam_metaname != NULL ) {
			g_free( pam_metaname );
			pam_metaname = NULL;
		}
		g_string_free ( command, TRUE);
/* */
		data_need_conversion_flag = 0;
		data_conversion_impossible = 0;
		cmd = kimono_get_pam_exec(node->pam->name, node->ID, &pam_type, &pam_id, 0, &pam_metaname);
		if ( !cmd ) {
			NotifyNode(node, failed);
			if (isSetError) {
				buf = g_strdup_printf("Unknown pam");
				error(buf);
				g_free(buf);
			}
			return -1;
		}
		output_ports_string = kimono_get_pam_info(pam_metaname, "outputs");
		if ( output_ports_string ) {
			ports_set_type(node->outConn, output_ports_string);
			g_free(output_ports_string);
		}
		input_ports_string = kimono_get_pam_info(pam_metaname, "inputs");
		if ( input_ports_string ) {
			ports_set_type(node->inConn, input_ports_string);
			g_free(input_ports_string);
		}
		scepar = door_pam_get_scepar ( pam_type );
		command = g_string_new((const gchar*)cmd);
		command = g_string_append_c(command, scepar);
		g_free(cmd);
		if ( node->id ) {
			buf = g_uri_unescape_string((const char *)node->id, (const char*)NULL);
			if ( buf ) {
				buf = g_strstrip(buf);
				if ( strlen(buf) > 0 ) {
					command = g_string_append(command, buf);
					command = g_string_append_c(command, scepar);
				}
				g_free(buf);
			}
		}
		cepar = door_pam_get_cepar ( pam_type );
		if ( node->nInConns > 0 ) {
			buf = door_pam_conversion_helper(command->str, node, &data_need_conversion_flag, &data_conversion_impossible, cepar, scepar);
			command = g_string_assign(command, buf);
			g_free(buf);
		}
	}
	olapfs = door_get_config_path(node->oServer->hname, node->oServer->port, node->oServer->proto);
	if ( node->nOutConns > 0 ) {
		command = node_fill_out_conn_to_cmd(node, pam_id, 0, command, olapfs, cepar);
	}
	flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_DO_NOT_REAP_CHILD;
/*	flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;
	flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD;*/
	g_message("Cmd[%d]: %s", node->ID, command->str);
	if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
		if ( gerror ) {
			g_debug("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
			g_error_free(gerror);
			g_free(olapfs);
			NotifyNode(node, failed);
			g_free(pam_id);
			return -1;
		}
	}
	node->uid = g_strdup_printf("%s", pam_id);
	g_free(pam_id);
	if ( !g_spawn_async (olapfs, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, &child_pid, &gerror) ) {
		if ( gerror ) {
			g_debug("g_spawn_async failed for %s\nwith %s", command->str, gerror->message);
			g_error_free(gerror);
			g_free(olapfs);
			g_strfreev(margv);
			NotifyNode(node, failed);
			g_free(pam_id);
			return -1;
		}
	} else {
		gsource = kimono_child_watch_add(child_pid, gsrc_pam_func, node);
	}
	g_string_free(command, TRUE);
	g_free(olapfs);
	g_strfreev(margv);
	g_free(pam_metaname);
	n = node->timeDelay;
	r = 0;
	node_status = running;
	for (i = 0; i < n; i++) {
		g_mutex_lock ((node->parent_mutex));
		r = *(node->parent_signal);
		g_mutex_unlock ((node->parent_mutex));
		g_mutex_lock (&(node->statusMutex));
		node_status = node->status;
		g_mutex_unlock (&(node->statusMutex));
/*		kimono_main_context_iteration();*/
		if ( r != 0 || node_status != running ) {
			g_source_destroy (gsource);
			break;
		}
		g_usleep(G_USEC_PER_SEC);
	}
	if ( node_status == failed ) {
		if (isSetError) {
			buf = g_strdup_printf("Node: %d failed", node->ID);
			error(buf);
			g_free(buf);
		}
		return -1;
	} else if ( node_status == running ) {
		gnc_gpid_kill(child_pid);
		if ( r != 0 ) { /* terminated */
			NotifyNode(node, waiting);
			if (isSetError) {
				buf = g_strdup_printf("Node: %d terminated", node->ID);
				error(buf);
				g_free(buf);
			}
		} else { /* timeout */
			NotifyNode(node, failed);
			if (isSetError) {
				buf = g_strdup_printf("Node: %d\nServer timeout", node->ID);
				error(buf);
				g_free(buf);
			}
		}
		nodeCleanOutPut (node);
		return -1;
	} else if ( node_status == completed ) {
		return 0;
	}
	return 1;
}

int door_get_ins(Node*node)
{
	int k, j;
	static int clock = 0;
	char*from_path;
	char*to_path;
	char*buf;
	char*base;
	char**ext;
	int next;
	char*olapfs;
	int res;
	char*ins_id;
	char*ins_exec;
	int ins_type;
	PortType ptype;
	base = g_path_get_basename(node->file);
	ext = g_strsplit(base, ".", MAXTOKENS);
	g_free(base);
	if ( ext ) {
		next = g_strv_length(ext);
		ins_exec = kimono_get_ins_exec("ins", node->ID, &ins_type, &ins_id);
		buf = g_strdup_printf("%s.out.0.%s", ins_id, ext[next - 1]);
		g_free(ins_exec);
		clock++;
		ptype = string2type2(ext[next - 1]);
		g_strfreev(ext);
		olapfs = door_get_config_path(node->oServer->hname, node->oServer->port, node->oServer->proto);
		to_path = g_build_filename (olapfs, buf, NULL);
		g_free(olapfs);
		from_path = g_strdup(node->file);
		if ( copy_big_files(from_path, to_path) ) {
			g_mutex_lock (&(node->statusMutex));
			g_mutex_unlock (&(node->statusMutex));
			NotifyNode(node, failed);
			g_free(from_path);
			g_free(to_path);
			g_free(ins_id);
			g_free(buf);
			return -1;
		} else {
			for( k = 0; k < node->nOutConns; k++ ) {
				for( j = 0; j < node->outConn[k].nConn; j++ ) {
					g_mutex_lock (&(node->outConn[k].channel[j]->connMutex));
					node->outConn[k].channel[j]->host = g_strdup(node->oServer->hname);
					node->outConn[k].channel[j]->port = node->oServer->port;
					node->outConn[k].channel[j]->file = g_strdup(buf);
					node->outConn[k].channel[j]->proto = g_strdup(node->oServer->proto);
					node->outConn[k].channel[j]->address = g_strdup(node->oServer->address);
					node->outConn[k].channel[j]->flag = 1;
					node->outConn[k].channel[j]->ptype = ptype;
					g_mutex_unlock (&(node->outConn[k].channel[j]->connMutex));
				}
			}
			node->uid = g_strdup(to_path);
			g_free(ins_id);
			g_mutex_lock (&(node->statusMutex));
			g_mutex_unlock (&(node->statusMutex));
			NotifyNode(node, completed);
			g_free(buf);
			g_free(from_path);
			g_free(to_path);
			return 0;
		}
		g_free(buf);
		g_free(ins_id);
		g_free(from_path);
		g_free(to_path);
	} else {
		g_mutex_lock (&(node->statusMutex));
		g_mutex_unlock (&(node->statusMutex));
		NotifyNode(node, failed);
		return -1;
	}
	return 1;
}

int door_get_ous(Node*node)
{
	char*from_path;
	char*to_path;
	char*olapfs;
	char*base;
	int res;
	olapfs = door_get_config_path(node->oServer->hname, node->oServer->port, node->oServer->proto);
	base = g_path_get_basename(node->inConn[0].channel[0]->file);
	from_path = g_build_filename(olapfs, base, NULL);
	to_path = g_strdup(node->file);
/*g_print("to:%s\n", to_path);
g_print("from:%s\n", from_path);
fflush(stdout);*/
	g_free(olapfs);
	g_free(base);
	if ( copy_big_files(from_path, to_path) ) {
		g_mutex_lock (&(node->statusMutex));
		g_mutex_unlock (&(node->statusMutex));
		NotifyNode(node, failed);
		g_free(from_path);
		g_free(to_path);
		return -1;
	} else {
		g_mutex_lock (&(node->statusMutex));
		g_mutex_unlock (&(node->statusMutex));
		NotifyNode(node, completed);
		g_free(from_path);
		g_free(to_path);
		return 0;
	}
}

int door_get_file(Node*ne)
{
	int k, j;
	for( k = 0; k < ne->nOutConns; k++ ) {
		for( j = 0; j < ne->outConn[k].nConn; j++ ) {
			g_mutex_lock (&(ne->outConn[k].channel[j]->connMutex));
			ne->outConn[k].channel[j]->host = g_strdup(ne->oServer->hname);
			ne->outConn[k].channel[j]->port = ne->oServer->port;
			if ( g_str_has_prefix((const gchar*)ne->file, "omero://") ) {
				ne->outConn[k].channel[j]->file = g_strdup(ne->file);
			} else {
				ne->outConn[k].channel[j]->file = g_path_get_basename(ne->file);
			}
			ne->outConn[k].channel[j]->proto = g_strdup(ne->oServer->proto);
			ne->outConn[k].channel[j]->address = g_strdup(ne->oServer->address);
			ne->outConn[k].channel[j]->flag = 1;
			g_mutex_unlock (&(ne->outConn[k].channel[j]->connMutex));
		}
	}
	g_mutex_lock (&(ne->statusMutex));
	g_mutex_unlock (&(ne->statusMutex));
	NotifyNode(ne, completed);
	return 1;
}

int door_get_display(Node*node)
{
	runNodeDisplay(node);
	return 1;
}

int door_get_sys(Node*node)
{
	int length;
	if ( node->uid == NULL )
		return -1;
	length = strlen(node->uid);
	if ( !strncmp(node->uid, "pam", (size_t)length) ) {
		node->buf = kimono_get_pam_info(node->label, node->file);
	} else if ( !strncmp(node->uid, "concepts", (size_t)length) ) {
		node->buf = kimono_get_concepts(node->label);
	} else if ( !strncmp(node->uid, "methods", (size_t)length) ) {
		node->buf = kimono_get_methods(node->label);
	} else if ( !strncmp(node->uid, "search", (size_t)length) ) {
		node->buf = kimono_get_search(node->label, node->id, node->file);
	} else if ( !strncmp(node->uid, "wksps", (size_t)length) ) {
		node->buf = kimono_get_wksps(node->label);
	} else if ( !strncmp(node->uid, "wksptypes", (size_t)length) ) {
		node->buf = kimono_get_wksptypes(node->label);
	} else if ( !strncmp(node->uid, "wkspjobs", (size_t)length) ) {
		node->buf = kimono_get_wkspjobs(node->label, node->id, node->file);
	} else if ( !strncmp(node->uid, "datatypes", (size_t)length) ) {
		node->buf = kimono_get_datatypes(node->label);
	} else if ( !strncmp(node->uid, "datas", (size_t)length) ) {
		node->buf = kimono_get_datas(node->label);
	} else if ( !strncmp(node->uid, "datainfo", (size_t)length) ) {
		node->buf = kimono_get_data_info(node->label, node->id);
	} else if ( !strncmp(node->uid, "wkspinfo", (size_t)length) ) {
		node->buf = kimono_get_wksp_info(node->label, node->id);
	} else if ( !strncmp(node->uid, "packages", (size_t)length) ) {
		node->buf = kimono_get_package_info(node->label, node->file);
	} else if ( !strncmp(node->uid, "compat", (size_t)length) ) {
		node->buf = kimono_get_compat(node->label, node->id, node->file);
	}
	if ( node->buf )
		return 0;
	return -1;
}

int door_put_sys(Node*node)
{
	char *buf;
	buf = (char*)NULL;
	if ( !strcmp(node->uid, "methods") ) {
		buf = kimono_put_method(node->label, node->id, node->file, node->buf);
		if ( node->buf ) {
			g_free ( node->buf );
		}
		node->buf = buf;
	} else if ( !strcmp(node->uid, "wksps") ) {
		buf = kimono_put_wksps(node->label, node->id, node->file, node->buf);
		if ( node->buf ) {
			g_free ( node->buf );
		}
		node->buf = buf;
	} else if ( !strcmp(node->uid, "wkspjobs") ) {
		buf = kimono_put_wkspsjobs(node->label, node->id, node->file, node->buf);
		if ( node->buf ) {
			g_free ( node->buf );
		}
		node->buf = buf;
	} else if ( !strcmp(node->uid, "datas") ) {
		buf = kimono_put_datas(node->label, node->id, node->file);
		if ( node->buf ) {
			g_free ( node->buf );
		}
		node->buf = buf;
	}
	if ( node->buf )
		return 0;
	return -1;
}

char*node_get_out_conn_file(Node*node, int index, char*pam_id)
{
	char*file, *buf;
	if ( node->outConn[index].type == any && node->nInConns > 0 ) {
		buf = path2string2(node->inConn[0].channel[node->inConn[0].index]->file);
	} else {
		buf = type2string2(node->outConn[index].type);
	}
	file = g_strdup_printf("%s.out.%d.%s", pam_id, index, buf);
	g_free(buf);
	return file;
}

int door_delete_pam(Node*node)
{
	int res, i;
	char *buf, *olapfs, *file;
	olapfs = door_get_config_path(node->oServer->hname, node->oServer->port, node->oServer->proto);
	for ( i = 0; i < node->nOutConns; i++ ) {
		file = node_get_out_conn_file(node, i, node->uid);
		buf = g_build_filename (olapfs, file, NULL);
		g_free(file);
		res = g_remove(buf);
		g_free(buf);
	}
	if ( res == 0 ) {
		kimono_free_resource(node->uid);
	}
	g_free(olapfs);
	return res;
}

int door_delete_ins(Node*node)
{
	int res;
	res = g_remove(node->uid);
	return res;
}

int door_delete_display(Node*node)
{
	nodeRmDisp(node);
	return 1;
}


int door_delete_sys(Node*node)
{
	if ( !strcmp(node->uid, "methods") ) {
		node->buf = kimono_delete_method(node->label);
	} else if ( !strcmp(node->uid, "wksps") ) {
		node->buf = kimono_delete_wksps(node->label);
	} else if ( !strcmp(node->uid, "wkspjobs") ) {
		node->buf = kimono_delete_wkspjobs(node->label, node->id);
	} else if ( !strcmp(node->uid, "datas") ) {
		node->buf = kimono_delete_datas(node->label);
	}
	if ( node->buf )
		return 0;
	return -1;
}

int rest_get(Node*node)
{
	int res;
	res = -1;
	switch( node->type ) {
		case pam:
			res = rest_get_pam(node);
		break;
		case ins:
			res = rest_get_ins(node);
		break;
		case ous:
			res = rest_get_ous(node);
		break;
		case file:
			res = rest_get_file(node);
		break;
		case display:
//			res = door_get_display(node);
		break;
		case sys:
			res = rest_get_sys(node);
		break;
	}
	return res;
}

int rest_put(Node*node)
{
	int res;
	res = -1;
	switch( node->type ) {
		case pam:

		break;
		case ins:

		break;
		case ous:

		break;
		case file:

		break;
		case display:

		break;
		case sys:
			res = rest_put_sys(node);
		break;
	}
	return res;
}

int rest_update(Node*node)
{
	return -1;
}

int rest_delete(Node*node)
{
	int res;
	res = -1;
	switch( node->type ) {
		case pam:
			res = rest_delete_pam(node);
		break;
		case ins:
//			res = door_delete_ins(node);
		break;
		case ous:

		break;
		case file:

		break;
		case display:
//			res = door_delete_display(node);
		break;
		case sys:
			res = rest_delete_sys(node);
		break;
	}
	return res;
}

static void soup_callback_send(SoupSession *session, SoupMessage *msg, gpointer data)
{
	Node*node;
	node = (Node*)data;
	int res;
	res = failed;
	if ( msg->status_code == HTTP_OK && msg->response_body && msg->response_body->data && msg->response_body->length > 1 ) {
		sscanf(msg->response_body->data,"%d", &res);
		node->buf = g_strdup(msg->response_body->data);
	}
//	if ( msg->status_code != SOUP_STATUS_CANCELLED ) {
		NotifyNode(node, (NodeStatus)res);
//	}
}

static void soup_msg_callback_wrote_body(SoupMessage *msg, gpointer user_data)
{
	int*data;
	data = (int*)user_data;
/*
	g_debug("msg %d written", (*data));
*/
	*data = 0;
}

static void soup_callback_send_sys(SoupSession *session, SoupMessage *msg, gpointer data)
{
	Node*node;
	node = (Node*)data;
	if ( msg->status_code == HTTP_OK && msg->response_body && msg->response_body->data && msg->response_body->length > 1 ) {
		node->buf = g_strdup(msg->response_body->data);
	}
}

void soup_callback_auth(SoupSession *session, SoupMessage *msg, SoupAuth *auth, gboolean retrying, gpointer user_data)
{
	SoupURI*uri = (SoupURI*)user_data;
	if ( retrying )
		return;
	soup_auth_authenticate(auth, (const char *)uri->user, (const char *)uri->password);
}

int rest_get_pam(Node*node)
{
	char*cmd;
	char *file;
	char* buf;
	GError *gerror = NULL;
	int i, j, n, r;
	char *pam_id;
	GString *command;
	NodeStatus node_status;
	SoupSession*session;
	SoupMessage *msg;
	guint status;
	pam_id = NULL;
	SoupURI* uri;
	GMainContext*soup_context;
	int default_delay, number_of_retries;
	uri = soup_uri_from_os(node->oServer);
	buf = g_strdup_printf("%s/PAM", node->oServer->address);
	soup_uri_set_path(uri, buf);
	g_free(buf);
	command = g_string_new((const gchar*)node->pam->name);
	command = g_string_append_c(command, '&');
	if ( node->nInConns > 0 ) {
		i = 0;
		g_mutex_lock (&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
		g_string_append_printf(command, "%s", node->inConn[i].channel[ node->inConn[i].index ]->file);
		g_mutex_unlock (&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
		for( i = 1; i < node->nInConns; i++ ) {
			g_mutex_lock (&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
			g_string_append_printf(command, ",%s", node->inConn[i].channel[ node->inConn[i].index ]->file);
			g_mutex_unlock (&(node->inConn[i].channel[ node->inConn[i].index ]->connMutex));
		}
	}
	command = g_string_append_c(command, '&');
	if ( node->id ) {
//		buf = soup_uri_encode(node->id, NULL);
		buf = g_uri_escape_string((const char *)node->id, NULL, TRUE);
		command = g_string_append(command, buf);
		g_free(buf);
	}
	soup_uri_set_query(uri, (const char *)command->str);
	g_string_free(command, TRUE);
/*fprintf(stderr, "2ut:%s\n2ut:%s\n", soup_uri_to_string(uri, FALSE), soup_uri_to_string(uri, TRUE));
fflush(stderr);*/
	session = soup_session_async_new();
	g_signal_connect(session, "authenticate", (GCallback)soup_callback_auth, uri);
	msg = soup_message_new_from_uri ("GET", uri);
//	soup_uri_free(uri);
	soup_context = soup_session_get_async_context(session);
//	soup_context = g_main_context_default();
	soup_session_queue_message (session, msg, (SoupSessionCallback)soup_callback_send, node);
//	g_main_context_wakeup(soup_context);
	default_delay = node->timeDelay;
	number_of_retries = 4;
	g_signal_connect(msg, "wrote_body", (GCallback)soup_msg_callback_wrote_body, &number_of_retries);
	n = (int)ceil(0.25*(double)default_delay);
	for (i = 0; i < n; i++) {
		g_mutex_lock (node->parent_mutex);
		r = *(node->parent_signal);
		g_mutex_unlock (node->parent_mutex);
		g_mutex_lock (&(node->statusMutex));
		node_status = node->status;
		g_mutex_unlock (&(node->statusMutex));
#ifndef G_OS_WIN32
		g_main_context_iteration(soup_context, FALSE);
#endif /* G_OS_WIN32*/
		if ( r != 0 || node_status != running || number_of_retries == 0 ) {
			break;
		}
		g_usleep(G_USEC_PER_SEC);
	}
	if ( number_of_retries != 0 ) {
		g_debug("Couldn't send %d", number_of_retries);
		soup_session_cancel_message(session, msg, SOUP_STATUS_CANCELLED);
	}
	n = default_delay;
	r = 0;
	node_status = running;
	for (i = 0; i < n; i++) {
		g_mutex_lock (node->parent_mutex);
		r = *(node->parent_signal);
		g_mutex_unlock (node->parent_mutex);
		g_mutex_lock (&(node->statusMutex));
		node_status = node->status;
		g_mutex_unlock (&(node->statusMutex));
#ifndef G_OS_WIN32
		g_main_context_iteration(soup_context, FALSE);
#endif /* G_OS_WIN32*/
		if ( r != 0 || node_status != running ) {
			break;
		}
		g_usleep(G_USEC_PER_SEC);
	}
	soup_uri_free(uri);
	if ( node_status == failed ) {
		soup_session_abort(session);
		return -1;
	} else if ( node_status == running ) {
		if ( r != 0 ) { /* terminated */
			soup_session_abort(session);
			NotifyNode(node, waiting);
			return -1;
		} else { /* timeout */
			soup_session_abort(session);
			NotifyNode(node, failed);
			return -1;
		}
	} else if ( node_status == completed ) {
		char**response_parts = g_strsplit(node->buf, " ", MAXTOKENS);
		if ( response_parts ) {
			int n_parts = g_strv_length(response_parts);
			if ( n_parts == 3 ) {
				uri = soup_uri_new((const char *)response_parts[1]);
				pam_id = response_parts[2];
				if ( node->terminal ) {
					node->uid = g_strdup_printf("%s", pam_id);
				} else {
					node->uid = g_strdup_printf("%s", pam_id);
					for( i = 0; i < node->nOutConns; i++ ) {
						file = node_get_out_conn_file(node, i, pam_id);
						for( j = 0; j < node->outConn[i].nConn; j++ ) {
							g_mutex_lock (&(node->outConn[i].channel[j]->connMutex));
							node->outConn[i].channel[j]->host = g_strdup(uri->host);
							node->outConn[i].channel[j]->file = g_strdup(file);
							node->outConn[i].channel[j]->proto = g_strdup(uri->scheme);
							node->outConn[i].channel[j]->address = g_strdup(uri->path);
							node->outConn[i].channel[j]->port = uri->port;
							node->outConn[i].channel[j]->flag = 1;
							g_mutex_unlock (&(node->outConn[i].channel[j]->connMutex));
						}
						g_free(file);
					}
				}
				soup_uri_free(uri);
			} else {
				if (isSetError) {
					buf = g_strdup_printf("Node: %d\nServer responded\n%s", node->ID, node->buf);
					error(buf);
					g_free(buf);
				}
				soup_session_abort(session);
				g_free(node->buf);
				node->buf = NULL;
				return -1;
			}
			g_strfreev(response_parts);
		} else {
			if (isSetError) {
				buf = g_strdup_printf("Node: %d\nServer responded\n%s", node->ID, node->buf);
				error(buf);
				g_free(buf);
			}
			soup_session_abort(session);
			g_free(node->buf);
			node->buf = NULL;
			return -1;
		}
		soup_session_abort(session);
		g_free(node->buf);
		node->buf = NULL;
		return 0;
	}
	return 1;
}

int rest_get_sys(Node*node)
{
	char*cmd;
	char *file;
	char* buf;
	GError *gerror = NULL;
	int i, j;
	char *pam_id;
	GString *command;
	NodeStatus node_status;
	SoupSession*session;
	SoupMessage *msg;
	guint status;
	pam_id = NULL;
	SoupURI* uri;
	uri = soup_uri_from_os(node->oServer);
	buf = g_strdup_printf("%s/SYS", node->oServer->address);
	soup_uri_set_path(uri, buf);
	g_free(buf);
	command = g_string_new(node->uid);
	g_string_append_printf(command,"&%s", node->label);
	g_string_append_printf(command,"&%s", node->file);
	if ( node->id ) {
		g_string_append_printf(command,"&%s", node->id);
	}
	soup_uri_set_query(uri, command->str);
//fprintf(stderr, "2ut:%s\n2ut:%s\n", soup_uri_to_string(uri, FALSE), soup_uri_to_string(uri, TRUE));
//fflush(stderr);
	session = soup_session_sync_new();
//	g_object_set(G_OBJECT(session), SOUP_SESSION_TIMEOUT, 30, NULL);
//	g_object_set(G_OBJECT(session), SOUP_SESSION_IDLE_TIMEOUT, 30, NULL);
	g_signal_connect(session, "authenticate", (GCallback)soup_callback_auth, uri);
	msg = soup_message_new_from_uri ("GET", uri);
	soup_uri_free(uri);
	i = soup_session_send_message (session, msg);
	if ( i == HTTP_OK && msg->response_body && msg->response_body->data && msg->response_body->length > 1 ) {
		node->buf = g_strdup(msg->response_body->data);
	}
	soup_session_abort(session);
	g_object_unref(msg);
	if ( !node->buf ) {
/*		g_debug(command->str);*/
		g_string_free(command, TRUE);
		return -1;
	} else {
		g_string_free(command, TRUE);
		return 0;
	}
	return 1;
}

int rest_get_file(Node*node)
{
	int k, j;
	for( k = 0; k < node->nOutConns; k++ ) {
		for( j = 0; j < node->outConn[k].nConn; j++ ) {
			g_mutex_lock (&(node->outConn[k].channel[j]->connMutex));
			node->outConn[k].channel[j]->host = g_strdup(node->oServer->hname);
			node->outConn[k].channel[j]->port = node->oServer->port;
			if ( g_str_has_prefix((const gchar*)node->file, "omero://") ) {
				node->outConn[k].channel[j]->file = g_uri_escape_string((const char *)node->file, NULL, TRUE);
			} else {
				node->outConn[k].channel[j]->file = g_path_get_basename(node->file);
			}
			node->outConn[k].channel[j]->proto = g_strdup(node->oServer->proto);
			node->outConn[k].channel[j]->address = g_strdup(node->oServer->address);
			node->outConn[k].channel[j]->flag = 1;
			g_mutex_unlock (&(node->outConn[k].channel[j]->connMutex));
		}
	}
	NotifyNode(node, completed);
	return 0;
}

int rest_put_sys(Node*node)
{
	char*cmd;
	char *file;
	char* buf;
	GError *gerror = NULL;
	int i, j;
	char *pam_id;
	GString *command;
	NodeStatus node_status;
	SoupSession*session;
	SoupMessage *msg;
	guint status;
	pam_id = NULL;
	SoupURI* uri;
	uri = soup_uri_from_os(node->oServer);
	buf = g_strdup_printf("%s/SYS", node->oServer->address);
	soup_uri_set_path(uri, buf);
	g_free(buf);
	command = g_string_new(node->uid);
	g_string_append_printf(command,"&%s", node->label);
	g_string_append_printf(command,"&%s", node->file);
	if ( node->id ) {
		g_string_append_printf(command,"&%s", node->id);
	}
	soup_uri_set_query(uri, command->str);
	session = soup_session_sync_new();
	g_signal_connect(session, "authenticate", (GCallback)soup_callback_auth, uri);
	msg = soup_message_new_from_uri ("PUT", uri);
	soup_uri_free(uri);
	if ( node->buf != NULL ) {
		soup_message_set_request ( msg, "text/html", SOUP_MEMORY_COPY, (const char *)node->buf, (gsize) strlen(node->buf));
		g_free(node->buf);
		node->buf = NULL;
	}
	i = soup_session_send_message (session, msg);
	if ( i == HTTP_OK && msg->response_body && msg->response_body->data && msg->response_body->length > 1 ) {
		node->buf = g_strdup(msg->response_body->data);
	}
	soup_session_abort(session);
	g_object_unref(msg);
	if ( !node->buf ) {
		g_debug(command->str);
		g_string_free(command, TRUE);
		return -1;
	} else {
		g_string_free(command, TRUE);
		return 0;
	}
	return 1;
}

int rest_delete_pam(Node*node)
{
	return -1;
}

int rest_delete_sys(Node*node)
{
	char*cmd;
	char *file;
	char* buf;
	GError *gerror = NULL;
	int i, j;
	char *pam_id;
	GString *command;
	NodeStatus node_status;
	SoupSession*session;
	SoupMessage *msg;
	guint status;
	pam_id = NULL;
	SoupURI* uri;
	uri = soup_uri_from_os(node->oServer);
	buf = g_strdup_printf("%s/SYS", node->oServer->address);
	soup_uri_set_path(uri, buf);
	g_free(buf);
	command = g_string_new(node->uid);
	g_string_append_printf(command,"&%s", node->label);
	if ( node->file )
		g_string_append_printf(command,"&%s", node->file);
	if ( node->id )
		g_string_append_printf(command,"&%s", node->id);
	soup_uri_set_query(uri, command->str);
	session = soup_session_sync_new();
	g_signal_connect(session, "authenticate", (GCallback)soup_callback_auth, uri);
	msg = soup_message_new_from_uri ("DELETE", uri);
	soup_uri_free(uri);
	i = soup_session_send_message (session, msg);
	if ( i == HTTP_OK && msg->response_body && msg->response_body->data && msg->response_body->length > 1 ) {
		node->buf = g_strdup(msg->response_body->data);
	}
	soup_session_abort(session);
	g_object_unref(msg);
	if ( !node->buf ) {
		g_debug(command->str);
		g_string_free(command, TRUE);
		return -1;
	} else {
		g_string_free(command, TRUE);
		return 0;
	}
	return 1;
}

int rest_get_ins(Node*node)
{
	char*cmd;
	char *file;
	char* buf;
	GError *gerror = NULL;
	int i, j, n, r;
	char *pam_id;
	GString *command;
	NodeStatus node_status;
	SoupSession*session;
	SoupMessage *msg;
	guint status;
	pam_id = NULL;
	SoupURI* uri;
	uri = soup_uri_from_os(node->oServer);
	buf = g_strdup_printf("%s/INS", node->oServer->address);
	soup_uri_set_path(uri, buf);
	g_free(buf);
	command = g_string_new((const gchar*)node->file);
	soup_uri_set_query(uri, (const char *)command->str);
	g_string_free(command, TRUE);
//fprintf(stderr, "2ut:%s\n2ut:%s\n", soup_uri_to_string(uri, FALSE), soup_uri_to_string(uri, TRUE));
//fflush(stderr);
	session = soup_session_async_new();
	g_signal_connect(session, "authenticate", (GCallback)soup_callback_auth, uri);
	msg = soup_message_new_from_uri ("GET", uri);
	soup_uri_free(uri);
	soup_session_queue_message (session, msg, (SoupSessionCallback)soup_callback_send, node);
	n = node->timeDelay;
	r = 0;
	node_status = running;
	for (i = 0; i < n; i++) {
		g_mutex_lock (node->parent_mutex);
		r = *(node->parent_signal);
		g_mutex_unlock (node->parent_mutex);
		g_mutex_lock (&(node->statusMutex));
		node_status = node->status;
		g_mutex_unlock (&(node->statusMutex));
		kimono_main_context_iteration();
		if ( r != 0 || node_status != running ) {
			break;
		}
		g_usleep(G_USEC_PER_SEC);
	}
	if ( node_status == failed ) {
		soup_session_abort(session);
		return -1;
	} else if ( node_status == running ) {
		if ( r != 0 ) { /* terminated */
			soup_session_abort(session);
			NotifyNode(node, waiting);
			return -1;
		} else { /* timeout */
			soup_session_abort(session);
			NotifyNode(node, failed);
			if (isSetError) {
				buf = g_strdup_printf("Node: %d\nServer timeout", node->ID);
				error(buf);
				g_free(buf);
			}
			return -1;
		}
	} else if ( node_status == completed ) {
		char**response_parts = g_strsplit(node->buf, " ", MAXTOKENS);
		if ( response_parts ) {
			int n_parts = g_strv_length(response_parts);
			if ( n_parts == 3 ) {
				uri = soup_uri_new((const char *)response_parts[1]);
				pam_id = response_parts[2];
				if ( node->terminal ) {
					node->uid = g_strdup_printf("%s", pam_id);
				} else {
					node->uid = g_strdup_printf("%s", pam_id);
					i = 0;
					buf = type2string(node->outConn[i].type);
					file = g_strdup_printf("%s.out.%d%s", pam_id, i, buf);
					g_free(buf);
					for( j = 0; j < node->outConn[i].nConn; j++ ) {
						g_mutex_lock (&(node->outConn[i].channel[j]->connMutex));
						node->outConn[i].channel[j]->host = g_strdup(uri->host);
						node->outConn[i].channel[j]->file = g_strdup(file);
						node->outConn[i].channel[j]->proto = g_strdup(uri->scheme);
						node->outConn[i].channel[j]->address = g_strdup(uri->path);
						node->outConn[i].channel[j]->port = uri->port;
						node->outConn[i].channel[j]->flag = 1;
						g_mutex_unlock (&(node->outConn[i].channel[j]->connMutex));
					}
					g_free(file);
				}
				soup_uri_free(uri);
			} else {
				if (isSetError) {
					buf = g_strdup_printf("Node: %d\nServer responded\n%s", node->ID, node->buf);
					error(buf);
					g_free(buf);
				}
				soup_session_abort(session);
				g_free(node->buf);
				node->buf = NULL;
				return -1;
			}
			g_strfreev(response_parts);
		} else {
			if (isSetError) {
				buf = g_strdup_printf("Node: %d\nServer responded\n%s", node->ID, node->buf);
				error(buf);
				g_free(buf);
			}
			soup_session_abort(session);
			g_free(node->buf);
			node->buf = NULL;
			return -1;
		}
		soup_session_abort(session);
		g_free(node->buf);
		node->buf = NULL;
		return 0;
	}
	return 1;
}

int rest_get_ous(Node*node)
{
	char*cmd;
	char *file;
	char* buf;
	GError *gerror = NULL;
	int i, j, n, r;
	char *pam_id;
	GString *command;
	NodeStatus node_status;
	SoupSession*session;
	SoupMessage *msg;
	guint status;
	pam_id = NULL;
	SoupURI* uri;
	uri = soup_uri_from_os(node->oServer);
	buf = g_strdup_printf("%s/OUS", node->oServer->address);
	soup_uri_set_path(uri, buf);
	g_free(buf);
	command = g_string_new((const gchar*)node->file);
	g_mutex_lock (&(node->inConn[0].channel[ node->inConn[0].index ]->connMutex));
	g_string_append_printf(command, "&%s", node->inConn[0].channel[node->inConn[0].index]->file);
	g_mutex_unlock (&(node->inConn[0].channel[ node->inConn[0].index ]->connMutex));
	soup_uri_set_query(uri, (const char *)command->str);
	g_string_free(command, TRUE);
//fprintf(stderr, "2ut:%s\n2ut:%s\n", soup_uri_to_string(uri, FALSE), soup_uri_to_string(uri, TRUE));
//fflush(stderr);
	session = soup_session_async_new();
	g_signal_connect(session, "authenticate", (GCallback)soup_callback_auth, uri);
	msg = soup_message_new_from_uri ("GET", uri);
	soup_uri_free(uri);
	soup_session_queue_message (session, msg, (SoupSessionCallback)soup_callback_send, node);
	n = node->timeDelay;
	r = 0;
	node_status = running;
	for (i = 0; i < n; i++) {
		g_mutex_lock (node->parent_mutex);
		r = *(node->parent_signal);
		g_mutex_unlock (node->parent_mutex);
		g_mutex_lock (&(node->statusMutex));
		node_status = node->status;
		g_mutex_unlock (&(node->statusMutex));
		kimono_main_context_iteration();
		if ( r != 0 || node_status != running ) {
			break;
		}
		g_usleep(G_USEC_PER_SEC);
	}
	if ( node_status == failed ) {
		soup_session_abort(session);
		return -1;
	} else if ( node_status == running ) {
		if ( r != 0 ) { /* terminated */
			soup_session_abort(session);
			NotifyNode(node, waiting);
			return -1;
		} else { /* timeout */
			soup_session_abort(session);
			NotifyNode(node, failed);
			if (isSetError) {
				buf = g_strdup_printf("Node: %d\nServer timeout", node->ID);
				error(buf);
				g_free(buf);
			}
			return -1;
		}
	} else if ( node_status == completed ) {
		char**response_parts = g_strsplit(node->buf, " ", MAXTOKENS);
		if ( response_parts ) {
			int n_parts = g_strv_length(response_parts);
			if ( n_parts == 3 ) {
				uri = soup_uri_new((const char *)response_parts[1]);
				pam_id = response_parts[2];
				if ( node->terminal ) {
					node->uid = g_strdup_printf("%s", pam_id);
				} else {
					node->uid = g_strdup_printf("%s", pam_id);
					i = 0;
					buf = type2string(node->outConn[i].type);
					file = g_strdup_printf("%s.out.%d%s", pam_id, i, buf);
					g_free(buf);
					for( j = 0; j < node->outConn[i].nConn; j++ ) {
						g_mutex_lock (&(node->outConn[i].channel[j]->connMutex));
						node->outConn[i].channel[j]->host = g_strdup(uri->host);
						node->outConn[i].channel[j]->file = g_strdup(file);
						node->outConn[i].channel[j]->proto = g_strdup(uri->scheme);
						node->outConn[i].channel[j]->address = g_strdup(uri->path);
						node->outConn[i].channel[j]->port = uri->port;
						node->outConn[i].channel[j]->flag = 1;
						g_mutex_unlock (&(node->outConn[i].channel[j]->connMutex));
					}
					g_free(file);
				}
				soup_uri_free(uri);
			} else {
				if (isSetError) {
					buf = g_strdup_printf("Node: %d\nServer responded\n%s", node->ID, node->buf);
					error(buf);
					g_free(buf);
				}
				soup_session_abort(session);
				g_free(node->buf);
				node->buf = NULL;
				return -1;
			}
			g_strfreev(response_parts);
		} else {
			if (isSetError) {
				buf = g_strdup_printf("Node: %d\nServer responded\n%s", node->ID, node->buf);
				error(buf);
				g_free(buf);
			}
			soup_session_abort(session);
			g_free(node->buf);
			node->buf = NULL;
			return -1;
		}
		soup_session_abort(session);
		g_free(node->buf);
		node->buf = NULL;
		return 0;
	}
	return 1;
}

int rests_get(Node*node)
{
	int res;
	res = -1;
	switch( node->type ) {
		case pam:
			res = rest_get_pam(node);
		break;
		case ins:
			res = rest_get_ins(node);
		break;
		case ous:
			res = rest_get_ous(node);
		break;
		case file:
			res = rest_get_file(node);
		break;
		case display:
//			res = door_get_display(node);
		break;
		case sys:
			res = rest_get_sys(node);
		break;
	}
	return res;
}

SoupURI*soup_uri_from_os(OlapServer*os)
{
	SoupURI*uri;
	uri = soup_uri_new(NULL);
	if ( !strcmp(os->proto, "rests") ) {
		soup_uri_set_scheme(uri, "https");
		soup_uri_set_user(uri, os->login);
		soup_uri_set_password(uri, os->password);
	} else {
		soup_uri_set_scheme(uri, "http");
	}
	soup_uri_set_host(uri, os->hname);
	if ( os->port > 0 )
		soup_uri_set_port(uri, os->port);
	return uri;
}

char*node_pam_get_version(Node*ne)
{
	int res;
	Node*sproxy;
	char*version;
	sproxy = proxy_sys_node("pam");
	sproxy->label = strcpy(sproxy->label, ne->pam->name);
	sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
	sproxy->oServer = ne->oServer;
	sproxy->file = strcpy(sproxy->file, "implementor");
	res = door_call("GET", sproxy);
	if ( res != 0 ) {
		g_error("Can't get Impelemntor for %s", ne->pam->name);
	}
	sproxy->label = strcpy(sproxy->label, sproxy->buf);
	sproxy->file = strcpy(sproxy->file, "version");
	free(sproxy->buf);
	sproxy->buf = NULL;
	proxy_sys_node_set_mode(sproxy, "packages");
	res = door_call("GET", sproxy);
	if ( res != 0 ) {
		g_error("Can't get version for %s:%s", sproxy->label, ne->pam->name);
	}
	version = g_strdup(sproxy->buf);
	free(sproxy->buf);
	proxy_sys_node_del(sproxy);
	return version;
}

char*node_pam_get_compat_string(Node*ne)
{
	int res;
	Node*sproxy;
	char*compat_string;
	sproxy = proxy_sys_node("compat");
	sproxy->label = strcpy(sproxy->label, ne->pam->name);
	sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
	sproxy->oServer = ne->oServer;
	sproxy->file = strcpy(sproxy->file, "API");
	sproxy->id = (char*)realloc(sproxy->id, MAX_RECORD * sizeof(char) );
	sproxy->id = strcpy(sproxy->id, ne->version);
	res = door_call("GET", sproxy);
	if ( res != 0 ) {
		g_debug("Can't get API for %s", ne->pam->name);
		compat_string = NULL;
	} else {
		compat_string = g_strdup(sproxy->buf);
	}
	if ( sproxy->buf )
		free(sproxy->buf);
	proxy_sys_node_del(sproxy);
	return compat_string;
}

void node_set_defaults(Node*node)
{
	int i, j, need_undo;
	int length;
	char*pattern;
	char*sample;
	UiDef *ud;
	ud = getUiDef(node);
	if ( ud ) {
		if ( node->id )
			free( node->id );
		node->id = (char*)calloc(MAX_RECORD, sizeof(char));
		node->id = strcpy(node->id, ud->format);
		node->id = (char*)realloc(node->id, MAX_RECORD * sizeof(char));
		pattern = (char*)calloc(MAX_RECORD, sizeof(char));
		for( i = 0; i < ud->length; i++ ) {
			sprintf(pattern, "$%d", i + 1);
			sample = ud->opts[i].value[0];
			instrsubst(node->id, pattern, sample);
		}
		free(pattern);
		deleteUiDef(ud);
	}
	node->version = node_pam_get_version(node);
}

void node_set_id_from_string(Node*node, int in_id, char*in_str)
{
	int i;
	char*pattern;
	char*sample, **opts;
	UiDef *ud;
	if ( node->type == pam ) {
		ud = getUiDef(node);
		if ( ud ) {
			opts = (char**)calloc(ud->length + 1, sizeof(char*));
			opts[ud->length] = (char*)NULL;
			pattern = (char*)calloc(MAX_RECORD, sizeof(char));
			for( i = 0; i < ud->length; i++ ) {
				sprintf(pattern, "$%d", i + 1);
				sample = outstrsubst(node->id, ud->format, pattern);
				if ( !sample )
					opts[i] = g_strdup(ud->opts[i].value[0]);
				else
					opts[i] = sample;
			}
			if ( node->id )
				free( node->id );
			node->id = (char*)calloc(MAX_RECORD, sizeof(char));
			node->id = strcpy(node->id, ud->format);
			node->id = (char*)realloc(node->id, MAX_RECORD * sizeof(char));
			for( i = 0; i < ud->length; i++ ) {
				sprintf(pattern, "$%d", i + 1);
				if ( i == in_id ) {
					instrsubst(node->id, pattern, in_str);
				} else {
					instrsubst(node->id, pattern, opts[i]);
				}
			}
			free(pattern);
			g_strfreev(opts);
			deleteUiDef(ud);
		}
	} else if ( node->type == ins ) {
		if ( node->file )
			free( node->file );
		node->file = g_strdup(in_str);
	}
}

void node_reset_id(Node*node)
{
	int i, j;
	int length;
	char*pattern;
	char*sample, **opts;
	char*compat_string;
	UiDef *ud;
	ud = getUiDef(node);
	if ( ud ) {
		compat_string = node_pam_get_compat_string(node);
		if ( compat_string ) {
			opts = (char**)calloc(ud->length + 1, sizeof(char*));
			opts[ud->length] = (char*)NULL;
			pattern = (char*)calloc(MAX_RECORD, sizeof(char));
			for( i = 0; i < ud->length; i++ ) {
				sprintf(pattern, "$%d", i + 1);
				sample = outstrsubst(node->id, compat_string, pattern);
				if ( !sample )
					opts[i] = g_strdup(ud->opts[i].value[0]);
				else
					opts[i] = sample;
			}
			if ( node->id )
				free( node->id );
			node->id = (char*)calloc(MAX_RECORD, sizeof(char));
			node->id = strcpy(node->id, ud->format);
			node->id = (char*)realloc(node->id, MAX_RECORD * sizeof(char));
			for( i = 0; i < ud->length; i++ ) {
				sprintf(pattern, "$%d", i + 1);
				instrsubst(node->id, pattern, opts[i]);
			}
			free(pattern);
			free(compat_string);
			g_strfreev(opts);
		}
		deleteUiDef(ud);
	}
/*	node->version = node_pam_get_version(node);*/
}

void node_check_version(Node*ne)
{
	char*p_version;
	p_version = node_pam_get_version(ne);
	if ( kimono_db_pak_version_newer(p_version, ne->version) > 0 ) {
		node_reset_id(ne);
		g_free(ne->version);
		ne->version = p_version;
	}
}
