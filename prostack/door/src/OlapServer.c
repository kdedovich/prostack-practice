/***************************************************************************
 *            OlapServer.c
 *
 *  Tue Feb 14 15:06:52 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>
#include <OlapServer.h>
#include <sys/time.h>
#include <math.h>

#include <libsoup/soup.h>

#define BUFFER_SIZE 1024
#ifndef MAX_RECORD
#define MAX_RECORD 256
#endif

static void (*logger)(char*msg);
static void (*error)(char*msg);
static short isSetLogger = 0;
static short isSetError = 0;

void setErrorOlapServer(void (*ptr)(char*msg))
{
	isSetError = 1;
	error = ptr;
}

void setLoggerOlapServer(void (*ptr)(char*msg))
{
	isSetLogger = 1;
	logger = ptr;
}

void deleteOlapServer(OlapServer*oServer)
{
	if (oServer->nicname) free(oServer->nicname);
	if (oServer->address) free(oServer->address);
	if (oServer->proto) free(oServer->proto);
	if (oServer->login) free(oServer->login);
	if (oServer->password) free(oServer->password);
	free(oServer->hname);
	free(oServer);
}

OlapServer*dupOlapServer(OlapServer*o)
{
	OlapServer*os;
	os = newOlapServerV();
	os->hname = strcpy(os->hname, o->hname);
	os->port = o->port;
	os->ID = o->ID;
	os->nicname = g_strdup(o->nicname);
	os->proto = g_strdup(o->proto);
	os->address = g_strdup(o->address);
	os->login = g_strdup(o->login);
	os->password = g_strdup(o->password);
	os->is_default = o->is_default;
	return os;
}

OlapServer*newOlapServer(char*host, int port, int ID)
{
	OlapServer*os;
	os = newOlapServerV();
	os->hname = strcpy(os->hname, host);
	os->port = port;
	os->ID = ID;
	return os;
}


OlapServer*newOlapServerV()
{
	OlapServer*os;
	os = (OlapServer*)malloc(sizeof(OlapServer));
	os->hname = (char*)calloc(MAX_RECORD, sizeof(char));
	os->port = -1;
	os->ID = -1;
	os->is_default = 0;
	os->nicname = NULL;
	os->address = NULL;
	os->login = NULL;
	os->password = NULL;
	os->proto = NULL;
	return os;
}

OlapServer*olapserverParse(GKeyFile*gkf, char*grp, int ID)
{
	GError*gerror = NULL;
	int i, j, port;
	OlapServer*os;
	char*str;
	char*buf;
	os = NULL;
	str = g_key_file_get_string(gkf, (const gchar *)grp, "host", &gerror);
	if ( gerror ) {
		g_debug("olap_server_parse error:%s", gerror->message);
		g_error_free(gerror);
		return os;
	}
	port = g_key_file_get_integer(gkf, (const gchar *)grp, "port", &gerror);
	if ( gerror ) {
		g_debug("olap_server_parse error:%s", gerror->message);
		g_error_free(gerror);
		return os;
	}
	os = newOlapServer(str, port, ID);
	g_free(str);
	str = g_key_file_get_string(gkf, (const gchar *)grp, "name", &gerror);
	if ( gerror ) {
		g_debug("olap_server_parse error:%s", gerror->message);
		g_error_free(gerror);
		return os;
	}
	os->nicname = str;
	str = g_key_file_get_string(gkf, (const gchar *)grp, "address", &gerror);
	if ( gerror ) {
		g_debug("olap_server_parse error:%s", gerror->message);
		g_error_free(gerror);
		return os;
	}
	os->address = str;
	str = g_key_file_get_string(gkf, (const gchar *)grp, "proto", &gerror);
	if ( gerror ) {
		g_debug("olap_server_parse error:%s", gerror->message);
		g_error_free(gerror);
		return os;
	}
	os->proto = str;
	str = g_key_file_get_string(gkf, (const gchar *)grp, "login", &gerror);
	if ( gerror ) {
		g_debug("olap_server_parse error:%s", gerror->message);
		g_error_free(gerror);
		gerror = NULL;
	}
	os->login = str;
	str = g_key_file_get_string(gkf, (const gchar *)grp, "password", &gerror);
	if ( gerror ) {
		g_debug("olap_server_parse error:%s", gerror->message);
		g_error_free(gerror);
		gerror = NULL;
	}
	os->password = str;
	return os;
}

void olapserverPrint(OlapServer*os, GKeyFile*gkf)
{
	GError*gerror = NULL;
	char*grp;
	if ( os->is_default == 1 ) {
		grp = g_strdup("Default");
	} else {
		grp = g_strdup(os->nicname);
	}
	g_key_file_set_value(gkf, grp, "name", os->nicname);
	g_key_file_set_value(gkf, grp, "host", os->hname);
	g_key_file_set_integer(gkf, grp, "port", os->port);
	g_key_file_set_value(gkf, grp, "address", os->address);
	g_key_file_set_value(gkf, grp, "proto", os->proto);
	if (os->login) {
		g_key_file_set_value(gkf, grp, "login", os->login);
	} else {
		g_key_file_set_value(gkf, grp, "login", "");
	}
	if (os->password) {
		g_key_file_set_value(gkf, grp, "password", os->password);
	} else {
		g_key_file_set_value(gkf, grp, "password", "");
	}
	g_free(grp);
}

OlapServer*olapserver_from_string(char*str)
{
	OlapServer*os;
	SoupURI *soup_uri;
	if ( !str ) {
		return NULL;
	}
	soup_uri = soup_uri_new ( (const char *)str);
	if ( !soup_uri ) {
		return NULL;
	}
	os = newOlapServerV();
	free(os->hname);
	os->hname = g_strdup(soup_uri->host);
	os->port = (int)soup_uri->port;
	os->nicname = g_strdup("default");
	if ( g_str_has_prefix( (const gchar *)str, "door" ) ) {
		os->proto = g_strdup("door");
	} else if ( g_str_has_prefix( (const gchar *)str, "rest" ) ) {
		os->proto = g_strdup("rest");
	} else if ( g_str_has_prefix( (const gchar *)str, "rests" ) ) {
		os->proto = g_strdup("rests");
	} else {
		g_error("can't parse %s\n", str);
	}
	os->address = g_strdup(soup_uri->path);
	os->is_default = 1;
	os->ID = 0;
	if ( soup_uri->user ) {
		os->login = g_strdup(soup_uri->user);
	}
	if ( soup_uri->password ) {
		os->password = g_strdup(soup_uri->password);
	}
	soup_uri_free(soup_uri);
	return os;
}
