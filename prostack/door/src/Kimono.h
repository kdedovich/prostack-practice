/***************************************************************************
 *            Kimono.h
 *
 *  Wed Feb 20 11:39:06 2008
 *  Copyright  2008  kozlov
 *  <kozlov@freya>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef _KIMONO_H
#define _KIMONO_H

#include <sqlite3.h>
#include <glib.h>

#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif

G_BEGIN_DECLS

char*kimono_get_pam_exec(char*pam_name, int node_id, int*pam_type, char**pam_id, int use_metaname, char**pam_metaname);
char*kimono_get_pam_info(char*pam_name, char*pam_info_type);
void kimono_free_resource(char*);
void kimono_init(int*argc, char***argv, GError **err);
void kimono_shutdown();
char*kimono_get_concepts(char*label);
char*kimono_get_methods(char*label);
char*sqlite3_table_to_string(char**sq_result, int sq_rows, int sq_columns);
char*kimono_get_search(char*search, char*concept, char*implementor);
char* kimono_put_method(char*mname, char* spath, char*uri, char*contents);
void kimono_main_context_wakeup();
GSource* kimono_child_watch_add(GPid pid, GSourceFunc func, gpointer data);
/*guint kimono_child_watch_add(GPid pid, GChildWatchFunc func, Node*data);*/
gboolean kimono_launch_default_for_uri(const char *uri, GError**gerror);
char*kimono_get_screens();
void kimono_put_screen(char*cmd);
void kimono_delete_screen(char*dname, char*dext);
char* kimono_delete_method(char*mname);
void kimono_update_screen_default(char*dname, char*dext, gboolean setting);
char*kimono_strcheck(char*str, char*needle, char*replacement);
char*door_get_config_path(char*hname, int port, char*proto);
char*door_init_config_path();
char*door_get_config_string(char*hname, int port, char*proto);
int door_init_config_port();
int door_get_config_port();
char*door_get_config_home();
char*door_get_db_name();
char*door_get_user();
char*kimono_get_ins_exec(char*ins_name, int node_id, int*ins_type, char**ins_id);
gint kim_get_thread_pool_size();
gchar*subst_data_dir(gchar*ORIGIN);

char*kimono_get_wksps(char*label);
char*kimono_get_wksptypes(char*label);
char*kimono_get_wksp_info(char*pam_name, char*pam_info_type);
char* kimono_put_wksps(char*mname, char* spath, char*uri, char*buf);
char* kimono_delete_wkspjobs(char*mname, char*cmd);
char* kimono_put_wkspsjobs(char*mname, char*files, char*msg, char*buf);
char*kimono_get_wkspjobs(char*label, char*id, char*file);
char*kimono_get_data_info(char*data_name, char*data_info_type);
char*kimono_get_wksp_info(char*wksp_name, char*wksp_info_type);
char*kimono_update_wkspjobs_nodes(char*job_name, int node_id, char*status);
char*kimono_update_wkspjobs_status(char*job_name, char*value);
gboolean kimono_main_context_iteration();
char*kimono_get_datas(char*label);
char*kimono_get_datatypes(char*label);
char* kimono_put_datas(char*mname, char*spath, char*uri);
char* kimono_delete_datas(char*mname);
char* kimono_delete_wksps(char*mname);
int door_get_config_port();
char*kimono_db_get_pak_version(char*pak);
int kimono_db_pak_version_newer(char*p_version, char*version);
char*kimono_get_package_info(char*pak_name, char*pak_info_type);
char*kimono_get_compat(char*name, char*version, char*column);
int kimono_run_wkspjob(char*name, char*user, char*inputs, char*outputs, char*wksplibtag);
void kimono_dump_command(char*command, char*mname, int delete);
int kimono_restore_command(GError**err);
double kimono_get_monotonic_time ();
double kimono_get_real_time ();
double kimono_get_time_of_day ();
void kimono_context_run();
void kimono_context_stop();

G_END_DECLS

#endif /* _KIMONO_H */


