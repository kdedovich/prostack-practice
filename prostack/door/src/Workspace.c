/***************************************************************************
 *            Workspace.c
 *
 *  Thu Feb  9 16:02:50 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <math.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <ListOfNodes.h>
#include <Node.h>
#include <Workspace.h>
#include <Kimono.h>
#define MAX_TOKENS 256
#include <gio/gio.h>

#define KIMONO_WORKSPACE_ERROR g_spawn_kimono_workspace_error_quark ()
GQuark g_spawn_kimono_workspace_error_quark (void)
{
	return g_quark_from_static_string ("kimono-workspace-error-quark");
}

typedef enum
{
	KIMONO_WORKSPACE_ERROR_METHODS, /* Couldn't install new method */
	KIMONO_WORKSPACE_ERROR_COMPILATION /* Couldn't install new method */
} KimonoWorkspaceError;


static void (*error)(char*msg);
static void (*tracer)(Node*ne);
static short isSetTracer = 0;
static short isSetError = 0;

void setError(void (*ptr)(char*msg))
{
	isSetError = 1;
	error = ptr;
}

void setTracer(void (*ptr)(Node*ne))
{
	isSetTracer = 1;
	tracer = ptr;
}

Workspace*workspaceEmpty()
{
	Workspace*ws;
	ws = workspaceCreate();
	ws->numberOfNodes = 0;
	ws->numberOfMacro = 0;
	ws->listOfNodes = (ListOfNodes*)NULL;
	return ws;
}

Workspace*workspace_from_file(const char*filename)
{
	Workspace*ws;
	GKeyFile*gkf;
	GError*gerror = NULL;
	GFile*gfile;
	char*contents, *etag_out;
	int length;
	gsize size;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
	len = -1;
	converted = g_locale_to_utf8((const gchar *)filename, len, &bytes_read, &bytes_written, &gerror);
	if ( gerror )
		g_error("Can't convert scenario name: %s with %s\n at Workspace.c:87", filename, gerror->message);
/*	gfile = g_file_new_for_commandline_arg((const char*)(g_path_skip_root(converted)));*/
	gfile = g_file_new_for_commandline_arg((const char*)converted);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, &gerror) ) {
		if ( gerror ) {
			g_error("workspace_from_file: can't load %s\n with %s\n at Workspace.c:92", converted, gerror->message);
		}
		g_error("workspace_from_file: unknown error");
	}
	g_free(converted);
#else
	gfile = g_file_new_for_commandline_arg(filename);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, &gerror) ) {
		if ( gerror ) {
			g_error("workspace_from_file: can't load %s\n with %s\n at Workspace.c:101", filename, gerror->message);
		}
		g_error("workspace_from_file: unknown error");
	}
#endif
	length = (int)size;
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)contents, length, G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, &gerror ) ) {
		g_debug("workspace_from_file:%s", gerror->message);
		g_error_free(gerror);
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
		ws = workspaceEmpty();
	} else {
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
		ws = workspaceParse(gkf);
	}
	g_key_file_free(gkf);
	return ws;
}

Workspace*workspaceCreate()
{
	Workspace*ws;
	ws = (Workspace*)malloc(sizeof(Workspace));
	ws->author = (char*)calloc(MAX_RECORD, sizeof(char));
	ws->name = (char*)calloc(MAX_RECORD, sizeof(char));
	ws->id = (char*)calloc(MAX_RECORD, sizeof(char));
	ws->comment = NULL;
	ws->listOfNodes = (ListOfNodes*)NULL;
	ws->cur_ID = 0;
	ws->cur_macro_ID = 1;
	g_mutex_init(&(ws->stopMutex));
	g_mutex_init(&(ws->integrMutex));
	g_mutex_init(&(ws->statusMutex));
	ws->clean = 0;
	ws->status = WS_WAITING;
	ws->comp_inputs = NULL;
	ws->comp_outputs = NULL;
	ws->comp_ostring = NULL;
	ws->comp_uidescr = NULL;
	return ws;
}

void workspaceDelete(Workspace*ws)
{
	free(ws->author);
	free(ws->name);
	free(ws->id);
	g_mutex_clear(&(ws->stopMutex));
	g_mutex_clear(&(ws->integrMutex));
	g_mutex_clear(&(ws->statusMutex));
	if ( ws->comp_inputs )
		g_free(ws->comp_inputs);
	if ( ws->comp_outputs )
		g_free(ws->comp_outputs);
	if ( ws->comp_uidescr )
		g_free(ws->comp_uidescr);
	if ( ws->comp_ostring )
		g_free(ws->comp_ostring);
	free(ws);
}

void workspaceDeleteStruct(Workspace*ws)
{
	deleteListOfNodes(ws->listOfNodes);
	free(ws->author);
	free(ws->name);
	free(ws->id);
	free(ws);
}

void g_thread_pool_func(gpointer data, gpointer user_data)
{
	Node *node;
	node = (Node*)data;
	door_call("GET", node);
}

gpointer wksp_run_func(gpointer data)
{
	Workspace*ws = (Workspace*)data;
	int n;
	int j;
	int i;
	int h;
	int has_waiting;
	int has_running;
	short flag;
	gint thread_pool_size;
	ListOfNodes *curr;
	GThreadPool*thread_node_pool;
	GError*gerror = NULL;
	WorkspaceStatus wstatus;
	NodeStatus node_status;
	n = 0;
	flag = 0;
	thread_pool_size = kim_get_thread_pool_size();
	thread_node_pool = g_thread_pool_new(g_thread_pool_func, NULL, thread_pool_size, FALSE, &gerror);
	if ( gerror ) {
		if (isSetError)
			error(gerror->message);
		g_error_free(gerror);
		return NULL;
	}
	wstatus = WS_RUNNING;
	while ( 1 ) {
		h = 0;
		g_mutex_lock (&(ws->stopMutex));
		h = ws->clean;
		g_mutex_unlock (&(ws->stopMutex));
		if ( h ) {
			wstatus = WS_TERMINATED;
			break;
		}
		has_waiting = 1;
		has_running = 0;
		g_mutex_lock (&(ws->integrMutex));
		for (curr = ws->listOfNodes, j = 0; curr; curr = curr->next, j++) {
			g_mutex_lock (&(curr->node->statusMutex));
			node_status = curr->node->status;
			g_mutex_unlock (&(curr->node->statusMutex));
			if ( node_status == failed ) {
				flag = 1;
				wstatus = WS_FAILED;
				g_debug("Status node = %d", wstatus);
				break;
			} else if ( node_status == running ) {
				has_running = 1;
			}
			if ( isReady(curr->node, &has_waiting) ) {
				g_mutex_lock (&(ws->stopMutex));
				curr->node->parent_mutex = (&(ws->stopMutex));
				curr->node->parent_signal = &(ws->clean);
				curr->node->parent_name = ws->name;
				g_mutex_unlock (&(ws->stopMutex));
				notifyNode(curr->node, running);
				g_thread_pool_push(thread_node_pool, curr->node, &gerror);
				if ( gerror ) {
					flag = 1;
					if (isSetError)
						error(gerror->message);
					g_error_free(gerror);
					wstatus = WS_FAILED;
					break;
				}
				has_running = 1;
				n++;
			}
		}
		g_mutex_unlock (&(ws->integrMutex));
		if ( wstatus == WS_FAILED ) {
			break;
		} else if ( n == ( ws->numberOfNodes - ws->numberOfMacro ) ) {
			wstatus = WS_COMPLETED;
			break;
		}/* else if ( has_running == 0 ) {
			g_mutex_unlock (ws->integrMutex);
			wstatus = WS_COMPLETED;
			break;
		}*/
	}
/*	g_thread_pool_stop_unused_threads();*/
/*	while (g_thread_pool_unprocessed(thread_node_pool) > 0) {
		kimono_main_context_iteration();
	}*/
	g_thread_pool_free(thread_node_pool, FALSE, TRUE);
	g_debug("Status ws = %d", wstatus);
	workspace_set_status(ws, wstatus);
	kimono_context_stop();
	g_thread_exit (NULL);
}

void workspaceRun(Workspace*ws)
{
	GError*gerror = NULL;
	GThread *gthread = g_thread_try_new ("wksp_run_func",
                  (GThreadFunc) wksp_run_func,
                  (gpointer) ws,
                  &gerror);
	if ( gerror ) {
		if (isSetError)
			error(gerror->message);
		g_error_free(gerror);
		workspace_set_status(ws, WS_FAILED);
		return;
	}
	kimono_context_run();
	g_thread_join (gthread);
}

void notifyNode(Node*ne, NodeStatus status)
{
	g_mutex_lock (&(ne->statusMutex));
	ne->status = status;
	if ( status == completed || status == failed || status == terminated ) {
		ne->microseconds = kimono_get_time_of_day () - ne->microseconds;
	} else if ( status == running ) {
		ne->microseconds = kimono_get_time_of_day ();
	}
	g_mutex_unlock (&(ne->statusMutex));
	if ( isSetTracer)
		tracer(ne);
}

void traceText(Node*ne)
{
	int i;
	char *buf, *olapfs, *file;
	struct stat file_stat;
	GString *strout;
	strout = g_string_new("node ");
	if ( ne->type == pam ) {
		g_string_append_printf(strout, "%s %s %d", ne->label, ne->pam->name, ne->ID);
	} else {
		g_string_append_printf(strout, "%s file %d", ne->label, ne->ID);
	}
	switch (ne->status) {
		case running:
			g_string_append_printf(strout, " running");
		break;
		case waiting:
			g_string_append_printf(strout, " waiting");
		break;
		case completed:
			g_string_append_printf(strout, " completed");
			g_string_append_printf(strout, " in %16.3f ms", ne->microseconds);
#ifndef G_OS_WIN32
			olapfs = door_get_config_path(ne->oServer->hname, ne->oServer->port, ne->oServer->proto);
			for ( i = 0; i < ne->nOutConns; i++ ) {
				file = node_get_out_conn_file(ne, i, ne->uid);
				buf = g_build_filename (olapfs, file, NULL);
				g_free(file);
				if ( g_file_test((const gchar*)buf, G_FILE_TEST_EXISTS) ) {
					if ( 0 == stat( (const char *)buf, &file_stat) ) {
			 			g_string_append_printf(strout, " size[%d]=%lld bytes", i, (long long) file_stat.st_size);
					}
				}
				g_free(buf);
			}
			g_free(olapfs);
#endif
		break;
		case failed:
			g_string_append_printf(strout, " failed");
		break;
		case terminated:
			g_string_append_printf(strout, " terminated");
		break;
	}
	g_message(strout->str);
	g_string_free(strout, TRUE);
}

void traceDb(Node*ne)
{
	char*str_status;
	str_status = node_status_to_string(ne->status);
	kimono_update_wkspjobs_nodes(ne->parent_name, ne->ID, str_status);
	free(str_status);
}

void workspaceRmTemp(Workspace*ws)
{
	ListOfNodes *curr;
	for (curr = ws->listOfNodes; curr; curr = curr->next) {
		door_call("DELETE", curr->node);
	}
}

void workspaceCleanOutputs(Workspace*ws)
{
	ListOfNodes *curr;
	for (curr = ws->listOfNodes; curr; curr = curr->next) {
		nodeCleanOutPut(curr->node);
	}
}

void workspaceAddNode(Workspace*ws, Node*ne)
{
	g_mutex_lock (&(ws->integrMutex));
	if ( !ws->listOfNodes ) {
		ws->listOfNodes = listOfNodesInitWithNode(ne);
		ws->cur_ID = ws->numberOfNodes;
		ne->ID = ws->cur_ID;
		ws->numberOfNodes++;
		ws->cur_ID++;
		g_mutex_unlock (&(ws->integrMutex));
		return;
	}
	if ( ws->cur_ID == 0 ) {
		ws->cur_ID = 1 + maxIDListOfNodes(ws->listOfNodes);
	}
	ne->ID = ws->cur_ID;
	ws->cur_ID++;
	add2ListOfNodes(ws->listOfNodes, ne);
	ws->numberOfNodes++;
	g_mutex_unlock (&(ws->integrMutex));
}


void workspaceRestoreNode(Workspace*ws, Node*ne)
{
	g_mutex_lock (&(ws->integrMutex));
	if ( !ws->listOfNodes ) {
		ws->listOfNodes = listOfNodesInitWithNode(ne);
		ws->numberOfNodes++;
		g_mutex_unlock (&(ws->integrMutex));
		return;
	}
	add2ListOfNodes(ws->listOfNodes, ne);
	ws->numberOfNodes++;
	g_mutex_unlock (&(ws->integrMutex));
}


void deleteNodeFromWS(Workspace*ws, Node*node)
{
	ListOfNodes*curr, *temp;
	int i, j, hn, hp, k;
	Node*ne2;
	g_mutex_lock (&(ws->integrMutex));
	for( j =0; j < node->nInConns; j++) {
		for ( k = 0; k < node->inConn[j].nConn; k++ ) {
			hn = node->inConn[j].channel[k]->sourceNodeID;
			ne2 = nodeByID(ws->listOfNodes, hn);
			hp = node->inConn[j].channel[k]->sourcePortID - 1;
			if ( ne2->outConn[hp].nConn > 0 ) {
				for ( i = 0; i < ne2->outConn[hp].nConn - 1; i++ ) {
					if ( ne2->outConn[hp].channel[i] == node->inConn[j].channel[k] ) {
						ne2->outConn[hp].channel[i] = ne2->outConn[hp].channel[ne2->outConn[hp].nConn - 1];
						break;
					}
				}
				ne2->outConn[hp].nConn--;
				ne2->outConn[hp].channel = (Connection**)realloc(ne2->outConn[hp].channel, ne2->outConn[hp].nConn * sizeof(Connection*) );
			}
			deleteConnection(node->inConn[j].channel[k]);
			node->inConn[j].channel[k] = (Connection*)NULL;
		}
		free(node->inConn[j].channel);
		node->inConn[j].channel = (Connection**)NULL;
		node->inConn[j].nConn = 0;
	}
	if ( node->nInConns > 0)
		free(node->inConn);
	for( j =0; j < node->nOutConns; j++) {
		for ( i = 0; i < node->outConn[j].nConn; i++ ) {
			hn = node->outConn[j].channel[i]->destNodeID;
			hp = node->outConn[j].channel[i]->destPortID - 1;
			ne2 = nodeByID(ws->listOfNodes, hn);
			if ( ne2->nInConns > hp ) {
				int flag;
				flag = 0;
				for ( k = 0; k < ne2->inConn[hp].nConn; k++ ) {
//					if ( ne2->inConn[hp].channel[k] == node->outConn[j].channel[i] ) {
					if ( connection_equal( ne2->inConn[hp].channel[k], node->outConn[j].channel[i] ) == 1 ) {
						ne2->inConn[hp].channel[k] = ne2->inConn[hp].channel[ ne2->inConn[hp].nConn - 1 ];
						flag = 1;
						break;
					}
				}
				if ( flag ) {
					ne2->inConn[hp].nConn--;
					if ( ne2->inConn[hp].nConn > 0 ) {
						ne2->inConn[hp].channel = (Connection**)realloc( ne2->inConn[hp].channel, ne2->inConn[hp].nConn * sizeof(Connection*) );
					} else {
						free(ne2->inConn[hp].channel);
						ne2->inConn[hp].channel = (Connection**)NULL;
					}
				}
			}
			deleteConnection(node->outConn[j].channel[i]);
		}
	}
	if ( node->nOutConns > 0)
		free(node->outConn);
	if ( ws->numberOfNodes == 1 ) {
		free(ws->listOfNodes);
		ws->listOfNodes = (ListOfNodes*)NULL;
	} else {
		for(curr = ws->listOfNodes; curr; curr = curr->next) {
			if ( curr->node->ID == node->ID ) {
				if ( curr == ws->listOfNodes ) {
					ws->listOfNodes = curr->next;
					ws->listOfNodes->prev = (ListOfNodes*)NULL;
					free(curr);
				} else {
					temp = curr->next;
					curr->prev->next = curr->next;
					if (temp) temp->prev = curr->prev;
					free(curr);
				}
				break;
			}
		}
	}
	deleteNode(node);
	ws->numberOfNodes--;
	g_mutex_unlock (&(ws->integrMutex));
}

int workspace_to_file(Workspace*ws, char*fn)
{
	GKeyFile*gkf;
	char*data;
	int length;
	char*comment;
	ListOfNodes*curr;
	Node*ne;
	int i, j, k;
	GError*gerror = NULL;
	GFile*gfile;
	char *etag, *new_etag;
	gsize size;
	int error_occur = 0;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
#endif
	data = workspace_to_string (ws, &length);
#ifdef G_OS_WIN32
	len = -1;
	converted = g_locale_to_utf8((const gchar *)fn, len, &bytes_read, &bytes_written, &gerror);
	if ( gerror )
		g_error("Can't convert image name: %s", gerror->message);
	gfile = g_file_new_for_commandline_arg(fn);
	g_free(converted);
	new_etag = g_file_get_parse_name(gfile);
	if ( !g_file_set_contents(new_etag, data, length, &gerror) ) {
		if ( gerror ) {
			g_debug("workspace_to_file:%s", gerror->message);
			fflush(stdout);
			g_error_free(gerror);
			gerror = NULL;
			error_occur = 1;
		}
		g_debug("workspace_to_file: unknown error");
	}
#else /* !G_OS_WIN32 */
	gfile = g_file_new_for_commandline_arg(fn);
	etag = NULL;
	new_etag = NULL;
	if ( !g_file_replace_contents(gfile, data, length, etag, FALSE, G_FILE_CREATE_NONE, &new_etag, (GCancellable*)NULL, &gerror) ) {
		if ( gerror ) {
			g_debug("workspace_to_file:%s", gerror->message);
			fflush(stdout);
			g_error_free(gerror);
			gerror = NULL;
			error_occur = 1;
		}
		g_debug("workspace_to_file: unknown error");
	}
#endif /* G_OS_WIN32 */
	if ( new_etag ) {
		g_free(new_etag);
	}
	g_object_unref(gfile);
	g_free(data);
	return error_occur;
}

void workspaceCompile2Script(Workspace*ws, char*name, int*nodeIDs, int nNodeIDs, char *path)
{
	/* produces name.pl - a script to run ws by olapd */
	/* produces name.in - inports desc = file nodes */
	/* produces name.out - outports desc = save nodes */
	/* produces name.ui2 - merged ui descr from nodeIDs */
	char *fn;
	FILE*fp;
	UiDef**uiDefs;
	ListOfNodes*curr, *lon;
	int i;
	if ( nNodeIDs > 0 ) {
		uiDefs = (UiDef**)calloc(nNodeIDs, sizeof(UiDef*));
		lon = (ListOfNodes*)NULL;
		for ( i = 0; i < nNodeIDs; i++) {
			Node*ne;
			for ( curr = ws->listOfNodes; curr; curr = curr->next ) {
				if ( curr->node->ID == nodeIDs[i] ) {
					ne = curr->node;
					break;
				}
			}
			if ( !lon ) {
				lon = listOfNodesInitWithNode(ne);
			} else {
				add2ListOfNodes(lon, ne);
			}
			uiDefs[i] = getUiDef(ne);
		}
	}
	fn = (char*)calloc(MAX_RECORD, sizeof(char));

	fn = strcpy(fn, path);
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
#ifndef G_OS_WIN32
	mkdir(fn, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_IXUSR|S_IXGRP|S_IXOTH);
#else
	g_mkdir(fn, 0);
#endif
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
	fn = strcat(fn, ".pl");
	fp =fopen(fn, "w");
	if ( nNodeIDs > 0 ) {
		writeWSScript(fp, nodeIDs, nNodeIDs, name, uiDefs);
	} else {
		writeWSScript1(fp, name);
	}
	fclose(fp);
#ifndef G_OS_WIN32
	chmod(fn, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IXUSR|S_IXGRP);
#endif
	fn = strcpy(fn, path);
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
	fn = strcat(fn, ".in");
	fp =fopen(fn, "w");
	writeWSin(fp, ws);
	fclose(fp);

	fn = strcpy(fn, path);
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
	fn = strcat(fn, ".out");
	fp =fopen(fn, "w");
	writeWSout(fp, ws);
	fclose(fp);

	if ( nNodeIDs > 0 ) {
		fn = strcpy(fn, path);
		fn = strcat(fn, "/");
		fn = strcat(fn, name);
		fn = strcat(fn, "/");
		fn = strcat(fn, name);
		fn = strcat(fn, ".ui2");
		fp =fopen(fn, "w");
		writeWSui2(fp, nodeIDs, nNodeIDs, name, uiDefs, lon);
		fclose(fp);
	}

	fn = strcpy(fn, path);
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
	fn = strcat(fn, "/");
	fn = strcat(fn, name);
	fn = strcat(fn, ".ap");

	workspace_to_file(ws, fn);

	free(fn);
}

void workspace_to_module(Workspace*ws)
{
	/* produces name.pl - a script to run ws by olapd */
	/* produces name.in - inports desc = file nodes */
	/* produces name.out - outports desc = save nodes */
	/* produces name.ui2 - merged ui descr from nodeIDs */
	UiDef*uiDef;
	ListOfNodes*curr;
	GString*inputs, *outputs, *uidescr, *ostring;
	int nodes_with_gui = 0, nodes_file = 0, nodes_save = 0;
	int node_gui_length = 0;
	Node*ne;
	int k, j, i;
	inputs = g_string_new ("");
	outputs = g_string_new ("");
	uidescr = g_string_new ("");
	ostring = g_string_new ("");
/* ui */
	for ( curr = ws->listOfNodes; curr; curr = curr->next ) {
/* inputs */
		if ( curr->node->type == file ) {
			nodes_file++;
		}
/* outputs */
		if ( curr->node->type == pam && !strcmp(curr->node->pam->name, "save") ) {
			nodes_save++;
		}
		if ( curr->node->info->array[curr->node->info->n - 1] == 1 ) {
			ne = curr->node;
			uiDef = getUiDef(ne);
			if ( uiDef != NULL ) {
				node_gui_length += uiDef->length;
				for( k = 0; k < uiDef->length; k++ ) {
					g_string_append_printf(uidescr, "Node \"%s\"[%d]: %s;", ne->label,ne->ID, uiDef->opts[k].label);
					if ( !strcmp(uiDef->opts[k].type, "choice") ) {
						g_string_append_printf(uidescr, "choice;%d;", uiDef->opts[k].lval);
						for( j = 0; j < uiDef->opts[k].lval; j++ ) {
							g_string_append_printf(uidescr, "%s;", uiDef->opts[k].value[j]);
						}
					} else if ( !strcmp(uiDef->opts[k].type, "bool") ) {
						g_string_append_printf(uidescr, "bool;%s;%s;", uiDef->opts[k].value[0], uiDef->opts[k].value[1]);
					} else {
						g_string_append_printf(uidescr, "%s;%s;", uiDef->opts[k].type, uiDef->opts[k].value[0]);
					}
					uidescr = g_string_append_c(uidescr, '\n');
					g_string_append_printf (ostring , "%d.%d", ne->ID, k);
					if ( k < uiDef->length - 1 ) {
						ostring = g_string_append_c (ostring , ',');
					}
				}
				deleteUiDef (uiDef);
				nodes_with_gui++;
			}
		}
	}
	if ( nodes_with_gui > 0 ) {
		char *buf = g_strdup_printf("%d;\n", node_gui_length);
		uidescr = g_string_prepend (uidescr, (const gchar*)buf);
		g_free(buf);
		uidescr = g_string_append_c(uidescr, '\n');
		g_string_append_printf(uidescr, "-y ");
		for( i = 0; i < node_gui_length - 1; i++ ) {
			g_string_append_printf(uidescr, "$%d,", i + 1);
		}
		g_string_append_printf(uidescr, "$%d;", node_gui_length);
	}
/* inputs */
	if ( nodes_file > 0 ) {
		g_string_append_printf(inputs, "tif");
		for ( i = 1; i < nodes_file; i++) {
			g_string_append_printf(inputs, ",tif");
		}
	}
/* outputs */
	if ( nodes_save > 0 ) {
		g_string_append_printf(outputs, "tif");
		for ( i = 1; i < nodes_save; i++) {
			g_string_append_printf(outputs, ",tif");
		}
	}
	ws->comp_inputs = g_strdup(inputs->str);
	ws->comp_outputs = g_strdup(outputs->str);
	ws->comp_uidescr = g_strdup(uidescr->str);
	ws->comp_ostring = g_strdup(ostring->str);
	g_string_free (uidescr, TRUE);
	g_string_free (inputs, TRUE);
	g_string_free (outputs, TRUE);
	g_string_free (ostring, TRUE);
}

void writeWSScript(FILE*fp, int*nodeIDs, int nNodeIDs, char*name, UiDef**uiDefs)
{
static const char s1[] =
"#!/usr/bin/perl\n"
"use strict;\n"
"my $nOptions;\n"
"my @opts;\n"
"my $apFile = \"$ENV{httpd}/apfiles/%s.ap\";\n"
"my $specFile;\n"
"my $ostr;\n"
"my $i;\n"
"if ( $#ARGV < 4 ) {\n"
"    print STDOUT \"This is %s.pl. Usage: optionString inhostsString infileString outhostsString outfilesString\\n\";\n"
"    exit(0);\n"
"}\n"
"$ostr = $ARGV[0];\n"
"if ( $#ARGV > 4 ) {\n"
"    for ( $i = 1; $i <= $#ARGV - 4; $i++ ) {\n"
"        $ostr = $ostr . \" \" . $ARGV[$i];\n"
"    }\n"
"}\n"
"$specFile = $$ . \".spec\";\n"
"$nOptions = %d - 1;\n"
"@opts = split /\\,/,$ostr;\n"
"if ( $#opts != $nOptions ) { die(\"%s.pl: wrong number of options!\"); } \n"
"open( OUT, \">$specFile\" ) || die(\"%s.pl: couldn't open $specFile!\");\n"
"select OUT;\n"
"printf(\"";

static const char s3[] =
");\n"
"close( OUT );\n"
"system(\"komet -s $specFile $apFile $ARGV[$#ARGV - 3]  $ARGV[$#ARGV - 2]  $ARGV[$#ARGV - 1]  $ARGV[$#ARGV] &> /dev/null \");\n"
"system(\"rm -f $specFile\");\n";

	int i, j, k;
	k = 0;
	for( i = 0; i < nNodeIDs; i++ )
		k+= uiDefs[i]->length;
	fprintf(fp, s1, name, name, k, name, name);
	for ( i = 0; i < nNodeIDs; i++) {
		char *buffer;
		buffer = mFormat(uiDefs[i]->format);
		fprintf(fp, "%d;%s;;;;\\n", nodeIDs[i], buffer);
		free(buffer);
	}
	fprintf(fp, "\"");
	k = 0;
	for ( i = 0; i < nNodeIDs; i++) {
		for ( j = 0; j < uiDefs[i]->length; j++ ) {
			fprintf(fp, ",$opts[%d]", k);
			k++;
		}
	}
	fprintf(fp, s3);
}


void writeWSScript1(FILE*fp, char*name)
{
static const char s1[] =
"#!/usr/bin/perl\n"
"use strict;\n"
"my $apFile = \"$ENV{httpd}/apfiles/%s.ap\";\n"
"if ( $#ARGV != 3 ) {\n"
"    print STDOUT \"This is %s.pl. Usage: inhostsString infileString outhostsString outfilesString\\n\";\n"
"    exit(0);\n"
"}\n";

static const char s3[] =
"system(\"komet $apFile $ARGV[0]  $ARGV[1]  $ARGV[2]  $ARGV[3] &> /dev/null \");\n";

	fprintf(fp, s1, name, name);
	fprintf(fp, s3);
}


char*mFormat(char*format)
{
	char*sample;
	int i;
	int j;
	sample = (char*)NULL;
	i = 0;
	j = 0;
	while ( format[j] != '\0' ) {
		while ( format[j] != '\0' && format[j] != '$' ) {
			sample = (char*)realloc(sample, (i + 1) * sizeof(char));
			sample[i] = format[j];
			i++;
			j++;
		}
		if ( format[j] != '\0' ) {
			j++;
			sample = (char*)realloc(sample, (i + 1) * sizeof(char));
			sample[i] = '%';
			i++;
			sample = (char*)realloc(sample, (i + 1) * sizeof(char));
			sample[i] = 's';
			i++;
			while ( format[j] != '\0' && isdigit((int)format[j]) ) {
				j++;
			}
		}
	}
	sample = (char*)realloc(sample, (i + 1) * sizeof(char));
	sample[i] = '\0';
	return sample;
}

void writeWSin(FILE*fp, Workspace*ws)
{
	ListOfNodes*curr;
	int i;
	i = 0;
	for ( curr = ws->listOfNodes; curr; curr = curr->next) {
			if ( curr->node->type == file ) {
				i++;
				fprintf(fp, "%d image:.tif\n", i);
			}
	}
}

void writeWSout(FILE*fp, Workspace*ws)
{
	ListOfNodes*curr;
	int i;
	i = 0;
	for ( curr = ws->listOfNodes; curr; curr = curr->next) {
			if ( curr->node->type == pam && !strcmp(curr->node->pam->name, "save") ) {
				i++;
				fprintf(fp, "%d image:.tif\n", i);
			}
	}
}

void writeWSui2(FILE*fp, int*nodeIDs, int nNodeIDs, char*name, UiDef**uiDefs, ListOfNodes*lon)
{
	int i, n, k, j;
	ListOfNodes*curr;
	n = 0;
	for( i = 0; i < nNodeIDs; i++ )
		n+= uiDefs[i]->length;
	fprintf(fp, "%d;\n", n);
	for( i = 0, curr = lon; i < nNodeIDs && curr; i++, curr = curr->next ) {
		for( k = 0; k < uiDefs[i]->length; k++ ) {
			fprintf(fp, "Node \"%s\"[%d]: %s;", curr->node->label,nodeIDs[i], uiDefs[i]->opts[k].label);
			if ( !strcmp(uiDefs[i]->opts[k].type, "choice") ) {
				fprintf(fp, "choice;%d;", uiDefs[i]->opts[k].lval);
				for( j = 0; j < uiDefs[i]->opts[k].lval; j++ ) {
					fprintf(fp, "%s;", uiDefs[i]->opts[k].value[j]);
				}
			} else if ( !strcmp(uiDefs[i]->opts[k].type, "bool") ) {
				fprintf(fp, "bool;%s;%s;", uiDefs[i]->opts[k].value[0], uiDefs[i]->opts[k].value[1]);
			} else {
				fprintf(fp, "%s;%s;", uiDefs[i]->opts[k].type, uiDefs[i]->opts[k].value[0]);
			}
			fprintf(fp, "\n");
		}
	}
	fprintf(fp, "\n");
	for( i = 0; i < n - 1; i++ ) {
		fprintf(fp, "$%d,", i + 1);
	}
	fprintf(fp, "$%d;", n);
}


void listOfNodesPropagateStatus(ListOfNodes *lon, Node*ne, NodeStatus status)
{
	int p, c, ID, n;
	Node *ne1, *ne2;
	int ret;
	ListOfNodes*list, *curr;
	list = listOfNodesInitWithNode(ne);
	curr = list;
	n = 0;
	while ( curr ) {
		ne2 = curr->node;
		g_mutex_lock (&(ne2->statusMutex));
		door_call("DELETE", ne2);
		if ( ne2->status == completed )
			nodeCleanOutPut(ne2);
		g_mutex_unlock (&(ne2->statusMutex));
		notifyNode(ne2, waiting);
			for ( p = 0; p < ne2->nOutConns; p++ ) {
				for ( c = 0; c < ne2->outConn[p].nConn; c++ ) {
					ID = ne2->outConn[p].channel[c]->destNodeID;
					ne1 = nodeByID(lon, ID);
					ret = hasNodeByID(list, ID);
					if ( ne1 && !ret ) {
						g_mutex_lock (&(ne1->statusMutex));
						if ( ne1->status == completed || ne1->status == failed ) {
							g_mutex_unlock (&(ne1->statusMutex));
							add2ListOfNodes(list, ne1);
						} else {
							g_mutex_unlock (&(ne1->statusMutex));
						}
					}
				}
			}
			curr = curr->next;
	}
	deleteListOfNodes(list);
}

void workspaceAddMacro(Workspace*ws, Node*macro_node)
{
	workspaceAddNode(ws, macro_node);
	g_mutex_lock (&(ws->integrMutex));
	if ( ws->numberOfMacro == 0 ) {
		ws->cur_macro_ID = 1;
		macro_node->info->array[5] = ws->cur_macro_ID;
		ws->numberOfMacro++;
		ws->cur_macro_ID++;
	} else {
		if ( ws->cur_macro_ID == 1 ) {
			ws->cur_macro_ID = 1 + maxMacroIDListOfNodes(ws->listOfNodes);
		}
		macro_node->info->array[5] = ws->cur_macro_ID;
		ws->numberOfMacro++;
		ws->cur_macro_ID++;
	}
	mark_selected_as_macro(ws->listOfNodes, macro_node->info->array[5]);
	macro_node->file = (char*)realloc(macro_node->file, MAX_RECORD * sizeof(char));
	node_macro_set_ports_string(ws->listOfNodes, macro_node);
	node_macro_set_in_ports(macro_node);
	node_macro_set_out_ports(macro_node);
	node_macro_set_connections(ws->listOfNodes, macro_node);
	g_mutex_unlock (&(ws->integrMutex));
}


void workspaceDeleteMacro(Workspace*ws, Node*macro_node)
{
	ListOfNodes *curr;
	int i;
	g_mutex_lock (&(ws->integrMutex));
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[0] == macro_node->info->array[5] ) {
			curr->node->info->array[0] = macro_node->info->array[0];
		}
	}
	ws->numberOfMacro--;
	g_mutex_unlock (&(ws->integrMutex));
	deleteNodeFromWS(ws, macro_node);
}

void workspaceRebuildMacro(Workspace*ws, Node*macro_node)
{
	int i, j, k, ic, oc;
	Node*ne;
	ListOfNodes*curr;
	Connection*conn;
	g_mutex_lock (&(ws->integrMutex));
	node_macro_set_ports_string(ws->listOfNodes, macro_node);
/* delete connections to macro_node here */
	for ( i = 0; i < macro_node->nInConns; i++) {
		for ( k = 0; k < macro_node->inConn[i].nConn; k++ ) {
			ne = nodeByID(ws->listOfNodes, macro_node->inConn[i].channel[k]->sourceNodeID);
			oc = macro_node->inConn[i].channel[k]->sourcePortID - 1;
			conn = (Connection*)NULL;
			for ( j = 0; j < ne->outConn[oc].nConn; j++ ) {
				if ( ne->outConn[oc].channel[j] == macro_node->inConn[i].channel[k] ) {
					conn = ne->outConn[oc].channel[j];
					ne->outConn[oc].channel[j] = ne->outConn[oc].channel[ne->outConn[oc].nConn - 1];
					break;
				}
			}
			if ( conn ) {
				ne->outConn[oc].nConn--;
				if ( ne->outConn[oc].nConn > 0 ) {
					ne->outConn[oc].channel = (Connection**)realloc(ne->outConn[oc].channel, ne->outConn[oc].nConn * sizeof(Connection*) );
				} else {
					free(ne->outConn[oc].channel);
					ne->outConn[oc].channel = (Connection**)NULL;
				}
			}
			deleteConnection(macro_node->inConn[i].channel[k]);
			macro_node->inConn[i].channel[k] = (Connection*)NULL;
			free(macro_node->inConn[i].channel);
			macro_node->inConn[i].channel = (Connection**)NULL;
			macro_node->inConn[i].nConn = 0;
		}
	}
	macro_node->nInConns = 0;
	free(macro_node->inConn);
	macro_node->inConn = (Port*)NULL;
	node_macro_set_in_ports(macro_node);
/* delete connections from macro_node here */
	for ( i = 0; i < macro_node->nOutConns; i++) {
		for ( j = 0; j < macro_node->outConn[i].nConn; j++ ) {
			ne = nodeByID(ws->listOfNodes, macro_node->outConn[i].channel[j]->destNodeID);
			ic = macro_node->outConn[i].channel[j]->destPortID - 1;
			conn = (Connection*)NULL;
			for ( k = 0; k < ne->inConn[ic].nConn; k++ ) {
				if ( ne->inConn[ic].channel[k] == macro_node->outConn[i].channel[j] ) {
					conn = ne->inConn[ic].channel[k];
					ne->inConn[ic].channel[k] = ne->inConn[ic].channel[ne->inConn[ic].nConn - 1];
					break;
				}
			}
			if ( conn ) {
				ne->inConn[ic].nConn--;
				if ( ne->inConn[ic].nConn > 0 ) {
					ne->inConn[ic].channel = (Connection**)realloc(ne->inConn[ic].channel, ne->inConn[ic].nConn * sizeof(Connection*) );
				} else {
					free(ne->inConn[ic].channel);
					ne->inConn[ic].channel = (Connection**)NULL;
				}
			}
			deleteConnection(macro_node->outConn[i].channel[j]);
			macro_node->outConn[i].channel[j] = (Connection*)NULL;
			free(macro_node->outConn[i].channel);
			macro_node->outConn[i].channel = (Connection**)NULL;
			macro_node->outConn[i].nConn = 0;
		}
	}
	macro_node->nOutConns = 0;
	free(macro_node->outConn);
	macro_node->outConn = (Port*)NULL;
	node_macro_set_out_ports(macro_node);
	node_macro_set_connections(ws->listOfNodes, macro_node);
	g_mutex_unlock (&(ws->integrMutex));
}

void node_macro_set_connections(ListOfNodes*lon, Node*macro_node)
{
	Node*ne;
	int k, j, ic, oc, est;
	ListOfNodes*curr;
	Connection*conn;
	ic = 1;
	oc = 1;
	for (curr = lon; curr; curr = curr->next) {
		if ( curr->node->info->array[0] == macro_node->info->array[5] ) {
			for ( j = 0; j < curr->node->nInConns; j++ ) {
				est = 0;
				for ( k = 0; k < curr->node->inConn[j].nConn; k++ ) {
					ne = nodeByID(lon, curr->node->inConn[j].channel[k]->sourceNodeID);
					if ( node_macro_inclcudes_node( lon, ne, macro_node) == 0 ) {
						conn = newConnection( curr->node->inConn[j].channel[k]->sourceNodeID, curr->node->inConn[j].channel[k]->sourcePortID, macro_node->ID, ic);
						installConnection(lon, conn);
						est = 1;
					}
				}
				if ( curr->node->inConn[j].nConn == 0 )
					est = 1;
				if ( est )
					ic++;
			}
			for ( j = 0; j < curr->node->nOutConns; j++ ) {
				est = 0;
				for ( k = 0; k < curr->node->outConn[j].nConn; k++ ) {
					ne = nodeByID(lon, curr->node->outConn[j].channel[k]->destNodeID);
					if ( node_macro_inclcudes_node( lon, ne, macro_node) == 0 ) {
						conn = newConnection( macro_node->ID, oc,  curr->node->outConn[j].channel[k]->destNodeID, curr->node->outConn[j].channel[k]->destPortID);
						installConnection(lon, conn);
						est = 1;
					}
				}
				if ( curr->node->outConn[j].nConn == 0 ) {
					est = 1;
				}
				if ( est )
					oc++;
			}
		}
	}
}

int node_macro_inclcudes_node(ListOfNodes*lon, Node*node, Node*macro_node)
{
	int includes;
	Node*ne;
	includes = 0;
	if ( node->info->array[0] == macro_node->info->array[5] ) {
		includes = 1;
		return includes;
	} else {
		while ( node->info->array[0] != 0 ) {
			node = macro_node_by_number(lon, node->info->array[0]);
			if ( node->info->array[0] == macro_node->info->array[5] ) {
				includes = 1;
				return includes;
			}
		}
	}
	return includes;
}


void mark_selected_as_macro(ListOfNodes*lon, int macro_number)
{
	ListOfNodes*curr;
	for (curr = lon; curr; curr = curr->next) {
		if ( curr->node->info->array[curr->node->info->n - 1] == 1 ) {
			curr->node->info->array[0] = macro_number;
		}
	}
}

void node_macro_set_ports_string(ListOfNodes*lon, Node*macro_node)
{
	char*buf, *buf_i, *buf_o;
	ListOfNodes*curr;
	int j, k;
	Node*ne;
	buf_i = (char*)calloc(MAX_RECORD, sizeof(char));
	buf_o = (char*)calloc(MAX_RECORD, sizeof(char));
	buf = (char*)calloc(MAX_RECORD, sizeof(char));
	for (curr = lon; curr; curr = curr->next) {
		if ( curr->node->info->array[0] == macro_node->info->array[5] ) {
			for ( j = 0; j < curr->node->nInConns; j++ ) {
				for ( k = 0; k < curr->node->inConn[j].nConn; k++ ) {
					ne = nodeByID( lon, curr->node->inConn[j].channel[0]->sourceNodeID);
					if ( node_macro_inclcudes_node( lon, ne, macro_node) == 0 ) {
						sprintf(buf, "%d.%d;", curr->node->ID, j);
						buf_i = strcat(buf_i, buf);
						break;
					}
				}
				if ( curr->node->inConn[j].nConn == 0 ) {
					sprintf(buf, "%d.%d;", curr->node->ID, j);
					buf_i = strcat(buf_i, buf);
				}
			}
			for ( j = 0; j < curr->node->nOutConns; j++ ) {
				for ( k = 0; k < curr->node->outConn[j].nConn; k++ ) {
					ne = nodeByID( lon, curr->node->outConn[j].channel[k]->destNodeID);
					if ( node_macro_inclcudes_node( lon, ne, macro_node) == 0 ) {
						sprintf(buf, "%d.%d;", curr->node->ID, j);
						buf_o = strcat(buf_o, buf);
						break;
					}
				}
				if ( curr->node->outConn[j].nConn == 0 ) {
					sprintf(buf, "%d.%d;", curr->node->ID, j);
					buf_o = strcat(buf_o, buf);
				}
			}
		}
	}
	sprintf(macro_node->file, "i=%so=%s", buf_i, buf_o);
	free(buf);
	free(buf_i);
	free(buf_o);
}

Node* ws_get_ins_node(Workspace*ws)
{
	ListOfNodes*l1;
	char *buf;
	l1= ws->listOfNodes;
	while ( l1 && l1->node->type != ins )
		l1= l1->next;
	if ( !l1 && isSetError ) {
		buf = (char*)calloc(MAX_RECORD, sizeof(char));
		sprintf(buf, "no node with type ins");
		error(buf);
		free(buf);
		return NULL;
	}
	return l1->node;
}

void ws_set_host_port(Workspace*ws, char*server, int portnum)
{
	ListOfNodes *curr;
	Node*ne;
	for ( curr= ws->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		ne->oServer->hname = (char*)realloc(ne->oServer->hname, MAX_RECORD * sizeof(char));
		ne->oServer->hname = strcpy(ne->oServer->hname, server);
		ne->oServer->port = portnum;
		if ( ne->type == ins ) {
			ne->buf = g_path_get_basename((const gchar*)ne->file);
		} else if ( ne->type == ous ) {
			ne->buf = g_path_get_basename((const gchar*)ne->file);
		}
	}
}

void ws_set_ins_realtive(Workspace*ws, gchar*type_relative_ins, int relative_ins_paths)
{
	ListOfNodes *curr;
	Node*ne;
	char *buf;
	if ( relative_ins_paths == 1 ) {
		for ( curr= ws->listOfNodes; curr; curr = curr->next ) {
			ne = curr->node;
			if ( ne->type == ins ) {
				buf = g_path_get_basename(ne->file);
				g_free(ne->file);
				ne->file = g_build_filename(type_relative_ins, buf, NULL);
				g_free(buf);
			}
		}
	}
}

void ws_set_ins_ous(Workspace*ws, char *inses, char*mountpoint, char*server, int portnum)
{
	ListOfNodes *curr;
	Node*ne;
	char *buf;
	buf = g_path_get_basename(inses);
	for ( curr= ws->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		ne->oServer->hname = (char*)realloc(ne->oServer->hname, MAX_RECORD * sizeof(char));
		ne->oServer->hname = strcpy(ne->oServer->hname, server);
		ne->oServer->port = portnum;
		ne->status = waiting;
		nodeCleanOutPut(ne);
		if ( ne->type == ins ) {
			ne->file = (char*)realloc(ne->file, MAX_RECORD * sizeof(char));
			sprintf(ne->file, "%s%s", inses, ne->buf);
			ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
			sprintf(ne->id, "%s/temp/%s:%d", mountpoint, server, portnum);
		} else if ( ne->type == ous ) {
			ne->file = (char*)realloc(ne->file, MAX_RECORD * sizeof(char));
			sprintf(ne->file, "%s/%s%s", mountpoint, buf, ne->buf);
			ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
			sprintf(ne->id, "%s/temp", mountpoint);
		}
	}
}

void ws_set_ins_ous_jobid(Workspace*ws, char*mountpoint, char *jobid)
{
	ListOfNodes *curr;
	Node*ne;
	for ( curr= ws->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		ne->status = waiting;
		nodeCleanOutPut(ne);
		if ( ne->type == ous ) {
			ne->file = (char*)realloc(ne->file, MAX_RECORD * sizeof(char));
			sprintf(ne->file, "%s/%s%s", mountpoint, jobid, ne->buf);
			ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
			sprintf(ne->id, "%s/temp", mountpoint);
		}
	}
}

void ws_set_file_save(Workspace*ws, char**input_pointers, char **output_pointers, int n_input_pointers, int n_output_pointers)
{
	ListOfNodes *curr;
	Node*ne;
	int fkounter = 0, skounter = 0;
	for ( curr= ws->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		if ( ne->type == file && fkounter < n_input_pointers ) {
			ne->file = g_strdup(input_pointers[fkounter]);
			fkounter++;
		} else if ( ne->type == pam && !strcmp(ne->pam->name, "save") && skounter < n_output_pointers ) {
			ne->id = g_strdup(output_pointers[skounter]);
			skounter++;
		}
	}
}


int ws_get_num_failed_nodes(Workspace*ws)
{
	ListOfNodes *curr;
	Node*ne;
	int counter;
	counter = 0;
	for ( curr= ws->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		if ( ne->status == failed )
			counter++;
	}
	return counter;
}

Workspace*workspaceParse(GKeyFile*gkf)
{
	Workspace*ws;
	int n;
	char**grps;
	int ngrps;
	char**ckeys;
	int nckeys;
	int i, j, k;
	Node*ne;
	Connection**conn;
	char*start_grp;
	gsize size;
	int relative_ins_paths = 0;
	gchar*type_relative_ins, *str;
	GError*gerror = NULL;
	start_grp = g_key_file_get_start_group(gkf);
	if ( !start_grp ) {
		return NULL;
	} else if ( start_grp && strcmp(start_grp, "Workspace") ) {
		g_free(start_grp);
		return NULL;
	} else if ( start_grp && !strcmp(start_grp, "Workspace") ) {
		if ( 0 == g_key_file_get_integer(gkf, (const gchar *)start_grp, "nodes", &gerror) ){
			g_free(start_grp);
			return NULL;
		}
	}
	if ( ( type_relative_ins = g_key_file_get_string(gkf, (const gchar *)start_grp, "RelativeINS", &gerror) ) != NULL) {
		if ( !strcmp(type_relative_ins, "Once") ) {
			relative_ins_paths = 1;
			g_free(type_relative_ins);
		}
	} else {
		if ( gerror ) {
			g_debug(gerror->message);
			g_error_free (gerror);
			gerror = NULL;
		}
	}
	ws = workspaceCreate();
	str = g_key_file_get_locale_string(gkf, (const gchar *)start_grp, "Name", NULL, &gerror);
	if ( gerror ) {
		g_debug("Name for workspace is not given: %s", gerror->message);
		g_error_free(gerror);
		gerror = NULL;
		if (ws->name != NULL) {
			g_free(ws->name);
			ws->name = NULL;
		}
	} else {
		if (ws->name != NULL) {
			g_free(ws->name);
		}
		ws->name = str;
	}
	str = g_key_file_get_comment (gkf, (const gchar *)start_grp, "Name", &gerror);
	if ( gerror ) {
		g_debug("Comment for workspace is not given:%s", gerror->message);
		g_error_free(gerror);
		gerror = NULL;
		if (ws->comment != NULL) {
			g_free(ws->comment);
			ws->comment = NULL;
		}
	} else {
		if (ws->comment != NULL) {
			g_free(ws->comment);
		}
		ws->comment = str;
	}
	g_free(start_grp);
	grps = g_key_file_get_groups(gkf, &size);
	ngrps = (int)size;
	i = 0;
	for ( j = 0; j < ngrps; j++ ) {
		if ( g_str_has_prefix(grps[j], "Node:")) {
			ne = nodeParse(gkf, grps[j]);
			if ( i == 0 )
				ws->listOfNodes = listOfNodesInitWithNode(ne);
			else
				add2ListOfNodes(ws->listOfNodes, ne);
			i++;
		}
	}
	ws->numberOfNodes = i;
	ws->cur_ID = 1 + maxIDListOfNodes(ws->listOfNodes);
	ws->numberOfMacro = macroNodes(ws->listOfNodes);
	ws->cur_macro_ID = 1 + maxMacroIDListOfNodes(ws->listOfNodes);
	g_strfreev(grps);
	ckeys = g_key_file_get_keys(gkf, "Connections", &size, &gerror);
	nckeys = (int)size;
	if ( ckeys ) {
		for ( i = 0; i < nckeys; i++ ) {
			conn = outPortParse(gkf, ckeys[i], &n);
			for ( j = 0; j < n; j++ ) {
				installConnection(ws->listOfNodes, conn[j]);
			}
		}
		g_strfreev(ckeys);
	}
	if ( relative_ins_paths == 1 ) {
		type_relative_ins = g_get_current_dir ();
		ws_set_ins_realtive(ws, type_relative_ins, relative_ins_paths);
		g_free(type_relative_ins);
	}
	return ws;
}

void workspacePaste(Workspace*wksp, Workspace*addon, int x, int y)
{
	int node_offset;
	int macro_offset;
	ListOfNodes*curr;
	Node*ne;
	int x_offset, y_offset, j, k;
	g_mutex_lock (&(wksp->integrMutex));
	if ( !wksp->listOfNodes ) {
		wksp->cur_ID = wksp->numberOfNodes;
	}
	if ( wksp->cur_ID == 0 && wksp->numberOfNodes > 0 && wksp->listOfNodes ) {
		wksp->cur_ID = 1 + maxIDListOfNodes(wksp->listOfNodes);
	}
	if ( wksp->numberOfMacro == 0 ) {
		wksp->cur_macro_ID = 1;
	} else {
		if ( wksp->cur_macro_ID == 1  && wksp->listOfNodes ) {
			wksp->cur_macro_ID = 1 + maxMacroIDListOfNodes(wksp->listOfNodes);
		}
	}
	node_offset = wksp->cur_ID;
	macro_offset = wksp->cur_macro_ID;
	ne = addon->listOfNodes->node;
	for ( curr = addon->listOfNodes; curr; curr = curr->next ) {
		for ( j = 0; j < curr->node->nOutConns; j++ ) {
			for ( k = 0; k < curr->node->outConn[j].nConn; k++ ) {
				curr->node->outConn[j].channel[k]->sourceNodeID += node_offset;
				curr->node->outConn[j].channel[k]->destNodeID += node_offset;
			}
		}
	}
	for ( curr = addon->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		ne->ID += node_offset;
		ne->info->array[2] += x;
		ne->info->array[1] += y;
		if ( !wksp->listOfNodes )
			wksp->listOfNodes = listOfNodesInitWithNode(ne);
		else
			add2ListOfNodes(wksp->listOfNodes, ne);
		if ( ne->type == macro ) {
			ne->info->array[5] += macro_offset;
		}
		if ( ne->info->array[0] > 0 ) {
			ne->info->array[0] += macro_offset;
		}
	}
	for ( curr = addon->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		if ( ne->type == macro ) {
			node_macro_set_ports_string(wksp->listOfNodes, ne);
		}
	}
	wksp->numberOfNodes += addon->numberOfNodes;
	wksp->cur_ID = 1 + maxIDListOfNodes(wksp->listOfNodes);
	wksp->cur_macro_ID = 1 + maxMacroIDListOfNodes(wksp->listOfNodes);
	wksp->numberOfMacro = macroNodes(wksp->listOfNodes);
	g_mutex_unlock (&(wksp->integrMutex));
}

void workspacePrint(Workspace*wksp, GKeyFile*gkf)
{
	GError *gerror = NULL;
	g_key_file_set_value(gkf, "Workspace", "n", "1");
	if (wksp->name != NULL && strlen(wksp->name) > 0) {
		g_key_file_set_string (gkf, "Workspace", "Name", (const gchar*)wksp->name);
	}
	if (wksp->comment != NULL && strlen(wksp->comment) > 0) {
		g_key_file_set_comment(gkf, "Workspace", "Name", (const gchar *)wksp->comment, &gerror);
		if ( gerror ) {
			g_debug("workspace comment error:%s", gerror->message);
			g_error_free(gerror);
		}
	}
}

void workspacePrintNNodes(Workspace*wksp, GKeyFile*gkf, int n)
{
	g_key_file_set_integer(gkf, "Workspace", "nodes", n);
}

void workspace_set_status(Workspace*ws, WorkspaceStatus status)
{
	g_mutex_lock (&(ws->statusMutex));
	ws->status = status;
	g_mutex_unlock (&(ws->statusMutex));
}

WorkspaceStatus workspace_get_status(Workspace*ws)
{
	WorkspaceStatus status;
	g_mutex_lock (&(ws->statusMutex));
	status = ws->status;
	g_mutex_unlock (&(ws->statusMutex));
	return status;
}

void getBbWorkspace(Workspace*ws, int *wsWidth, int *wsHeight, int Pegas)
{
	ListOfNodes *curr;
	*wsWidth = 20;
	*wsHeight = 20;
	for (curr = ws->listOfNodes; curr; curr = curr->next) {
		if ( curr->node->info->array[2] +20 > (*wsWidth) )
			(*wsWidth) = curr->node->info->array[2] +20;
		if ( curr->node->info->array[1] +20 > (*wsHeight) )
			(*wsHeight) = curr->node->info->array[1] +20;
	}
	if ( Pegas ) {
		if ( *wsHeight > 2000 ) {
			for (curr = ws->listOfNodes; curr; curr = curr->next) {
				curr->node->info->array[1] = (int)floor( 2000 * (double)curr->node->info->array[1] / (double)(*wsHeight) );
			}
			*wsHeight = 2000;
		}
		if ( *wsWidth > 2000 ) {
			for (curr = ws->listOfNodes; curr; curr = curr->next) {
				curr->node->info->array[2] = (int)floor( 2000 * (double)curr->node->info->array[2] / (double)(*wsWidth) );
			}
			*wsWidth = 2000;
		}
	}
	for (curr = ws->listOfNodes; curr; curr = curr->next) {
		setBBNode(curr->node);
	}
	*wsHeight += 100;
	*wsWidth += 20;
}

void workspaceTrimCoordinates(Workspace*ws)
{
	ListOfNodes *curr;
	int xmin, ymin;
	xmin = 65536;
	ymin = 65536;
	for (curr = ws->listOfNodes; curr; curr = curr->next) {
		if ( xmin > curr->node->info->array[1] )
			xmin = curr->node->info->array[1];
		if ( ymin > curr->node->info->array[2] )
			ymin = curr->node->info->array[2];
	}
	for (curr = ws->listOfNodes; curr; curr = curr->next) {
		curr->node->info->array[1] -= xmin;
		curr->node->info->array[2] -= ymin;
	}
}

void setBBNode(Node*node)
{
	int nodeWidth = 30;
	int nodeHeight = 20;
	int n;
	n = ( node->nInConns > node->nOutConns ) ? node->nInConns : node->nOutConns;
	nodeHeight += n * 15;
	node->info->array[3] = nodeWidth;
	node->info->array[4] = nodeHeight;
	node->info->array[node->info->n - 1] = 0;
}

Node*ws_get_selected_node_by_xy(Workspace*ws, int x, int y, int *inPort, int *outPort, int *Conn, int MACRO_NUMBER)
{
	int i, j;
	Node *ne = (Node*)NULL;
	ListOfNodes *curr;
	*inPort = -1;
	*outPort = -1;
	*Conn = -1;
		for (curr = ws->listOfNodes; curr; curr = curr->next) {
			if ( curr->node->info->array[0] == MACRO_NUMBER && curr->node->info->array[2] <= x && x <= curr->node->info->array[2] + curr->node->info->array[3] && curr->node->info->array[1] <= y && y <= curr->node->info->array[1] + curr->node->info->array[4] ) {
				ne = curr->node;
				for ( i = 0; i < curr->node->nInConns; i++) {
					if ( curr->node->info->array[2] <= x && x <= curr->node->info->array[2] + 10 && curr->node->info->array[1] + (i + 1) * 15 - 5 <= y && y <= curr->node->info->array[1] + (i + 1) * 15 + 5 ) {
						*inPort = i;
						break;
					}
				}
				for ( i = 0; i < curr->node->nOutConns; i++) {
					if ( curr->node->info->array[2] + curr->node->info->array[3] - 10 <= x && x <= curr->node->info->array[2] + curr->node->info->array[3] && curr->node->info->array[1] + (i + 1) * 15 - 5 <= y && y <= curr->node->info->array[1] + (i + 1) * 15 + 5 ) {
						*outPort = i;
						break;
					}
				}
				break;
			}
		}
	return ne;
}

void ws_clear_node_selections(Workspace*ws, int *inPort, int *outPort, int *Conn, int MACRO_NUMBER)
{
	ListOfNodes *curr;
	int i;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[ curr->node->info->n - 1] == 1) {
			curr->node->info->array[ curr->node->info->n - 1] = 0;
		}
	}
	*inPort = -1;
	*outPort = -1;
	*Conn = -1;
}

Node*ws_find_one_selected(Workspace*ws)
{
	ListOfNodes *curr;
	int i;
	Node*ne;
	ne = (Node*)NULL;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[ curr->node->info->n - 1] == 1) {
			ne = curr->node;
			break;
		}
	}
	return ne;
}

Workspace*workspace_from_string(gchar*contents, int length, GError**gerror)
{
	Workspace*ws;
	GKeyFile*gkf;
	gkf = g_key_file_new();
	ws = NULL;
	if ( g_key_file_load_from_data(gkf, (const gchar*)contents, length, G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, gerror ) ) {
		ws = workspaceParse(gkf);
	}
	g_key_file_free(gkf);
	return ws;
}

int workspace_set_inputs(Workspace*ws, char*wsinputs, char*inputs)
{
	char**wsin;
	char**in;
	int wsl;
	int l;
	int node_id, in_id;
	int i;
	Node*node;
	wsin = g_strsplit(wsinputs, ",", MAX_TOKENS);
	if ( !wsin ) {
		return -1;
	}
	in = g_strsplit(inputs, ",", MAX_TOKENS);
	if ( !in ) {
		if ( wsin ) {
			g_strfreev(wsin);
		}
		return -1;
	}
	wsl = g_strv_length(wsin);
	l = g_strv_length(in);
	if ( l != wsl ) {
		if ( wsin ) {
			g_strfreev(wsin);
		}
		if ( in ) {
			g_strfreev(in);
		}
		return -1;
	}
	for ( i = 0; i < wsl; i++ ) {
		sscanf(wsin[i], "%d.%d", &node_id, &in_id);
		node = nodeByID (ws->listOfNodes, node_id);
		node_set_id_from_string(node, in_id, in[i]);
	}
	if ( wsin ) {
		g_strfreev(wsin);
	}
	if ( in ) {
		g_strfreev(in);
	}
	return 0;
}

int workspace_set_outputs(Workspace*ws, char*wsoutputs, char*outputs)
{
	char**wsout;
	char**out;
	int wsl;
	int l;
	int node_id, in_id;
	Node*node;
	wsout = g_strsplit(wsoutputs, ",", MAX_TOKENS);
	if ( !wsout ) {
		return -1;
	}
	out = g_strsplit(outputs, ",", MAX_TOKENS);
	if ( !out ) {
		if ( wsout ) {
			g_strfreev(wsout);
		}
		return -1;
	}
	wsl = g_strv_length(wsout);
	l = g_strv_length(out);
	if ( l != wsl ) {
		if ( wsout ) {
			g_strfreev(wsout);
		}
		if ( out ) {
			g_strfreev(out);
		}
		return -1;
	}
	for ( l = 0; l < wsl; l++ ) {
		sscanf(wsout[l], "%d.%d", &node_id, &in_id);
		node = nodeByID (ws->listOfNodes, node_id);
		node_set_id_from_string(node, in_id, out[l]);
	}
	if ( wsout ) {
		g_strfreev(wsout);
	}
	if ( out ) {
		g_strfreev(out);
	}
	return 0;
}

WorkspaceStatus workspace_run_from_lib(char*mname, char*user, char*inputs, char*outputs, char*wksplibtag)
{
	Workspace*ws;
	WorkspaceStatus wstatus;
	GError*gerror = NULL;
	gchar*contents;
	char*wsinputs;
	char*wsoutputs;
	int length;
	contents = (gchar*)kimono_get_wksp_info(wksplibtag, "apfile");
	length = strlen(contents);
	if ( !( ws = workspace_from_string(contents, length, &gerror) ) ) {
		g_debug("workspace_run_from_lib:%s", gerror->message);
		g_error_free(gerror);
		g_free(contents);
		return WS_FAILED;
	}
	g_free(contents);
	wsinputs = (gchar*)kimono_get_wksp_info(wksplibtag, "inputs");
	wsoutputs = (gchar*)kimono_get_wksp_info(wksplibtag, "outputs");
	g_mutex_lock (&(ws->stopMutex));
	workspace_set_inputs(ws, wsinputs, inputs);
	workspace_set_outputs(ws, wsoutputs, outputs);
	ws->clean = 0;
	ws->name = strcpy(ws->name, mname);
	g_mutex_unlock (&(ws->stopMutex));
	g_free(wsinputs);
	g_free(wsoutputs);
	workspace_set_status(ws, WS_RUNNING);
	setTracer(traceDb);
	setNotifyNode(notifyNode);
	kimono_update_wkspjobs_status(mname, "1");
	workspaceRun(ws);
	wstatus = workspace_get_status(ws);
	if ( wstatus == WS_RUNNING) {
			kimono_update_wkspjobs_status(mname, "1");
	} else if ( wstatus == WS_FAILED) {
			kimono_update_wkspjobs_status(mname, "2");
	} else if ( wstatus == WS_COMPLETED) {
			kimono_update_wkspjobs_status(mname, "3");
	} else 	if ( wstatus == WS_TERMINATED) {
			kimono_update_wkspjobs_status(mname, "4");
	}
	workspaceRmTemp(ws);
	return wstatus;
}

char*workspace_to_string(Workspace*ws, int*length)
{
	GKeyFile*gkf;
	char*data;
	char*comment;
	ListOfNodes*curr;
	Node*ne;
	int i, j, k;
	GError*gerror = NULL;
	gsize size;
	comment = g_strdup_printf(" ProStack scenario");
	gkf = g_key_file_new();
	workspacePrint(ws, gkf);
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		ne = curr->node;
		nodePrint(ne, gkf);
		for ( j = 0; j < ne->nOutConns; j++ ) {
			for ( k = 0; k < ne->outConn[j].nConn; k++ ) {
				outPortPrint(ne, j, k, gkf);
			}
		}
	}
	workspacePrintNNodes(ws, gkf, ws->numberOfNodes);
	if ( ws->comp_inputs ) {
		g_key_file_set_string (gkf, "Compilation", "inputs", (const gchar*)ws->comp_inputs);
	}
	if ( ws->comp_outputs ) {
		g_key_file_set_string (gkf, "Compilation", "outputs", (const gchar*)ws->comp_outputs);
	}
	if ( ws->comp_uidescr ) {
		char*buf = g_strescape(ws->comp_uidescr, "");
		g_key_file_set_string (gkf, "Compilation", "uidescr", (const gchar*)buf);
		g_free(buf);
	}
	if ( ws->comp_ostring ) {
		g_key_file_set_string (gkf, "Compilation", "ostring", (const gchar*)ws->comp_ostring);
	}
	g_key_file_set_comment(gkf, NULL, NULL, (const gchar *)comment, &gerror);
	if ( gerror ) {
		g_debug("workspace_to_string error:%s", gerror->message);
		g_error_free(gerror);
	}
	g_free(comment);
	data = g_key_file_to_data(gkf, &size, &gerror);
	(*length) = (int)size;
	if ( gerror ) {
		g_debug("workspace_to_string error:%s", gerror->message);
		g_error_free(gerror);
	}
	g_key_file_free(gkf);
	return data;
}

int workspace_to_wksplib(Workspace*ws, Node*sys_node, char*inputs, char*outputs, char*wksplibtag, char*spath, char*message)
{
	char*contents;
	int length;
	int res;
	contents = workspace_to_string(ws, &length);
	if ( sys_node->id )
		g_free(sys_node->id);
	sys_node->id = g_strdup(spath);
	sys_node->label = strcpy(sys_node->label, wksplibtag);
	if ( sys_node->file )
		g_free(sys_node->file);
	sys_node->file = g_strdup_printf("%s=%s", inputs, outputs);
	sys_node->buf = g_strdup_printf("%s\r\n$section\r\n%s", message, contents);
	res = door_call("PUT", sys_node);
	g_free(contents);
	return res;
}

void workspaceNormalizeIDs(Workspace*wksp)
{
	ListOfNodes*curr;
	Node*ne;
	int j, k, node_id;
	if ( !wksp->listOfNodes ) {
		wksp->cur_ID = wksp->numberOfNodes;
	}
	if ( wksp->cur_ID == 0 && wksp->numberOfNodes > 0 && wksp->listOfNodes ) {
		wksp->cur_ID = 1 + maxIDListOfNodes(wksp->listOfNodes);
	}
	if ( wksp->numberOfMacro == 0 ) {
		wksp->cur_macro_ID = 1;
	} else {
		if ( wksp->cur_macro_ID == 1  && wksp->listOfNodes ) {
			wksp->cur_macro_ID = 1 + maxMacroIDListOfNodes(wksp->listOfNodes);
		}
	}
	for ( curr = wksp->listOfNodes, node_id = 0; curr; curr = curr->next, node_id++ ) {
		ne = curr->node;
		g_return_if_fail( ne->buf == NULL );
		ne->buf = g_strdup_printf("%d", node_id);
	}
	for ( curr = wksp->listOfNodes; curr; curr = curr->next ) {
		for ( j = 0; j < curr->node->nOutConns; j++ ) {
			for ( k = 0; k < curr->node->outConn[j].nConn; k++ ) {
				ne = nodeByID (wksp->listOfNodes, curr->node->outConn[j].channel[k]->sourceNodeID);
				node_id = atoi(ne->buf);
				curr->node->outConn[j].channel[k]->sourceNodeID = node_id;
				ne = nodeByID (wksp->listOfNodes, curr->node->outConn[j].channel[k]->destNodeID);
				node_id = atoi(ne->buf);
				curr->node->outConn[j].channel[k]->destNodeID = node_id;
			}
		}
	}
	for ( curr = wksp->listOfNodes; curr; curr = curr->next ) {
		ne = curr->node;
		node_id = atoi(ne->buf);
		curr->node->ID = node_id;
		g_free(ne->buf);
		ne->buf = NULL;
	}
	for ( curr = wksp->listOfNodes, node_id = 0; curr; curr = curr->next, node_id++ ) {
		if ( curr->node->type == macro ) {
			workspaceRebuildMacro(wksp, curr->node);
		}
	}
	wksp->cur_ID = 1 + maxIDListOfNodes(wksp->listOfNodes);
	wksp->cur_macro_ID = 1 + maxMacroIDListOfNodes(wksp->listOfNodes);
	wksp->numberOfMacro = macroNodes(wksp->listOfNodes);
}

void workspaceCorrectMacroOnPaste(Workspace*wksp, int MACRO_NUMBER)
{
	ListOfNodes*curr;
	for ( curr = wksp->listOfNodes; curr; curr = curr->next ) {
		if ( curr->node->info->array[0] == 0 ) {
			curr->node->info->array[0] = MACRO_NUMBER;
		}
	}
}

int workspace_to_methods(gchar*Name, gchar*metaname, int metaweight, gchar*Executable, int type, gchar*uidescr, gchar*message, gchar*inputs, gchar*outputs, gchar*ostring, char*spath, char*str, OlapServer*os, GError**gerror)
{
	Node*sproxy;
	char*apfile;
	int res;
	GError*err = NULL;
	g_return_val_if_fail (gerror == NULL || *gerror == NULL, 1);
	sproxy = proxy_sys_node("methods");
	sproxy->oServer = os;
	if ( sproxy->label )
		g_free(sproxy->label);
	sproxy->label = g_strdup(Name);
	if ( sproxy->id )
		g_free(sproxy->id);
	sproxy->id = g_strdup(spath);
	if ( sproxy->file )
		g_free(sproxy->file);
	if ( type == 1 ) { /* Just command */
		sproxy->file = g_strdup_printf("%s=%d=%s=%s", Executable, type, inputs, outputs);
		sproxy->buf = g_strdup_printf("%s\r\n$section\r\n%s\r\n$section\r\n%s\r\n$section\r\n%d", uidescr, message, metaname, metaweight);
	} else if ( type == 2 ) { /* ProStack Scenario */
		GFile*gfile;
		char*etag_out;
		gsize size;
#ifdef G_OS_WIN32
		gchar*converted;
		gssize len;
		gsize bytes_read;
		gsize bytes_written;
		len = -1;
		converted = g_locale_to_utf8((const gchar *)str, len, &bytes_read, &bytes_written, &err);
		if ( err != NULL ) {
			g_propagate_error (gerror, err);
			return 1;
		}
		gfile = g_file_new_for_commandline_arg((const char*)converted);
		if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &apfile, &size, &etag_out, &err) ) {
			if ( err != NULL ) {
				g_object_unref(gfile);
				g_propagate_error (gerror, err);
				return 1;
			}
			g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_METHODS,            /* error code */
                   "GFile error %s", /* error message format string */
                   str);
			g_object_unref(gfile);
			return 1;
		}
		g_free(converted);
#else
		gfile = g_file_new_for_commandline_arg(str);
		if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &apfile, &size, &etag_out, &err) ) {
			if ( err != NULL ) {
				g_object_unref(gfile);
				g_propagate_error (gerror, err);
				return 1;
			}
			g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_METHODS,            /* error code */
                   "GFile error %s", /* error message format string */
                   str);
			g_object_unref(gfile);
			return 1;
		}
#endif
		g_free(etag_out);
		g_object_unref(gfile);
		sproxy->file = g_strdup_printf("%s=%d=%s=%s", Executable, type, inputs, outputs);
		sproxy->buf = g_strdup_printf("%s\r\n$section\r\n%s\r\n$section\r\n%s\r\n$section\r\n%s\r\n$section\r\n%s\r\n$section\r\n%d", ostring, uidescr, message, apfile, metaname, metaweight);
		g_free(apfile);
	} else if ( type == 3 ) { /* Java Class */
		sproxy->file = g_strdup_printf("%s=%d=%s=%s", Executable, type, inputs, outputs);
		sproxy->buf = g_strdup_printf("%s\r\n$section\r\n%s\r\n$section\r\n%s\r\n$section\r\n%s\r\n$section\r\n%d", uidescr, message, str, metaname, metaweight);
	} else if ( type == 4 ) { /* Fiji Macro */
		sproxy->file = g_strdup_printf("%s=%d=%s=%s", Executable, type, inputs, outputs);
		sproxy->buf = g_strdup_printf("%s\r\n$section\r\n%s\r\n$section\r\n%s\r\n$section\r\n%d", uidescr, message, metaname, metaweight);
	}
	res = door_call("PUT", sproxy);
	if ( strncmp(sproxy->buf, "OK", 2) ) {
		g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_METHODS,            /* error code */
                   "%s", /* error message format string */
                   sproxy->buf);
		g_free(sproxy->buf);
		proxy_sys_node_del(sproxy);
		return 1;
	} else {
		g_free(sproxy->buf);
		proxy_sys_node_del(sproxy);
		return 0;
	}
}

int workspace_get_compilation_infos(GKeyFile*gkf, gchar**uidescr, gchar**inputs, gchar**outputs, gchar**ostring, GError**gerror)
{
	GError*err = NULL;
	int res = 0;
	char*start_grp, *str;
	gsize size;
	g_return_val_if_fail (gerror == NULL || *gerror == NULL, 1);
	start_grp = g_key_file_get_start_group(gkf);
	if ( !start_grp ) {
		g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_COMPILATION,            /* error code */
                   "No start group%s", /* error message format string */
                   "!");
		return 1;
	} else if ( start_grp && strcmp(start_grp, "Workspace") ) {
		g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_COMPILATION,            /* error code */
                   "Start group:%s", /* error message format string */
                   start_grp);
		g_free(start_grp);
		return 1;
	} else if ( start_grp && !strcmp(start_grp, "Workspace") ) {
		if ( 0 == g_key_file_get_integer(gkf, (const gchar *)start_grp, "nodes", &err) ){
			g_propagate_error (gerror, err);
			g_free(start_grp);
			return 1;
		}
	}
	g_free(start_grp);
	if ( ( str = g_key_file_get_string(gkf, "Compilation", "inputs", &err) ) != NULL) {
		(*inputs) = g_strdup(str);
		g_free(str);
	} else {
		if ( err != NULL ) {
			g_propagate_error (gerror, err);
			return 1;
		}
	}
	if ( ( str = g_key_file_get_string(gkf, "Compilation", "outputs", &err) ) != NULL) {
		(*outputs) = g_strdup(str);
		g_free(str);
	} else {
		if ( err != NULL ) {
			g_propagate_error (gerror, err);
			return 1;
		}
	}
	if ( ( str = g_key_file_get_string(gkf, "Compilation", "uidescr", &err) ) != NULL) {
/*		(*uidescr) = g_strescape((const gchar*)str, "");*/
		(*uidescr) = g_strcompress((const gchar*)str);
		g_free(str);
	} else {
		if ( err != NULL ) {
			g_propagate_error (gerror, err);
			return 1;
		}
	}
	if ( ( str = g_key_file_get_string(gkf, "Compilation", "ostring", &err) ) != NULL) {
		(*ostring) = g_strdup(str);
		g_free(str);
	} else {
		if ( err != NULL ) {
			g_propagate_error (gerror, err);
			return 1;
		}
	}
	return res;
}

int workspace_get_compilation_infos_from_file(const char*str, gchar**uidescr, gchar**inputs, gchar**outputs, gchar**ostring, GError**gerror)
{
	int res = 0;
	GKeyFile*gkf;
	GError*err = NULL;
	GFile*gfile;
	char*apfile, *etag_out;
	int length;
	gsize size;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
	g_return_val_if_fail (gerror == NULL || *gerror == NULL, 1);
	len = -1;
	converted = g_locale_to_utf8((const gchar *)str, len, &bytes_read, &bytes_written, &err);
	if ( err != NULL ) {
		g_propagate_error (gerror, err);
		return 1;
	}
	gfile = g_file_new_for_commandline_arg((const char*)converted);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &apfile, &size, &etag_out, &err) ) {
		if ( err != NULL ) {
			g_object_unref(gfile);
			g_propagate_error (gerror, err);
			return 1;
		}
		g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_METHODS,            /* error code */
                   "GFile error %s", /* error message format string */
                   str);
		g_object_unref(gfile);
		return 1;
	}
	g_free(converted);
#else
	g_return_val_if_fail (gerror == NULL || *gerror == NULL, 1);
	gfile = g_file_new_for_commandline_arg(str);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &apfile, &size, &etag_out, &err) ) {
		if ( err != NULL ) {
			g_object_unref(gfile);
			g_propagate_error (gerror, err);
			return 1;
		}
		g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_METHODS,            /* error code */
                   "GFile error %s", /* error message format string */
                   str);
		g_object_unref(gfile);
		return 1;
	}
#endif
	g_free(etag_out);
	g_object_unref(gfile);
	length = (int)size;
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)apfile, length, G_KEY_FILE_NONE, &err ) ) {
		if ( err != NULL ) {
			g_propagate_error (gerror, err);
			res = 1;
		} else {
			g_set_error (gerror,
                   KIMONO_WORKSPACE_ERROR,                 /* error domain */
                   KIMONO_WORKSPACE_ERROR_METHODS,            /* error code */
                   "GKeyFile error %s", /* error message format string */
                   str);
			res = 1;
		}
		return res;
	} else {
		g_free(apfile);
		res = workspace_get_compilation_infos(gkf, uidescr, inputs, outputs, ostring, &err);
		if ( err != NULL ) {
			g_propagate_error (gerror, err);
		}
	}
	g_key_file_free(gkf);
	return res;
}

