/***************************************************************************
 *            Port.h
 *
 *  Wed Feb 15 12:41:47 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _PORT_H
#define _PORT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Connection.h>

typedef struct Port {
	int ID;
	short isInput;
	int nConn;
	Connection **channel;
	char*label;
	PortType type;
	PortType meta_type;
	int index;
} Port;

char*type2string(PortType type);
PortType string2type(char*buffer);
char*type2string2(PortType type);
PortType string2type2(char*buffer);
Port* parsePorts(char*respond, int *nConns);
Port* portsParse(char*response, int *nConns);
PortType path2type2(char*buffer);
char*path2string2(char*path);
void ports_set_type(Port*p, char*ports_string);

#ifdef __cplusplus
}
#endif

#endif /* _PORT_H */
