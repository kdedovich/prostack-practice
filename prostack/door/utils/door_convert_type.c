/***************************************************************************
 *            door_convert_type.c
 *
 *  Thu Feb  2 10:06:19 2012
 *  Copyright  2012  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <unistd.h>
#include <Node.h>
#include <door_convert_type.h>

static char*input_types;
static char*output_types;
static char*conv_indices;

static GOptionEntry entries[] = 
{
	{ "door-convert-input-types", 0, 0, G_OPTION_ARG_STRING, &input_types, "Obligatory input types", "Comma-separated list of input types" },
	{ "door-convert-indices", 0, 0, G_OPTION_ARG_STRING, &conv_indices, "Obligatory indices", "Comma-separated list of indices" },
	{ "door-convert-output-types", 0, 0, G_OPTION_ARG_STRING, &output_types, "Obligatory output types", "Comma-separated list of output types" },
	{ NULL }
};


int main(int argc,char **argv)
{
	gchar*input_file_name;
	gchar*output_file_name;
	gchar*output_file_name_lock;
	gchar**input_exts, **output_exts, **conv_pos, **input_names, **ext, *p;
	int i, number_of_conversions = 0, index, next, child_exit_status;
	int have_locks = 0;
	int flaggs;
	int argcloc;
	char **argvloc;
	gchar**margv;
	int argcp;
	GString *command;
	GOptionContext *context;
	GError *gerror = NULL;
	context = g_option_context_new ("- convert data to correct type and call the operator");
	g_option_context_add_main_entries(context, (const GOptionEntry *)entries, "door");
	g_option_context_set_ignore_unknown_options(context, TRUE);
	if (!g_option_context_parse (context, &argc, &argv, &gerror)) {
		g_error ("option parsing failed: %s\n", gerror->message);
	}
	g_option_context_free (context);
	if ( input_types == NULL || conv_indices == NULL || output_types == NULL ) {
		g_error("%s called with wrong options", argv[0]);
	}
	input_names = g_strsplit( argv[ argc - 2 ], ",", MAXTOKENS);
	input_exts = g_strsplit(input_types, ",", MAXTOKENS);
	output_exts = g_strsplit(output_types, ",", MAXTOKENS);
	conv_pos = g_strsplit(conv_indices, ",", MAXTOKENS);
	number_of_conversions = g_strv_length(conv_pos);
	do {
		have_locks = 0;
		for ( i = 0; i < number_of_conversions; i++ ) {
			index = (int)g_strtod(conv_pos[i], NULL);
			input_file_name = g_strdup(input_names[index]);
			ext = g_strsplit(input_file_name, ".", MAXTOKENS);
			next = g_strv_length(ext);
			p = ext[ next - 1 ];
			ext[ next - 1 ] = g_strdup("lock");
			g_free(p);
			output_file_name_lock = g_strjoinv(".", ext);
			p = ext[ next - 1 ];
			ext[ next - 1 ] = g_strdup(output_exts[ i ]);
			g_free(p);
			output_file_name = g_strjoinv(".", ext);
			p = input_names[index];
			input_names[index] = g_strdup(output_file_name);
			g_free(p);
			g_strfreev(ext);
			if ( g_file_test((const gchar*)output_file_name_lock, G_FILE_TEST_EXISTS) ) {
				have_locks = 1;
				g_free(input_file_name);
				g_free(output_file_name);
				g_free(output_file_name_lock);
				continue;
			}
			if ( g_file_test((const gchar*)output_file_name, G_FILE_TEST_EXISTS) ) {
				g_free(input_file_name);
				g_free(output_file_name);
				g_free(output_file_name_lock);
				continue;
			}
			g_file_set_contents((const gchar *)output_file_name_lock, "", -1, NULL);
/*		"convert input_file_name output_file_name"
		here we probably should consult database
		for the name of the converter */
		/* FIXME */
/* image to tif case - use imagemagick */
			if ( ( !g_strcmp0 (input_exts[i], "jpg") || !g_strcmp0 (input_exts[i], "png") || !g_strcmp0 (input_exts[i], "bmp") || !g_strcmp0 (input_exts[i], "gif") ) && !g_strcmp0 (output_exts[i], "tif") ) {
				command = g_string_new("convert ");
				command = g_string_append(command, input_file_name);
				command = g_string_append_c(command, ' ');
				command = g_string_append(command, output_file_name);
			} else if ( !g_strcmp0 (input_exts[i], "lqr") && !g_strcmp0 (output_exts[i], "tif") ) {
/* rasdaman to tif case - use rascon package */
				command = g_string_new("lazyrasql -o write");
				command = g_string_append_c(command, ' ');
				command = g_string_append(command, input_file_name);
				command = g_string_append_c(command, ' ');
				command = g_string_append(command, output_file_name);
			}
			if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
				if ( gerror ) {
					g_error("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
				}
			}
			g_string_free(command, TRUE);
			flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL;
			flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;
/*		flaggs = G_SPAWN_SEARCH_PATH;*/
			if ( !g_spawn_sync (NULL, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, NULL, NULL, &child_exit_status, &gerror) ) {
				if ( gerror ) {
					g_error("g_spawn_sync failed for %s\nwith %s", argv[0], gerror->message);
				}
			}
			g_unlink((const gchar *)output_file_name_lock);
			g_strfreev(margv);
			g_free(input_file_name);
			g_free(output_file_name);
			g_free(output_file_name_lock);
		}
		g_usleep(G_USEC_PER_SEC);
	} while ( have_locks == 1 );
	argcloc = argc - 2;
	argvloc = g_new0(char*, argcloc + 1);
	for ( i = 0; i < argcloc - 2; i++ ) {
		argvloc[ i ] = argv[ i + 2 ];
	}
	p = g_strjoinv(",", input_names);
	argvloc[ argcloc - 2 ] = p;
	argvloc[ argcloc - 1 ] = argv[ argc - 1 ];
	argvloc[ argcloc ] = NULL;
	flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL;
	flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;
/*	flaggs = G_SPAWN_SEARCH_PATH;*/
	if ( !g_spawn_sync (NULL, argvloc, NULL, flaggs, NULL, NULL, NULL, NULL, &child_exit_status, &gerror) ) {
		if ( gerror ) {
			g_error("g_spawn_async failed for %s\nwith %s", argv[0], gerror->message);
		}
	}
	g_free(p);
	g_free(argvloc);
	return child_exit_status;
}
