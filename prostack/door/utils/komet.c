/***************************************************************************
 *            komet.c
 *
 *  Thu Feb 16 15:15:34 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Workspace.h>
#include <Kimono.h>
#include <glib.h>
#include <unistd.h>
#include <komet.h>
#include <locale.h>

Workspace*workspaceFromFile(char*filename);

static const char help[]     =

"Usage: komet [options] <operation> <input file> <output file>\n\n"

"Argument:\n"
"  <image>          input data file\n\n"

"Options:\n"
"  -h                  prints this help message\n"
"  -p <val>            parameter\n"
"  -r <n>              repetitions\n"
"  -s <x,x1,y,y1>      set geometry\n"
"  -v                  print version and compilation date\n"

"Please report bugs to <kozlov@spbcas.ru>. Thank you!\n";

static const char usage[]    =
"Usage: komet   [-h]\n"
"  -p <val>            parameter\n"
"  -r <n>              repetitions\n"
"  -s <x,x1,y,y1>      set geometry\n"
"                 [-v]\n";

static short isSetTracer = 1;
static short isSetError = 1;
static short isSetLogger = 1;

static int hasSpec;
static char*specFile;

static int hasSubst;
static char *inhosts = NULL;
static char *infiles = NULL;
static char *outhosts = NULL;
static char *outfiles = NULL;
static char *nstring = NULL;
static char *ostring = NULL;

static int batch = 0;
static int noexec = 0;
static int normalize = 0;
static int writeap = 0;
static int wkspjob = 0;
static int wksplib = 0;
static int oper = 0;
static char *operation;
static char *batch_string;

static int merge;
static int convert;

int ParseCommandLine(int argc, char**argv)
{
	int         c;                     /* used to parse command line options */
/* external declarations for command line option parsing (unistd.h) */
	extern char *optarg;                     /* command line option argument */
	extern int  optind;                /* pointer to current element of argv */
	extern int  optopt;               /* contain option character upon error */
/* set the version string */
	optarg = NULL;
	while( (c = getopt(argc, argv, OPTS)) != -1 ) {
		switch(c) {
			case 'c':
				convert = 1;
			break;
			case 'e':
				isSetError = 0;
			break;
			case 'h':                                            /* -h help option */
				g_error(help);
			break;
			case 'j':
				wkspjob = 1;
			break;
			case 'k':
				wksplib = 1;
			break;
			case 'l':
				isSetLogger = 0;
			break;
			case 'm':
				merge = 1;
			break;
			case 'n':
				noexec = 1;
			break;
			case 'o':
				oper = 1;
				operation = g_strdup(optarg);
			break;
			case 'p':
				batch = 1;
				batch_string = g_strdup(optarg);
			break;
			case 's':
				hasSpec = 1;
				specFile = g_strdup(optarg);
			break;
			case 't':
				isSetTracer = 0;
			break;
			case 'x':
				nstring = g_strdup(optarg);
			break;
			case 'y':
				ostring = g_strdup(optarg);
			break;
			case 'w':
				writeap = 1;
			break;
			case 'z':
				normalize = 1;
			break;
			case ':':
				g_error("komet: need an argument for option -%c", optopt);
			break;
			case '?':
			default:
				g_error("komet: unrecognized option -%c", optopt);
		}
	}
	return optind;
}

void e(char*msg)
{
	fprintf(stderr,"\n %s", msg);
	fflush(stderr);
}

void l(char*msg)
{
	fprintf(stdout,"\n %s", msg);
	fflush(stdout);
}

int opt_oper(int *optind, int argc, char**argv)
{
	char*inputs_list = NULL, *outputs_list = NULL;
	char**input_pointers = NULL, **output_pointers = NULL;
	int n_input_pointers = 0, n_output_pointers = 0;
	char*ws_string;
	GError*gerror = NULL;
	Workspace*ws;
	if ( (*optind) < argc ) {
		inputs_list = g_strdup(argv[(*optind)]);
		input_pointers = g_strsplit(inputs_list, ",", MAX_RECORD);
		if ( input_pointers ) {
			n_input_pointers = g_strv_length(input_pointers);
		}
		(*optind)++;
	}
	if ( (*optind) < argc ) {
		outputs_list = g_strdup(argv[(*optind)]);
		output_pointers = g_strsplit(outputs_list, ",", MAX_RECORD);
		if ( output_pointers ) {
			n_output_pointers = g_strv_length(output_pointers);
		}
	}
	ws_string = kimono_get_pam_info (operation, "apfile");
	if ( ws_string == NULL ) {
		g_error("No registered operation %s", operation);
	}
	if ( nstring == NULL && ostring != NULL ) {
		nstring = kimono_get_pam_info (operation, "plfile");
		if ( nstring == NULL ) {
			g_error("Operation %s doesn't have nodes for options %s", operation, ostring);
		}
	}
	ws = workspace_from_string(ws_string, strlen(ws_string), &gerror);
	if ( gerror != NULL ) {
		g_error(gerror->message);
	}
	if ( nstring != NULL && ostring != NULL ) {
		workspace_set_inputs(ws, nstring, ostring);
	}
	ws_set_file_save(ws, input_pointers, output_pointers, n_input_pointers, n_output_pointers);
	setNotifyNode(notifyNode);
	workspaceRun(ws);
	workspaceRmTemp(ws);
	kimono_shutdown();
	return 0;
}

int main(int argc,char **argv)
{
	int optind, ind; /* returned by getopt from ParseCommandLine: index of the first non-option argument */
	FILE*fp;
	char *fnam;
	gchar*gcontenttypeguess, *mimetypecontent;
	gboolean result_uncertain;
	char *envptr;
	Workspace*ws;
	hasSpec = 0;
	hasSubst = 0;
	merge = 0;
	convert = 0;
	g_setenv("LANG", setlocale (LC_ALL, ""), TRUE);
	kimono_init(&argc, &argv, NULL);
	fnam = (char *)calloc(MAX_RECORD, sizeof(char));
	optind = ParseCommandLine(argc, argv);
	if ( !oper && !normalize && !wkspjob && !merge && !convert && optind + 1 < argc - 1 && optind + 4 > argc - 1 ) {
		g_error("komet: incomplete command line for substitution");
	} else if ( optind + 1 < argc - 1 ) {
		hasSubst = 1;
	}
	if ( oper == 1 ) {
		return opt_oper (&optind, argc, argv);
	}
	if ( normalize == 1 ) {
		fnam = strcpy(fnam, argv[optind]);
		ws = workspace_from_file(fnam);
		workspaceNormalizeIDs(ws);
		fnam = strcpy(fnam, argv[optind + 1]);
		workspace_to_file(ws, fnam);
		kimono_shutdown();
		return 0;
	}
	if ( wkspjob == 1 ) {
		char *mname;
		char *user;
		char *inputs;
		char *outputs;
		char *wksplibtag;
		if ( optind + 4 > argc - 1 ) {
			g_error("komet wkspjob: command line error optind + 4 = %d, argc - 1 = %d", optind + 4, argc - 1);
		}
		mname = g_strdup(argv[optind]);
		user = g_strdup(argv[optind + 1]);
		inputs = g_strdup(argv[optind + 2]);
		outputs = g_strdup(argv[optind + 3]);
		wksplibtag = g_strdup(argv[optind + 4]);
		workspace_run_from_lib(mname, user, inputs, outputs, wksplibtag);
		kimono_shutdown();
		return 0;
	}
	if ( wksplib == 1 ) {
		char *inputs;
		char *outputs;
		char *wksplibtag;
		char *spath;
		char *message;
		char *server_string;
		Node*sys_node;
		if ( optind + 6 > argc - 1 ) {
			g_error("komet wksplib: command line error optind + 3 = %d, argc - 1 = %d", optind + 6, argc - 1);
		}
		inputs = g_strdup(argv[optind]);
		outputs = g_strdup(argv[optind + 1]);
		wksplibtag = g_strdup(argv[optind + 2]);
		fnam = strcpy(fnam, argv[optind + 3]);
		server_string = g_strdup(argv[optind + 4]);
		spath = g_strdup(argv[optind + 5]);
		message = g_strdup(argv[optind + 6]);
		ws = workspace_from_file(fnam);
		if ( !ws )
			g_error("%s not a valid workspace!\n", fnam);
		sys_node = proxy_sys_node("wksps");
		sys_node->oServer = olapserver_from_string (server_string);
		if ( !sys_node->oServer )
			g_error("%s not a valid server!\n", server_string);
		workspace_to_wksplib(ws, sys_node, inputs, outputs, wksplibtag, spath, message);
		free(sys_node->buf);
		proxy_sys_node_del(sys_node);
		kimono_shutdown();
		return 0;
	}
	if ( optind >= argc ) {
		g_error(usage);
	}
	fnam = strcpy(fnam, argv[optind]);
	gcontenttypeguess = g_content_type_guess ( (const gchar *)fnam, NULL, 0, &result_uncertain);
	mimetypecontent = g_content_type_get_mime_type ((const gchar *)gcontenttypeguess);
	g_warning("Type is: %s;", gcontenttypeguess);
	g_warning("Type is: %s;", mimetypecontent);
	if ( g_str_has_prefix ( (const gchar *)mimetypecontent, "image") ) {
		int arg_c;
		char**arg_v;
		int opt_ind;
		oper = 1;
		operation = g_strdup("default");
		opt_ind = 0;
		arg_c = 0;
		arg_v = g_new(char*, 1);
		arg_v[0] = NULL;
		ostring = fnam;
		return opt_oper (&opt_ind, arg_c, arg_v);
	}
	if ( isSetError && !batch ) {
		setError(e);
		setErrorNode(e);
		setErrorOlapServer(e);
		setErrorLon(l);
	}
	if ( isSetTracer && !batch ) {
		setTracer(traceText);
	}
	if ( isSetLogger && !batch ) {
		setLoggerNode(l);
		setLoggerOlapServer(l);
	}
	setNotifyNode(notifyNode);
	if ( convert ) {
		ws = workspaceFromFile(fnam);
		if ( !ws )
			g_error("%s not a valid workspace!\n", fnam);
		fnam = strcpy(fnam, argv[optind + 1]);
		workspace_to_file(ws, fnam);
		kimono_shutdown();
		return 0;
	}
	ws = workspace_from_file(fnam);
	if ( !ws ) {
		g_error("%s not a valid workspace!\n", fnam);
	}
	if ( merge ) {
		Workspace*addon;
		int wsWidth, wsHeight;
		fnam = strcpy(fnam, argv[optind + 1]);
		addon = workspace_from_file(fnam);
		if ( !addon )
			g_error("%s not a valid workspace!\n", fnam);
		getBbWorkspace(ws, &wsWidth, &wsHeight, 0);
		wsWidth = 20;
		workspaceTrimCoordinates(addon);
		workspaceNormalizeIDs(addon);
		workspaceCorrectMacroOnPaste(addon, 0);
		workspacePaste(ws, addon, wsWidth, wsHeight);
		fnam = strcpy(fnam, argv[optind + 2]);
		workspace_to_file(ws, fnam);
		kimono_shutdown();
		return 0;
	}
	if ( hasSpec ) {
		parseSpec(ws, specFile );
		free(specFile);
	}
	if ( hasSubst ) {
		int hacc;

		inhosts = (char*)calloc(MAX_RECORD, sizeof(char));
		infiles = (char*)calloc(MAX_RECORD, sizeof(char));
		outhosts = (char*)calloc(MAX_RECORD, sizeof(char));
		outfiles = (char*)calloc(MAX_RECORD, sizeof(char));
		inhosts = strcpy(inhosts, argv[optind + 1]);
		infiles = strcpy(infiles, argv[optind + 2]);
		outhosts = strcpy(outhosts, argv[optind + 3]);
		outfiles = strcpy(outfiles, argv[optind + 4]);
		hacc = strlen(inhosts);
		if ( inhosts[hacc - 1] == '/' ) {
			inhosts[hacc - 1] = '\0';
			inhosts = (char*)realloc(inhosts, hacc * sizeof(char));
		}
		parseSubst(ws, inhosts, infiles, outhosts, outfiles);
		free(inhosts);
		free(infiles);
		free(outhosts);
		free(outfiles);
	}
	if ( batch ) {
		FILE*fp;
		g_message("batch_string: %s", batch_string);
		char *address, *mountpoint, *directory, *server, *logfile, *host, *statefile, *send_cmd;
		gchar**settings, *dir_entry;
		int kounter, num_ins, len, clen, port;
		char **inses;
		char *first_id;
		Node *ne;
		GDir *dirptr;
		settings = g_strsplit((const gchar*)batch_string, ",", -1);
		if ( !settings[0] ) {
			g_error("Wrong address after fork");
		} else {
			address = g_strdup(settings[0]);
			g_message("address: %s", address);
		}
		if ( !settings[1] ) {
			g_error("Wrong mountpoint after fork");
		} else {
			mountpoint = g_strdup(settings[1]);
			g_message("mountpoint: %s", mountpoint);
		}
		if ( !settings[2] ) {
			g_error("Wrong directory after fork");
		} else {
			directory = g_strdup(settings[2]);
			g_message("directory: %s", directory);
		}
		if ( !settings[3] ) {
			g_error("Wrong server after fork");
		} else {
			server = g_strdup(settings[3]);
			g_message("server: %s", server);
		}
		g_strfreev(settings);
		settings = g_strsplit((const gchar*)server, ":", -1);
		if ( settings[0] ) {
			host = g_strdup(settings[0]);
			g_message("host: %s", host);
		} else {
			g_error("Wrong host after fork");
		}
		if ( settings[1] ) {
			port = atoi((char*)settings[1]);
			g_message("port: %d", port);
		} else {
			g_error("Wrong port after fork");
		}
		g_strfreev(settings);
#ifndef G_OS_WIN32
		logfile = (char*)calloc(MAX_RECORD, sizeof(char));
		sprintf(logfile, "%s/komet_%d.log", mountpoint, (int)getpid());
		statefile = (char*)calloc(MAX_RECORD, sizeof(char));
		sprintf(statefile, "%s/komet_%d.state", mountpoint, (int)getpid());
#else
		logfile = g_build_filename(mountpoint, "komet.log", NULL);
		g_message("log: %s", logfile);
		statefile = g_build_filename(mountpoint, "komet.state", NULL);
		g_message("state: %s", statefile);
#endif
		ws_set_host_port(ws, server, port);
		ne = ws_get_ins_node(ws);
		first_id = g_strdup(ne->buf);
		g_message("first_id: %s", first_id);
		len = strlen(first_id);
		fp = fopen(logfile, "a");
		if ( !fp ) {
			g_error("Can't use log %s", logfile);
		}
		fprintf(fp, "first id %s len %d\n", first_id, len);
		fclose(fp);
		dirptr = g_dir_open(directory, 0, NULL);
		if ( !dirptr ) {
			g_error("Wrong dirptr after fork");
		}
		num_ins = 0;
		inses = NULL;
		while ( ( dir_entry = (gchar*)g_dir_read_name(dirptr) ) ) {
			g_message("dir_entry: %s", dir_entry);
			if ( g_str_has_suffix((const gchar*)dir_entry, (const gchar*)first_id) ) {
				fp = fopen(logfile, "a");
				fprintf(fp, "%d;%s\n", num_ins, (char*)dir_entry);
				fclose(fp);
				inses = (char**)realloc(inses, ( num_ins + 1 ) * sizeof(char*) );
				inses[num_ins] = (char*)calloc(MAX_RECORD, sizeof(char));
				clen = strlen((char*)dir_entry);
				inses[num_ins] = strcpy(inses[num_ins], directory);
				inses[num_ins] = strcat(inses[num_ins], G_DIR_SEPARATOR_S);
				inses[num_ins] = strncat(inses[num_ins], (char*)dir_entry, clen - len);
				num_ins++;
			}
		}
		g_dir_close(dirptr);
		fp = fopen(statefile, "w");
		if ( !fp ) {
			g_error("Can't use state %s", statefile);
		}
		fprintf(fp, "%d from %d\n", 0, num_ins);
		fclose(fp);
		for (kounter = 0; kounter < num_ins; kounter++ ) {
			int failcounter = 0;
			fp = fopen(logfile,"a");
			fprintf(fp, "%d from %d %s starting -", kounter + 1, num_ins, inses[kounter]);
			fclose(fp);
			ws_set_ins_ous(ws, inses[kounter], mountpoint, host, port);
			if ( writeap ) {
				char *apname, *appath;
				char *bins;
				char*bfnam;
				bins = g_path_get_basename(inses[kounter]);
				bfnam = g_path_get_basename(fnam);
				apname = g_strdup_printf("%s_%s", bins, bfnam);
				appath = g_build_filename(mountpoint, apname, NULL);
				fp = fopen(logfile,"a");
				fprintf(fp, "%s-", appath);
				fclose(fp);
				workspace_to_file(ws, appath);
				g_free(bfnam);
				g_free(bins);
				g_free(apname);
				g_free(appath);
			}
			if ( !noexec ) {
				workspaceRun(ws);
				failcounter = ws_get_num_failed_nodes(ws);
				workspaceRmTemp(ws);
				workspaceCleanOutputs(ws);
			}
			fp = fopen(logfile,"a");
			fprintf(fp, " comleted with %d failed nodes\n", failcounter);
			fclose(fp);
			fp = fopen(statefile,"w");
			fprintf(fp, "%d from %d\n", kounter + 1, num_ins);
			fclose(fp);
		}
		remove(statefile);
#ifndef G_OS_WIN32
		send_cmd = (char*)calloc(MAX_RECORD, sizeof(char));
		sprintf(send_cmd, "cat '%s' | mail -s pid_%d %s", logfile, getpid(), address);
		system(send_cmd);
		free(send_cmd);
#endif
	} else {
		workspaceRun(ws);
		workspaceRmTemp(ws);
	}
	kimono_shutdown();
	return 0;
}

void parseSpec(Workspace*ws, char*specFile )
{
	char*record;
	char*buffer;
	ListOfNodes*curr;
	Node *ne;
	int nID;
	int len, lr;
	int i;
	FILE *fp;
	char c;
	fp = fopen(specFile, "r");
	if ( !fp )
		g_error("parseSpec: cann't open spec");
	record = (char*)calloc(MAX_RECORD, sizeof(char));
	while ( ( record = fgets(record, MAX_RECORD, fp ) ) ) {
		c = *record;
		if ( c == '#' )
			continue;
		i = 0;
		lr = strlen(record);
		buffer = str2ud(record, i, lr, &len);
		nID = atoi(buffer);
		free(buffer);
		ne = (Node*)NULL;
		for (curr = ws->listOfNodes; curr; curr = curr->next) {
			if ( curr->node->ID == nID ) {
				ne = curr->node;
				break;
			}
		}
		if ( !ne )
			g_error("parseSpec: wrong id");
		i += len + 1;
		buffer = str2ud(record, i, lr, &len);
		if ( len > 0 ) {
			ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
			ne->id = strcpy(ne->id, buffer);
		}
		free(buffer);
		i += len + 1;
		buffer = str2ud(record, i, lr, &len);
		if ( len > 0 ) {
			ne->file = (char*)realloc(ne->file, MAX_RECORD * sizeof(char));
			ne->file = strcpy(ne->file, buffer);
		}
		free(buffer);
		i += len + 1;
		buffer = str2ud(record, i, lr, &len);
		if ( len > 0 ) {
			ne->oServer->hname = (char*)realloc(ne->oServer->hname, MAX_RECORD * sizeof(char));
			ne->oServer->hname = strcpy(ne->oServer->hname, buffer);
		}
		free(buffer);
		i += len + 1;
		buffer = str2ud(record, i, lr, &len);
		if ( len > 0 ) {
			ne->oServer->port = atoi(buffer);
		}
		free(buffer);
	}
	free(record);
	fclose(fp);
}


void parseSubst(Workspace*ws, char*inhosts, char*infiles, char*outhosts, char*outfiles)
{
	char*buffer;
	char*host;
	char*port;
	ListOfNodes*curr;
	Node *ne;
	int lenh, lrh;
	int i;
	int lenf, lrf;
	int j;
	int olenh, olrh;
	int oi;
	int olenf, olrf;
	int oj;
	i = 0;
	j = 0;
	oi = 0;
	oj = 0;
	lrh = strlen(inhosts);
	lrf = strlen(infiles);
	olrh = strlen(outhosts);
	olrf = strlen(outfiles);
	for (curr = ws->listOfNodes; curr; curr = curr->next) {
		if ( curr->node->type == file ) {
			if ( i == lrh )
				g_error("komet: no inhost to substitue");
			if ( j == lrf )
				g_error("komet: no infile to substitue");
			ne = curr->node;
			buffer = str2ud2(inhosts, i, lrh, &lenh);
			i += lenh + 1;
			ne->oServer->port = atoi( &buffer[lenh - 4] );
			buffer = (char*)realloc(buffer, (lenh - 4) * sizeof(char) );
			buffer[lenh - 5] = '\0';
			ne->oServer->hname = (char*)realloc(ne->oServer->hname, MAX_RECORD * sizeof(char));
			ne->oServer->hname = strcpy(ne->oServer->hname, buffer);
			free(buffer);
			buffer = str2ud2(infiles, j, lrf, &lenf);
			j += lenf + 1;
			ne->file = (char*)realloc(ne->file, MAX_RECORD * sizeof(char));
			ne->file = strcpy(ne->file, buffer);
			free(buffer);
		} else if ( curr->node->type == pam && !strcmp(curr->node->pam->name, "save") ) {
			if ( oi == olrh )
				g_error("komet: no outhost to substitue");
			if ( oj == olrf )
				g_error("komet: no outfile to substitue");
			ne = curr->node;
			buffer = str2ud2(outhosts, oi, olrh, &olenh);
			oi += olenh + 1;
			ne->oServer->port = atoi( &buffer[olenh - 4] );
			buffer = (char*)realloc(buffer, (olenh - 4) * sizeof(char) );
			buffer[olenh - 5] = '\0';
			ne->oServer->hname = (char*)realloc(ne->oServer->hname, MAX_RECORD * sizeof(char));
			ne->oServer->hname = strcpy(ne->oServer->hname, buffer);
			free(buffer);
			buffer = str2ud2(outfiles, oj, olrf, &olenf);
			oj += olenf + 1;
			ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
			ne->id = strcpy(ne->id, buffer);
			free(buffer);
		}
	}
}
