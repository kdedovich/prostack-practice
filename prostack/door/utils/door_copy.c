/***************************************************************************
 *            door_copy.c
 *
 *  Thu Nov 19 16:38:47 2009
 *  Copyright  2009  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <unistd.h>
#include <Node.h>
#include <door_copy.h>

int main(int argc,char **argv)
{
	gchar*input_file_name;
	gchar*output_file_name;
	GError *gerror = NULL;
#ifdef G_OS_WIN32
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
	gchar*converted;
#endif
	if ( argc < 2 ) {
		return -1;
	}
	if ( argc > 2 && !strcmp(argv[argc - 3], "--reverse")) {
#ifdef G_OS_WIN32
		len = -1;
		converted = g_locale_to_utf8((const gchar *)argv[argc - 1], len, &bytes_read, &bytes_written, &gerror);
		if ( gerror ) {
			g_error("Can't convert file name: %s", gerror->message);
		}
		g_free(converted);
		len = -1;
		converted = g_locale_to_utf8((const gchar *)argv[argc - 2], len, &bytes_read, &bytes_written, &gerror);
		if ( gerror ) {
			g_error("Can't convert file name: %s", gerror->message);
		}
		g_free(converted);
		input_file_name = g_strdup(argv[argc - 1]);
		output_file_name = g_strdup(argv[argc - 2]);
#else
		input_file_name = g_strdup(argv[argc - 1]);
		output_file_name = g_strdup(argv[argc - 2]);
#endif
		copy_big_files(input_file_name, output_file_name);
	} else {
#ifdef G_OS_WIN32
		len = -1;
		converted = g_locale_to_utf8((const gchar *)argv[argc - 2], len, &bytes_read, &bytes_written, &gerror);
		if ( gerror ) {
			g_error("Can't convert file name: %s", gerror->message);
		}
		g_free(converted);
		len = -1;
		converted = g_locale_to_utf8((const gchar *)argv[argc - 1], len, &bytes_read, &bytes_written, &gerror);
		if ( gerror ) {
			g_error("Can't convert file name: %s", gerror->message);
		}
		g_free(converted);
		input_file_name = g_strdup(argv[argc - 2]);
		output_file_name = g_strdup(argv[argc - 1]);
#else
		input_file_name = g_strdup(argv[argc - 2]);
		output_file_name = g_strdup(argv[argc - 1]);
#endif
		copy_big_files(input_file_name, output_file_name);
	}
	return 0;
}
