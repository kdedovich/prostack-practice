/***************************************************************************
                          Pattern.c  -  description
                             -------------------
    begin                : ðÔÎ íÁÊ 14 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <float.h>
#include <string.h>
#include <ctype.h>
#include <glib.h>
#include <Pattern.h>

static int ndigits = 2;

Pattern*pattern_new(int nnucs, double xstart, double xend, double ystart, double yend)
{
	Pattern*p;
	p = (Pattern *)malloc(sizeof(Pattern));
	p->xrange = Range_create(xstart, xend);
	p->yrange = Range_create(ystart, yend);
	p->nnucs = nnucs;
	p->array = (DataPoint*)calloc(p->nnucs, sizeof(DataPoint));
	p->ngenes = 0;
	p->channel = (int*)NULL;
	p->gene_ids = (char*)NULL;
	return p;
}

double pattern_get_prot_conc_val(Pattern*p, int label, int channel)
{
	int i;
	double val;
	for( i = 0; i < p->nnucs; i++ ) {
		if ( p->array[i].nuc_number == label) {
			val = p->array[i].prot_conc[channel];
		}
	}
	return val;
}

int com(const void*a,const void*s)
{
	DataPoint *z;
	DataPoint *y;
	z = ( DataPoint*)a;
	y = ( DataPoint*)s;
	if ( z->AP_coord < y->AP_coord ) return -1;
	if ( z->AP_coord == y->AP_coord ) return 0;
	if ( z->AP_coord > y->AP_coord ) return 1;
}

int com_nuc(const void*a,const void*s)
{
	DataPoint *z;
	DataPoint *y;
	z = ( DataPoint*)a;
	y = ( DataPoint*)s;
	if ( z->nuc_number < y->nuc_number ) 
		return -1;
	else if ( z->nuc_number > y->nuc_number ) 
		return 1;
	return 0;
}

void Pattern_write(FILE*fp,Pattern*p)
{
	int i;
	int j;
	double precision = (double)ndigits;
	double prec0 = pow(10.0, precision);
	double prec = 10.0 * prec0;
	qsort((void*)p->array, p->nnucs, sizeof(DataPoint), com_nuc);
	for( i = 0; i < p->nnucs; i++ ) {
		fprintf(fp," %6d",p->array[i].nuc_number);
		fprintf(fp," %*.*f %*.*f", ndigits + 6, ndigits , p->array[i].AP_coord, ndigits + 6, ndigits, p->array[i].DV_coord);
		for( j = 0; j < p->ngenes; j++ ) {
			double conc = p->array[i].prot_conc[j];
			conc = floor( conc * prec ) / prec;
			conc = floor( conc * prec0 + 0.5 ) / prec0;
/*			fprintf(fp," %*.*f",ndigits+6,ndigits,p->array[i].prot_conc[j]);*/
			fprintf(fp," %*.*f", ndigits + 6, ndigits , conc);
		}
		fprintf(fp,"\n");
	}
}


void Pattern_delete(Pattern *p)
{
	int i;
	for ( i = 0; i < p->nnucs; i++ ) {
		free(p->array[i].prot_conc);
	}
	Range_delete(p->xrange);
	Range_delete(p->yrange);
	free(p->array);
	free(p->gene_ids);
	free(p->channel);
	free(p);
}

Range*Range_create(double start,double end)
{
	Range*p;
	p = (Range*)malloc(sizeof(Range));
	p->start = start;
	p->end = end;
	return p;
}

void Range_delete(Range*p)
{
	free(p);
}

Pattern *Pattern_create(FILE*fp, int channel)
{
	Pattern *p;
	double*tempdata;
	double d_ptr;
	double tempnumber;
	int i;
	int chan;
	int ngenes;
	int num_columns_in_line;
	char*base;
	int lead_punct;
	int c;
	char*record;
	char       *fmt  = NULL;             /* used as format for sscanf        */
	char       *skip = NULL;             /* skip this stuff!                 */
	const char init_fmt[] = "%*lg ";      /* initial format string            */
	const char skip_fmt[] = "%*lg ";     /* skip one more float              */
	const char read_fmt[] = "%lg ";      /* skip one more float              */
	base    = (char *)calloc(MAX_RECORD, sizeof(char));
	fmt     = (char *)calloc(MAX_RECORD, sizeof(char));
	skip    = (char *)calloc(MAX_RECORD, sizeof(char));
	p = (Pattern *)malloc(sizeof(Pattern));
	p->xrange = Range_create(0,0);
	p->yrange = Range_create(0,0);
	p->array = NULL;
	p->nnucs = 0;
	p->ngenes = 0;
	while ( (base=fgets(base, MAX_RECORD, fp)) ) {
		record     = base;                  /* base always points to start   */
		lead_punct = 0;                     /* of string                     */
/* while loop: parses and processes each line from the data file *************/
		c=(int)*record;
		while ( c != '\0' ) {
			if( isdigit(c) ) {                            /* number means data */
				record = base;                  /* reset pointer to start of str */
				if ( 1 != sscanf(record, "%lg ", &tempnumber) )
					g_error("Pattern Pattern_createWith: error reading %s", base);
				tempdata = NULL;
				num_columns_in_line=0;
				skip = strcpy(skip, init_fmt); /* format to be skipped by sscanf */
				fmt  = strcpy(fmt, skip);     /* all this stuff here is to get */
				fmt  = strcat(fmt, read_fmt);  /* the fmt string for sscanf to */
				while ( 1 == sscanf(record, (const char *)fmt, &d_ptr) ) {
					tempdata = (double*)realloc(tempdata, ( num_columns_in_line + 1 ) * sizeof(double));
					tempdata [num_columns_in_line] = d_ptr;
					num_columns_in_line++;
					skip = strcat(skip, skip_fmt);           /* the correct format */
					fmt  = strcpy(fmt, skip);     /* all this stuff here is to get */
					fmt  = strcat(fmt, read_fmt);  /* the fmt string for sscanf to */
				}
				ngenes = num_columns_in_line - 2;
				if ( p->ngenes == 0 )
					p->ngenes = ngenes;
				else if ( p->ngenes != ngenes )
					g_error("Pattern Pattern_create: length of line changed");
				p->array = (DataPoint*)realloc(p->array, (p->nnucs+1)*sizeof(DataPoint));
				p->array[p->nnucs].nuc_number = (int)tempnumber;
				p->array[p->nnucs].AP_coord = tempdata[0];
				p->array[p->nnucs].DV_coord = tempdata[1];
				p->array[p->nnucs].prot_conc = (double*)calloc(ngenes,sizeof(double));
				chan = channel+1; /* number of column for eve in tempdata */
				if ( chan >= ngenes )
					g_error("Pattern Pattern_create: no channel %d in datafile", channel);
				d_ptr = tempdata[2];
				tempdata[2] = tempdata[chan];
				tempdata[chan] = d_ptr;
				for ( i = 0; i < ngenes; i++ ) {
					p->array[p->nnucs].prot_conc[i]=tempdata[i+2];
				}
				p->nnucs++;
				free(tempdata);
				break;
			} else if ( isalpha(c) ) {                    /* letter means comment */
/* put comment to annotation struct */
				break;
			} else if ( c == '-' ) {                /* next two elsifs for punct */
				if ( ((int)*(record+1)) == '.')
					record++;
				lead_punct = 1;
				c=(int)*(++record);
			} else if ( c == '.' ) {
				lead_punct = 1;
				c=(int)*(++record);
			} else if ( ispunct(c) ){               /* other punct means comment */
/* put comment to annotation struct */
				break;
			} else if ( isspace(c) ) {             /* ignore leading white space */
				if ( lead_punct ) {              /* white space after punct means */
					break;                                              /* comment */
/* put comment to annotation struct */
				} else {
					c=(int)*(++record);            /* get next character in record */
				}
			} else {
				g_error("Pattern Pattern_createWith: Illegal character in %s", base);
			}
		}/* while \0 */
	}/* while base */
	p->channel = (int*)calloc(ngenes,sizeof(int));
	p->gene_ids = (char*)calloc(ngenes,sizeof(char));
	p->channel[0] = channel;
	for( i = 1; i < ngenes; i++ ) {
		if ( channel != i )
			p->channel[i] = i;
	}
	free(base);
	free(fmt);
	free(skip);
	qsort((void*)p->array,p->nnucs,sizeof(DataPoint),com);
	return p;
}
