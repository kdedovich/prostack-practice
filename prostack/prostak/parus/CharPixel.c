/***************************************************************************
                          CharPixel.c  -  description
                             -------------------
    begin                : ðÔÎ íÁÒ 25 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>


#include <math.h>
#include <grf.h>
#include <CharImage.h>
#include <CharVolume.h>
#include <CharPixel.h>
#include <ImageInfo.h>
#include <Pattern.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_cdf.h>

#include <complex.h>
#include <fftw3.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_cblas.h>

CharPixel*NewCharPixel(uint32 row, uint32 column, unsigned char intensity)
{
	CharPixel*cp;

	cp = (CharPixel*)malloc(sizeof(CharPixel));

	cp->row = row;
	cp->column = column;
	cp->intensity = intensity;
	cp->label = INIT;
	cp->distance = 0;
	return cp;
}


CharPixel*NewFictitiousCharPixel()
{
	CharPixel*cp;
	cp = (CharPixel*)malloc(sizeof(CharPixel));
	cp->label = FICTITIOUS;
	return cp;
}


int compare(const void *a, const void *b)
{
	CharPixel**x;
	CharPixel**y;

	CharPixel*x1;
	CharPixel*y1;

	x = (CharPixel**)a;
	y = (CharPixel**)b;

	x1 = *x;
	y1 = *y;

	if ( x1->intensity > y1->intensity ) return 1;
	if ( x1->intensity < y1->intensity ) return -1;
	return 0;
}


CharPixelStructure*NewCharPixelStructure(CharImage*ci)
{
	CharPixelStructure*cps;
	uint32 row;
	uint32 column;
	uint32 index;

	cps = (CharPixelStructure*)malloc(sizeof(CharPixelStructure));
	cps->pixels = (CharPixel**)calloc(ci->number, sizeof(CharPixel*));
	cps->size = ci->number;

	for ( row = 0; row < ci->rows ; row++ ) {
		for ( column = 0; column < ci->columns ; column++ ) {
			index = row * ci->columns + column;
			cps->pixels[index] = NewCharPixel(row,column,ci->pixels[index]);
			cps->pixels[index]->index = index;
		}
	}

	for ( row = 1; row < ci->rows - 1 ; row++ ) {
		for ( column = 1; column < ci->columns - 1 ; column++ ) {
			index = row * ci->columns + column;
			cps->pixels[index]->Nneigh = 4;
			cps->pixels[index]->neigh = (struct CharPixel**)calloc(4, sizeof(struct CharPixel*));
			cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
			cps->pixels[index]->neigh[1] = cps->pixels[index + ci->columns];
			cps->pixels[index]->neigh[2] = cps->pixels[index - 1];
			cps->pixels[index]->neigh[3] = cps->pixels[index + 1];
		}
	}

	row = 0;
	for ( column = 1; column < ci->columns - 1 ; column++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 3;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index + ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index - 1];
		cps->pixels[index]->neigh[2] = cps->pixels[index + 1];
	}

	row = ci->rows - 1;
	for ( column = 1; column < ci->columns - 1 ; column++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 3;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index - 1];
		cps->pixels[index]->neigh[2] = cps->pixels[index + 1];
	}

	column = 0;
	for ( row = 1; row < ci->rows - 1 ; row++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 3;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index + ci->columns];
		cps->pixels[index]->neigh[2] = cps->pixels[index + 1];
	}

	column = ci->columns - 1;
	for ( row = 1; row < ci->rows - 1 ; row++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 3;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index + ci->columns];
		cps->pixels[index]->neigh[2] = cps->pixels[index - 1];
	}

	row = 0;
	column = 0;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 2;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(2, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index + ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index + 1];


	row = 0;
	column = ci->columns - 1;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 2;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(2, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index + ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index - 1];


	row = ci->rows - 1;
	column = ci->columns - 1;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 2;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(2, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index - 1];


	row = ci->rows - 1;
	column = 0;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 2;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(2, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index + 1];
/*
	qsort((void *)cps->pixels,(size_t) cps->size,(size_t) sizeof(CharPixel*), compare);
*/
	return cps;
}

CharImage*CharImageFastWaterShed(CharImage*di, int connectivity)
{
	CharImage *wshd;
	CharPixelStructure *cps;
	CharPixel *cp;
	int IntensityLevel, HMIN, HMAX;
	int current_dist;
	int index1, index2;
	uint32 i, j;
	int current_label;
	fifo *queue;

	if ( connectivity == 4 )
		cps = NewCharPixelStructure(di);
	else if ( connectivity == 8 )
		cps = NewCharPixelStructure8(di);

	qsort((void *)cps->pixels,(size_t) cps->size,(size_t) sizeof(CharPixel*), compare);
	current_label = 0;
	current_dist = 0;
	index1 = 0;
	index2 = 0;
	HMIN = (int)cps->pixels[0]->intensity;
	HMAX = (int)cps->pixels[cps->size-1]->intensity;
	queue = fifo_new();
	for ( IntensityLevel = HMIN; IntensityLevel <= HMAX; IntensityLevel++ ) {
		for ( i = index1; i < cps->size; i++) {
			cp = cps->pixels[i];
			if ( (int)cp->intensity != IntensityLevel ) {
				index1 = i;
				break;
			}
			cp->label = MASK;
			for ( j = 0; j < cp->Nneigh; j++) {
				if ( cp->neigh[j]->label >= WSHED ) {
					cp->distance = 1;
					fifo_add(queue, cp);
				}
			}
		}/* end for */
		current_dist = 1;
		fifo_add_fictitious(queue);
		while (1) {
			int flag  = 0;
			cp = fifo_remove(queue);
			if ( cp->label == FICTITIOUS ) {
				if ( fifo_empty(queue) ) {
					break;
				} else {
					fifo_add_fictitious(queue);
					current_dist++;
					cp = fifo_remove(queue);
				}
			}
			for ( j = 0; j < cp->Nneigh; j++) {
				if ( cp->neigh[j]->distance <= current_dist && cp->neigh[j]->label >= WSHED ) {
					if ( cp->neigh[j]->label > WSHED ) {
						if ( cp->label == MASK || cp->label == WSHED ) {
							if ( !flag ) cp->label = cp->neigh[j]->label;
						} else if ( cp->label != cp->neigh[j]->label ) {
							cp->label = WSHED;
							flag = 1;
						}
					} else if ( cp->label == MASK ) {
						cp->label = WSHED;
					}
				} else if ( cp->neigh[j]->label == MASK && cp->neigh[j]->distance == 0 ) {
					cp->neigh[j]->distance = current_dist + 1;
					fifo_add(queue, cp->neigh[j]);
				}
			}
		}/* end while */
		for ( i = index2; i < cps->size; i++) {
			cp = cps->pixels[i];
			if ( (int)cp->intensity != IntensityLevel ) {
				index2 = i;
				break;
			}
			cp->distance = 0;
			if ( cp->label == MASK ) {
				current_label++;
				cp->label = current_label;
				fifo_add(queue, cp);
				while ( !fifo_empty(queue) ) {
					cp = fifo_remove(queue);
					for ( j = 0; j < cp->Nneigh; j++) {
						if ( cp->neigh[j]->label == MASK ) {
							fifo_add(queue, cp->neigh[j]);
							cp->neigh[j]->label = current_label;
						}
					}
				}
			}
		}
	}/* end main for */
/*
fprintf(stdout,"\n label: %d",current_label);
*/
	current_label++;
	wshd = CharImageClone(di);
	for ( i = 0; i < di->number; i++) {
		wshd->pixels[cps->pixels[i]->index] = ( cps->pixels[i]->label == WSHED && !CharPixelAllNeighWSHED(cps->pixels[i]) ) ? EmptyPixel:FilledPixel;
	}
/*	fifo_delete(queue);
This causes memory leakage...but
*/
	CharPixelStructureDelete(cps);
	return wshd;
}

int CharPixelAllNeighWSHED(CharPixel *cp)
{
	int i;
	for ( i = 0; i < cp->Nneigh; i++)
		if ( cp->neigh[i]->label != WSHED )
			return 0;
	return 1;
}

void fifo_delete(fifo*queue)
{
	if(queue->head)List_delete(queue->head);
	free(queue);
}

void List_delete(List*head)
{
	if ( head->next )
		List_delete(head->next);
	free(head);
}

fifo*fifo_new()
{
	fifo*q;

	q = (fifo*)malloc(sizeof(fifo));
	q->head = (List*)NULL;
	q->tail = (List*)NULL;
	q->size = 0;

	return q;
}

void fifo_add(fifo*queue, CharPixel*cp)
{
	List*p;

	p = (List*)malloc(sizeof(List));
	p->pixel = cp;

	if ( queue->head ) {
		p->next = queue->head;
		p->next->prev = p;
	} else {
		p->next = (List*)NULL;
	}
	p->prev = (List*)NULL;
	queue->head = p;
	queue->size++;
	if (!queue->tail) queue->tail = p;
/*
fprintf(stdout,"\nfifo_add: c: %u ; r: %u ; size: %d ; label: %d ",cp->row,cp->column,queue->size, cp->label);
*/
}

CharPixel*fifo_remove(fifo*queue)
{
	CharPixel*cp;
	List*t;

	t = queue->tail;
	cp = t->pixel;

	queue->tail = queue->tail->prev;
	if (queue->tail) queue->tail->next = (List*)NULL;
	free(t);
	queue->size--;

	return cp;
}

void fifo_add_fictitious(fifo*queue)
{
	List*p;

	p = (List*)malloc(sizeof(List));
	p->pixel = NewFictitiousCharPixel();

	if ( queue->head ) {
		p->next = queue->head;
		p->next->prev = p;
	} else {
		p->next = (List*)NULL;
	}
	p->prev = (List*)NULL;
	queue->head = p;
	queue->size++;
	if (!queue->tail) queue->tail = p;
}

int fifo_empty(fifo*queue)
{
	if (queue->size == 0) return 1;
	return 0;
}

void CharPixelStructureDelete(CharPixelStructure*cps)
{
	uint32 i;

	for ( i = 0; i < cps->size; i++ ) {
		CharPixelDelete(cps->pixels[i]);
	}
	free(cps->pixels);
	free(cps);
}

void CharPixelDelete(CharPixel*cp)
{
	free(cp->neigh);
	free(cp);
}

List*InitList(CharPixel*cp)
{
	List*p;

	p = (List*)malloc(sizeof(List));
	p->pixel = cp;
	p->next = (List*)NULL;

	return p;
}

void ListTour(List*plist)
{
	List*cur;
	uint32 i;

	cur = plist;
	while (cur) {
		CharPixel*cp;
		cp = cur->pixel;
		for ( i = 0; i < cp->Nneigh; i++ ) {
			if ( cp->neigh[i]->intensity == FilledPixel && cp->neigh[i]->distance == 0 ) {
				ListAdd(plist,cp->neigh[i]);
				cp->neigh[i]->distance = 1;
			}
		}
		cur = cur->next;
	}
}

void ListAdd(List*plist, CharPixel*cp)
{
	List*cur,*targ;
	cur = plist;
	while (cur) {
		targ = cur;
		cur = cur->next;
	}
	targ->next = InitList(cp);
}


void MarkList(List*plist, int label)
{
	List*cur;
	cur = plist;
	while (cur) {
		cur->pixel->label = label;
/*
fprintf(stdout,"\n row = %u; column = %u",cur->pixel->row,cur->pixel->column);
*/
		cur = cur->next;
	}
/*
fprintf(stdout,"\n ************************");
*/
}

Blob*NewBlob(List*plist, CharImage*data, int label)
{
	Blob*b;
	List*cur;

	b = (Blob*)malloc(sizeof(Blob));
	b->energid_pixels = (uint32*)NULL;
	b->Nenergid_pixels = 0;
	b->nuclear_pixels = (uint32*)NULL;
	b->Nnuclear_pixels = 0;
	cur = plist;
	while (cur) {
		b->energid_pixels = (uint32*)realloc(b->energid_pixels, (b->Nenergid_pixels + 1) * sizeof(uint32));
		b->energid_pixels[b->Nenergid_pixels] = cur->pixel->index;
		b->Nenergid_pixels++;
		if ( data->pixels[cur->pixel->index] > EmptyPixel ) {
			b->nuclear_pixels = (uint32*)realloc(b->nuclear_pixels, (b->Nnuclear_pixels + 1) * sizeof(uint32));
			b->nuclear_pixels[b->Nnuclear_pixels] = cur->pixel->index;
			b->Nnuclear_pixels++;
		}
		cur = cur->next;
	}
/*
fprintf(stdout,"\n %u %u", b->Nnuclear_pixels, b->Nenergid_pixels);
*/
	b->cm = NewCentroid(b, data->columns, data->rows);
	b->label = label;
/*
fprintf(stdout,"\n c %u %7.3f %7.3f ", label, b->cm->x, b->cm->y);
fflush(stdout);*/
	return b;
}

CharImage*CharImageFillFeature(CharImage*dp, int connectivity, int can_touch)
{
	CharPixelStructure *cps;
	CharPixel *cp;
	uint32 i, j, row, column;
	int current_label;
	int index;
	List*plist;
	int max_corrections;

	max_corrections = 1;
	if ( can_touch )
		max_corrections = 2;
	if ( connectivity == 4 )
		cps = NewCharPixelStructure(dp);
	else if ( connectivity == 8 )
		cps = NewCharPixelStructure8(dp);
	current_label = 0;

	for ( i = 0; i < dp->number; i++ ) {
		cp = cps->pixels[i];
		if ( cp->intensity == FilledPixel && cp->distance == 0 ) {
			plist = InitList(cp);
			cp->distance = 1;
			for ( j = 0; j < cp->Nneigh; j++ ) {
				if ( cp->neigh[j]->intensity == FilledPixel && cp->neigh[j]->distance == 0 ) {
					ListAdd(plist, cp->neigh[j]);
					cp->neigh[j]->distance = 1;
				}
			}
			ListTour(plist);
			MarkList(plist, current_label);
			List_delete(plist);
			current_label++;
		}
	}
/*
fprintf(stdout,"\n label: %u", current_label);
fflush(stdout);
*/
	for ( row = 0; row < dp->rows; row++ ) {
		for ( column = 0; column < dp->columns; column++ ) {
			int label1, label2;
			int label3, label4;
			int corrections = 0;
			i = row * dp->columns + column;
			cp = cps->pixels[i];
			if ( cp->intensity == EmptyPixel &&  cp->label == INIT ) {
				index = column;
				j = row * dp->columns + index;
				label1 = INIT;
				while ( index >= 0 && cps->pixels[j]->label == INIT ) {
					index--;
					j = row * dp->columns + index;
				}
				if ( index >= 0 ) {
					label1 = cps->pixels[j]->label;
				} else {
					label1 = (int)FICTITIOUS;
				}
				index = column;
				j = row * dp->columns + index;
				label2 = INIT;
				while ( index < dp->columns && cps->pixels[j]->label == INIT ) {
					index++;
					j = row * dp->columns + index;
				}
				if ( index < dp->columns ) {
					label2 = cps->pixels[j]->label;
				} else {
					label2 = (int)FICTITIOUS;
				}
				if ( label1 == (int)FICTITIOUS && label2 != (int)FICTITIOUS  && label2 != (int)INIT ) {
					label1 = label2;
					corrections++;
				} else if ( label2 == (int)FICTITIOUS && label1 != (int)FICTITIOUS  && label1 != (int)INIT ) {
					label2 = label1;
					corrections++;
				}
				index = row;
				j = index * dp->columns + column;
				label3 = INIT;
				while ( index >= 0 && cps->pixels[j]->label == INIT ) {
					index--;
					j = index * dp->columns + column;
				}
				if ( index >= 0 ) {
					label3 = cps->pixels[j]->label;
				} else {
					label3 = (int)FICTITIOUS;
				}
				index = row;
				j = index * dp->columns + column;
				label4 = INIT;
				while ( index < dp->rows && cps->pixels[j]->label == INIT ) {
					index++;
					j = index * dp->columns + column;
				}
				if ( index < dp->rows ) {
					label4 = cps->pixels[j]->label;
				} else {
					label4 = (int)FICTITIOUS;
				}
				if ( label3 == (int)FICTITIOUS && label4 != (int)FICTITIOUS  && label4 != (int)INIT ) {
					label3 = label4;
					corrections++;
				} else if ( label4 == (int)FICTITIOUS && label3 != (int)FICTITIOUS  && label3 != (int)INIT ) {
					label4 = label3;
					corrections++;
				}
				if ( corrections < max_corrections && label1 != INIT && label1 != FICTITIOUS && label1 == label2 && label1 == label3 && label1 == label4 ) {
					cp->label = label1;
				}
			}
		}
	}
	for ( i = 0; i < dp->number; i++ ) {
		dp->pixels[cps->pixels[i]->index] = ( cps->pixels[i]->label != INIT ) ? FilledPixel:EmptyPixel;
	}
	CharPixelStructureDelete(cps);
	return dp;
}


CharPixelStructure*NewCharPixelStructure8(CharImage*ci)
{
	CharPixelStructure*cps;
	uint32 row;
	uint32 column;
	uint32 index;

	cps = (CharPixelStructure*)malloc(sizeof(CharPixelStructure));
	cps->pixels = (CharPixel**)calloc(ci->number, sizeof(CharPixel*));
	cps->size = ci->number;

	for ( row = 0; row < ci->rows ; row++ ) {
		for ( column = 0; column < ci->columns ; column++ ) {
			index = row * ci->columns + column;
			cps->pixels[index] = NewCharPixel(row,column,ci->pixels[index]);
			cps->pixels[index]->index = index;
		}
	}

	for ( row = 1; row < ci->rows - 1 ; row++ ) {
		for ( column = 1; column < ci->columns - 1 ; column++ ) {
			index = row * ci->columns + column;
			cps->pixels[index]->Nneigh = 8;
			cps->pixels[index]->neigh = (struct CharPixel**)calloc(8, sizeof(struct CharPixel*));
			cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
			cps->pixels[index]->neigh[1] = cps->pixels[index + ci->columns];
			cps->pixels[index]->neigh[2] = cps->pixels[index - 1];
			cps->pixels[index]->neigh[3] = cps->pixels[index + 1];
			cps->pixels[index]->neigh[4] = cps->pixels[index - ci->columns - 1];
			cps->pixels[index]->neigh[5] = cps->pixels[index + ci->columns + 1];
			cps->pixels[index]->neigh[6] = cps->pixels[index + ci->columns - 1];
			cps->pixels[index]->neigh[7] = cps->pixels[index - ci->columns + 1];
		}
	}

	row = 0;
	for ( column = 1; column < ci->columns - 1 ; column++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 5;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(5, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index + ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index - 1];
		cps->pixels[index]->neigh[2] = cps->pixels[index + 1];
		cps->pixels[index]->neigh[3] = cps->pixels[index + ci->columns - 1];
		cps->pixels[index]->neigh[4] = cps->pixels[index + ci->columns + 1];
	}

	row = ci->rows - 1;
	for ( column = 1; column < ci->columns - 1 ; column++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 5;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(5, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index - 1];
		cps->pixels[index]->neigh[2] = cps->pixels[index + 1];
		cps->pixels[index]->neigh[3] = cps->pixels[index - ci->columns - 1];
		cps->pixels[index]->neigh[4] = cps->pixels[index - ci->columns + 1];
	}

	column = 0;
	for ( row = 1; row < ci->rows - 1 ; row++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 5;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(5, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index + ci->columns];
		cps->pixels[index]->neigh[2] = cps->pixels[index + 1];
		cps->pixels[index]->neigh[3] = cps->pixels[index + ci->columns + 1];
		cps->pixels[index]->neigh[4] = cps->pixels[index - ci->columns + 1];
	}

	column = ci->columns - 1;
	for ( row = 1; row < ci->rows - 1 ; row++ ) {
		index = row * ci->columns + column;
		cps->pixels[index]->Nneigh = 5;
		cps->pixels[index]->neigh = (struct CharPixel**)calloc(5, sizeof(struct CharPixel*));
		cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
		cps->pixels[index]->neigh[1] = cps->pixels[index + ci->columns];
		cps->pixels[index]->neigh[2] = cps->pixels[index - 1];
		cps->pixels[index]->neigh[3] = cps->pixels[index + ci->columns -1];
		cps->pixels[index]->neigh[4] = cps->pixels[index - ci->columns - 1];
	}

	row = 0;
	column = 0;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 3;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index + ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index + 1];
	cps->pixels[index]->neigh[2] = cps->pixels[index + ci->columns + 1];

	row = 0;
	column = ci->columns - 1;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 3;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index + ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index - 1];
	cps->pixels[index]->neigh[2] = cps->pixels[index + ci->columns - 1];

	row = ci->rows - 1;
	column = ci->columns - 1;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 3;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index - 1];
	cps->pixels[index]->neigh[2] = cps->pixels[index - ci->columns - 1];

	row = ci->rows - 1;
	column = 0;
	index = row * ci->columns + column;
	cps->pixels[index]->Nneigh = 3;
	cps->pixels[index]->neigh = (struct CharPixel**)calloc(3, sizeof(struct CharPixel*));
	cps->pixels[index]->neigh[0] = cps->pixels[index - ci->columns];
	cps->pixels[index]->neigh[1] = cps->pixels[index + 1];
	cps->pixels[index]->neigh[2] = cps->pixels[index - ci->columns + 1];
/*
	qsort((void *)cps->pixels,(size_t) cps->size,(size_t) sizeof(CharPixel*), compare);
*/
	return cps;
}

BlobImage*blob(BlobInfo*bi, CharImage*d, CharImage*data, int connectivity)
{
	BlobImage*bimage;
	CharImage*wtsd;
	CharPixelStructure*cps;
	CharPixel*cp;
	uint32 i, j;
	int current_label;

	bimage = (BlobImage*)malloc(sizeof(BlobImage));
	if ( d ) {
		wtsd = d;
		bimage->type = nuclei_and_energids;
	} else {
		wtsd = data;
		bimage->type = only_nuclei;
	}

	bimage->columns = wtsd->columns;
	bimage->rows = wtsd->rows;
	bimage->NBlobs = 0;
	bimage->list = (BlobList*)NULL;
	if ( connectivity == 8 )
		cps = NewCharPixelStructure8(wtsd);
	else
		cps = NewCharPixelStructure(wtsd);
	current_label = 0;
	for ( i = 0; i < wtsd->number; i++ ) {
		List*plist;
		Blob*b;
		cp = cps->pixels[i];
		if ( cp->intensity == FilledPixel && cp->distance == 0 ) {
			plist = InitList(cp);
			cp->distance = 1;
			for ( j = 0; j < cp->Nneigh; j++ ) {
				if ( cp->neigh[j]->intensity == FilledPixel && cp->neigh[j]->distance == 0 ) {
					ListAdd(plist,cp->neigh[j]);
					cp->neigh[j]->distance = 1;
				}
			}
			ListTour(plist);
			b = NewBlob(plist, data, current_label);
			if ( CheckBlobInfo(bi, b, bimage) ) {
				BlobListAdd(&(bimage->list), b);
				current_label++;
				bimage->NBlobs++;
			} else {
				BlobDelete(b);
			}
			List_delete(plist);
		}
	}
/*	bimage->list = sort_linked_list(bimage->list, 1, BlobListCompare, NULL, NULL);*/
	return bimage;
}

int CheckBlobInfo(BlobInfo*bi, Blob*b, BlobImage*bimage)
{
	if ( bi->MaxBlobs == bimage->NBlobs )
		return 0;
	if ( (uint32)bi->MaxSize < b->Nnuclear_pixels || b->Nnuclear_pixels < (uint32)bi->MinSize )
		return 0;
	return 1;
}

void BlobListAdd(BlobList**list, Blob* b)
{
	BlobList*p;
	p = (BlobList*)malloc(sizeof(BlobList));
	p->next = (*list);
	p->blob = b;
	(*list) = p;
}

int BlobListCompare(void *p, void *q, void *user)
{
	BlobList*P;
	BlobList*Q;

	P = (BlobList*)p;
	Q = (BlobList*)q;

	if ( P->blob->cm->x > Q->blob->cm->x )
		return 1;
	if ( P->blob->cm->x < Q->blob->cm->x )
		return -1;
	return 0;
}

void BlobImagePrint(BlobImage*bi, FILE*fp)
{
	int label;
	BlobList*cur;

	fprintf(fp,"%u %u %u\n", bi->NBlobs, bi->rows, bi->columns);
	switch ( bi->type ) {
		case only_nuclei:
		fprintf(fp,"only_nuclei\n");
		for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
			int i;
			fprintf(fp,"%u ", cur->blob->label);
			fprintf(fp,"%8.4f %8.4f ", cur->blob->cm->x, cur->blob->cm->y);
			fprintf(fp,"\t%u\n", cur->blob->Nnuclear_pixels);
			for ( i = 0; i < cur->blob->Nnuclear_pixels; i++) {
				fprintf(fp, "%u ", cur->blob->nuclear_pixels[i]);
			}
			fprintf(fp,"\n");
		}
		break;
		case nuclei_and_energids:
		fprintf(fp,"nuclei_and_energids\n");
		for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
			int i;
			fprintf(fp,"%u ", cur->blob->label);
			fprintf(fp,"%8.4f %8.4f\n", cur->blob->cm->x, cur->blob->cm->y);
			fprintf(fp,"\t%u\n", cur->blob->Nnuclear_pixels);
			for ( i = 0; i < cur->blob->Nnuclear_pixels; i++) {
				fprintf(fp, "%u ", cur->blob->nuclear_pixels[i]);
			}
			fprintf(fp,"\n");
			fprintf(fp,"\t%u\n", cur->blob->Nenergid_pixels);
			for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
				fprintf(fp, "%u ", cur->blob->energid_pixels[i]);
			}
			fprintf(fp,"\n");
		}
		break;
	}
}

Centroid*NewCentroid(Blob*b, uint32 columns, uint32 rows)
{
	Centroid*cm;
	int i;
	cm = (Centroid*)malloc(sizeof(Centroid));
	cm->x = 0;
	cm->y = 0;
	for ( i = 0; i < b->Nnuclear_pixels; i++ ) {
		double column = (double)(b->nuclear_pixels[i] % columns);
		double row = ( (double)b->nuclear_pixels[i] - column ) / (double)columns;
		cm->x += 100.0 * column / (double)columns;
		cm->y += 100.0 * row / (double)rows;
	}
	if ( b->Nnuclear_pixels ) {
		cm->x /= (double)b->Nnuclear_pixels;
		cm->y /= (double)b->Nnuclear_pixels;
	}
	return cm;
}

BlobInfo*NewBlobInfo(int MaxSize, int MinSize, uint32 MaxBlobs)
{
	BlobInfo*blin;
	blin = (BlobInfo*)malloc(sizeof(BlobInfo));
	blin->MaxSize = MaxSize;
	blin->MinSize = MinSize;
	blin->MaxBlobs = MaxBlobs;
	return blin;
}

BlobImage*BlobImageCreate(char*file)
{
	FILE*fp;
	BlobImage*bimage;
	fp = fopen(file, "r");
	if ( !fp )
		g_error("prostak: BlobImageCreate can't open %s", file);
	bimage = BlobImageRead(fp);
	fclose(fp);
	return bimage;
}

BlobImage*BlobImageRead(FILE*fp)
{
	BlobImage*bimage;
	uint32 i;
	int label;
	char*buffer;

	bimage = (BlobImage*)malloc(sizeof(BlobImage));
	buffer = (char*)calloc(MAX_RECORD, sizeof(char));
	bimage->list = (BlobList*)NULL;

	fscanf(fp,"%*s");
	fscanf(fp,"%u %u %u\n", &(bimage->NBlobs), &(bimage->rows), &(bimage->columns));
	buffer = fgets(buffer, 255, fp);
	if ( !strcmp(buffer, "only_nuclei\n") ) {
		bimage->type = only_nuclei;
	} else if ( !strcmp(buffer, "nuclei_and_energids\n") ) {
		bimage->type = nuclei_and_energids;
	}

	switch ( bimage->type ) {
		case only_nuclei:
			for ( label = 0; label < bimage->NBlobs; label++ ) {
				Blob*b;
				b = DoBlob();
				b->cm = DoCentroid();
				fscanf(fp,"%u ", &(b->label));
				fscanf(fp,"%lf %lf\n", &(b->cm->x), &(b->cm->y));
				fscanf(fp,"\t%u\n", &(b->Nnuclear_pixels));
				b->nuclear_pixels = (uint32*)calloc(b->Nnuclear_pixels, sizeof(uint32));
				for ( i = 0; i < b->Nnuclear_pixels; i++) {
					fscanf(fp, "%u ", &(b->nuclear_pixels[i]));
				}
				b->Nenergid_pixels = 0;
				b->energid_pixels = (uint32*)NULL;
				BlobListAdd(&(bimage->list), b);
			}
		break;
		case nuclei_and_energids:
			for ( label = 0; label < bimage->NBlobs; label++ ) {
				Blob*b;
				b = DoBlob();
				b->cm = DoCentroid();
				fscanf(fp,"%u ", &(b->label));
				fscanf(fp,"%lf %lf\n", &(b->cm->x), &(b->cm->y));
				fscanf(fp,"\t%u\n", &(b->Nnuclear_pixels));
				b->nuclear_pixels = (uint32*)calloc(b->Nnuclear_pixels, sizeof(uint32));
				for ( i = 0; i < b->Nnuclear_pixels; i++) {
					fscanf(fp, "%u ", &(b->nuclear_pixels[i]));
				}
				fscanf(fp,"\n\t%u\n", &(b->Nenergid_pixels));
				b->energid_pixels = (uint32*)calloc(b->Nenergid_pixels, sizeof(uint32));
				for ( i = 0; i < b->Nenergid_pixels; i++) {
					fscanf(fp, "%u ", &(b->energid_pixels[i]));
				}
				BlobListAdd(&(bimage->list), b);
			}
		break;
	}
	return bimage;
}

Blob*DoBlob()
{
	Blob*b;
	b = (Blob*)malloc(sizeof(Blob));
	b->energid_pixels = (uint32*)NULL;
	b->Nenergid_pixels = 0;
	b->nuclear_pixels = (uint32*)NULL;
	b->Nnuclear_pixels = 0;
	b->cm = (Centroid*)NULL;
	return b;
}

Centroid*DoCentroid()
{
	Centroid*cm;
	cm = (Centroid*)malloc(sizeof(Centroid));
	cm->x = 0;
	cm->y = 0;
	return cm;
}

Pattern*BlobImage2Pattern(BlobImage*bi)
{
	Pattern *p;
	int label;
	BlobList*cur;
	p = pattern_new(bi->NBlobs, 0, 100, 0, 100);
	for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
		p->array[label].nuc_number = cur->blob->label;
		p->array[label].AP_coord = cur->blob->cm->x;
		p->array[label].DV_coord = cur->blob->cm->y;
		p->array[label].prot_conc = (double*)NULL;
	}
	return p;
}

void AddChannel(ChannelType ct, Pattern*p, CharImage*image, BlobImage*bi, int channel, char protein, BlobDataType flag)
{
	int label;
	BlobList*cur;
	BlobData*bd;
	if ( p->ngenes < channel ) p->ngenes = channel;
	p->channel = (int*)realloc(p->channel, ( p->ngenes ) * sizeof(int));
	p->gene_ids = (char*)realloc(p->gene_ids, ( p->ngenes ) * sizeof(char));
	p->channel[channel-1] = channel;
	p->gene_ids[channel-1] = protein;
	if ( ct == nuclear ) {
		switch ( flag ) {
			case area:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
/*fprintf(stdout,"\n %u", label);
fflush(stdout);*/
					p->array[label].prot_conc[channel-1] = bd->area;
					BolbDataDelete(bd);
				}
			break;
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
/*fprintf(stdout,"\n %u", label);
fflush(stdout);*/
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
				}
			break;
			case muc:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->muc;
					BolbDataDelete(bd);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
				}
			break;
		}
	} else if ( ct == energid ) {
		switch ( flag ) {
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
				}
			break;
			case muc:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->muc;
					BolbDataDelete(bd);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
				}
			break;
		}
	} else if ( ct == outnuc ) {
		uint32 *pixels;
		uint32 npixels;
		uint32 i, j;
		switch ( flag ) {
			case varbc:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					double area_in;
					double area_out;
					double mean_in;
					double mean_out;
					double var_in;
					double var_out;
					double mean;
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					mean = bd->mean;
					BolbDataDelete(bd);
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					area_out = bd->area;
					mean_out = bd->mean;
					var_out = bd->var;
					BolbDataDelete(bd);
					free(pixels);
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					area_in = bd->area;
					mean_in = bd->mean;
					var_in = bd->var;
					BolbDataDelete(bd);
					p->array[label].prot_conc[channel-1] = area_in * ( mean_in - mean ) * ( mean_in - mean );
					p->array[label].prot_conc[channel-1] += area_out * ( mean_out - mean ) * ( mean_out - mean );
					p->array[label].prot_conc[channel-1] /= area_out * var_out + area_in * var_in;
				}
			break;
			case area:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->area;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
		}
	}

}


void AddChannel1(ChannelType ct, Pattern*p, CharImage*image, BlobImage*bi, int channel, char protein, BlobDataType flag)
{
	int label;
	BlobList*cur;
	BlobData*bd;
	if ( p->ngenes < channel ) p->ngenes = channel;
	p->channel = (int*)realloc(p->channel, ( p->ngenes ) * sizeof(int));
	p->gene_ids = (char*)realloc(p->gene_ids, ( p->ngenes ) * sizeof(char));
	p->channel[channel-1] = channel;
	p->gene_ids[channel-1] = protein;
	if ( ct == nuclear ) {
		switch ( flag ) {
			case area:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
/*fprintf(stdout,"\n %u", label);
fflush(stdout);*/
					p->array[label].prot_conc[channel-1] = bd->area;
					BolbDataDelete(bd);
				}
			break;
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData1(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels,"tfunc");
/*fprintf(stdout,"\n %u", label);
fflush(stdout);*/
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
				}
			break;
			case muc:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->muc;
					BolbDataDelete(bd);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
				}
			break;
		}
	} else if ( ct == energid ) {
		switch ( flag ) {
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
				}
			break;
			case muc:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->muc;
					BolbDataDelete(bd);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
				}
			break;
		}
	} else if ( ct == outnuc ) {
		uint32 *pixels;
		uint32 npixels;
		uint32 i, j;
		switch ( flag ) {
			case varbc:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					double area_in;
					double area_out;
					double mean_in;
					double mean_out;
					double var_in;
					double var_out;
					double mean;
					bd = GetBlobData(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					mean = bd->mean;
					BolbDataDelete(bd);
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					area_out = bd->area;
					mean_out = bd->mean;
					var_out = bd->var;
					BolbDataDelete(bd);
					free(pixels);
					bd = GetBlobData(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					area_in = bd->area;
					mean_in = bd->mean;
					var_in = bd->var;
					BolbDataDelete(bd);
					p->array[label].prot_conc[channel-1] = area_in * ( mean_in - mean ) * ( mean_in - mean );
					p->array[label].prot_conc[channel-1] += area_out * ( mean_out - mean ) * ( mean_out - mean );
					p->array[label].prot_conc[channel-1] /= area_out * var_out + area_in * var_in;
				}
			break;
			case area:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->area;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobData(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
		}
	}

}


void BolbDataDelete(BlobData*p)
{
	free(p);
}

BlobData* GetBlobData(unsigned char*data, uint32 *indices, uint32 n)
{
	BlobData*bd;
	uint32 i;
	int med;
	double mean;
	double mean2;
	double max;
	double min;
	double median;
	double area;
	double muc;
	unsigned char *pp;
	mean = 0;
	mean2 = 0;
	max = (double)EmptyPixel;
	min = (double)FilledPixel;
	median = (double)EmptyPixel;
	pp = (unsigned char*)calloc(n, sizeof(unsigned char));
	area = 0;
	for ( i = 0; i < n ; i++) {
		register double p = (double)data[indices[i]];
		if ( p < min ) min = p;
		if ( p > max ) max = p;
		mean += p;
		mean2 += p * p;
		pp [i] = p;
		area += 1.0;
	}
	muc = mean;
	qsort((void*)pp, n, sizeof(unsigned char), com1);
	med = (int)( 0.5 * (double)n + 0.5 ) - 1;
	median = pp [med];
	free(pp);
	bd = (BlobData*)malloc(sizeof(BlobData));
	if ( area > 1 ) {
		register double a = mean2 / ( area - 1.0 ) - mean * mean / ( area * ( area - 1.0 ) );
		bd->mean = mean / area;
		bd->var = a;
		bd->stdev = sqrt(a);
	} else {
		bd->mean = mean;
		bd->var = 0;
		bd->stdev = 0;
	}
	bd->max = max;
	bd->min = min;
	bd->median = median;
	bd->area = area;
	bd->muc = muc;
	return bd;
}

double ProcTFunc(double x)
{
	return 2*sqrt(x+8.0/3.0);
}

BlobData* GetBlobData1(unsigned char*data, uint32 *indices, uint32 n, char*tag)
{
	BlobData*bd;
	uint32 i;
	uint32 nmedian, m;
	double mean;
	double mean2;
	double max;
	double min;
	double median;
	double area;
	double muc;
	double (*ProcFunc)(double);
	mean = 0;
	mean2 = 0;
	max = (double)EmptyPixel;
	min = (double)FilledPixel;
	median = (double)EmptyPixel;
	nmedian = 0;
	m = (uint32)( (double)n / 2.0 );
	area = 0;
	ProcFunc = NULL;
	if ( tag ) {
		if ( !strcmp(tag, "tfunc") ) {
			ProcFunc = ProcTFunc;
		} else {
			g_error("GetBlobData1 %s function is not implemented!\n", tag);
		}
	}
	for ( i = 0; i < n ; i++) {
/*		register unsigned char p = data[indices[i]];*/
		register double p = ProcFunc((double)data[indices[i]]);
		if ( p < min ) min = p;
		if ( p > max ) max = p;
		mean += p;
		mean2 += p * p;
		if ( p >= median && nmedian < m ) {
			median = p;
			nmedian++;
		}
/*		if ( p > EmptyPixel )*/
			area += 1.0;
	}
	muc = mean;
	bd = (BlobData*)malloc(sizeof(BlobData));
/*
	bd->mean = mean / n;
	bd->stdev =  mean2 / ( n - 1 ) - mean * mean / ( n * ( n - 1 ) );
*/
	if ( area > 1 ) {
		register double a = mean2 / ( area - 1.0 ) - mean * mean / ( area * ( area - 1.0 ) );
		bd->mean = mean / area;
		bd->var = a;
		bd->stdev = sqrt(a);
	} else {
		bd->mean = mean;
		bd->var = 0;
		bd->stdev = 0;
	}
	bd->max = max;
	bd->min = min;
	bd->median = median;
	bd->area = area;
	bd->muc = muc;
	return bd;
}


CharImage*BlobImage2CharImage(BlobImage*bi)
{
	CharImage*ci;
	uint32 i, j;
	BlobList*cur;
	ci = CharImageNew(bi->columns, bi->rows);
	for ( j = 0; j < ci->number; j++ )
		ci->pixels[j] = EmptyPixel;
	for ( i = 0, cur = bi->list; i < bi->NBlobs; i++, cur = cur->next ) {
		for ( j = 0; j < cur->blob->Nnuclear_pixels; j++ ) {
			ci->pixels[cur->blob->nuclear_pixels[j]] = FilledPixel;
		}
	}
	return ci;
}

SuperImage*BlobImage2SuperImage(BlobImage*bi, Pattern*p, int bits_per_pixel_in, int bits_per_pixel_out)
{
	SuperImage*si;
	uint32 i, j;
	BlobList*cur;
	double scale, filled_pixel, pix;
	si = SuperImageNew(bi->columns, bi->rows, bits_per_pixel_out);
	filled_pixel = (double)si->filledPixel;
	scale = (double)bits_per_pixel_in + 1.0;
	for ( i = 0, cur = bi->list; i < bi->NBlobs; i++, cur = cur->next ) {
		pix = pattern_get_prot_conc_val(p, cur->blob->label, 0);
		for ( j = 0; j < cur->blob->Nnuclear_pixels; j++ ) {
			si->pixels[cur->blob->nuclear_pixels[j]] = filled_pixel * pix / scale;
		}
	}
	return si;
}

CharImage*BlobImageIndex2CharImage(BlobImage*bi, int index)
{
	CharImage*ci;
	uint32 i, j;
	BlobList*cur;
	ci = CharImageNew(bi->columns, bi->rows);
	for ( j = 0; j < ci->number; j++ )
		ci->pixels[j] = EmptyPixel;
	for ( i = 0, cur = bi->list; i < bi->NBlobs; i++, cur = cur->next ) {
		if ( i == index ) {
			for ( j = 0; j < cur->blob->Nnuclear_pixels; j++ ) {
				ci->pixels[cur->blob->nuclear_pixels[j]] = FilledPixel;
			}
			break;
		}
	}
	return ci;
}

BlobImage*BlobImageCheckShape(BlobImage*bi, ShapeDescriptor*sd, double p, CheckShapeType cst)
{
	uint32 i;
	BlobList*cur;
	for ( i = 0, cur = bi->list; i < bi->NBlobs; i++, cur = cur->next ) {
		ShapeDescriptor*s;
		double similarity;
		s = ShapeDescriptorNew(cur->blob, bi->rows, bi->columns);
		similarity = ShapeDescriptorSimilarity(s, sd);
		fprintf(stdout,"\n %u %u %.16f", i, cur->blob->label, similarity);
		switch (cst) {
			case shape_accept:
				if ( similarity > p ) {
					cur->blob->label = FICTITIOUS;
				}
			break;
			case shape_reject:
				if ( similarity < p ) {
					cur->blob->label = FICTITIOUS;
				}
			break;
			default:
				g_error("parus unknown check shape type %d", cst);
		}
	}
	bi = BlobImageEraseFictitious(bi);
	return bi;
}

BlobImage*BlobImageEraseFictitious(BlobImage*bi)
{
	BlobList*cur, *prev;
	int n;

	n = 0;
	cur = bi->list;
	while ( cur && cur->blob->label == FICTITIOUS ) {
		bi->list = cur->next;
		prev = cur;
		cur = bi->list;
		BlobListDelete(prev);
		n++;
	}
	prev = cur;
	cur = cur->next;
	if ( !cur ) return bi;
	while (cur) {
		if ( cur->blob->label == FICTITIOUS ) {
			prev->next = cur->next;
			BlobListDelete(cur);
			cur = prev;
			n++;
		}
		prev = cur;
		cur = cur->next;
	}
	bi->NBlobs -= n;
	return bi;
}

void BlobListDelete(BlobList*cur)
{
	BlobDelete(cur->blob);
	free(cur);
}

void ShapeDescriptorDelete(ShapeDescriptor*sd)
{
	free(sd);
}

/*
BlobImage*BlobImageCreate(BlobInfo*bi, CharImage*wtsd, CharImage*nuc, int connectivity)
{
	BlobImage*bimage;
	CharPixelStructure*cps;
	CharPixel*cp;
	uint32 i, j;
	int current_label;
	bimage = (BlobImage*)malloc(sizeof(BlobImage));
	if ( d ) {
		wtsd = d;
		bimage->type = nuclei_and_energids;
	} else {
		wtsd = data;
		bimage->type = only_nuclei;
	}

	bimage->columns = wtsd->columns;
	bimage->rows = wtsd->rows;
	bimage->NBlobs = 0;
	bimage->list = (BlobList*)NULL;
	cps = NewCharPixelStructure8(wtsd);
	current_label = 0;
	for ( i = 0; i < wtsd->number; i++ ) {
		List*plist;
		Blob*b;
		cp = cps->pixels[i];
		if ( cp->intensity == FilledPixel && cp->distance == 0 ) {
			plist = InitList(cp);
			cp->distance = 1;
			for ( j = 0; j < cp->Nneigh; j++ ) {
				if ( cp->neigh[j]->intensity == FilledPixel && cp->neigh[j]->distance == 0 ) {
					ListAdd(plist,cp->neigh[j]);
					cp->neigh[j]->distance = 1;
				}
			}
			ListTour(plist);
			b = NewBlob(plist, data, current_label);
			if ( CheckBlobInfo(bi, b, bimage) ) {
				BlobListAdd(&(bimage->list), b);
				current_label++;
				bimage->NBlobs++;
			} else {
				BlobDelete(b);
			}
			List_delete(plist);
		}
	}
	return bimage;
}
*/
double varianceBwClasses(CharImage*image, BlobImage*bi)
{
	double estimator;
	int label;
	BlobList*cur;
	BlobData*bd;
	double area_in;
	double area_out;
	double mean_in;
	double mean_out;
	double var_in;
	double var_out;
	double mean;
	uint32 *pixels;
	uint32 npixels;
	uint32 *pixels_in;
	uint32 npixels_in;
	uint32 *pixels_out;
	uint32 npixels_out;
	uint32 i, j;
	npixels = 0;
	pixels = (uint32 *)NULL;
	npixels_in = 0;
	pixels_in = (uint32 *)NULL;
	npixels_out = 0;
	pixels_out = (uint32 *)NULL;
	for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
		for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
			int out = 1;
			pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
			pixels[npixels] = cur->blob->energid_pixels[i];
			npixels++;
			for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
				if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
					out = 0;
					break;
				}
			}
			if ( out == 1 ) {
				pixels_out = (uint32 *)realloc(pixels_out, ( npixels_out + 1 ) * sizeof(uint32));
				pixels_out[npixels_out] = cur->blob->energid_pixels[i];
				npixels_out++;
			} else {
				pixels_in = (uint32 *)realloc(pixels_in, ( npixels_in + 1 ) * sizeof(uint32));
				pixels_in[npixels_in] = cur->blob->energid_pixels[i];
				npixels_in++;
			}
		}
	}
	bd = GetBlobData(image->pixels, pixels, npixels);
	mean = bd->mean;
	BolbDataDelete(bd);
	bd = GetBlobData(image->pixels, pixels_out, npixels_out);
	area_out = bd->area;
	mean_out = bd->mean;
	var_out = bd->var;
	BolbDataDelete(bd);
	bd = GetBlobData(image->pixels, pixels_in, npixels_in);
	area_in = bd->area;
	mean_in = bd->mean;
	var_in = bd->var;
	BolbDataDelete(bd);
	estimator = area_in * ( mean_in - mean ) * ( mean_in - mean );
	estimator += area_out * ( mean_out - mean ) * ( mean_out - mean );
	estimator /= area_out * var_out + area_in * var_in;
	free(pixels);
	free(pixels_in);
	free(pixels_out);
	return estimator;
}

void vvarbc(GKeyFile*gkf, char*output_group, char*class_out_group, char*class_in_group, char*key_out, char*key_in)
{
	double estimator;
	double area_in;
	double area_out;
	double mean_in;
	double mean_out;
	double var_in;
	double var_out;
	double mean, variance_between_classes, variance_inside_class;
	double nu1, nu2, quantile;
	char *buf;
	GError*gerror = NULL;
	shape_descriptor_get_data_field(gkf, class_in_group, key_in, &mean_in, 0, &gerror);
	if ( gerror ) return;
	shape_descriptor_get_data_field(gkf, class_in_group, key_in, &var_in, 1, &gerror);
	if ( gerror ) return;
	shape_descriptor_get_data_field(gkf, class_out_group, key_out, &mean_out, 0, &gerror);
	if ( gerror ) return;
	shape_descriptor_get_data_field(gkf, class_out_group, key_out, &var_out, 1, &gerror);
	if ( gerror ) return;
	shape_descriptor_get_data_field(gkf, class_in_group, "m000", &area_in, 0, &gerror);
	if ( gerror ) return;
	shape_descriptor_get_data_field(gkf, class_out_group, "m000", &area_out, 0, &gerror);
	if ( gerror ) return;
	mean = ( area_in * mean_in + area_out * mean_out ) / ( area_in + area_out );
	variance_between_classes = area_in * ( mean_in - mean ) * ( mean_in - mean ) + area_out * ( mean_out - mean ) * ( mean_out - mean );
	buf = g_strdup_printf("%13.6f", variance_between_classes);
	g_key_file_set_value(gkf, output_group, "variance_between_classes", buf);
	g_free(buf);
	variance_inside_class = area_out * var_out + area_in * var_in;
	buf = g_strdup_printf("%13.6f", variance_inside_class);
	g_key_file_set_value(gkf, output_group, "variance_inside_class", buf);
	g_free(buf);
	estimator = variance_between_classes / variance_inside_class;
	buf = g_strdup_printf("%13.6f", estimator);
	g_key_file_set_value(gkf, output_group, "estimator", buf);
	g_free(buf);
	nu1 = 1;
	nu2 = 1e+12;
	quantile = gsl_cdf_fdist_P (estimator, nu1, nu2);
	buf = g_strdup_printf("%5.1f", quantile * 100.0);
	g_key_file_set_value(gkf, output_group, "FTestPerCent", buf);
	g_free(buf);
}

Pattern*CharImagePPix(CharImage*di, double ac, double bc, double ar, double br, double t0, double tN, char*lineDraw)
{
	Pattern *p;
	CharImage*dl;
	int i, N, nn;
	double dc, dr, dt;
	int c, r;
	p = (Pattern *)malloc(sizeof(Pattern));
	p->xrange = Range_create(0, 100);
	p->yrange = Range_create(0, 100);
	p->nnucs = 0;
	p->array = (DataPoint*)NULL;
	p->ngenes = 1;
	p->channel = (int*)calloc( p->ngenes, sizeof(int));
	p->gene_ids = (char*)calloc( p->ngenes + 1, sizeof(char));
	p->channel[0] = 1;
	p->gene_ids = strcpy(p->gene_ids, "a");
	if ( lineDraw ) {
		dl = CharImageClone(di);
	}
	N = (int)( tN - t0 + 0.5);
	nn = 0;
	for ( i = 0, dt = t0; i< N; i++, dt += 1.0 ) {
		dc = ac * dt + bc;
		dr = ar * dt + br;
		c = (int)( dc + 0.5 );
		r = (int)( dr + 0.5 );
		if ( 0 <= c && c < di->columns && 0<= r && r< di->rows ) {
			p->array = (DataPoint*)realloc(p->array, ( p->nnucs + 1 ) * sizeof(DataPoint));
			p->array[p->nnucs].nuc_number = nn;
			p->array[p->nnucs].AP_coord = dc;
			p->array[p->nnucs].DV_coord = dr;
			p->array[p->nnucs].prot_conc = (double*)calloc(p->ngenes, sizeof(double));
			p->array[p->nnucs].prot_conc[0] = (double)di->pixels[ r * di->columns + c];
			p->nnucs++;
			nn++;
			if ( lineDraw ) {
				dl->pixels[ r * di->columns + c] = (unsigned char)FilledPixel;
			}
		}
	}
	if ( lineDraw ) {
		CharImageWrite(dl, lineDraw);
		CharImageDelete(dl);
	}
	return p;
}


Pattern*CharImageMPPix(CharImage*di, double t0, double tN, CharImage*mi)
{
	Pattern *p;
	int i, nn;
	double dc, dr;
	int c, r;
	p = (Pattern *)malloc(sizeof(Pattern));
	p->xrange = Range_create(0, 100);
	p->yrange = Range_create(0, 100);
	p->nnucs = 0;
	p->array = (DataPoint*)NULL;
	p->ngenes = 1;
	p->channel = (int*)calloc( p->ngenes, sizeof(int));
	p->gene_ids = (char*)calloc( p->ngenes + 1, sizeof(char));
	p->channel[0] = 1;
	p->gene_ids = strcpy(p->gene_ids, "a");
	nn = 0;
	for ( c = 0; c < mi->columns; c++ ) {
		for ( r = 0; r < mi->rows; r++ ) {
			if ( mi->pixels[ r * mi->columns + c ] == FilledPixel ) {
				if ( t0 > nn ) {
					nn++;
					continue;
				} else if ( t0 <= nn && nn <= tN ) {
					dc = (double)c;
					dr = (double)r;
					p->array = (DataPoint*)realloc(p->array, ( p->nnucs + 1 ) * sizeof(DataPoint));
					p->array[p->nnucs].nuc_number = nn;
					p->array[p->nnucs].AP_coord = dc;
					p->array[p->nnucs].DV_coord = dr;
					p->array[p->nnucs].prot_conc = (double*)calloc(p->ngenes, sizeof(double));
					p->array[p->nnucs].prot_conc[0] = (double)di->pixels[ r * di->columns + c];
					p->nnucs++;
					nn++;
					mi->pixels[ r * mi->columns + c ] = (unsigned char)( 0.5 * (double)FilledPixel + 0.5);
				} else if ( nn > tN ) {
					break;
				}
			}
		}
	}
	return p;
}


void BlobImage2Polar(BlobImage*bi, double xmean, double ymean)
{
	int label;
	BlobList*cur;
	for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
		free(cur->blob->cm);
		cur->blob->cm = NewCentroidPolar(cur->blob, bi->columns, bi->rows, xmean, ymean);
	}
}

Centroid*NewCentroidPolar(Blob*b, uint32 columns, uint32 rows, double xmean, double ymean)
{
	Centroid*cm;
	int i;
	double theta, radius;
	cm = (Centroid*)malloc(sizeof(Centroid));
	cm->x = 0;
	cm->y = 0;
	for ( i = 0; i < b->Nnuclear_pixels; i++ ) {
		double column = (double)(b->nuclear_pixels[i] % columns) - xmean;
		double row = ymean - ( (double)b->nuclear_pixels[i] - column ) / (double)columns;
		cart2pol(column, row, &theta, &radius);
		cm->x += theta;
		cm->y += radius;
	}
	if ( b->Nnuclear_pixels ) {
		cm->x /= (double)b->Nnuclear_pixels;
		cm->y /= (double)b->Nnuclear_pixels;
	}
	return cm;
}

BlobData* GetBlobDataSuper(double*data, uint32 *indices, uint32 n)
{
	BlobData*bd;
	uint32 i;
	uint32 nmedian, m;
	double mean;
	double mean2;
	double max;
	double min;
	double median;
	double area;
	mean = 0;
	mean2 = 0;
	max = data[indices[0]];
	min = data[indices[0]];
	median = data[indices[0]];
	nmedian = 0;
	m = (uint32)( (double)n / 2.0 );
	area = 0;
	for ( i = 0; i < n ; i++) {
		register double p = data[indices[i]];
		if ( p < min ) min = p;
		if ( p > max ) max = p;
		mean += p;
		mean2 += p * p;
		if ( p >= median && nmedian < m ) {
			median = p;
			nmedian++;
		}
		area += 1.0;
	}
	bd = (BlobData*)malloc(sizeof(BlobData));
	if ( area > 1 ) {
		register double a = mean2 / ( area - 1.0 ) - mean * mean / ( area * ( area - 1.0 ) );
		bd->mean = mean / area;
		bd->var = a;
		bd->stdev = sqrt(a);
	} else {
		bd->mean = mean;
		bd->var = 0;
		bd->stdev = 0;
	}
	bd->max = max;
	bd->min = min;
	bd->median = median;
	bd->area = area;
	return bd;
}

double varBwClasses(SuperImage*image, BlobImage*bi)
{
	double estimator;
	int label;
	BlobList*cur;
	BlobData*bd;
	double area_in;
	double area_out;
	double mean_in;
	double mean_out;
	double var_in;
	double var_out;
	double mean;
	uint32 *pixels;
	uint32 npixels;
	uint32 *pixels_in;
	uint32 npixels_in;
	uint32 *pixels_out;
	uint32 npixels_out;
	uint32 i, j;
	npixels = 0;
	pixels = (uint32 *)NULL;
	npixels_in = 0;
	pixels_in = (uint32 *)NULL;
	npixels_out = 0;
	pixels_out = (uint32 *)NULL;
	for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
		for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
			int out = 1;
			pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
			pixels[npixels] = cur->blob->energid_pixels[i];
			npixels++;
			for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
				if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
					out = 0;
					break;
				}
			}
			if ( out == 1 ) {
				pixels_out = (uint32 *)realloc(pixels_out, ( npixels_out + 1 ) * sizeof(uint32));
				pixels_out[npixels_out] = cur->blob->energid_pixels[i];
				npixels_out++;
			} else {
				pixels_in = (uint32 *)realloc(pixels_in, ( npixels_in + 1 ) * sizeof(uint32));
				pixels_in[npixels_in] = cur->blob->energid_pixels[i];
				npixels_in++;
			}
		}
	}
	bd = GetBlobDataSuper(image->pixels, pixels, npixels);
	mean = bd->mean;
	BolbDataDelete(bd);
	bd = GetBlobDataSuper(image->pixels, pixels_out, npixels_out);
	area_out = bd->area;
	mean_out = bd->mean;
	var_out = bd->var;
	BolbDataDelete(bd);
	bd = GetBlobDataSuper(image->pixels, pixels_in, npixels_in);
	area_in = bd->area;
	mean_in = bd->mean;
	var_in = bd->var;
	BolbDataDelete(bd);
	estimator = area_in * ( mean_in - mean ) * ( mean_in - mean );
	estimator += area_out * ( mean_out - mean ) * ( mean_out - mean );
	estimator /= area_out * var_out + area_in * var_in;
	free(pixels);
	free(pixels_in);
	free(pixels_out);
	return estimator;
}


void AddChannelSuper(ChannelType ct, Pattern*p, SuperImage*image, BlobImage*bi, int channel, char protein, BlobDataType flag)
{
	int label;
	BlobList*cur;
	BlobData*bd;
	if ( p->ngenes < channel ) p->ngenes = channel;
	p->channel = (int*)realloc(p->channel, ( p->ngenes ) * sizeof(int));
	p->gene_ids = (char*)realloc(p->gene_ids, ( p->ngenes ) * sizeof(char));
	p->channel[channel-1] = channel;
	p->gene_ids[channel-1] = protein;
	if ( ct == nuclear ) {
		switch ( flag ) {
			case area:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
/*fprintf(stdout,"\n %u", label);
fflush(stdout);*/
					p->array[label].prot_conc[channel-1] = bd->area;
					BolbDataDelete(bd);
				}
			break;
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
/*fprintf(stdout,"\n %u", label);
fflush(stdout);*/
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
				}
			break;
		}
	} else if ( ct == energid ) {
		switch ( flag ) {
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
				}
			break;
		}
	} else if ( ct == outnuc ) {
		uint32 *pixels;
		uint32 npixels;
		uint32 i, j;
		switch ( flag ) {
			case varbc:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					double area_in;
					double area_out;
					double mean_in;
					double mean_out;
					double var_in;
					double var_out;
					double mean;
					bd = GetBlobDataSuper(image->pixels, cur->blob->energid_pixels, cur->blob->Nenergid_pixels);
					mean = bd->mean;
					BolbDataDelete(bd);
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					area_out = bd->area;
					mean_out = bd->mean;
					var_out = bd->var;
					BolbDataDelete(bd);
					free(pixels);
					bd = GetBlobDataSuper(image->pixels, cur->blob->nuclear_pixels, cur->blob->Nnuclear_pixels);
					area_in = bd->area;
					mean_in = bd->mean;
					var_in = bd->var;
					BolbDataDelete(bd);
					p->array[label].prot_conc[channel-1] = area_in * ( mean_in - mean ) * ( mean_in - mean );
					p->array[label].prot_conc[channel-1] += area_out * ( mean_out - mean ) * ( mean_out - mean );
					p->array[label].prot_conc[channel-1] /= area_out * var_out + area_in * var_in;
				}
			break;
			case area:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->area;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case mean:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->mean;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case max:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->max;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case min:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->min;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case median:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->median;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case stdev:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->stdev;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
			case var:
				for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
					npixels = 0;
					pixels = (uint32 *)NULL;
					for ( i = 0; i < cur->blob->Nenergid_pixels; i++) {
						int out = 0;
						for ( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
							if ( cur->blob->nuclear_pixels[j] == cur->blob->energid_pixels[i] ) {
								out = 1;
								break;
							}
						}
						if ( out == 0 ) {
							pixels = (uint32 *)realloc(pixels, ( npixels + 1 ) * sizeof(uint32));
							pixels[npixels] = cur->blob->energid_pixels[i];
							npixels++;
						}
					}
					p->array[label].prot_conc = (double*)realloc(p->array[label].prot_conc, ( p->ngenes ) * sizeof(double));
					bd = GetBlobDataSuper(image->pixels, pixels, npixels);
					p->array[label].prot_conc[channel-1] = bd->var;
					BolbDataDelete(bd);
					free(pixels);
				}
			break;
		}
	}
}

static double (*microscope_point_spread_func)(double k, double l, double parameter);

struct microscope_score_f_params {
	int window;
	SuperImage*si;
	BlobImage*bi;
	SuperImage*step_image;
	SuperImage*blurred_step_image;
	BlobImage*bimask;
	double *pix_nuc;
	double *pix_cyt;
};

double microscope_point_spread_func_confocal_bessel(double k, double l, double parameter)
{
	int kk = 1;
	double S = 1, term;
	double x, y;
	x = k * parameter; y = l * parameter;
	term = 1;
	while( fabs ( term ) > 0.0001 ) {
		term = term * ( -x * x - y * y ) / ( 4.0 * kk * ( kk + 1 ) );
		S += term;
		kk++;
	}
	return S * S * S * S;
}


double microscope_point_spread_func_bessel(double k, double l, double parameter)
{
	double r;
	double d;
	gsl_sf_result result;
	int status;
	r = parameter * parameter * ( k * k + l * l );
	d = gsl_sf_bessel_J0( r );
	return d * d * d * d;
}

double microscope_point_spread_func_gauss(double k, double l, double parameter)
{
	double d = gsl_sf_erf_Z( parameter * sqrt( k * k + l * l ));
	return d;
}

double microscope_point_spread_func_exp(double k, double l, double parameter)
{
	double d = gsl_sf_exp( parameter * sqrt( k * k + l * l ));
	return parameter * parameter * d;
}

double microscope_point_spread_func_energy(int window, double parameter)
{
	double psf;
	int k, l;
	psf = 0;
	for ( k = -window; k <= window; k++ ) {
		for ( l = -window; l <= window; l++ ) {
			psf += microscope_point_spread_func((double)k, (double)l, parameter);
		}
	}
	return psf;
}

double microscope_point_spread_func_ones(double k, double l, double parameter)
{
	return 1.0;
}

double microscope_score_f (double x, void *p)
{
	double R, Rm, Cm, Nm, *Sm;
	double D2n;
	double Dnc;
	double Dn;
	double Dc;
	double ne;
	double dot;
	int i, j;
	BlobList*cur, *cur_bimask;
	double psf;
	struct microscope_score_f_params * params = (struct microscope_score_f_params *)p;
	SuperImage*si = params->si;
	BlobImage*bi = params->bi;
	SuperImage*step_image = params->step_image;
	SuperImage*blurred_step_image = params->blurred_step_image;
	BlobImage*bimask = params->bimask;
	int window = params->window;
	double *pix_nuc = params->pix_nuc;
	double *pix_cyt = params->pix_cyt;
	R = 0;
	psf = microscope_point_spread_func_energy(window, x);
/*	fprintf(stderr,"\n arg=%f psf = %f", x, psf);*/
	for( i = 0, cur = bi->list, cur_bimask = bimask->list; i < bi->NBlobs; i++, cur = cur->next, cur_bimask = cur_bimask->next) {
		Sm = (double*)calloc(cur_bimask->blob->Nenergid_pixels, sizeof(double));
		D2n = 0;
		Dnc = 0;
		Dn = 0;
		Dc = 0;
		ne = 0;
		for( j = 0; j < cur_bimask->blob->Nenergid_pixels; j++) {
//			Sm[j] = super_image_convolve_1(step_image, cur_bimask->blob->energid_pixels[j], window, x) / psf;
			D2n += Sm[j] * Sm[j];
			Dnc += Sm[j];
			Dn += Sm[j] * si->pixels [ cur_bimask->blob->energid_pixels[j] ];
			Dc += si->pixels [ cur_bimask->blob->energid_pixels[j] ];
			ne++;
		}
		Cm = (Dn * Dnc - Dc * D2n) / ( Dnc * Dnc - D2n*ne );
		Nm = (Dc - Cm * (ne - Dnc) ) / Dnc;
		pix_nuc[i] = Nm;
		pix_cyt[i] = Cm;
		Rm = 0;
		for( j = 0; j < cur_bimask->blob->Nenergid_pixels; j++) {
			dot = Cm *( 1 - Sm[j] ) + Nm * Sm[j];
			if ( dot > blurred_step_image->filledPixel )
				dot = blurred_step_image->filledPixel;
			else if ( dot < 0 )
				dot = 0;
			blurred_step_image->pixels[ cur_bimask->blob->energid_pixels[j] ] = dot;
			Rm += ( dot - si->pixels[ cur_bimask->blob->energid_pixels[j] ] ) * ( dot - si->pixels[ cur_bimask->blob->energid_pixels[j] ] );
		}
		R += Rm;
		free(Sm);
	}
	return R;
}

double first_guess(double a, double b, int iter, double fa, double fb, void*p, double*fx)
{
	double x, fy;
	int i;
	double dx = (b - a)/iter;
	x = a + dx;
	for ( i = 1 ; i < iter - 1; i++) {
		fy = microscope_score_f(x, p);
		if ( fy < fa && fy < fb )
			break;
		x = x + dx;
	}
	*fx = fy;
	return x;
}

SuperImage*super_image_get_microscope_parameter(double*msigma, SuperImage*si, BlobImage*bi, int *window, double sigma_a, double sigma_b, char*psf_type, int iter_max, double criterion, BlobImage*bimask, int log_flag)
{
	SuperImage*step_image, *blurred_step_image;
	double sigma;
	int iter;
	double crit;
	int i, j, k;
	double Cm, Nm, Rm, R, R0;
	double *pix_nuc;
	double *pix_cyt;
	double psf;
	BlobList*cur, *cur_bi;
	struct microscope_score_f_params p;
	gsl_function F;
	int status;
	const gsl_min_fminimizer_type *T;
	gsl_min_fminimizer *s;
	double fa;
	double fb;
	double fx;
	if ( sigma_a < 0 )
		sigma_a = -sigma_a;
	if ( sigma_b < 0 )
		sigma_b = -sigma_b;
	if ( sigma_a > sigma_b ) {
		sigma = sigma_a;
		sigma_a = sigma_b;
		sigma_b = sigma;
	}
	sigma = 0.5 * (sigma_a + sigma_b);
	init_psf_func(window, sigma, psf_type);
	step_image = SuperImageNew(si->columns, si->rows, si->bits_per_pixel);
	blurred_step_image = SuperImageNew(si->columns, si->rows, si->bits_per_pixel);
	for( i = 0, cur = bimask->list, cur_bi = bi->list; i < bimask->NBlobs; i++, cur = cur->next, cur_bi = cur_bi->next) {
		for( j = 0; j < cur->blob->Nenergid_pixels; j++) {
			step_image->pixels[ cur->blob->energid_pixels[j] ] = 1.0 / 3.0;
		}
		for( j = 0; j < cur->blob->Nnuclear_pixels; j++) {
			step_image->pixels[ cur->blob->nuclear_pixels[j] ] = 3.0 / 3.0;
		}
		for( j = 0; j < cur_bi->blob->Nnuclear_pixels; j++) {
			step_image->pixels[ cur_bi->blob->nuclear_pixels[j] ] = 2.0 / 3.0;
		}
	}
	pix_cyt = (double*)calloc(bi->NBlobs, sizeof(double));
	pix_nuc = (double*)calloc(bi->NBlobs, sizeof(double));
	p.window = (*window);
	p.si = si;
	p.bi = bi;
	p.step_image = step_image;
	p.blurred_step_image = blurred_step_image;
	p.bimask = bimask;
	p.pix_nuc = pix_nuc;
	p.pix_cyt = pix_cyt;
	F.function = &microscope_score_f;
	F.params = &p;
	T = gsl_min_fminimizer_brent;
	s = gsl_min_fminimizer_alloc (T);
	fa = microscope_score_f(sigma_a, &p);
	fb = microscope_score_f(sigma_b, &p);
	sigma = first_guess(sigma_a, sigma_b, iter_max, fa, fb, &p, &fx);
	if ( log_flag ) {
		fprintf(stderr, "\nwindow=%d fa=%f fb=%f fx=%f sigma=%f ", (*window), fa, fb, fx, sigma);
	}
	gsl_min_fminimizer_set (s, &F, sigma, sigma_a, sigma_b);
	if ( log_flag ) {
		fprintf(stderr, "using %s method\n", gsl_min_fminimizer_name (s));
		fprintf(stderr, "%5s %4s [%9s, %9s] %9s %9s\n", "iter", "cost", "lower", "upper", "min", "err(est)");
	}
	crit = 0;
	for ( iter = 0; iter < iter_max; iter++ ) {
		status = gsl_min_fminimizer_iterate (s);
		sigma = gsl_min_fminimizer_x_minimum (s);
		sigma_a = gsl_min_fminimizer_x_lower (s);
		sigma_b = gsl_min_fminimizer_x_upper (s);
		status = gsl_min_test_interval (sigma_a, sigma_b, criterion, 0.0);
		if (status == GSL_SUCCESS)
			break;
		if ( log_flag ) {
			fprintf(stderr, "%5d, %.7f, [%.7f, %.7f] %.7f %.7f\n", iter, gsl_min_fminimizer_f_minimum(s), sigma_a, sigma_b, sigma, sigma_b - sigma_a);
		}
	}
	if ( log_flag ) {
		fprintf(stderr, "%5d, %.7f, [%.7f, %.7f] %.7f %.7f\n", iter, gsl_min_fminimizer_f_minimum(s), sigma_a, sigma_b, sigma, sigma_b - sigma_a);
	}
	gsl_min_fminimizer_free (s);
	free(pix_nuc);
	free(pix_cyt);
	SuperImageDelete(step_image);
	*msigma = sigma;
	return blurred_step_image;
}

double super_image_convolve_2_devide(SuperImage*input, SuperImage*current, int number, int window, gsl_matrix *psf_matrix)
{
	double psf_1, psf_2;
	int k, l, c, r, i, j;
	int column, row;
	psf_1 = 0;
	psf_2 = 0;
	row = number / (int)(input->columns);
	column = number - row * (int)(input->columns);
	for ( k = -window; k <= window; k++ ) {
		c = column + k;
		i = k + window;
		if ( c > 0 && c < input->columns ) {
			for ( l = -window; l <= window; l++ ) {
				r = row + l;
				j = l + window;
				if ( r > 0 && r < input->rows ) {
					psf_1 += input->pixels[ r * input->columns + c ] * gsl_matrix_get(psf_matrix, i, j);
					psf_2 += current->pixels[ r * current->columns + c ] * gsl_matrix_get(psf_matrix, i, j);
				}
			}
		}
	}
	if ( psf_2 <= 0 ) {
		return 1;
	}
	return psf_1 / psf_2;
}

double super_image_convolve_1(SuperImage*input, int number, int window, gsl_matrix *psf_matrix)
{
	double psf, psf_1, psf_2;
	int k, l, c, r, i, j;
	int column, row;
	psf = 0;
	psf_2 = 0;
	row = number / (int)(input->columns);
	column = number - row * (int)(input->columns);
	for ( k = -window; k <= window; k++ ) {
		c = column + k;
		i = k + window;
		if ( c > 0 && c < input->columns ) {
			for ( l = -window; l <= window; l++ ) {
				r = row + l;
				j = l + window;
				if ( r > 0 && r < input->rows ) {
					psf_1 = gsl_matrix_get(psf_matrix, i, j);
					psf_2 += psf_1;
					psf += input->pixels[ r * input->columns + c ] * psf_1;//gsl_matrix_get(psf_matrix, i, j);
				}
			}
		}
	}
	if ( psf_2 > 0 ) {
		psf /= psf_2;
	}
	return psf;
}

double super_image_convolve_0(SuperImage*output, SuperImage*current, int row, int column, int r, int c)
{
	double conv;
	int k, l, i, j;
	conv = 0;
	for ( i = 0; i < output->columns; i++ ) {
		k = c + i;
		if ( k > 0 && k < output->columns ) {
			for ( j = 0; j < output->rows; j++ ) {
				l = r + j;
				if ( l > 0 && l < output->rows ) {
					conv += current->pixels[ j * current->columns + i ] * output->pixels[ l * output->columns + k ];
				}
			}
		}
	}
	return conv;
}

double super_image_laplace(SuperImage*input, int number)
{
	double laplace;
	int column, row;
	int c, r;
	row = number / (int)(input->columns);
	column = number - row * (int)(input->columns);
	laplace = 0;
	r = row;
	c = column - 1;
	if ( c < 0 )
		c = 1;
	laplace += input->pixels[ r * input->columns + c ] - input->pixels[ row * input->columns + column ];
	c = column + 1;
	if ( c >= input->columns )
		c = input->columns - 2;
	laplace += input->pixels[ r * input->columns + c ]- input->pixels[ row * input->columns + column ];
	c = column;
	r = row - 1;
	if ( r < 0 )
		r = 1;
	laplace += input->pixels[ r * input->columns + c ] - input->pixels[ row * input->columns + column ];
	r = row + 1;
	if ( r >= input->rows )
		r = input->rows - 2;
	laplace += input->pixels[ r * input->columns + c ] - input->pixels[ row * input->columns + column ];
	return laplace / 2.0;
}

gsl_matrix *psf_matrix_set(int n, double sigma)
{
	int i, j, k, l;
	double psf_1, psf_2;
	gsl_matrix *psf_matrix;
	psf_matrix = gsl_matrix_calloc ((size_t) 2 * n + 1, (size_t) 2 * n + 1);
	psf_2 = 0;
	for ( i = -n; i <= n; i++ ) {
		k = i + n;
		for ( j = -n; j <= n; j++ ) {
			l =  j + n;
			psf_1 = microscope_point_spread_func((double)i, (double)j, sigma);
			psf_2 += psf_1;
			gsl_matrix_set (psf_matrix, (size_t) k, (size_t) l, psf_1);
		}
	}
//	gsl_matrix_scale(psf_matrix, 1.0 / psf_2 );
/*
fprintf(stderr, "PSF:\n\n");
	for ( i = -n; i <= n; i++ ) {
		k = i + n;
		for ( j = -n; j <= n; j++ ) {
			l =  j + n;
fprintf(stderr, "%.8f ", gsl_matrix_get (psf_matrix, (size_t) k, (size_t) l ));
		}
fprintf(stderr, "\n");
	}*/
	return psf_matrix;
}

void psf_matrix_normalize(gsl_matrix *psf_matrix)
{
	int i, j;
	double psf_2;
	psf_2 = 0;
	for ( i = 0; i < psf_matrix->size1; i++ ) {
		for ( j = 0; j < psf_matrix->size2; j++ ) {
			psf_2 += gsl_matrix_get (psf_matrix, (size_t) i, (size_t) j);
		}
	}
	gsl_matrix_scale(psf_matrix, 1.0 / psf_2 );
}

void init_psf_func(int *window, double sigma, char*psf_type)
{
	if ( !strcmp(psf_type, "bessel") ) {
		if ( (*window) < 0 )
			*window = (int)( 1.0 / sigma * M_PI + 2 );
		microscope_point_spread_func = microscope_point_spread_func_bessel;
	} else if ( !strcmp(psf_type, "gauss") ) {
		if ( (*window) < 0 )
			*window = (int) ( 6.0 / sigma );
		microscope_point_spread_func = microscope_point_spread_func_gauss;
	} else if ( !strcmp(psf_type, "exp") ) {
		if ( (*window) < 0 )
			*window = (int) ( 6.0 / sigma );
		microscope_point_spread_func = microscope_point_spread_func_exp;
	} else if ( !strcmp(psf_type, "confocal_bessel") ) {
		if ( (*window) < 0 )
			*window = (int)( 1.0 / sigma * M_PI + 2 );
		microscope_point_spread_func = microscope_point_spread_func_confocal_bessel;
	} else if ( !strcmp(psf_type, "ones") ) {
		if ( (*window) < 0 )
			*window = (int)( sigma );
		microscope_point_spread_func = microscope_point_spread_func_ones;
	} else {
		g_error("Unknown psf type %s", psf_type);
	}
}

SuperImage*super_image_deconvolve_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, int iter_max, double rho, double criterion)
{
	SuperImage*output, *current;
	int toggle;
	int iter;
	double psf, laplace, stopping, dot, dott;
	gsl_matrix *psf_matrix;
	int i, j, k;
	init_psf_func(window, sigma, psf_type);
	psf_matrix = psf_matrix_set((*window), sigma);
	current = SuperImageCopy(input);
	output = SuperImageClone(input);
	for ( iter = 0; iter < iter_max; iter++ ) {
		for ( i = 0; i < input->number; i++ ) {
			psf = super_image_convolve_1(current, i, (*window), psf_matrix);
			if ( psf > 0 ) {
				output->pixels[i] = input->pixels[i] / psf;
			} else {
				output->pixels[i] = 1.0;
			}
		}
		stopping = 0;
		for ( i = 0; i < input->number; i++ ) {
			psf = super_image_convolve_1(output, i, (*window), psf_matrix);
			laplace = super_image_laplace(current, i);
			dott = current->pixels[i];
			dot = dott * psf / ( 1.0 - rho * laplace );
			stopping += ( dott - dot ) * ( dott - dot );
			current->pixels[i] = dot;
		}
		stopping = sqrt ( stopping / (double)(input->number) );
/*fprintf(stderr, "Deconvolution iter=%d criterion=%f\n", iter, stopping);*/
		if ( stopping < criterion ) {
			break;
		}
	}
	SuperImageDelete(output);
	gsl_matrix_free(psf_matrix);
	return current;
}

double super_image_sum(SuperImage*current)
{
	double res = 0;
	int i;
	for ( i = 0; i < current->number; i++ ) {
		res += current->pixels[i];
	}
	return res;
}

SuperImage*super_image_deconvolve_blind_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, int iter_max, double rho, double criterion, int iter_blind_max, double **psf_array)
{
	SuperImage*output, *current;
	int iter;
	int iter_blind;
	int r, c, row, column;
	double psf, laplace, stopping, dot, dott, input_energy;
	gsl_matrix *psf_matrix;
	int i, j, k, psf_dim, rows05, columns05;;
	init_psf_func(window, sigma, psf_type);
	psf_matrix = psf_matrix_set((*window), sigma);
	psf_dim = 2 * (*window) + 1;
	rows05 = input->rows / 2;
	columns05 = input->columns / 2;
	current = SuperImageCopy(input);
	output = SuperImageClone(input);
	for ( iter_blind = 0; iter_blind < iter_blind_max; iter_blind++ ) {
		k = 0;
		for ( iter = 0; iter < iter_max; iter++ ) {
			for ( i = 0; i < input->number; i++ ) {
				psf = super_image_convolve_1(current, i, (*window), psf_matrix);
				if ( psf > 0 ) {
					output->pixels[i] = input->pixels[i] / psf;
				} else {
					output->pixels[i] = 1.0;
				}
			}
			stopping = 0;
			for ( i = 0; i < input->number; i++ ) {
				psf = super_image_convolve_1(output, i, (*window), psf_matrix);
				laplace = super_image_laplace(current, i);
				dott = current->pixels[i];
				dot = dott * psf / ( 1.0 - rho * laplace );
				stopping += ( dott - dot ) * ( dott - dot );
				current->pixels[i] = dot;
			}
			stopping = sqrt ( stopping / (double)(input->number) );
			k++;
/*fprintf(stderr, "Blind %d Deconvolution iter=%d criterion=%f\n", iter_blind, iter, stopping);*/
			if ( stopping < criterion ) {
				break;
			}
		}
/* PSF estimation */
		input_energy = super_image_sum(current);
		for ( iter = 0; iter < k; iter++ ) {
			for ( i = 0; i < input->number; i++ ) {
				psf = super_image_convolve_1(current, i, (*window), psf_matrix);
				if ( psf > 0 ) {
					output->pixels[i] = input->pixels[i] / psf / input_energy;
				} else {
					output->pixels[i] = 1.0;
				}
			}
			for ( i = 0; i < psf_dim; i++ ) {
				r = i + rows05;
				for ( j = 0; j < psf_dim; j++ ) {
					c = j + columns05;
					psf = super_image_convolve_0(output, current, row, column, r, c);
					dott = psf_matrix->data[i * psf_dim + j];
					psf_matrix->data[i * psf_dim + j] = dott;
				}
			}
/*fprintf(stderr, "Blind %d PSF iter=%d\n", iter_blind, iter);*/
		}
		psf_matrix_normalize(psf_matrix);
		if ( k == 1 ) {
/*fprintf(stderr, "Blind %d PSF k=%d\n", iter_blind, k);*/
			break;
		}
	}
	SuperImageDelete(output);
	(*psf_array) = (double*)calloc(psf_dim * psf_dim, sizeof(double));
	for ( i = 0; i < psf_dim; i++ ) {
		for ( j = 0; j < psf_dim; j++ ) {
			(*psf_array)[ i * psf_dim + j ] = psf_matrix->data[ i * psf_dim + j ];
		}
	}
	gsl_matrix_free(psf_matrix);
	return current;
}

gsl_matrix *psf_matrix_set_full(int n, double sigma, int rows, int columns)
{
	int i, j, k, l, support;
	double psf_1, psf_2;
	gsl_matrix *psf_matrix;
	support = 2 * n + 1;
	psf_matrix = gsl_matrix_calloc ((size_t) rows, (size_t) columns);
	psf_2 = 0;
	for ( i = -n; i <= n; i++ ) {
		k = i + n;
		for ( j = -n; j <= n; j++ ) {
			l =  j + n;
			psf_1 = microscope_point_spread_func((double)i, (double)j, sigma);
			psf_2 += psf_1;
			gsl_matrix_set (psf_matrix, (size_t) k, (size_t) l, psf_1);
		}
	}
	gsl_matrix_scale(psf_matrix, 1.0 / psf_2 );
	return psf_matrix;
}

gsl_matrix *psf_matrix_set_data(int n, double sigma, int *dim)
{
	int i, j, k, l, support;
	double psf_1, psf_2;
	gsl_matrix *psf_matrix;
	support = 2 * n + 1;
	psf_matrix = gsl_matrix_calloc ((size_t) support, (size_t) support);
	psf_2 = 0;
	for ( i = -n; i <= n; i++ ) {
		k = i + n;
		for ( j = -n; j <= n; j++ ) {
			l =  j + n;
			psf_1 = microscope_point_spread_func((double)i, (double)j, sigma);
			psf_2 += psf_1;
			gsl_matrix_set (psf_matrix, (size_t) k, (size_t) l, psf_1);
		}
	}
	gsl_matrix_scale(psf_matrix, 1.0 / psf_2 );
	*dim = support;
	return psf_matrix;
}

gsl_matrix *psf_matrix_set_data_full(int n, double sigma, int rows_dim, int columns_dim, int *dim)
{
	int i, j, k, l, support, x1, x2;
	double psf_1, psf_2;
	gsl_matrix *psf_matrix;
	support = 2 * n + 1;
	psf_matrix = gsl_matrix_calloc ((size_t) rows_dim, (size_t) columns_dim);
	psf_2 = 0;
	x1 = n;
	x2 = n;
	for ( i = -n; i <= n; i++ ) {
		k = i + n;
		for ( j = -n; j <= n; j++ ) {
			l =  j + n;
			psf_1 = microscope_point_spread_func((double)i, (double)j, sigma);
			psf_2 += psf_1;
			gsl_matrix_set (psf_matrix, (size_t) (i + x1), (size_t) (j + x2), psf_1);
		}
	}
/*	for ( i = -n; i <= n; i++ ) {
		for ( j = -n; j <= n; j++ ) {
			psf_1 = microscope_point_spread_func((double)i, (double)j, sigma);
			psf_2 += psf_1;
		}
	}
	for ( i = 0; i <= n; i++ ) {
		for ( j = 0; j <= n; j++ ) {
			psf_1 = microscope_point_spread_func((double)i, (double)j, sigma);
//			psf_2 += psf_1;
			gsl_matrix_set (psf_matrix, (size_t) i, (size_t) j, psf_1);
		}
	}*/
	gsl_matrix_scale(psf_matrix, 1.0 / psf_2 );
	*dim = support;
	return psf_matrix;
}

double gsl_matrix_convolve(gsl_matrix * a, gsl_matrix * b, int lag1, int lag2, int i1, int i2)
{
	double conv;
	int asize1 = a->size1;
	int asize2 = a->size2;
	int bsize1 = b->size1;
	int bsize2 = b->size2;
	int window1, window2;
	int j1, j2;
	int k1, k2;
	int h1, h2;
	conv = 0;
	window1 = (int)( 0.5 * (double)bsize1 );
	window2 = (int)( 0.5 * (double)bsize2 );
	for ( j1 = -window1; j1 <= bsize1 - window1; j1++ ) {
		k1 = i1 + j1;
		h1 = j1 + window1 + lag1;
		if ( k1 > 0 && k1 < asize1 && h1 > 0 && h1 < bsize1 ) {
			for ( j2 = -window2; j2 <= bsize2 - window2; j2++ ) {
				k2 = i2 + j2;
				h1 = j1 + window2 + lag2;
				if ( k2 > 0 && k2 < asize2 && h2 > 0 && h2 < bsize2 ) {
					conv += a->data[ k1 * asize2 + k2 ] * b->data[ h1 * bsize2 + h2 ];
				}
			}
		}
	}
	return conv;
}

int gsl_matrix_autocorrellation(gsl_matrix * a, gsl_matrix * c)
{
	int result = GSL_SUCCESS;
	int asize1 = a->size1;
	int asize2 = a->size2;
	int csize1 = c->size1;
	int csize2 = c->size2;
	int i1, i2;
	int j1, j2;
	int k1, k2;
	int lag1, lag2;
	if ( csize1 != 2 * asize1 - 1 || csize2 != 2 * asize2 - 1 ) {
		fprintf ( stderr, "Wrong dimensions in gsl_matrix_autocorrelation\n");
		exit(1);
	}
	for ( lag1 =  - asize1; lag1 < asize1 - 1; lag1++ ) {
		i1 = lag1 + asize1;
		for ( lag2 = - asize2; lag2 < asize2 - 1; lag2++ ) {
			i2 = lag2 + asize2;
			c->data[ i1 * csize2 + i2 ] = gsl_matrix_convolve( a, a, lag1, lag2, i1, i2);
		}
	}
	return result;
}

int gsl_matrix_complex_abs(gsl_matrix_complex * a, gsl_matrix_complex * c)
{
	int result = GSL_SUCCESS;
	int asize1 = a->size1;
	int asize2 = a->size2;
	int csize1 = c->size1;
	int csize2 = c->size2;
	int i1, i2;
	int j1, j2;
	int k1, k2;
	int lag1, lag2;
	if ( csize1 != asize1 || csize2 != asize2 ) {
		fprintf ( stderr, "Wrong dimensions in gsl_matrix_complex_abs\n");
		exit(1);
	}
	csize2 *= 2;
	for ( i1 = 0; i1 < asize1; i1++ ) {
		for ( i2 = 0; i2 < asize2; i2++ ) {
			double x = a->data[ i1 * csize2 + 2 * i2 ];
			double y = a->data[ i1 * csize2 + 2 * i2 + 1];
			c->data[ i1 * csize2 + 2 * i2 ] = sqrt( x * x + y * y );
			c->data[ i1 * csize2 + 2 * i2 + 1 ] = 0.0;
		}
	}
	return result;
}

int gsl_matrix_complex_conj(gsl_matrix_complex * a, gsl_matrix_complex * c)
{
	int result = GSL_SUCCESS;
	int asize1 = a->size1;
	int asize2 = a->size2;
	int csize1 = c->size1;
	int csize2 = c->size2;
	int i1, i2;
	int j1, j2;
	int k1, k2;
	int lag1, lag2;
	if ( csize1 != asize1 || csize2 != asize2 ) {
		fprintf ( stderr, "Wrong dimensions in gsl_matrix_complex_conj\n");
		exit(1);
	}
	csize2 *= 2;
	for ( i1 = 0; i1 < asize1; i1++ ) {
		for ( i2 = 0; i2 < asize2; i2++ ) {
			double x = a->data[ i1 * csize2 + 2 * i2];
			double y = a->data[ i1 * csize2 + 2 * i2 + 1];
			c->data[ i1 * csize2 + 2 * i2 ] = x;
			c->data[ i1 * csize2 + 2 * i2 + 1 ] = -y;
		}
	}
	return result;
}

int gsl_matrix_complex_conj_trans(gsl_matrix_complex * a, gsl_matrix_complex * c)
{
	int result = GSL_SUCCESS;
	int asize1 = a->size1;
	int asize2 = a->size2;
	int csize1 = c->size1;
	int csize2 = c->size2;
	int i1, i2;
	int j1, j2;
	int k1, k2;
	int lag1, lag2;
	if ( csize1 != asize1 || csize2 != asize2 || asize1 != asize2 ) {
		fprintf ( stderr, "Wrong dimensions in gsl_matrix_complex_conj\n");
		exit(1);
	}
	csize2 *= 2;
	for ( i1 = 0; i1 < asize1; i1++ ) {
		for ( i2 = 0; i2 < asize2; i2++ ) {
			double x = a->data[ i1 * csize2 + 2 * i2];
			double y = a->data[ i1 * csize2 + 2 * i2 + 1];
			c->data[ i2 * csize2 + 2 * i1 ] = x;
			c->data[ i2 * csize2 + 2 * i1 + 1 ] = -y;
		}
	}
	return result;
}

int gsl_matrix_complex_add_constant_if_zero(gsl_matrix_complex * a, double gamma)
{
	int result = GSL_SUCCESS;
	int asize1 = a->size1;
	int asize2 = a->size2;
	int csize2;
	int i1, i2;
	double addon = 1.0 / gamma;
	csize2 = 2 * asize2;
	for ( i1 = 0; i1 < asize1; i1++ ) {
		for ( i2 = 0; i2 < asize2; i2++ ) {
			double x = a->data[ i1 * csize2 + 2 * i2];
			double y = a->data[ i1 * csize2 + 2 * i2 + 1];
			if ( x == 0.0 && y == 0.0 ) {
				a->data[ i1 * csize2 + 2 * i2 ] = x + addon;
			}
		}
	}
	return result;
}

int gsl_matrix_complex_inv_with_threshold(gsl_matrix_complex * a, double gamma)
{
	int result = GSL_SUCCESS;
	int asize1 = a->size1;
	int asize2 = a->size2;
	int i1, i2;
	double abs2;
	int csize2;
	csize2 = 2 * asize2;
	if ( gamma > 0.0 ) {
		for ( i1 = 0; i1 < asize1; i1++ ) {
			for ( i2 = 0; i2 < asize2; i2++ ) {
				double x = a->data[ i1 * csize2 + 2 * i2];
				double y = a->data[ i1 * csize2 + 2 * i2 + 1];
				double ix = x;
				double iy = -y;
				abs2 = x * x + y * y;
				if ( abs2 * gamma > 1.0 ) {
					a->data[ i1 * csize2 + 2 * i2 ] = ix / abs2;
					a->data[ i1 * csize2 + 2 * i2 + 1 ] = iy / abs2;
				} else {
					a->data[ i1 * csize2 + 2 * i2 ] = gamma * ix;
					a->data[ i1 * csize2 + 2 * i2 + 1 ] = gamma * iy;
				}
			}
		}
	} else {
		for ( i1 = 0; i1 < asize1; i1++ ) {
			for ( i2 = 0; i2 < asize2; i2++ ) {
				double x = a->data[ i1 * csize2 + 2 * i2];
				double y = a->data[ i1 * csize2 + 2 * i2 + 1];
				double ix = x;
				double iy = -y;
				abs2 = x * x + y * y;
				a->data[ i1 * csize2 + 2 * i2 ] = ix / abs2;
				a->data[ i1 * csize2 + 2 * i2 + 1 ] = iy / abs2;
			}
		}
	}
	return result;
}

fftw_complex *invert_psf_matrix_ft(int *window, double sigma, char*psf_type, double gamma, int rows_dim, int columns_dim)
{
	fftw_complex *psf_ft;
	int rows_dim2, psf_dim;
	gsl_matrix_complex_view psf_ft_gsl;
	gsl_matrix *psf_matrix;
	fftw_plan psf_plan;
	unsigned fftw_flags;
	fftw_flags = FFTW_ESTIMATE;
	rows_dim2 = rows_dim * columns_dim;
	psf_matrix = psf_matrix_set_data_full((*window), sigma, rows_dim, columns_dim, &psf_dim);
	psf_ft = (fftw_complex*) fftw_malloc(rows_dim2 * sizeof(fftw_complex));
	psf_ft_gsl = gsl_matrix_complex_view_array((double*)psf_ft, (size_t)rows_dim, (size_t)columns_dim);
	psf_plan = fftw_plan_dft_r2c_2d(rows_dim, columns_dim, psf_matrix->data, psf_ft, fftw_flags);
	fftw_execute(psf_plan);
	gsl_matrix_free(psf_matrix);
	fftw_destroy_plan(psf_plan);
/* Handle singular case */
	if ( gamma > 0 ) {
		gsl_matrix_complex_add_constant_if_zero(&psf_ft_gsl.matrix, gamma);
	}
	gsl_matrix_complex_inv_with_threshold(&psf_ft_gsl.matrix, gamma);
	return psf_ft;
}

SuperImage*super_image_deconvolve_wiener_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, double noise_var, double gamma, double alpha)
{
	SuperImage*output;
	int rows_dim, columns_dim, rows_dim2;
	fftw_complex *img_ft, *psf_ft;
	fftw_complex *img_autocorr_ft;
	fftw_plan img_ft_plan, img_rev_plan;
	unsigned fftw_flags;
	gsl_matrix_complex_view img_ft_gsl, img_autocorr_ft_gsl, psf_ft_gsl, out_ft_gsl;
	gsl_matrix_view img_orig_gsl, img_autocorr_gsl, output_gsl;
	gsl_matrix_complex *worker_gsl, *trans_psf;
	fftw_flags = FFTW_ESTIMATE;
/* PSF */
	init_psf_func(window, sigma, psf_type);
/* Fourie transform of original */
	output = SuperImagePadGeometry(input, (uint32) (*window), (uint32) 0, (uint32) (*window), (uint32) 0);
	img_ft = (fftw_complex*) fftw_malloc(output->number * sizeof(fftw_complex));
	img_ft_plan = fftw_plan_dft_r2c_2d((int)output->rows, (int)output->columns, output->pixels, img_ft, fftw_flags);
	fftw_execute(img_ft_plan);
	img_ft_gsl = gsl_matrix_complex_view_array((double*)img_ft, (int)output->rows, (int)output->columns);
	fftw_destroy_plan(img_ft_plan);
	rows_dim = output->rows;
	columns_dim = output->columns;
/* PSF */
	psf_ft = invert_psf_matrix_ft(window, sigma, psf_type, gamma, rows_dim, columns_dim);
	psf_ft_gsl = gsl_matrix_complex_view_array((double*)psf_ft, (size_t)rows_dim, (size_t)columns_dim);
/* PSD */
	rows_dim2 = rows_dim * columns_dim;
	img_autocorr_ft = (fftw_complex*) fftw_malloc(rows_dim2 * sizeof(fftw_complex));
	img_autocorr_ft_gsl = gsl_matrix_complex_view_array((double*)img_autocorr_ft, rows_dim, columns_dim);
	gsl_matrix_complex_abs (&img_ft_gsl.matrix, &img_autocorr_ft_gsl.matrix);
	gsl_matrix_complex_mul_elements(&img_autocorr_ft_gsl.matrix, &img_autocorr_ft_gsl.matrix);
	gsl_matrix_complex_scale (&img_autocorr_ft_gsl.matrix, gsl_complex_rect ( 1.0 / (double)rows_dim2, 0.0) );
/* */
	trans_psf = gsl_matrix_complex_calloc((size_t)rows_dim, (size_t)columns_dim);
	gsl_matrix_complex_memcpy(trans_psf, &img_autocorr_ft_gsl.matrix);
	gsl_matrix_complex_add_constant ( trans_psf, gsl_complex_rect ( -noise_var, 0.0 ) );
/* Denominator */
	worker_gsl = gsl_matrix_complex_calloc((size_t)rows_dim, (size_t)columns_dim);
	gsl_matrix_complex_memcpy(worker_gsl, &img_autocorr_ft_gsl.matrix);
	gsl_matrix_complex_add_constant ( trans_psf, gsl_complex_rect ( ( alpha - 1.0 ) * noise_var, 0.0 ) );
	gsl_matrix_complex_div_elements (trans_psf, worker_gsl);
	gsl_matrix_complex_mul_elements(trans_psf, &psf_ft_gsl.matrix);
	gsl_matrix_complex_free(worker_gsl);
	fftw_free(psf_ft);
	fftw_free(img_autocorr_ft);
	gsl_matrix_complex_mul_elements(&img_ft_gsl.matrix, trans_psf);
	img_rev_plan = fftw_plan_dft_c2r_2d((int)output->rows, (int)output->columns, img_ft, output->pixels, fftw_flags);
	fftw_execute(img_rev_plan);
	fftw_destroy_plan(img_rev_plan);
	fftw_free(img_ft);
	output_gsl = gsl_matrix_view_array(output->pixels, (int)output->rows, (int)output->columns);
	gsl_matrix_scale (&output_gsl.matrix, 1.0 / (double)output->number );
	output = SuperImageCropGeometry(output, (uint32) 0, (uint32) (*window), (uint32) 0, (uint32) (*window));
/* */
	return output;
}

SuperImage*super_image_deconvolve_tm_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, double noise_var, double gamma)
{
	SuperImage*output,*current;
	double input_energy;
	int rows_dim, columns_dim, rows_dim2, i;
	fftw_complex *img_ft, *psf_ft;
	fftw_complex *img_autocorr_ft;
	fftw_plan img_ft_plan, img_rev_plan;
	unsigned fftw_flags;
	gsl_matrix_complex_view img_ft_gsl, img_autocorr_ft_gsl, psf_ft_gsl, out_ft_gsl;
	gsl_matrix_view img_orig_gsl, img_autocorr_gsl, output_gsl;
	gsl_matrix_complex *worker_gsl, *trans_psf;
	fftw_flags = FFTW_ESTIMATE;
/* PSF */
	init_psf_func(window, sigma, psf_type);
/* Fourie transform of original */
	output = SuperImagePadGeometry(input, (uint32) (*window), (uint32) 0, (uint32) (*window), (uint32) 0);
	current = SuperImageClone(output);
	rows_dim = output->rows;
	columns_dim = output->columns;
	rows_dim2 = rows_dim * columns_dim;
	input_energy = super_image_sum(output);
	for ( i = 0; i < rows_dim2; i++ ) {
		current->pixels[i] = super_image_laplace(output, i) / input_energy;
	}
	img_ft = (fftw_complex*) fftw_malloc(output->number * sizeof(fftw_complex));
	img_ft_plan = fftw_plan_dft_r2c_2d((int)output->rows, (int)output->columns, output->pixels, img_ft, fftw_flags);
	fftw_execute(img_ft_plan);
	img_ft_gsl = gsl_matrix_complex_view_array((double*)img_ft, (int)output->rows, (int)output->columns);
/* PSF */
	psf_ft = invert_psf_matrix_ft(window, sigma, psf_type, gamma, rows_dim, columns_dim);
	psf_ft_gsl = gsl_matrix_complex_view_array((double*)psf_ft, (size_t)rows_dim, (size_t)columns_dim);
/* PSD */
	img_autocorr_ft = (fftw_complex*) fftw_malloc(rows_dim2 * sizeof(fftw_complex));
	fftw_execute_dft_r2c(img_ft_plan, current->pixels, img_autocorr_ft);
	img_autocorr_ft_gsl = gsl_matrix_complex_view_array((double*)img_autocorr_ft, rows_dim, columns_dim);
	fftw_destroy_plan(img_ft_plan);
	gsl_matrix_complex_abs (&img_autocorr_ft_gsl.matrix, &img_autocorr_ft_gsl.matrix);
	gsl_matrix_complex_mul_elements(&img_autocorr_ft_gsl.matrix, &img_autocorr_ft_gsl.matrix);
	gsl_matrix_complex_scale (&img_autocorr_ft_gsl.matrix, gsl_complex_rect ( -noise_var, 0.0) );
	gsl_matrix_complex_add_constant ( &img_autocorr_ft_gsl.matrix, gsl_complex_rect ( 1.0, 0.0 ) );
/* */
	gsl_matrix_complex_mul_elements( &img_autocorr_ft_gsl.matrix, &psf_ft_gsl.matrix);
	fftw_free(psf_ft);
	gsl_matrix_complex_mul_elements(&img_ft_gsl.matrix, &img_autocorr_ft_gsl.matrix);
	fftw_free(img_autocorr_ft);
	img_rev_plan = fftw_plan_dft_c2r_2d((int)output->rows, (int)output->columns, img_ft, output->pixels, fftw_flags);
	fftw_execute(img_rev_plan);
	fftw_destroy_plan(img_rev_plan);
	fftw_free(img_ft);
	output_gsl = gsl_matrix_view_array(output->pixels, (int)output->rows, (int)output->columns);
	gsl_matrix_scale (&output_gsl.matrix, 1.0 / (double)output->number );
	output = SuperImageCropGeometry(output, (uint32) 0, (uint32) (*window), (uint32) 0, (uint32) (*window));
/* */
	return output;
}

SuperImage*super_image_deconvolve_inv_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, double gamma)
{
	SuperImage*output;
	int rows_dim, columns_dim;
	fftw_complex *img_ft, *psf_ft;
	fftw_plan img_ft_plan, img_rev_plan;
	unsigned fftw_flags;
	gsl_matrix_complex_view img_ft_gsl, psf_ft_gsl;
	gsl_matrix_view output_gsl;
	fftw_flags = FFTW_ESTIMATE;
/* PSF */
	init_psf_func(window, sigma, psf_type);
/* Fourie transform of original */
	output = SuperImagePadGeometry(input, (uint32) (*window), (uint32) 0, (uint32) (*window), (uint32) 0);
	img_ft = (fftw_complex*) fftw_malloc(output->number * sizeof(fftw_complex));
	img_ft_plan = fftw_plan_dft_r2c_2d((int)output->rows, (int)output->columns, output->pixels, img_ft, fftw_flags);
	fftw_execute(img_ft_plan);
	img_ft_gsl = gsl_matrix_complex_view_array((double*)img_ft, (int)output->rows, (int)output->columns);
	fftw_destroy_plan(img_ft_plan);
	rows_dim = output->rows;
	columns_dim = output->columns;
	psf_ft = invert_psf_matrix_ft(window, sigma, psf_type, gamma, rows_dim, columns_dim);
	psf_ft_gsl = gsl_matrix_complex_view_array((double*)psf_ft, (size_t)rows_dim, (size_t)columns_dim);
/* */
	gsl_matrix_complex_mul_elements(&img_ft_gsl.matrix, &psf_ft_gsl.matrix);
	fftw_free(psf_ft);
	img_rev_plan = fftw_plan_dft_c2r_2d((int)rows_dim, (int)columns_dim, img_ft, output->pixels, fftw_flags);
	fftw_execute(img_rev_plan);
	fftw_destroy_plan(img_rev_plan);
	fftw_free(img_ft);
	output_gsl = gsl_matrix_view_array(output->pixels, rows_dim, columns_dim);
	gsl_matrix_scale (&output_gsl.matrix, 1.0 / (double)output->number );
	output = SuperImageCropGeometry(output, (uint32) 0, (uint32) (*window), (uint32) 0, (uint32) (*window));
/* */
	return output;
}

