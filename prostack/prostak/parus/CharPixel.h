/***************************************************************************
                          CharPixel.h  -  description
                             -------------------
    begin                : ðÔÎ íÁÒ 25 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <math.h>

#include <CharImage.h>

#ifndef CharPixel_H
#define CharPixel_H

typedef struct CharPixel {
	uint32 index;
	uint32 row;
	uint32 column;
	unsigned char intensity;
	int label;
	int distance;
	struct CharPixel**neigh;
	int Nneigh;
} CharPixel;

CharPixel*NewCharPixel(uint32 row, uint32 column, unsigned char intensity);
CharPixel*NewFictitiousCharPixel();
int compare(const void *a, const void *b);

typedef struct CharPixelStructure {
	CharPixel**pixels;
	uint32 size;
} CharPixelStructure;

CharPixelStructure*NewCharPixelStructure(CharImage*ci);

typedef struct List {
	CharPixel*pixel;
	struct List *next;
	struct List *prev;
} List;

typedef struct fifo {
	List*head;
	List*tail;
	int size;
} fifo;

void fifo_delete(fifo*queue);
fifo*fifo_new();
void fifo_add(fifo*queue, CharPixel*p);
CharPixel*fifo_remove(fifo*queue);
void fifo_add_fictitious(fifo*queue);
int fifo_empty(fifo*queue);
void List_delete(List*head);
int CharPixelAllNeighWSHED(CharPixel *cp);

void CharPixelDelete(CharPixel*cp);
void CharPixelStructureDelete(CharPixelStructure*cps);
CharPixelStructure*NewCharPixelStructure8(CharImage*ci);
void ListAdd(List*plist, CharPixel*cp);
void ListTour(List*plist);
List*InitList(CharPixel*cp);

int CheckBlobInfo(BlobInfo*bi, Blob*b, BlobImage*bimage);
void BlobListAdd(BlobList**list, Blob* b);
Blob*NewBlob(List*plist, CharImage*data, int label);
Centroid*NewCentroid(Blob*b, uint32 columns, uint32 rows);
int BlobListCompare(void *p, void *q, void *user);

Blob*DoBlob();
Centroid*DoCentroid();

typedef struct BlobData {
	double mean;
	double var;
	double stdev;
	double max;
	double min;
	double median;
	double area;
	double muc;
} BlobData;

BlobData* GetBlobData(unsigned char*data, uint32 *indices, uint32 n);
BlobData* GetBlobData1(unsigned char*data, uint32 *indices, uint32 n, char*tag);
void BlobListDelete(BlobList*cur);
BlobImage*BlobImageEraseFictitious(BlobImage*bi);
void BolbDataDelete(BlobData*p);
Centroid*NewCentroidPolar(Blob*b, uint32 columns, uint32 rows, double xmean, double ymean);

BlobData* GetBlobDataSuper(double*data, uint32 *indices, uint32 n);
void init_psf_func(int *window, double sigma, char*psf_type);

#endif
