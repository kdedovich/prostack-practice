/***************************************************************************
 *            CharVolume.c
 *
 *  Mon Jun 20 12:38:36 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  aint with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <math.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <grf.h>
#include <CharImage.h>
#include <CharVolume.h>
#include <ImageInfo.h>
/*#define BLI_IMAGE3D*/


#ifdef _OPENMP
	#include <omp.h>
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
#endif


static CharVolume*sv;
static int counter = 0;


CharVolume*CharVolumeResample(CharVolume*ci, double rfactor, double cfactor)
{
	CharVolume*resized;
	resized = NewCharVolume( (uint32)(cfactor * (double)ci->columns + 0.5), (uint32)(rfactor * (double)ci->rows + 0.5) , ci->planes);
/* rfactor plays in vertical direction */
	if ( rfactor > cfactor ) {
		CharVolume*resizedvert;
		if ( cfactor > 1 ) {
			resizedvert = NewCharVolume( ci->columns, (uint32)(rfactor * (double)ci->rows + 0.5 ) , ci->planes);
			CharVolumeResampleVertical( ci, rfactor, resizedvert );
			CharVolumeResampleHorizontal( resizedvert, cfactor, resized );
			CharVolumeDelete(resizedvert);
		} else {
			CharVolumeResampleVertical( ci, rfactor, resized );
		}
	} else {
		CharVolume*resizedhor;
		if ( rfactor > 1 ) {
			resizedhor = NewCharVolume( (uint32)(cfactor * (double)ci->columns + 0.5 ), ci->rows , ci->planes);
			CharVolumeResampleHorizontal( ci, cfactor, resizedhor);
			CharVolumeResampleVertical( resizedhor, rfactor, resized);
			CharVolumeDelete(resizedhor);
		} else {
			CharVolumeResampleHorizontal( ci, cfactor, resized);
		}
	}
	return resized;
}


void CharVolumeResampleVertical(CharVolume*ci, double factor, CharVolume*resized)
{
	uint32 row, plane;
	uint32 column;
	uint32 center;
	for ( plane = 0; plane < ci->planes; plane++ ) {
		for ( row = 0; row < resized->rows; row++ ) {
			center = (uint32)( (double)row + 0.5 ) / factor;
			for ( column = 0; column < resized->columns; column++ ) {
				resized->voxels[column][row][plane] = ci->voxels[column][center][plane];
			}
		}
	}
}


void CharVolumeResampleHorizontal(CharVolume*ci, double factor, CharVolume*resized)
{
	uint32 row, plane;
	uint32 column;
	uint32 center;
	for ( plane = 0; plane < ci->planes; plane++ ) {
		for ( column = 0; column < resized->columns; column++ ) {
			center = (uint32)( (double)column + 0.5 ) / factor;
			for ( row = 0; row < resized->rows; row++ ) {
				resized->voxels[column][row][plane] = ci->voxels[center][row][plane];
			}
		}
	}
}


CharVolume*CharVolumeShrink(CharVolume*ci, double rfactor, double cfactor)
{
	uint32 row, plane;
	uint32 column;
	uint32 row2;
	uint32 column2;
	uint32 rcenter;
	uint32 ccenter;
	CharVolume*resized;
	resized = NewCharVolume( (uint32)(cfactor * (double)ci->columns + 0.5), (uint32)(rfactor * (double)ci->rows + 0.5) , ci->planes);
	row2 = (uint32)( (double)resized->rows / 2.0 + 0.5);
	column2 = (uint32)( (double)resized->columns / 2.0  + 0.5);
/* rfactor plays in vertical direction */
	for ( plane = 0; plane < ci->planes; plane++ ) {
		for ( row = 0; row < row2; row++ ) {
			rcenter = (uint32)( (double)row / rfactor );
			for ( column = 0; column < column2; column++ ) {
				ccenter = (uint32)( (double)column / cfactor );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
			for ( column = column2; column < resized->columns; column++ ) {
				ccenter = (uint32)( (double)column / cfactor  + 0.5 );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
		}
		for ( row = row2; row < resized->rows; row++ ) {
			rcenter = (uint32)( (double)row / rfactor + 0.5);
			for ( column = 0; column < column2; column++ ) {
				ccenter = (uint32)( (double)column / cfactor );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
			for ( column = column2; column < resized->columns; column++ ) {
				ccenter = (uint32)( (double)column / cfactor  + 0.5 );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
		}
	}
	return resized;
}

CharVolume*CharVolumeExpand(CharVolume*ci, double rfactor, double cfactor)
{
	uint32 row, plane;
	uint32 column;
	uint32 row2;
	uint32 column2;
	uint32 rcenter;
	uint32 ccenter;
	CharVolume*resized;
	resized = NewCharVolume( (uint32)(cfactor * (double)ci->columns), (uint32)(rfactor * (double)ci->rows), ci->planes );
	row2 = (uint32)( (double)resized->rows / 2.0 + 0.5);
	column2 = (uint32)( (double)resized->columns / 2.0  + 0.5);
/* rfactor plays in vertical direction */
	for ( plane = 0; plane < ci->planes; plane++ ) {
		for ( row = 0; row < row2; row++ ) {
			rcenter = (uint32)( (double)row / rfactor );
			for ( column = 0; column < column2; column++ ) {
				ccenter = (uint32)( (double)column / cfactor );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
			for ( column = column2; column < resized->columns; column++ ) {
				ccenter = (uint32)( (double)column / cfactor  + 0.5 );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
		}
		for ( row = row2; row < resized->rows; row++ ) {
			rcenter = (uint32)( (double)row / rfactor + 0.5);
			for ( column = 0; column < column2; column++ ) {
				ccenter = (uint32)( (double)column / cfactor );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
			for ( column = column2; column < resized->columns; column++ ) {
				ccenter = (uint32)( (double)column / cfactor  + 0.5 );
				resized->voxels[column][row][plane] = ci->voxels[ccenter][rcenter][plane];
			}
		}
	}
	return resized;
}

CharVolume*CharVolumeTranspose(CharVolume*dp)
{
	CharVolume*di;
	uint32 c, r, p;
	di = NewCharVolume(dp->rows, dp->columns, dp->planes);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,di)
	for ( p = 0; p < di->planes; p++ ) {
		for ( c = 0; c < di->columns; c++ ) {
			for ( r = 0; r < di->rows; r++ ) {
				di->voxels[c][r][p] = dp->voxels[r][dp->rows - c - 1][p];
			}
		}
	}
	CharVolumeDelete(dp);
	return di;
}


CharVolume*CharVolumeReverseColumn(CharVolume*dp)
{
	CharVolume*di;
	uint32 c, r, p;
	di = NewCharVolume(dp->columns, dp->rows, dp->planes);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,di)
	for ( p = 0; p < di->planes; p++ ) {
		for ( c = 0; c < di->columns; c++ ) {
			for ( r = 0; r < di->rows; r++ ) {
				di->voxels[c][r][p] = dp->voxels[dp->columns - c - 1][r][p];
			}
		}
	}
	CharVolumeDelete(dp);
	return di;
}


CharVolume*CharVolumeReverseRow(CharVolume*dp)
{
	CharVolume*di;
	uint32 c, r, p;
	di = NewCharVolume(dp->columns, dp->rows, dp->planes);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,di)
	for ( p = 0; p < di->planes; p++ ) {
		for ( c = 0; c < di->columns; c++ ) {
			for ( r = 0; r < di->rows; r++ ) {
				di->voxels[c][r][p] = dp->voxels[c][dp->rows - r - 1][p];
			}
		}
	}
	CharVolumeDelete(dp);
	return di;
}

CharVolume*CharVolumeMul(CharVolume*di, CharVolume*di_j)
{
	uint32 column, row, plane;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(di_j,di)
	for ( row = 0; row < di->rows; row++ ) {
		for ( column = 0; column < di->columns; column++ ) {
				for ( plane = 0; plane < di->planes; plane++ ) {
					uint32 p = (plane < di_j->planes) ? plane : 0;
					di->voxels[column][row][plane] = (unsigned char)floor( (double)di->voxels[column][row][plane] * (double)di_j->voxels[column][row][p] / (double)FilledPixel + 0.5);
				}
		}
	}
	return di;
}

CharVolume*CharVolumeCropGeometry(CharVolume*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset)
{
	uint32 column, row, plane;
	uint32 Columns, Rows;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;
	CharVolume*di;
	rmax = dp->rows -1 - LowerOffset;
	cmax = dp->columns -1 - RightOffset;
	rmin = UpperOffset;
	cmin = LeftOffset;
	Rows = (uint32)( rmax - rmin + 1 );
	Columns = (uint32)( cmax - cmin + 1 );
	di = NewCharVolume(Columns, Rows, dp->planes);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,di,cmin,rmin)
	for ( plane = 0; plane < di->planes; plane++ ) {
		for ( column = 0; column < di->columns; column++ ) {
			for ( row = 0; row < di->rows; row++ ) {
				di->voxels[column][row][plane] = dp->voxels[column + cmin][row + rmin][plane];
			}
		}
	}
	CharVolumeDelete(dp);
	return di;
}


CharVolume*CharVolumePadGeometry(CharVolume*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset)
{
	uint32 column, row, plane;
	uint32 Columns, Rows;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;
	CharVolume*di;
	rmax = dp->rows + LowerOffset;
	cmax = dp->columns + RightOffset;
	rmin = UpperOffset;
	cmin = LeftOffset;
	Rows = (uint32)( rmax + rmin );
	Columns = (uint32)( cmax + cmin );
	di = NewCharVolume(Columns, Rows, dp->planes);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,di,cmin,rmin)
	for ( plane = 0; plane < di->planes; plane++ ) {
		for ( column = 0; column < dp->columns; column++ ) {
			for ( row = 0; row < dp->rows; row++ ) {
				di->voxels[column + cmin][row + rmin][plane] = dp->voxels[column][row][plane];
			}
		}
	}
	CharVolumeDelete(dp);
	return di;
}


CharVolume*CharVolumeRotate(CharVolume*di, double theta)
{
	CharVolume*ci;
	uint32 column, row, plane;
	uint32 column1, row1;
	uint32 column2, row2;
	double a00, a01, a10, a11;
	double z00, z01, z10, z11;
	double rdot, rmid;
	double cdot, cmid;
	unsigned char *pixels;
	ci = CharVolumeClone(di);
	a00 = cos( -theta );
	a01 = -sin( -theta );
	a10 = sin( -theta );
	a11 = cos( -theta );
	rdot = (double)( di->rows - 1);
	rmid = 0.5 * rdot;
	cdot = (double)( di->columns - 1);
	cmid = 0.5 * cdot;
	for ( plane = 0; plane < di->planes; plane++ ) {
		for ( row = 0; row < di->rows; row++ ) {
			rdot = (double)( row - rmid );
			for ( column = 0; column < di->columns; column++ ) {
				double c;
				double r;
				double x;
				double y;
				double c1;
				double r1;
				double c2;
				double r2;
				double pix;
				cdot = (double)( column - cmid );
				c = a00 * cdot + a01 * rdot;
				r = a10 * cdot + a11 * rdot;
				if ( c > 0 ) {
					c1 = floor(c + cmid);
					x = c + cmid - c1;
					c2 = c1 + 1.0;
				} else {
					c1 = ceil(c + cmid);
					x = c1 - c - cmid;
					c2 = c1 - 1.0;
				}
				if ( r > 0 ) {
					r1 = floor(r + rmid);
					y = r + rmid - r1;
					r2 = r1 + 1.0;
				} else {
					r1 = ceil(r + rmid);
					y = r1 - r - rmid;
					r2 = r1 - 1.0;
				}
				z00 = (double)EmptyPixel;
				z10 = (double)EmptyPixel;
				z01 = (double)EmptyPixel;
				z11 = (double)EmptyPixel;
				column1 = (uint32)c1;
				row1 = (uint32)r1;
				column2 = (uint32)c2;
				row2 = (uint32)r2;
				if ( 0 <= c1 && c1 < (double)di->columns && 0 <= r1 && r1 < (double)di->rows )
					z00 = (double)di->voxels[column1][row1][plane];
				if ( 0 <= c1 && c1 < (double)di->columns && 0 <= r2 && r2 < (double)di->rows )
					z10 = (double)di->voxels[column1][row2][plane];
				if ( 0 <= c2 && c2 < (double)di->columns && 0 <= r1 && r1 < (double)di->rows )
					z01 = (double)di->voxels[column2][row1][plane];
				if ( 0 <= c2 && c2 < (double)di->columns && 0 <= r2 && r2 < (double)di->rows )
					z11 = (double)di->voxels[column2][row2][plane];
				pix = z00 * ( 1 - x - y + x * y);
				pix += z01 * ( x - x * y);
				pix += z10 * ( y - x * y);
				pix += z11 * x * y;
				if ( z00 != (double)EmptyPixel && z00 == z01 && z00 == z11 && z00 == z10 )
					ci->voxels[column][row][plane] = di->voxels[column1][row1][plane];
				else
					ci->voxels[column][row][plane] = (unsigned char)floor(pix);
			}
		}
	}
	return ci;
}


CharImage*CharVolume2Image(CharVolume*di, char*oper)
{
	CharImage*ci;
	uint32 row, column, plane;
	unsigned char pix, vox;
	ci = CharImageNew(di->columns, di->rows);
	if ( !strcmp(oper, "max") ) {
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(ci,di) private(pix,vox,plane)
		for ( row = 0; row < ci->rows; row++ ) {
			for ( column = 0; column < ci->columns; column++ ) {
				pix = EmptyPixel;
				for ( plane = 0; plane < di->planes; plane++ ) {
					vox = di->voxels[column][row][plane];
					if ( pix < di->voxels[column][row][plane] )
						pix = vox;
				}
				ci->pixels[ row * ci->columns + column ] = pix;
			}
		}
	}
	return ci;
}

void CharVolumeDelete(CharVolume*cv)
{
	int i, j;
	for ( i = 0; i < cv->columns; i++) {
		for ( j = 0; j < cv->rows; j++) {
			free(cv->voxels[i][j]);
		}
		free(cv->voxels[i]);
	}
	free(cv->voxels);
	free(cv);
}

CharVolume*NewCharVolume(uint32 columns, uint32 rows, uint32 planes)
{
	CharVolume*cv;
	int i, j, k;
	cv = (CharVolume*)malloc(sizeof(CharVolume));
	cv->columns = columns;
	cv->rows = rows;
	cv->planes = planes;
	cv->voxels = (unsigned char***)calloc(columns, sizeof(unsigned char**));
	for ( i = 0; i < columns; i++) {
		cv->voxels[i] = (unsigned char**)calloc(rows, sizeof(unsigned char*));
		for ( j = 0; j < rows; j++) {
			cv->voxels[i][j] = (unsigned char*)calloc(planes, sizeof(unsigned char));
			for ( k = 0; k < planes; k++)
				cv->voxels[i][j][k] = EmptyPixel;
		}
	}
	return cv;
}

void CharVolumeAddPlane(CharVolume*cv, CharImage*ci, int plane)
{
	int i, j;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,ci,plane)
	for ( i = 0; i < cv->columns; i++) {
		for ( j = 0; j < cv->rows; j++) {
			cv->voxels[i][j][plane] = ci->pixels[ j * ci->columns + i];
		}
	}
}


CharImage*CharVolumeGetPlane(CharVolume*cv, int plane)
{
	int i, j, k;
	CharImage*ci;
	ci = CharImageNew(cv->columns, cv->rows);
	k = (plane > cv->planes - 1) ? 0 : plane;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,ci,k)
	for ( i = 0; i < cv->columns; i++) {
		for ( j = 0; j < cv->rows; j++) {
			ci->pixels[ j * ci->columns + i]= cv->voxels[i][j][k];
		}
	}
	return ci;
}

void CharVolumeSetPlane(CharVolume*cv, CharImage*new_plane, int plane)
{
	int i, j, l;
	if (plane > -1 && plane < cv->planes) {
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,new_plane,plane)
		for ( i = 0; i < cv->columns; i++) {
			for ( j = 0; j < cv->rows; j++) {
				cv->voxels[i][j][plane] = new_plane->pixels[ j * new_plane->columns + i];
			}
		}
	} else {
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(cv,new_plane,plane)
		for ( i = 0; i < cv->columns; i++) {
			for ( j = 0; j < cv->rows; j++) {
				for ( l = 0; l < cv->planes; l++) {
					cv->voxels[i][j][l] = new_plane->pixels[ j * new_plane->columns + i];
				}
			}
		}
	}
}


CharImage*CharVolumeGetRow(CharVolume*cv, int row)
{
	int i, j;
	CharImage*ci;
/*	ci->rows = cv->planes;
	ci->columns = cv->columns;
*/
	ci = CharImageNew(cv->columns, cv->planes);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,ci,row)
	for ( i = 0; i < cv->columns; i++) {
		for ( j = 0; j < cv->planes; j++) {
			ci->pixels[ j * ci->columns + i]= cv->voxels[i][row][j];
		}
	}
	return ci;
}

CharImage*CharVolumeGetColumn(CharVolume*cv, int column)
{
	int i, j;
	CharImage*ci;
/*	ci->rows = cv->planes;
	ci->columns = cv->columns;
*/
	ci = CharImageNew( cv->planes, cv->rows);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,ci,column)
	for ( i = 0; i < cv->rows; i++) {
		for ( j = 0; j < cv->planes; j++) {
			ci->pixels[ i * ci->columns + j]= cv->voxels[column][i][j];
		}
	}
	return ci;
}


CharVoxel*NewCharVoxel(uint32 row, uint32 column, uint32 plane, unsigned char intensity)
{
	CharVoxel*cv;
	cv = (CharVoxel*)malloc(sizeof(CharVoxel));
	cv->row = row;
	cv->column = column;
	cv->plane = plane;
	cv->intensity = intensity;
	cv->label = INIT;
	cv->distance = 0;
	return cv;
}

CharVoxel*NewFictitiousCharVoxel()
{
	CharVoxel*cv;
	cv = (CharVoxel*)malloc(sizeof(CharVoxel));
	cv->label = FICTITIOUS;
	return cv;
}

int compare_voxels(const void *a, const void *b)
{
	CharVoxel**x;
	CharVoxel**y;
	CharVoxel*x1;
	CharVoxel*y1;
	x = (CharVoxel**)a;
	y = (CharVoxel**)b;
	x1 = *x;
	y1 = *y;
	if ( x1->intensity > y1->intensity ) return 1;
	if ( x1->intensity < y1->intensity ) return -1;
	return 0;
}

CharVoxelStructure*NewCharVoxelStructure(CharVolume*cv, int connectivity )
{
	CharVoxelStructure*cvs;
	uint32 row;
	uint32 column;
	uint32 plane;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("NewCharVoxelStructure connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	cvs = (CharVoxelStructure*)malloc(sizeof(CharVoxelStructure));
	cvs->size = cv->planes * cv->columns * cv->rows;
	cvs->voxels = (CharVoxel**)calloc(cvs->size, sizeof(CharVoxel*));
	for ( column = 0; column < cv->columns ; column++ ) {
		for ( row = 0; row < cv->rows ; row++ ) {
			for ( plane = 0; plane < cv->planes ; plane++ ) {
				cvs->voxels[ plane * ( cv->columns * cv->rows ) + row * cv->columns +column ] = NewCharVoxel( row, column, plane, cv->voxels[column][row][plane]);
			}
		}
	}

	for ( plane = 0; plane < cv->planes; plane++ ) {
		for ( row = 0; row < cv->rows ; row++ ) {
			for ( column = 0; column < cv->columns ; column++ ) {
				int N;
				cvs->voxels[plane * ( cv->columns * cv->rows ) + row * cv->columns +column]->neigh = (struct CharVoxel**)NULL;
				N = 0;
				for ( n = 0; n < next; n += 3) {
					int c, r, p;
					c = ext[n] + column;
					r = ext[n+1] + row;
					p = ext[n+2] + plane;
					if ( ( 0 <= c ) && ( c < cv->columns) && ( 0 <= r ) && ( r < cv->rows ) && ( 0 <= p ) && ( p < cv->planes ) ) {
						cvs->voxels[ plane * ( cv->columns * cv->rows ) + row * cv->columns +column ]->neigh = (struct CharVoxel**)realloc(cvs->voxels[ plane * ( cv->columns * cv->rows ) + row * cv->columns +column ]->neigh, ( N + 1 ) * sizeof(struct CharVoxel*));
						cvs->voxels[ plane * ( cv->columns * cv->rows ) + row * cv->columns +column ]->neigh[N] = cvs->voxels[ p * ( cv->columns * cv->rows ) + r * cv->columns +c ];
						N++;
					}
				}
				cvs->voxels[ plane * ( cv->columns * cv->rows ) + row * cv->columns +column ]->Nneigh = N;
			}
		}
	}
	return cvs;
}

void VoxelFifoDelete(VoxelFifo*queue)
{
	if(queue->head)VoxelListDelete(queue->head);
	free(queue);
}

VoxelFifo*VoxelFifoNew()
{
	VoxelFifo*q;
	q = (VoxelFifo*)malloc(sizeof(VoxelFifo));
	q->head = (VoxelList*)NULL;
	q->tail = (VoxelList*)NULL;
	q->size = 0;
	return q;
}

void VoxelFifoAdd(VoxelFifo*queue, CharVoxel*cv)
{
	VoxelList*p;
	p = (VoxelList*)malloc(sizeof(VoxelList));
	p->pixel = cv;
	if ( queue->head ) {
		p->next = queue->head;
		p->next->prev = p;
	} else {
		p->next = (struct VoxelList*)NULL;
	}
	p->prev = (struct VoxelList*)NULL;
	queue->head = p;
	queue->size++;
	if (!queue->tail) queue->tail = p;
/*
fprintf(stdout,"\nfifo_add: c: %u ; r: %u ; size: %d ; label: %d ",cp->row,cp->column,queue->size, cp->label);
*/
}

CharVoxel*VoxelFifoRemove(VoxelFifo*queue)
{
	CharVoxel*cp;
	VoxelList*t;
	t = queue->tail;
	cp = t->pixel;
	queue->tail = queue->tail->prev;
	if (queue->tail) queue->tail->next = (VoxelList*)NULL;
	free(t);
	queue->size--;
	return cp;
}

void VoxelFifoAddFictitious(VoxelFifo*queue)
{
	VoxelList*p;
	p = (VoxelList*)malloc(sizeof(VoxelList));
	p->pixel = NewFictitiousCharVoxel();
	if ( queue->head ) {
		p->next = queue->head;
		p->next->prev = p;
	} else {
		p->next = (VoxelList*)NULL;
	}
	p->prev = (VoxelList*)NULL;
	queue->head = p;
	queue->size++;
	if (!queue->tail) queue->tail = p;
}

int VoxelFifoEmpty(VoxelFifo*queue)
{
	if (queue->size == 0) return 1;
	return 0;
}

void VoxelListDelete(VoxelList*head)
{
	if ( head->next )
		VoxelListDelete(head->next);
	free(head);
}

int CharVoxelAllNeighWSHED(CharVoxel *cp)
{
	int i;
	for ( i = 0; i < cp->Nneigh; i++)
		if ( cp->neigh[i]->label != WSHED )
			return 0;
	return 1;
}

void CharVoxelDelete(CharVoxel*cp)
{
	free(cp->neigh);
	free(cp);
}

void CharVoxelStructureDelete(CharVoxelStructure*cps)
{
	uint32 i;
	for ( i = 0; i < cps->size; i++ ) {
		CharVoxelDelete(cps->voxels[i]);
	}
	free(cps->voxels);
	free(cps);
}

void VoxelListAdd(VoxelList*plist, CharVoxel*cp)
{
	VoxelList*cur,*targ;
	cur = plist;
	while (cur) {
		targ = cur;
		cur = cur->next;
	}
	targ->next = InitVoxelList(cp);
}

void VoxelListTour(VoxelList*plist)
{
	VoxelList*cur;
	uint32 i;
	cur = plist;
	while (cur) {
		CharVoxel*cp;
		cp = cur->pixel;
		for ( i = 0; i < cp->Nneigh; i++ ) {
			if ( cp->neigh[i]->intensity == FilledPixel && cp->neigh[i]->distance == 0 ) {
				VoxelListAdd(plist,cp->neigh[i]);
				cp->neigh[i]->distance = 1;
			}
		}
		cur = cur->next;
	}
}

VoxelList*InitVoxelList(CharVoxel*cp)
{
	VoxelList*p;
	p = (VoxelList*)malloc(sizeof(VoxelList));
	p->pixel = cp;
	p->next = (VoxelList*)NULL;

	return p;
}

void CharVolumeFastWaterShed(CharVolume*di, int connectivity)
{
	CharVoxelStructure *cvs;
	CharVoxel *cv;
	int IntensityLevel, HMIN, HMAX;
	int current_dist;
	int index1, index2;
	uint32 i, j;
	int current_label;
	VoxelFifo *queue;
	cvs = NewCharVoxelStructure(di, connectivity);
	qsort((void *)cvs->voxels,(size_t) cvs->size,(size_t) sizeof(CharVoxel*), compare_voxels);
	current_label = 0;
	current_dist = 0;
	index1 = 0;
	index2 = 0;
	HMIN = (int)cvs->voxels[0]->intensity;
	HMAX = (int)cvs->voxels[cvs->size-1]->intensity;
	queue = VoxelFifoNew();
	for ( IntensityLevel = HMIN; IntensityLevel <= HMAX; IntensityLevel++ ) {
		for ( i = index1; i < cvs->size; i++) {
			cv = cvs->voxels[i];
			if ( (int)cv->intensity != IntensityLevel ) {
				index1 = i;
				break;
			}
			cv->label = MASK;
			for ( j = 0; j < cv->Nneigh; j++) {
				if ( cv->neigh[j]->label >= WSHED ) {
					cv->distance = 1;
					VoxelFifoAdd(queue, cv);
				}
			}
		}/* end for */
		current_dist = 1;
		VoxelFifoAddFictitious(queue);
		while (1) {
			int flag  = 0;
			cv = VoxelFifoRemove(queue);
			if ( cv->label == FICTITIOUS ) {
				if ( VoxelFifoEmpty(queue) ) {
					break;
				} else {
					VoxelFifoAddFictitious(queue);
					current_dist++;
					cv = VoxelFifoRemove(queue);
				}
			}
			for ( j = 0; j < cv->Nneigh; j++) {
				if ( cv->neigh[j]->distance <= current_dist && cv->neigh[j]->label >= WSHED ) {
					if ( cv->neigh[j]->label > WSHED ) {
						if ( cv->label == MASK || cv->label == WSHED ) {
							if ( !flag ) cv->label = cv->neigh[j]->label;
						} else if ( cv->label != cv->neigh[j]->label ) {
							cv->label = WSHED;
							flag = 1;
						}
					} else if ( cv->label == MASK ) {
						cv->label = WSHED;
					}
				} else if ( cv->neigh[j]->label == MASK && cv->neigh[j]->distance == 0 ) {
					cv->neigh[j]->distance = current_dist + 1;
					VoxelFifoAdd(queue, cv->neigh[j]);
				}
			}
		}/* end while */
		for ( i = index2; i < cvs->size; i++) {
			cv = cvs->voxels[i];
			if ( (int)cv->intensity != IntensityLevel ) {
				index2 = i;
				break;
			}
			cv->distance = 0;
			if ( cv->label == MASK ) {
				current_label++;
				cv->label = current_label;
				VoxelFifoAdd(queue, cv);
				while ( !VoxelFifoEmpty(queue) ) {
					cv = VoxelFifoRemove(queue);
					for ( j = 0; j < cv->Nneigh; j++) {
						if ( cv->neigh[j]->label == MASK ) {
							VoxelFifoAdd(queue, cv->neigh[j]);
							cv->neigh[j]->label = current_label;
						}
					}
				}
			}
		}
	}/* end main for */
/*
fprintf(stdout,"\n label: %d",current_label);
*/
	current_label++;
	for ( i = 0; i < cvs->size; i++) {
		di->voxels[cvs->voxels[i]->column][cvs->voxels[i]->row][cvs->voxels[i]->plane] = ( cvs->voxels[i]->label == WSHED && !CharVoxelAllNeighWSHED(cvs->voxels[i]) ) ? EmptyPixel:FilledPixel;
	}
	CharVoxelStructureDelete(cvs);
}


void CharVolumeCheapWaterShed(CharVolume*di, int connectivity)
{
	int ***labels;
	unsigned char ***distances;
	unsigned char IntensityLevel;
	int current_dist;
	int index1, index2;
	unsigned int i, j, k, size;
	int current_label;
	unsigned char HMIN, HMAX;
	unsigned int *indices, index;
	int pix;
	CheapFifo *queue;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("NewCharVoxelStructure connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
/* Instead of cvs */
	size = di->planes * di->columns * di->rows;
	indices = (unsigned int*)calloc(size, sizeof(unsigned int));
	labels = (int***)calloc(di->columns, sizeof(int**));
	distances = (unsigned char***)calloc(di->columns, sizeof(unsigned char**));
	for ( i = 0; i < di->columns; i++) {
		labels[i] = (int**)calloc(di->rows, sizeof(int*));
		distances[i] = (unsigned char**)calloc(di->rows, sizeof(unsigned char*));
		for ( j = 0; j < di->rows; j++) {
			labels[i][j] = (int*)calloc(di->planes, sizeof(int));
			distances[i][j] = (unsigned char*)calloc(di->planes, sizeof(unsigned char));
			for ( k = 0; k < di->planes; k++) {
				labels[i][j][k] = INIT;
				distances[i][j][k] = 0;
				indices[ k * ( di->columns * di->rows ) + j * di->columns + i ] = k * ( di->columns * di->rows ) + j * di->columns + i;
			}
		}
	}
/**/
	sv = di;
	qsort((void *)indices, (size_t)size,(size_t)sizeof(unsigned int), compare_indices);
	sv = (CharVolume*)NULL;
	current_label = 0;
	current_dist = 0;
	index1 = 0;
	queue = CheapFifoNew();
	index2 = (int)( indices[0] % ( di->columns * di->rows ) );
	i = (unsigned int)( index2 % di->columns );
	j = (unsigned int)( (double)( index2 - i ) / (double)di->columns );
	k = (unsigned int)( (double)(indices[0] - index2) / (double)( di->columns * di->rows ) );
	HMIN = di->voxels[i][j][k];
	index2 = (int)( indices[size-1] % ( di->columns * di->rows ) );
	i = (unsigned int)( index2 % di->columns );
	j = (unsigned int)( (double)( index2 - i ) / (double)di->columns );
	k = (unsigned int)( (double)(indices[size-1] - index2) / (double)( di->columns * di->rows ) );
	HMAX = di->voxels[i][j][k];
	index2 = 0;
	for ( IntensityLevel = HMIN; IntensityLevel <= HMAX; IntensityLevel++ ) {
/*fprintf(stdout,"\n level: %d label: %d", (int)IntensityLevel, current_label);
fflush(stdout);*/
		for ( index = index1; index < size; index++) {
			int resid1 = (int)( indices[index] % ( di->columns * di->rows ) );
			i = (unsigned int)( resid1 % di->columns );
			j = (unsigned int)( (double)( resid1 - i ) / (double)di->columns );
			k = (unsigned int)( (double)(indices[index] - resid1) / (double)( di->columns * di->rows ) );
			if ( (int)di->voxels[i][j][k] != IntensityLevel ) {
				index1 = index;
				break;
			}
			labels[i][j][k] = MASK;
			for ( n = 0; n < next; n += 3) {
				int c, r, p;
				c = ext[n] + i;
				r = ext[n+1] + j;
				p = ext[n+2] + k;
				if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
					if ( labels[c][r][p] >= WSHED ) {
						distances[i][j][k] = 1;
						CheapFifoAdd(queue, indices[index]);
					}
				}
			}
		}
/* end for */
		current_dist = 1;
		CheapFifoAddFictitious(queue);
		while (1) {
			int flag  = 0;
			int resid1;
			pix = CheapFifoRemove(queue);
/*fprintf(stdout,"\n level: %d dist: %d pix: %d", IntensityLevel, (int)current_dist, pix);
fflush(stdout);*/
			if ( pix == FICTITIOUS ) {
				if ( CheapFifoEmpty(queue) ) {
					break;
				} else {
					CheapFifoAddFictitious(queue);
					current_dist++;
					pix = CheapFifoRemove(queue);
				}
			}
			resid1 = (int)( pix % ( di->columns * di->rows ) );
			i = (unsigned int)( resid1 % di->columns );
			j = (unsigned int)( (double)( resid1 - i ) / (double)di->columns );
			k = (unsigned int)( (double)(pix - resid1) / (double)( di->columns * di->rows ) );
			for ( n = 0; n < next; n += 3) {
				int c, r, p;
				c = ext[n] + i;
				r = ext[n+1] + j;
				p = ext[n+2] + k;
				if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
					if ( distances[c][r][p] <= current_dist && labels[c][r][p] >= WSHED ) {
						if ( labels[c][r][p] > WSHED ) {
							if ( labels[i][j][k] == MASK || labels[i][j][k] == WSHED ) {
								if ( !flag ) labels[i][j][k] = labels[c][r][p];
							} else if ( labels[i][j][k] != labels[c][r][p] ) {
								labels[i][j][k] = WSHED;
								flag = 1;
							}
						} else if ( labels[i][j][k] == MASK ) {
							labels[i][j][k] = WSHED;
						}
					} else if ( labels[c][r][p] == MASK && distances[c][r][p] == 0 ) {
						distances[c][r][p] = current_dist + 1;
						CheapFifoAdd(queue, p * ( di->columns * di->rows ) + r * di->columns + c);
					}
				}
			}
		}/* end while */
		for ( index = index2; index < size; index++) {
			int resid1 = (int)( indices[index] % ( di->columns * di->rows ) );
			i = (unsigned int)( resid1 % di->columns );
			j = (unsigned int)( (double)( resid1 - i ) / (double)di->columns );
			k = (unsigned int)( (double)(indices[index] - resid1) / (double)( di->columns * di->rows ) );
			if ( (int)di->voxels[i][j][k] != IntensityLevel ) {
				index2 = index;
				break;
			}
			distances[i][j][k] = 0;
			if ( labels[i][j][k] == MASK ) {
				current_label++;
				labels[i][j][k] = current_label;
				CheapFifoAdd(queue, indices[index]);
				while ( !CheapFifoEmpty(queue) ) {
					pix = CheapFifoRemove(queue);
					resid1 = (int)( pix % ( di->columns * di->rows ) );
					i = (unsigned int)( resid1 % di->columns );
					j = (unsigned int)( (double)( resid1 - i ) / (double)di->columns );
					k = (unsigned int)( (double)(pix - resid1) / (double)( di->columns * di->rows ) );
					for ( n = 0; n < next; n += 3) {
						int c, r, p;
						c = ext[n] + i;
						r = ext[n+1] + j;
						p = ext[n+2] + k;
						if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
							if ( labels[c][r][p] == MASK ) {
								CheapFifoAdd(queue, p * ( di->columns * di->rows ) + r * di->columns + c);
								labels[c][r][p] = current_label;
							}
						}
					}
				}
			}
		}
	}/* end main for */
	for ( index = 0; index < size; index++) {
		int resid1 = (int)( index % ( di->columns * di->rows ) );
		int AllNeighWSHED = 1;
		i = (unsigned int)( resid1 % di->columns );
		j = (unsigned int)( (double)( resid1 - i ) / (double)di->columns );
		k = (unsigned int)( (double)(index - resid1) / (double)( di->columns * di->rows ) );
		for ( n = 0; n < next; n += 3) {
			int c, r, p;
			c = ext[n] + i;
			r = ext[n+1] + j;
			p = ext[n+2] + k;
			if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
				if ( labels[c][r][p] != WSHED ) {
					AllNeighWSHED = 0;
					break;
				}
			}
		}
		di->voxels[i][j][k] = ( labels[i][j][k] == WSHED && !AllNeighWSHED ) ? EmptyPixel:FilledPixel;
	}
	for ( i = 0; i < di->columns; i++) {
		for ( j = 0; j < di->rows; j++) {
			free(labels[i][j]);
			free(distances[i][j]);
		}
		free(labels[i]);
		free(distances[i]);
	}
	free(labels);
	free(distances);
	free(indices);
}


void CubisticDelete(Cubistic*c)
{
	free(c->cubes);
	free(c);
}

Cubistic*Partition(uint32 rows, uint32 columns, int size)
{
	Cubistic*cuby;
	uint32 row, crow;
	uint32 column, ccolumn;
	if ( rows % size != 0 || columns % size != 0 )
		g_error("Partition size is not the divisor %d, %d, %d", (int)rows, (int)columns, (int)size);
	cuby = (Cubistic*)malloc(sizeof(Cubistic));
	cuby->rows = (uint32)( (double)rows / (double)size );
	cuby->columns = (uint32)( (double)columns / (double)size );
	cuby->number = cuby->rows * cuby->columns;
	cuby->size = size;
	cuby->cubes = (Cube*)calloc(cuby->number, sizeof(Cube));
	for ( row = 0, crow = 0; crow < cuby->rows; row += size, crow++ ) {
		for ( column = 0, ccolumn = 0; ccolumn < cuby->columns; column += size, ccolumn++ ) {
			cuby->cubes[crow * cuby->columns + ccolumn ].RowStart = row;
			cuby->cubes[crow * cuby->columns + ccolumn ].RowEnd = row + size;
			cuby->cubes[crow * cuby->columns + ccolumn ].ColumnStart = column;
			cuby->cubes[crow * cuby->columns + ccolumn ].ColumnEnd = column + size;
			cuby->cubes[crow * cuby->columns + ccolumn ].r = 0;
			cuby->cubes[crow * cuby->columns + ccolumn ].c = 0;
		}
	}
	return cuby;
}

double Discrepance(CharImage*UpperPlane, CharImage*LowerPlane, double lambda, Cubistic*cuby, int connectivity)
{
	double cost;
	uint32 n;
	uint32 row;
	uint32 column;
	cost = 0;
	for ( n = 0; n < cuby->number; n++ ) {
		for ( row = cuby->cubes[n].RowStart; row < cuby->cubes[n].RowEnd ; row ++ ) {
			for ( column = cuby->cubes[n].ColumnStart; column < cuby->cubes[n].ColumnEnd; column++ ) {
				int wrow;
				int wcolumn;
				wrow = row + 2 * cuby->cubes[n].r;
				wcolumn = column + 2 * cuby->cubes[n].c;
				if ( ( 0 <= wrow ) && ( wrow < UpperPlane->rows ) && ( 0 <= wcolumn ) && ( wcolumn < UpperPlane->columns ) )
					cost += ( LowerPlane->pixels[ row * LowerPlane->columns + column ] - UpperPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] ) * ( LowerPlane->pixels[ row * LowerPlane->columns + column ] - UpperPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] );
				wrow = row - 2 * cuby->cubes[n].r;
				wcolumn = column - 2 * cuby->cubes[n].c;
				if ( ( 0 <= wrow ) && ( wrow < UpperPlane->rows ) && ( 0 <= wcolumn ) && ( wcolumn < UpperPlane->columns ) )
					cost += ( UpperPlane->pixels[ row * LowerPlane->columns + column ] - LowerPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] ) * ( UpperPlane->pixels[ row * LowerPlane->columns + column ] - LowerPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] );
			}
		}
	}
	return cost;
}


double DiscrepanceCube(CharImage*UpperPlane, CharImage*LowerPlane, double lambda, Cubistic*cuby, int connectivity, uint32 n)
{
	double cost;
	uint32 row;
	uint32 column;
	cost = 0;
	for ( row = cuby->cubes[n].RowStart; row < cuby->cubes[n].RowEnd ; row ++ ) {
		for ( column = cuby->cubes[n].ColumnStart; column < cuby->cubes[n].ColumnEnd; column++ ) {
			int wrow;
			int wcolumn;
			wrow = row + 2 * cuby->cubes[n].r;
			wcolumn = column + 2 * cuby->cubes[n].c;
			if ( ( 0 <= wrow ) && ( wrow < UpperPlane->rows ) && ( 0 <= wcolumn ) && ( wcolumn < UpperPlane->columns ) )
				cost += ( LowerPlane->pixels[ row * LowerPlane->columns + column ] - UpperPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] ) * ( LowerPlane->pixels[ row * LowerPlane->columns + column ] - UpperPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] );
			wrow = row - 2 * cuby->cubes[n].r;
			wcolumn = column - 2 * cuby->cubes[n].c;
			if ( ( 0 <= wrow ) && ( wrow < UpperPlane->rows ) && ( 0 <= wcolumn ) && ( wcolumn < UpperPlane->columns ) )
				cost += ( UpperPlane->pixels[ row * LowerPlane->columns + column ] - LowerPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] ) * ( UpperPlane->pixels[ row * LowerPlane->columns + column ] - LowerPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] );
		}
	}
	return cost;
}

CharImage*CharVolumeNewPlane(CharImage*UpperPlane, CharImage*LowerPlane, int k, double lambda, int connectivity)
{
	double cost;
	double min;
	uint32 n;
	CharImage *ci;
	Cubistic*cuby;
	int limit;
	cuby = Partition(UpperPlane->rows, UpperPlane->columns, k);
	limit = (int)((double)k / 2 );
	if ( lambda > 0 ) {
		int j, flag;
		min = Discrepance(UpperPlane, LowerPlane, lambda, cuby, connectivity);
		flag = 0;
		while ( !flag ) {
			flag = 1;
			for ( n = 0; n < cuby->number; n++ ) {
				for ( j = -limit; j <= limit; j++ ) {
					int pretweak;
					pretweak = cuby->cubes[n].c;
					cuby->cubes[n].c += j;
					cost = Discrepance(UpperPlane, LowerPlane, lambda, cuby, connectivity);
					if ( min > cost ) {
						min = cost;
						flag = 0;
					} else {
						cuby->cubes[n].c = pretweak;
					}
				}
				for ( j = -limit; j <= limit; j++ ) {
					int pretweak;
					pretweak = cuby->cubes[n].r;
					cuby->cubes[n].r += j;
					cost = Discrepance(UpperPlane, LowerPlane, lambda, cuby, connectivity);
					if ( min > cost ) {
						min = cost;
						flag = 0;
					} else {
						cuby->cubes[n].r = pretweak;
					}
				}
			}
		}
	} else if ( lambda == 0 ) {
		int jr, jc;
		double minc;
		for ( n = 0; n < cuby->number; n++ ) {
			minc = DiscrepanceCube(UpperPlane, LowerPlane, lambda, cuby, connectivity, n);
			min = minc;
			for ( jc = -limit; jc <= limit; jc++ ) {
				int pretweakc;
				pretweakc = cuby->cubes[n].c;
				cuby->cubes[n].c += jc;
				for ( jr = -limit; jr <= limit; jr++ ) {
					int pretweakr;
					pretweakr = cuby->cubes[n].r;
					cuby->cubes[n].r += jr;
					cost = DiscrepanceCube(UpperPlane, LowerPlane, lambda, cuby, connectivity, n);
					if ( minc > cost ) {
						minc = cost;
					} else {
						cuby->cubes[n].r = pretweakr;
					}
				}
				if ( min > minc ) {
					min = minc;
				} else {
					cuby->cubes[n].c = pretweakc;
				}
			}
		}
	} else {
		g_error("CharVolumeNewPlane lambda = %f < 0 !", lambda);
	}
	ci = Interpolate(cuby, UpperPlane, LowerPlane);
	return ci;
}

CharImage*Interpolate(Cubistic*cuby, CharImage*UpperPlane, CharImage*LowerPlane)
{
	CharImage*ci;
	uint32 row;
	uint32 column;
	uint32 n;
	ci = CharImageNew(UpperPlane->columns, UpperPlane->rows);
	for ( n = 0; n < cuby->number; n++ ) {
		for ( row = cuby->cubes[n].RowStart; row < cuby->cubes[n].RowEnd ; row ++ ) {
			for ( column = cuby->cubes[n].ColumnStart; column < cuby->cubes[n].ColumnEnd; column ++ ) {
				double pixel;
				int nn;
				int wrow;
				int wcolumn;
				pixel = 0;
				nn = 0;
				wrow = row + cuby->cubes[n].r;
				wcolumn = column + cuby->cubes[n].c;
				if ( ( 0 <= wrow ) && ( wrow < UpperPlane->rows ) && ( 0 <= wcolumn ) && ( wcolumn < UpperPlane->columns ) ) {
					pixel += (double)(UpperPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] );
					nn++;
				}
				wrow = row - cuby->cubes[n].r;
				wcolumn = column - cuby->cubes[n].c;
				if ( ( 0 <= wrow ) && ( wrow < UpperPlane->rows ) && ( 0 <= wcolumn ) && ( wcolumn < UpperPlane->columns ) ) {
					pixel += (double)(LowerPlane->pixels[ wrow * LowerPlane->columns + wcolumn ] );
					nn++;
				}
				ci->pixels[ row * ci->columns + column ] = (unsigned char)(pixel / nn);
			}
		}
	}
	return ci;
}


CharVolume*CreateCharVolume(char*ID, char*extension)
{
	FILE*listfp;
	int list;
	int done;
	struct dirent*direntptr;
	DIR*dirptr;
	char*record;
	int scanf_flag;
	int counter;
	CharImage*di;
	CharVolume*cv;
	char *infile;
	char **filelist;
	int j;
	if ( grf_is_tiff(ID) ) {
		cv = CharVolumeCreate(ID);
	} else {
		list = 0;
		dirptr = opendir(ID);
		if (!dirptr) {
			listfp = fopen(ID,"r");
			if (!listfp)
				g_error("CreateCharVolume Can't open %s neither as directory nor as file!",ID);
			else
				list = 1;
		}
		infile = (char *)calloc(MAX_RECORD, sizeof(char));
		record = (char*)calloc(MAX_RECORD,sizeof(char));
		done = 0;
		if (list) {
			if( ( record = fgets(record,MAX_RECORD,listfp) ) ) {
				scanf_flag = sscanf(record,"%s",infile);
				if ( scanf_flag != 1 )
					done = 1;
			} else {
				done = 1;
			}
		} else {
			direntptr = readdir(dirptr);
			if (!direntptr)
				done = 1;
			if (!done) {
				sprintf(infile,"%s/%s",ID,direntptr->d_name);
			}
		}
/* loop over list */
		counter = 0;
		filelist = (char**)NULL;
		while( !counter && !done) {
			if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
				filelist = (char**)realloc(filelist, (counter + 1) * sizeof(char*) );
				filelist[counter] = (char*)calloc(MAX_RECORD, sizeof(char));
				filelist[counter] = strcpy(filelist[counter], infile);
				counter++;
			}
			if (list) {
				if( ( record = fgets(record,MAX_RECORD,listfp) ) ) {
					scanf_flag = sscanf(record,"%s",infile);
					if ( scanf_flag != 1 )
						done = 1;
				} else {
					done = 1;
				}
			} else {
				direntptr = readdir(dirptr);
				if (!direntptr)
					done = 1;
				if (!done) {
					sprintf(infile,"%s/%s",ID,direntptr->d_name);
				}
			}
		}
		if ( !counter )
			g_error("Average empty");
		while(!done) {
			if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
				filelist = (char**)realloc(filelist, (counter + 1) * sizeof(char*) );
				filelist[counter] = (char*)calloc(MAX_RECORD, sizeof(char));
				filelist[counter] = strcpy(filelist[counter], infile);
				counter++;
			}
			if (list) {
				if( ( record = fgets(record,MAX_RECORD,listfp) ) ) {
					scanf_flag = sscanf(record,"%s",infile);
					if ( scanf_flag != 1 )
						done = 1;
					} else {
						done = 1;
					}
			} else {
				direntptr = readdir(dirptr);
				if (!direntptr)
					done = 1;
				if (!done) {
					sprintf(infile,"%s/%s",ID,direntptr->d_name);
				}
			}
		}
		if (list) {
			fclose(listfp);
		} else {
			closedir(dirptr);
		}
		j = 0;
		sprintf(infile, "%s", filelist[j]);
		di = CharImageCreate(infile);
		cv = NewCharVolume(di->columns, di->rows, counter);
		CharVolumeAddPlane(cv, di, j);
		CharImageDelete(di);
		for ( j = 1; j < counter; j++) {
			sprintf(infile, "%s", filelist[j]);
			di = CharImageCreate(infile);
			CharVolumeAddPlane(cv, di, j);
			CharImageDelete(di);
		}
		free(infile);
		free(record);
		for ( j = 0; j < counter; j++) {
			free(filelist[j]);
		}
		free(filelist);
	}
	return cv;
}


void CharVolumeWrite(CharVolume*cv, char*ID, char*extension)
{
	CharImage*di;
	char *infile;
	int plane;
	int flaggs = 0;
#ifndef G_OS_WIN32
	flaggs = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_IXUSR|S_IXGRP|S_IXOTH;
#endif
/* S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_IXUSR|S_IXGRP|S_IXOTH*/
	if ( g_str_has_suffix(ID, extension) ) {
		grf_write_char_volume(ID, cv->voxels, cv->rows, cv->columns, cv->planes);
	} else {
		g_mkdir(ID, flaggs);
		infile = (char *)calloc(MAX_RECORD, sizeof(char));
		for ( plane = 0; plane < cv->planes; plane++ ) {
			sprintf(infile,"%s/%d.%s", ID, plane, extension);
			di = CharVolumeGetPlane( cv, plane);
			CharImageWrite( di, infile);
			CharImageDelete(di);
		}
		free(infile);
	}
}

void CharVolumeWriteRGB(CharVolume*r, CharVolume*g, CharVolume*b, char*ID)
{
	unsigned char ****rgb;
	rgb = (unsigned char****)calloc(3, sizeof(unsigned char ***) );
	rgb[0] = r->voxels;
	rgb[1] = g->voxels;
	rgb[2] = b->voxels;
	grf_write_char_volume_rgb(ID, r->rows, r->columns, r->planes, rgb, 3);
	free(rgb);
}

int compare_indices(const void *a, const void *b)
{
	unsigned int*x;
	unsigned int*y;
	unsigned int i, j, k;
	unsigned char x1, y1;
	int resid;
	x = (unsigned int*)a;
	y = (unsigned int*)b;
	resid = (int)( *x % ( sv->columns * sv->rows ) );
	i = (unsigned int)( resid % sv->columns );
	j = (unsigned int)( (double)( resid - i ) / (double)sv->columns );
	k = (unsigned int)( (double)( *x - resid) / (double)( sv->columns * sv->rows ) );
	x1 = sv->voxels[i][j][k];
	resid = (int)( *y % ( sv->columns * sv->rows ) );
	i = (unsigned int)( resid % sv->columns );
	j = (unsigned int)( (double)( resid - i ) / (double)sv->columns );
	k = (unsigned int)( (double)( *y - resid) / (double)( sv->columns * sv->rows ) );
	y1 = sv->voxels[i][j][k];
	if ( x1 > y1 ) return 1;
	if ( x1 < y1 ) return -1;
	return 0;
}

CharVolume*CharVolumeRightView(CharVolume*cv)
{
	int i, j;
	CharVolume*c;
	c = NewCharVolume(cv->planes, cv->rows, cv->columns);
	for ( i = 0; i < cv->columns; i++) {
		CharImage*ci = CharVolumeGetColumn(cv, cv->columns - i - 1);
		CharVolumeAddPlane(c, ci, i);
		CharImageDelete(ci);
	}
	return c;
}


CharVolume*CharVolumeLeftView(CharVolume*cv)
{
	int i, j;
	CharVolume*c;
	c = NewCharVolume(cv->planes, cv->rows, cv->columns);
	for ( i = 0; i < cv->columns; i++) {
		CharImage*ci = CharVolumeGetColumn(cv, i);
		ci =CharImageReverseColumn(ci);
		CharVolumeAddPlane(c, ci, i);
		CharImageDelete(ci);
	}
	return c;
}

CharVolume*CharVolumeTopView(CharVolume*cv)
{
	int i, j;
	CharVolume*c;
	c = NewCharVolume(cv->columns, cv->planes, cv->rows);
	for ( i = 0; i < cv->rows; i++) {
		CharImage*ci = CharVolumeGetRow(cv, i);
		CharVolumeAddPlane(c, ci, i);
		CharImageDelete(ci);
	}
	return c;
}

CharVolume*CharVolumeStructElem(char *file)
{
	CharVolume*dp;
	int row, column, plane, *elem, nelem, max_1, max_2, max_3, median, n;
	uint32 dim_1, dim_2, dim_3;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	elem = (int*)calloc(nelem, sizeof(int));
	StructElemGetElem(fp, nelem, elem);
	fclose(fp);
	max_1 = 0;
	max_2 = 0;
	max_3 = 0;
	for ( n = 0; n < nelem; n += 3) {
		if ( max_1 < elem[n] )
			max_1 = elem[n];
		if ( max_2 < elem[n + 1] )
			max_2 = elem[n + 1];
		if ( max_3 < elem[n + 2] )
			max_3 = elem[n + 2];
	}
	dim_1 = (uint32)(2 * ( max_1 + 1 ) + 4);
	dim_2 = (uint32)(2 * ( max_2 + 1 ) + 4);
	dim_3 = (uint32)(2 * ( max_3 + 1 ) + 4);
	dp = NewCharVolume(dim_2, dim_1, dim_3);
	for ( n = 0; n < nelem; n += 3) {
		row = max_1 + 2 + elem[ n ];
		column = max_2 + 2 + elem[ n + 1 ];
		plane = max_3 + 2 + elem[ n + 2 ];
		dp->voxels[ column][row][ plane ] = FilledPixel;
	}
	return dp;
}

void StructElem3d(char*fout, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype)
{
	FILE*out;
	int row, column, plane, *elem, nelem, halfr, halfc, halfz, median, co;
	double a, b, d, x, c, r, z;

	if ( vsize < 3 || hsize < 3 || zsize < 3 || vsize % 2 == 0 || hsize % 2 == 0 || zsize % 2 == 0 )
		g_error("parus: StructElem error in sizes: should be > 3 and odd");
	halfr = (int)( (double)vsize / 2.0 );
	halfc = (int)( (double)hsize / 2.0 );
	halfz = (int)( (double)zsize / 2.0 );
	out = fopen(fout,"w");
	if ( !out )
		g_error("parus: StructElem can't open %s", fout);
	switch ( vhtype ) {
		case square:
			if ( ztype == square ) {
				nelem = 3 * vsize * hsize * zsize;
				elem = (int*)calloc(nelem, sizeof(int));
				for ( row = 0; row < vsize; row++) {
					for ( column = 0; column < hsize; column ++) {
						for ( plane = 0, co = 0; plane < 3 * zsize; plane += 3, co++) {
							elem[row * 3 * hsize * zsize + column * 3 * zsize + plane ] = row - halfr;
							elem[row * 3 * hsize * zsize + column * 3 * zsize + plane + 1] = column - halfc;
							elem[row * 3 * hsize * zsize + column * 3 * zsize + plane + 2] = co - halfz;
						}
					}
				}
			} else if ( ztype == disk ) {
				nelem = 0;
				elem = (int*)NULL;
				a = (double)vsize / 2.0;
				b = (double)zsize / 2.0;
				for ( row = 0; row < vsize; row++) {
					r = (double)( row - halfr ) / a;
					for ( plane = 0; plane < zsize; plane++) {
						c = (double)( plane - halfz ) / b;
						x = sqrt( r * r + c * c );
						if ( x <= 1.0 ) {
							for ( column = 0; column < hsize; column++) {
								elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
								elem[nelem] = row - halfr;
								elem[nelem + 1] = column - halfc;
								elem[nelem + 2] = plane - halfz;
								nelem += 3;
							}
						}
					}
				}
			} else {
				g_error("parus: StructElem type is not supported");
			}
		break;
		case disk:
			if ( ztype == square ) {
				nelem = 0;
				elem = (int*)NULL;
				a = (double)vsize / 2.0;
				b = (double)hsize / 2.0;
				for ( row = 0; row < vsize; row++) {
					r = (double)( row - halfr ) / a;
					for ( column = 0; column < hsize; column++) {
						c = (double)( column - halfc ) / b;
						x = sqrt( r * r + c * c );
						if ( x <= 1.0 ) {
							for ( plane = 0; plane < zsize; plane++) {
								elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
								elem[nelem] = row - halfr;
								elem[nelem + 1] = column - halfc;
								elem[nelem + 2] = plane - halfz;
								nelem += 3;
							}
						}
					}
				}
			} else if ( ztype == disk ) {
				nelem = 0;
				elem = (int*)NULL;
				a = (double)vsize / 2.0;
				b = (double)hsize / 2.0;
				d = (double)zsize / 2.0;
				for ( row = 0; row < vsize; row++) {
					r = (double)( row - halfr ) / a;
					for ( column = 0; column < hsize; column++) {
						c = (double)( column - halfc ) / b;
						for ( plane = 0; plane < zsize; plane++) {
							z = (double)( plane - halfz ) / d;
							x = sqrt( r * r + c * c + z * z );
							if ( x <= 1.0 ) {
								elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
								elem[nelem] = row - halfr;
								elem[nelem + 1] = column - halfc;
								elem[nelem + 2] = plane - halfz;
								nelem += 3;
							}
						}
					}
				}
			} else {
				g_error("parus: StructElem type is not supported");
			}
		break;
		default:
			g_error("parus: StructElem type is not supported");
	}
	median = (int)floor( 0.5 * ( (double)nelem / 3.0 ) );
	fprintf(out,"%d\n", nelem);
	fprintf(out,"%d\n", median);
	for ( row = 0; row < nelem; row++)
		fprintf(out,"%d ", elem[row]);
	fclose(out);
	free(elem);
}

int *build_struct_elem(int*elem_size, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype)
{
	int row, column, plane, *elem, nelem, halfr, halfc, halfz, median;
	double a, b, d, x, c, r, z;
	if ( vsize < 3 || hsize < 3 || zsize < 3 || vsize % 2 == 0 || hsize % 2 == 0 || zsize % 2 == 0 )
		g_error("parus: StructElem error in sizes: should be > 3 and odd");
	halfr = (int)( (double)vsize / 2.0 );
	halfc = (int)( (double)hsize / 2.0 );
	halfz = (int)( (double)zsize / 2.0 );
	switch ( vhtype ) {
		case square:
			if ( ztype == square ) {
				nelem = 0;
				elem = (int*)NULL;
				for ( row = 0; row < vsize; row++) {
					for ( column = 0; column < hsize; column ++) {
						for ( plane = 0; plane < zsize; plane ++) {
							elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
							elem[nelem] = row - halfr;
							elem[nelem + 1] = column - halfc;
							elem[nelem + 2] = plane - halfz;
							nelem += 3;
						}
					}
				}
			} else if ( ztype == disk ) {
				nelem = 0;
				elem = (int*)NULL;
				a = (double)vsize / 2.0;
				b = (double)zsize / 2.0;
				for ( row = 0; row < vsize; row++) {
					r = (double)( row - halfr ) / a;
					for ( plane = 0; plane < zsize; plane++) {
						c = (double)( plane - halfz ) / b;
						x = sqrt( r * r + c * c );
						if ( x <= 1.0 ) {
							for ( column = 0; column < hsize; column++) {
								elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
								elem[nelem] = row - halfr;
								elem[nelem + 1] = column - halfc;
								elem[nelem + 2] = plane - halfz;
								nelem += 3;
							}
						}
					}
				}
			} else {
				g_error("parus: StructElem type is not supported");
			}
		break;
		case disk:
			if ( ztype == square ) {
				nelem = 0;
				elem = (int*)NULL;
				a = (double)vsize / 2.0;
				b = (double)hsize / 2.0;
				for ( row = 0; row < vsize; row++) {
					r = (double)( row - halfr ) / a;
					for ( column = 0; column < hsize; column++) {
						c = (double)( column - halfc ) / b;
						x = sqrt( r * r + c * c );
						if ( x <= 1.0 ) {
							for ( plane = 0; plane < zsize; plane++) {
								elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
								elem[nelem] = row - halfr;
								elem[nelem + 1] = column - halfc;
								elem[nelem + 2] = plane - halfz;
								nelem += 3;
							}
						}
					}
				}
			} else if ( ztype == disk ) {
				nelem = 0;
				elem = (int*)NULL;
				a = (double)vsize / 2.0;
				b = (double)hsize / 2.0;
				d = (double)zsize / 2.0;
				for ( row = 0; row < vsize; row++) {
					r = (double)( row - halfr ) / a;
					for ( column = 0; column < hsize; column++) {
						c = (double)( column - halfc ) / b;
						for ( plane = 0; plane < zsize; plane++) {
							z = (double)( plane - halfz ) / d;
							x = sqrt( r * r + c * c + z * z );
							if ( x <= 1.0 ) {
								elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
								elem[nelem] = row - halfr;
								elem[nelem + 1] = column - halfc;
								elem[nelem + 2] = plane - halfz;
								nelem += 3;
							}
						}
					}
				}
			} else {
				g_error("parus: StructElem type is not supported");
			}
		break;
		default:
			g_error("parus: StructElem type is not supported");
	}
	*elem_size = nelem;
	return elem;
}

void StructElem3d_2(char*fout, int vsize, int hsize, int zsize, int width, StructElemType vhtype, StructElemType ztype)
{
	FILE*out;
	int *elem_1, nelem_1, median, *elem_2, nelem_2, *elem, nelem, i, j, found;
	out = fopen(fout,"w");
	if ( !out )
		g_error("parus: StructElem can't open %s", fout);
	elem_1 = build_struct_elem(&nelem_1, vsize, hsize, zsize, vhtype, ztype);
	elem_2 = build_struct_elem(&nelem_2, vsize - 2 * width, hsize - 2 * width, zsize - 2 * width, vhtype, ztype);
	nelem = 0;
	elem = (int*)NULL;
	for ( i = 0; i < nelem_1; i += 3 ) {
		found = 0;
		for ( j = 0; j < nelem_2; j += 3 ) {
			if ( ( elem_1[i] == elem_2[j] ) && ( elem_1[i + 1] == elem_2[j + 1] ) && ( elem_1[i + 2] == elem_2[j + 2] ) ) {
				found = 1;
			}
		}
		if ( found == 0 ) {
			elem = (int*)realloc(elem, (nelem + 3) * sizeof(int));
			elem[nelem] = elem_1[i];
			elem[nelem + 1] = elem_1[i + 1];
			elem[nelem + 2] = elem_1[i + 2];
			nelem += 3;
		}
	}
	median = (int)floor( 0.5 * ( (double)nelem / 3.0 ) );
	fprintf(out,"%d\n", nelem);
	fprintf(out,"%d\n", median);
	for ( i = 0; i < nelem; i++)
		fprintf(out,"%d ", elem[i]);
	fclose(out);
	free(elem);
	free(elem_1);
	free(elem_2);
}

CharVolume*CharVolumeMedianSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype)
{
	char *file;
	file = pr_get_name_temp();
	StructElem3d( file, vsize, hsize, zsize, vhtype, ztype);
	dp = CharVolumeMedianWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharVolumeMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharVolume*CharVolumeMedianWithSE(CharVolume*dp, char *file)
{
	uint32 column, row, plane;
	uint32 np;
	int npattern, nelem;
	int median;
	int med;
	int *pattern;
	CharVolume*dpm;
	unsigned char *p;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharVolumeMedianWithSE can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 3.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	p = (unsigned char*)calloc(npattern,sizeof(unsigned char));
	dpm = CharVolumeClone(dp);
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++ ) {
				int offset = 0;
				for ( np = 0; np < npattern; np++ ) {
					register int indexr = row + pattern[3*np];
					register int indexc = column + pattern[3*np+1];
					register int indexz = plane + pattern[3*np+2];
					if ( -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows && -1 < indexz && indexz < dp->planes ) {
						p[np] = dp->voxels[indexc][indexr][indexz];
					} else {
						p[np] = EmptyPixel;
						offset++;
					}
				}
				med = offset + (int)ceil( 0.5 * ((double)npattern - (double)offset )) - 1;
				qsort((void*)p, npattern, sizeof(unsigned char), com1);
				if ( med < npattern) {
					dpm->voxels[column][row][plane] = p[med];
				} else {
					dpm->voxels[column][row][plane] = EmptyPixel;
				}
			}
		}
	}
	CharVolumeDelete(dp);
	free(p);
	return dpm;
}


CharVolume*CharVolumeFMedianWithSE(CharVolume*dp, int npattern, int*pattern, int rank)
{
	uint32 column, row, plane;
	int np, np2;
	int index;
	int median;
/*int med;*/
	double factor;
	CharVolume*dpm;
	dpm = CharVolumeClone(dp);
	factor = 1.0 / ((double)rank);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,pattern,dpm,npattern,rank,factor) private(np,index,np2,median)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++ ) {
				int offset = 0;
				int hist[256];
				for ( np = 0; np < 256; np++ )
					hist[np] = 0;
				for ( np = 0, np2 = 0; np < npattern; np++, np2 += 3 ) {
					register int indexr = row + pattern[np2];
					register int indexc = column + pattern[np2 + 1];
					register int indexz = plane + pattern[np2 + 2];
					if ( -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows && -1 < indexz && indexz < dp->planes ) {
						index = (int)(dp->voxels[indexc][indexr][indexz]);
						hist[index]++;
					} else {
						offset++;
					}
				}
				median = 0;
				index = 0;
				np2 = (int)(factor * (npattern - offset));
				if ( (npattern - offset) % rank )
					np2++;
				for ( np = 0; np < 256; np++ ) {
					median += hist[np];
					if ( median >= np2 ) {
						index = np;
						break;
					}
					hist[np] = 0;
				}
				dpm->voxels[column][row][plane] = (unsigned char)index;
			}
		}
	}
	CharVolumeDelete(dp);
	return dpm;
}


CharVolume*CharVolumeLocalHEqSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype)
{
	char *file;
	file = pr_get_name_temp();
	StructElem3d( file, vsize, hsize, zsize, vhtype, ztype);
	dp = CharVolumeLocalHEqWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharVolumeMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}


CharVolume*CharVolumeLocalHEqWithSE(CharVolume*dp, char *file)
{
	uint32 column, row, plane;
	int np, np2;
	int npattern, nelem;
	int median;
	int *pattern;
	CharVolume*dpm;
	unsigned char p;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharVolumeMedianWithSE can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 3.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharVolumeClone(dp);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,pattern,dpm,npattern) private(np,p,np2)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++ ) {
				register double S;
				int base = 0;
				int histogram[256];
				for ( np = 0; np < 256; np++ )
					histogram[np] = 0;
				for ( np = 0; np < npattern; np++ ) {
					register int indexr = row + pattern[3*np];
					register int indexc = column + pattern[3*np+1];
					register int indexz = plane + pattern[3*np+2];
					if ( -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows && -1 < indexz && indexz < dp->planes ) {
						p = (int)dp->voxels[indexc][indexr][indexz];
						histogram[p]++;
						base++;
					}
				}
				np2 = (int)(dp->voxels[column][row][plane]) + 1;
				S = 0;
				for ( np = 0; np < np2; np++) {
					S += (double)histogram[np];
				}
				dpm->voxels[column][row][plane] = (unsigned char)floor( (double)FilledPixel * S / (double)base);
			}
		}
	}
	CharVolumeDelete(dp);
	return dpm;
}


CharVolume*CharVolumeClone(CharVolume*p)
{
	CharVolume*di;
	di = NewCharVolume(p->columns, p->rows, p->planes);
	return di;
}

CharVolume*CharVolumeBPDistance(CharVolume*dp, int repetitions)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageChampherDistance(ci, repetitions);
		CharVolumeSetPlane(dp, ci, plane);
	}
	CharImageDelete(ci);
	return dp;
}

CharVolume*CharVolumeDistance(CharVolume*dp)
{
	register uint32 column, row, plane;
	register double q1, q2, q3, q4, q5;
	q1 = 1.0;
	q2 = sqrt ( 2 ) * q1;
	q3 = (double)dp->rows / (double)dp->planes;
	q4 = sqrt( q1 * q1 + q3 * q3 );
	q5 = sqrt( q2 * q2 + q3 * q3 );
/* Forward  */
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,q1,q2,q3,q4,q5)
	for( row = 1; row < dp->rows - 1; row++) {
		for( column = 1; column < dp->columns - 1; column++) {
			for( plane = 1; plane < dp->planes - 1; plane++) {
				register double dist;
				register double d;
				dist = (double) dp->voxels[column][row][plane];
				d = (double) dp->voxels[column][row][plane - 1] + q3;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column][row - 1][plane - 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column][row + 1][plane - 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row][plane - 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row][plane - 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row - 1][plane - 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row + 1][plane - 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row - 1][plane - 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row + 1][plane - 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row][plane] + q1;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column][row - 1][plane] + q1;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row - 1][plane] + q2;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row - 1][plane] + q2;
				if( d < dist )
					dist = d;
				dp->voxels[column][row][plane] = ( dist < (double)FilledPixel ) ? (unsigned char)dist : (unsigned char)FilledPixel;
			}
		}
	}
/* Backward */
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,q1,q2,q3,q4,q5)
	for( row = dp->rows - 2; row > 0; row--) {
		for( column = dp->columns - 2; column > 0; column--) {
			for( plane = dp->planes - 2; plane > 0; plane--) {
				register double dist;
				register double d;
				dist = (double) dp->voxels[column][row][plane];
				d = (double) dp->voxels[column][row][plane + 1] + q3;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column][row - 1][plane + 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column][row + 1][plane + 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row][plane + 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row][plane + 1] + q4;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row - 1][plane + 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row + 1][plane + 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row - 1][plane + 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row + 1][plane + 1] + q5;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row][plane] + q1;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column][row + 1][plane] + q1;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column + 1][row + 1][plane] + q2;
				if( d < dist )
					dist = d;
				d = (double) dp->voxels[column - 1][row + 1][plane] + q2;
				if( d < dist )
					dist = d;
				dp->voxels[column][row][plane] = ( dist < (double)FilledPixel ) ? (unsigned char)dist : (unsigned char)FilledPixel;
			}
		}
	}
	return dp;
}


CharVolume*CharVolumeEdgeShenCastan(CharVolume*dp, double a1, double a2, double low, double high, uint32 window, uint32 segment, int connectivity)
{
	CharVolume*dpm;
	int column, row, plane;
	double***orig;
	double***isef1, ***isef2;
	int***bli;
	double***adgrad;
	orig = (double***)calloc(dp->columns, sizeof(double**));
	for ( column = 0; column < dp->columns; column++ ) {
		orig[column] = (double**)calloc(dp->rows, sizeof(double*));
		for ( row = 0; row < dp->rows; row++ ) {
			orig[column][row] = (double*)calloc(dp->planes, sizeof(double));
			for ( plane = 0; plane < dp->planes; plane++ ) {
				orig[column][row][plane] = (double)dp->voxels[column][row][plane];
			}
		}
	}
	isef1 = (double***)calloc(dp->columns, sizeof(double**));
	for ( column = 0; column < dp->columns; column++ ) {
		isef1[column] = (double**)calloc(dp->rows, sizeof(double*));
		for ( row = 0; row < dp->rows; row++ ) {
			isef1[column][row] = (double*)calloc(dp->planes, sizeof(double));
		}
	}
	ComputeISEF3d(isef1, a1, orig, dp->columns, dp->rows, dp->planes);
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			free(orig[column][row]);
		}
		free(orig[column]);
	}
	free(orig);
	isef2 = (double***)calloc(dp->columns, sizeof(double**));
	for ( column = 0; column < dp->columns; column++ ) {
		isef2[column] = (double**)calloc(dp->rows, sizeof(double*));
		for ( row = 0; row < dp->rows; row++ ) {
			isef2[column][row] = (double*)calloc(dp->planes, sizeof(double));
		}
	}
	ComputeISEF3d(isef2, a2, isef1, dp->columns, dp->rows, dp->planes);
	bli = (int***)calloc(dp->columns, sizeof(int**));
	for ( column = 0; column < dp->columns; column++ ) {
		bli[column] = (int**)calloc(dp->rows, sizeof(int*));
		for ( row = 0; row < dp->rows; row++ ) {
			bli[column][row] = (int*)calloc(dp->planes, sizeof(int));
		}
	}
	ComputeBLI3d(isef1, isef2, bli, dp->columns, dp->rows, dp->planes);
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			free(isef2[column][row]);
		}
		free(isef2[column]);
	}
	free(isef2);
	adgrad = (double***)calloc(dp->columns, sizeof(double**));
	for ( column = 0; column < dp->columns; column++ ) {
		adgrad[column] = (double**)calloc(dp->rows, sizeof(double*));
		for ( row = 0; row < dp->rows; row++ ) {
			adgrad[column][row] = (double*)calloc(dp->planes, sizeof(double));
		}
	}
	ComputeADGRAD3d(isef1, bli, dp->rows, dp->columns, dp->planes, adgrad, window);
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			free(isef1[column][row]);
		}
		free(isef1[column]);
	}
	free(isef1);
	dpm = CharVolumeClone(dp);
#ifdef BLI_IMAGE3D
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++ ) {
				dpm->voxels[column][row][plane] = (unsigned char)((double)bli[column][row][plane] * (double)FilledPixel);
			}
		}
	}
CharVolumeWrite(dpm,"bli","tif");
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++ ) {
				dpm->voxels[column][row][plane] = (unsigned char)(adgrad[column][row][plane] * 25.0);
			}
		}
	}
CharVolumeWrite(dpm,"adgrad","tif");
#endif
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			free(bli[column][row]);
		}
		free(bli[column]);
	}
	free(bli);
	HysteresisThreshold3d(dpm, adgrad, low, high, segment, connectivity);
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			free(adgrad[column][row]);
		}
		free(adgrad[column]);
	}
	free(adgrad);
	CharVolumeDelete(dp);
	return dpm;
}


void ComputeISEF3d(double***isef, double b, double***orig, uint32 columns, uint32 rows, uint32 planes)
{
	int column, row, plane;
	double c, c2;
	double *y1;
	double *y2;
	c = (1-b)/(1+b);
	c2 = b * c;
	y1 = (double*)calloc(rows, sizeof(double));
	y2 = (double*)calloc(rows, sizeof(double));
	for( plane = 0; plane < planes; plane++) {
		for( column = 0; column < (int)columns; column++) {
			y1[0] = c * orig[column][0][plane];
			y2[((int)rows - 1) ] = c2 * orig[column][((int)rows - 1) ][plane];
			for( row = 1; row < (int)rows ; row++) {
				y1[row] = c * orig[column][row][plane] + b * y1[ row - 1];
			}
			for( row = (int)rows - 2; row >= 0; row--) {
				y2[row] = c2 * orig[column][row][plane] + b * y2[row + 1];
			}
			isef[ column ][(int)rows-1][plane] = y1[(int)rows-1];
			for( row = 0; row < (int)rows - 1 ; row++) {
				isef[ column ][row][plane] = y1[row] + y2[row + 1];
			}
		}
	}
	y1 = (double*)realloc(y1, columns * sizeof(double));
	y2 = (double*)realloc(y2, columns * sizeof(double));
	for( row = 0; row < (int)rows ; row++) {
		for( plane = 0; plane < planes; plane++) {
			y1[0] = c * isef[0][ row][plane];
			y2[ (int)columns - 1] = c2 * isef[ (int)columns - 1][row][plane];
			for( column = 1; column < (int)columns ; column++) {
				y1[ column] = c * isef[ column][row][plane] + b * y1[column - 1];
			}
			for( column = (int)columns - 2; column >= 0 ; column--) {
				y2[column] = c2 * isef[ column][row][plane] + b * y2[ column + 1];
			}
			isef[(int)columns - 1][row][plane] = y1[(int)columns - 1];
			for( column = 0; column < (int)columns - 1; column++) {
				isef[column][row][plane] = y1[column] + y2[column + 1];
			}
		}
	}
	y1 = (double*)realloc(y1, planes * sizeof(double));
	y2 = (double*)realloc(y2, planes * sizeof(double));
	for( column = 0; column < columns; column++) {
		for( row = 0; row < (int)rows ; row++) {
			y1[0] = c * isef[column][row][0];
			y2[planes - 1] = c2 * isef[column][row][planes - 1];
			for( plane = 1; plane < planes; plane++) {
				y1[plane] = c * isef[ column][row][plane] + b * y1[plane - 1];
			}
			for( plane = planes - 2; plane >= 0 ; plane--) {
				y2[plane] = c2 * isef[ column][row][plane] + b * y2[plane+1];
			}
			isef[column][row][planes - 1] = y1[planes - 1];
			for( plane = 0; plane < (int)planes - 1; plane++) {
				isef[column][row][plane] = y1[plane] + y2[plane + 1];
			}
		}
	}
	free(y1);
	free(y2);
}

void ComputeBLI3d(double***isef1, double***isef2, int***bli, uint32 columns, uint32 rows, uint32 planes)
{
	uint32 column, row, plane;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(isef1,isef2,bli,columns,rows,planes)
	for( row = 0; row < rows; row++) {
		for( column = 0; column < columns ; column++) {
			for( plane = 0; plane < planes; plane++) {
				bli[ column][row][plane] = ( isef2[column][row][plane] > isef1[column][row][plane] );
			}
		}
	}
}

void ComputeADGRAD3d(double ***isef, int ***bli, uint32 rows, uint32 columns, uint32 planes, double ***adgrad, uint32 window)
{
	uint32 column, row, plane;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(isef,adgrad,bli,columns,rows,planes,window)
	for( row = 1; row < rows - 1; row++) {
		for( column = 1; column < columns - 1 ; column++) {
			for( plane = 1; plane < planes - 1; plane++) {
				adgrad[column][row][plane] = ( ZeroCrossing3d(isef, bli, row, column, plane, rows, columns, planes) ) ? AdGrad3d(isef, bli, row, column, plane, rows, columns, planes, window) : 0.0;
			}
		}
	}
}


int ZeroCrossing3d(double ***isef, int ***bli, uint32 row, uint32 column, uint32 plane, uint32 rows, uint32 columns, uint32 planes)
{
	if (bli[column][row][plane] == 1 && bli[column - 1][row][plane] == 0) /* positive z-c */ {
/*		if (isef[ (row+1 ) * columns + column] - isef[ ( row - 1 ) * columns + column] > 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if (bli[column][row][plane] == 1 && bli[column + 1][row][plane] == 0 ) /* positive z-c */ {
/*		if (isef[ row * columns + column+1] - isef[ row * columns + column - 1] > 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if ( bli[column][row][plane] == 1 && bli[column][row - 1][plane] == 0) /*negative z-c */ {
/*		if (isef[ ( row + 1 ) * columns + column] - isef[( row - 1 ) * columns + column] < 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if (bli[column][row][plane] == 1 && bli[column][row + 1][plane] == 0 ) /*negative z-c */ {
/*		if (isef[ row * columns + column + 1 ] - isef[ row * columns + column - 1] < 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if ( bli[column][row][plane] == 1 && bli[column][row][plane - 1] == 0) /*negative z-c */ {
/*		if (isef[ ( row + 1 ) * columns + column] - isef[( row - 1 ) * columns + column] < 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if (bli[column][row][plane] == 1 && bli[column][row][plane + 1] == 0 ) /*negative z-c */ {
/*		if (isef[ row * columns + column + 1 ] - isef[ row * columns + column - 1] < 0 )*/
			return 1;
	} /*else if (bli[row * columns + column] == 0 && bli[ ( row + 1 ) * columns + column] == 1) {
		if (isef[ (row+1 ) * columns + column] - isef[ ( row - 1 ) * columns + column] > 0 )
			return 1;
		else
			return 0;
	} else if (bli[row * columns + column] == 0 && bli[row * columns + column+1] == 1 ) {
		if (isef[ row * columns + column+1] - isef[ row * columns + column - 1] > 0 )
			return 1;
		else
			return 0;
	} else if ( bli[row * columns + column] == 0 && bli[ ( row-1 ) * columns + column ] == 1) {
		if (isef[ ( row + 1 ) * columns + column] - isef[( row - 1 ) * columns + column] < 0 )
			return 1;
		else
			return 0;
	} else if (bli[row * columns + column] == 0 && bli[row * columns + column-1] == 1 ) {
		if (isef[ row * columns + column + 1 ] - isef[ row * columns + column - 1] < 0 )
			return 1;
		else
			return 0;
	}*/ else /* not a z-c */
		return 0;
}

double AdGrad3d(double ***isef, int ***bli, uint32 row, uint32 column, uint32 plane, uint32 rows, uint32 columns, uint32 planes, uint32 window)
{
	double adgrad;
	uint32 co, ro, po;
	register double S1, S2;
	register int N1, N2;
	uint32 halfwin;
	uint32 Rstart, Rfinish, Cstart, Cfinish, Pstart, Pfinish;
	halfwin = (uint32)( (double)window / 2.0 );
	Rstart = ( (int)row - (int)halfwin >= 0 ) ? row - halfwin : 0;
	Cstart = ((int)column - (int)halfwin >= 0) ? column - halfwin : 0;
	Pstart = ((int)plane - (int)halfwin >= 0) ? plane - halfwin : 0;
	Rfinish = ((int)row + (int)halfwin < (int)rows - 1 ) ? row + halfwin : rows - 1;
	Cfinish = ((int)column + (int)halfwin < (int)columns - 1 ) ? column + halfwin : columns - 1;
	Pfinish = ((int)plane + (int)halfwin < (int)planes - 1 ) ? plane + halfwin : planes - 1;
	S1 = 0.0;
	S2 = 0.0;
	N1 = 0;
	N2 = 0;
	for ( ro = Rstart; ro <= Rfinish; ro++) {
		for ( co = Cstart; co <= Cfinish; co++) {
			for ( po = Pstart; po <= Pfinish; po++) {
				if ( bli[ co ][ro][po] ) {
					S1 += isef[co][ro][po];
					N1++;
				} else {
					S2 += isef[co][ro][po];
					N2++;
				}
			}
		}
	}

	if ( N1 )
		S1 /= (double)N1;
	if ( N2 )
		S2 /= (double)N2;
	adgrad = S2 - S1;
	if ( adgrad < 0 ) adgrad = -adgrad;
	return adgrad;
}

void HysteresisThreshold3d(CharVolume*dpm, double ***adgrad, double low, double high, uint32 segment, int connectivity)
{
	uint32 column, row, plane;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dpm)
	for( row = 0; row < dpm->rows; row++) {
		for( column = 0; column < dpm->columns ; column++) {
			for( plane = 0; plane < dpm->planes; plane++) {
				dpm->voxels[column][row][plane] = EmptyPixel;
			}
		}
	}
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dpm,adgrad,segment,low,high,connectivity)
	for( row = 0; row < dpm->rows; row++) {
		for( column = 0; column < dpm->columns ; column++) {
			for( plane = 0; plane < dpm->planes; plane++) {
				if ( dpm->voxels[column][row][plane] == EmptyPixel && adgrad[column][row][plane] > high ) {
					int length;
					PList3d*seg;
					seg = CheapSegment3d(dpm, adgrad, row, column, plane, low, connectivity);
					length = Length3d(seg);
					if ( length > segment )
						MarkPlist3d(seg, dpm, FilledPixel);
					else
						MarkPlist3d(seg, dpm, EmptyPixel);
					PList3dFree(seg);
				}
			}
		}
	}
}

int Length3d(PList3d*p)
{
	int length = 0;
	PList3d*cur;
	cur = p;
	while ( cur ) {
		length++;
		cur = cur->next;
	}
	return length;
}

void MarkPlist3d(PList3d*p, CharVolume*dpm, unsigned char intensity)
{
	PList3d*cur;
	cur = p;
	while ( cur ) {
		dpm->voxels[cur->column][cur->row][cur->plane] = intensity;
		cur = cur->next;
	}
}

void LabelDoubleByPList3d(PList3d*p, double*data, double value, int rows, int columns, int planes)
{
	PList3d*cur;
	cur = p;
	while ( cur ) {
		data[ cur->column + cur->row * columns + cur->plane * rows * columns ] = value;
		cur = cur->next;
	}
}


void PList3dFree(PList3d*p)
{
	PList3d*cur;
	cur = p;
	while ( cur ) {
		PList3d*temp;
		temp = cur;
		cur = cur->next;
		free(temp);
	}
}

CharVolume*CharVolumeDespeckle(CharVolume*cv)
{
	CharVolume*ci;
	int column, row, plane;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int connectivity = 26;
	int n;
	ci = CharVolumeClone(cv);
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				ci->voxels[column][row][plane] = EmptyPixel;
				if ( cv->voxels[column][row][plane] != EmptyPixel ) {
					for ( n = 0; n < connectivity; n++) {
						register int indexr = row + ext26[3 *n];
						register int indexc = column + ext26[3*n+1];
						register int indexz = plane + ext26[3*n+2];
						if ( -1 < indexc && indexc < cv->columns && -1 < indexr && indexr < cv->rows && -1 < indexz && indexz < cv->planes )
							if ( cv->voxels[ indexc ][ indexr][ indexz] != EmptyPixel )
								ci->voxels[column][row][plane] = cv->voxels[column][row][plane];
					}
				}
			}
		}
	}
	CharVolumeDelete(cv);
	return ci;
}


PList3d*CheapSegment3d(CharVolume*dp,double ***adgrad, uint32 row, uint32 column, uint32 plane, double low, int connectivity)
{
	PList3d*list, *cur;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	list = (PList3d*)malloc(sizeof(PList3d));
	list->row = row;
	list->column = column;
	list->plane = plane;
	list->next = (PList3d*)NULL;
	dp->voxels[column][row][plane] = 1;
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=3 ) {
			int r = ext[n] + cur->row;
			int c = ext[n + 1] + cur->column;
			int p = ext[n + 2] + cur->plane;
			if ( r >= 0 && r < dp->rows && c >= 0 && c < dp->columns &&  p >= 0 && p < dp->planes && dp->voxels[c][r][p] != 1 && adgrad[c][r][p] > low ) {
				PList3d*prev, *s;
				prev = cur;
				s = list;
				while ( prev ) {
					s = prev;
					prev = prev->next;
				}
				if ( ! (s->next = (PList3d*)malloc(sizeof(PList3d)) ) )
					g_error("PList3dGo can't allocate element");
				s->next->column = c;
				s->next->row = r;
				s->next->plane = p;
				s->next->next = (PList3d*)NULL;
				dp->voxels[c][r][p] = 1;
			}
		}
	}
	return list;
}


CharVolume*CharVolumeErosion(CharVolume*cv, int connectivity)
{
	CharVolume*ci;
	int column, row, plane;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	ci = CharVolumeClone(cv);
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				ci->voxels[column][row][plane] = cv->voxels[column][row][plane];
				if ( cv->voxels[column][row][plane] != EmptyPixel ) {
					for ( n = 0; n < next; n+=3 ) {
						register int indexr = row + ext[n];
						register int indexc = column + ext[n+1];
						register int indexz = plane + ext[n+2];
						if ( -1 < indexc && indexc < cv->columns && -1 < indexr && indexr < cv->rows && -1 < indexz && indexz < cv->planes )
							if ( cv->voxels[ indexc ][ indexr][ indexz] == EmptyPixel ) {
								ci->voxels[column][row][plane] = EmptyPixel;
								break;
							}
					}
				}
			}
		}
	}
	CharVolumeDelete(cv);
	return ci;
}

CharVolume*CharVolumeDilation(CharVolume*cv, int connectivity)
{
	CharVolume*ci;
	int column, row, plane;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	ci = CharVolumeClone(cv);
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				ci->voxels[column][row][plane] = cv->voxels[column][row][plane];
				if ( cv->voxels[column][row][plane] == EmptyPixel ) {
					for ( n = 0; n < next; n+=3 ) {
						register int indexr = row + ext[n];
						register int indexc = column + ext[n+1];
						register int indexz = plane + ext[n+2];
						if ( -1 < indexc && indexc < cv->columns && -1 < indexr && indexr < cv->rows && -1 < indexz && indexz < cv->planes )
							if ( cv->voxels[ indexc ][ indexr][ indexz] != EmptyPixel ) {
								ci->voxels[column][row][plane] = cv->voxels[ indexc ][ indexr][ indexz];
								break;
							}
					}
				}
			}
		}
	}
	CharVolumeDelete(cv);
	return ci;
}

PList3d*IntensitySegment3d(CharVolume*dp, unsigned char Intensity, uint32 row, uint32 column, uint32 plane, int connectivity, uint32 MaxSegment, int *SegmentLength, uint32 MinSegment)
{
	PList3d*list, *cur;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	list = (PList3d*)malloc(sizeof(PList3d));
	(*SegmentLength) = 1;
	list->row = row;
	list->column = column;
	list->plane = plane;
	list->next = (PList3d*)NULL;
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=3 ) {
			int r = ext[n] + cur->row;
			int c = ext[n + 1] + cur->column;
			int p = ext[n + 2] + cur->plane;
			if ( r >= 0 && r < dp->rows && c >= 0 && c < dp->columns &&  p >= 0 && p < dp->planes && dp->voxels[c][r][p] == Intensity && !IsLabeled3d(list, c, r, p) ) {
				PList3d*prev, *s;
				prev = cur;
				s = list;
				while ( prev ) {
					s = prev;
					prev = prev->next;
				}
				if ( ! (s->next = (PList3d*)malloc(sizeof(PList3d)) ) )
					g_error("PList3dGo can't allocate element");
				(*SegmentLength)++;
				s->next->column = c;
				s->next->row = r;
				s->next->plane = p;
				s->next->next = (PList3d*)NULL;
			}
/* these lines save us from infinite loop */
			if ( (*SegmentLength) > MaxSegment )
				return list;
		}
	}
	return list;
}

PList3d*IntensitySegment3dModified(CharVolume*dp, unsigned char Intensity, uint32 row, uint32 column, uint32 plane, int connectivity, uint32 MaxSegment, int *SegmentLength, uint32 MinSegment, unsigned char IntensityModified)
{
	PList3d*list, *cur;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	list = (PList3d*)malloc(sizeof(PList3d));
	(*SegmentLength) = 1;
	list->row = row;
	list->column = column;
	list->plane = plane;
	list->next = (PList3d*)NULL;
	dp->voxels[column][row][plane] = IntensityModified;
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=3 ) {
			int r = ext[n] + cur->row;
			int c = ext[n + 1] + cur->column;
			int p = ext[n + 2] + cur->plane;
			if ( r >= 0 && r < dp->rows && c >= 0 && c < dp->columns &&  p >= 0 && p < dp->planes && dp->voxels[c][r][p] == Intensity ) {
				PList3d*prev, *s;
				prev = cur;
				s = list;
				while ( prev ) {
					s = prev;
					prev = prev->next;
				}
				if ( ! (s->next = (PList3d*)malloc(sizeof(PList3d)) ) )
					g_error("PList3dGo can't allocate element");
				(*SegmentLength)++;
				s->next->column = c;
				s->next->row = r;
				s->next->plane = p;
				s->next->next = (PList3d*)NULL;
				dp->voxels[c][r][p] = IntensityModified;
			}
/* these lines save us from infinite loop */
			if ( (*SegmentLength) > MaxSegment )
				return list;
		}
	}
	return list;
}

int IsLabeled3d(PList3d*p, uint32 column, uint32 row, uint32 plane)
{
	PList3d*cur;
	cur = p;
	while ( cur ) {
		if ( cur->column == column && cur->row == row && cur->plane == plane )
			return 1;
		cur = cur->next;
	}
	return 0;
}


CharVolume*CharVolumeShapeSelect(CharVolume*cv, ShapeDescriptor3d*sd, double eps, CheckShapeType cstype, int connectivity, uint32 MaxSegment, uint32 MinSegment)
{
	CharVolume*ci;
	int column, row,plane;
	switch ( cstype ) {
		case shape_accept:
			ci = CharVolumeClone(cv);
		break;
		case shape_reject:
			ci = CharVolumeCopy(cv);
		break;
		default:
			g_error("Shape: Unknown %d", (int)cstype);
	}
	uint32 cvnumber = cv->columns * cv->rows * cv->planes;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(ci,cv,sd,eps,cstype,connectivity,MaxSegment,MinSegment,cvnumber)
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					ShapeDescriptor3d*csd;
					double dist;
					int SegmentLength;
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, cvnumber, &SegmentLength, MinSegment, EmptyPixel);
					if ( MinSegment < SegmentLength && SegmentLength < MaxSegment ) {
						csd = PList3d2ShapeDescriptor3d(list);
						dist = DistanceShape3d_1(sd, csd);
						if ( dist < eps ) {
							switch ( cstype ) {
								case shape_accept:
									MarkPlist3d(list, ci, FilledPixel);
								break;
								case shape_reject:
									MarkPlist3d(list, ci, EmptyPixel);
								break;
								default:
									g_error("Shape: Unknown %d", (int)cstype);
							}
						}
						free(csd);
					}
					PList3dFree(list);
				}
			}
		}
	}
	CharVolumeDelete(cv);
	return ci;
}

void CharVolumeShapeSelectList(CharVolume*cv, ShapeDescriptor3d*sd, double eps, CheckShapeType cstype, int connectivity, uint32 MaxSegment, uint32 MinSegment, FILE*fp)
{
	int column, row,plane;
	switch ( cstype ) {
		case shape_accept:
		break;
		case shape_reject:
		break;
		default:
			g_error("Shape: Unknown %d", (int)cstype);
	}
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					ShapeDescriptor3d*csd;
					double dist;
					int SegmentLength;
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, MaxSegment, &SegmentLength, MinSegment, EmptyPixel);
					fprintf(fp,"\n %u", SegmentLength);
					if ( MinSegment < SegmentLength && SegmentLength < MaxSegment ) {
						csd = PList3d2ShapeDescriptor3d(list);
						dist = DistanceShape3d_1(sd, csd);
						fprintf(fp," %f %f ", csd->m000, dist);
						fflush(fp);
						if ( dist < eps ) {
							switch ( cstype ) {
								case shape_accept:
									fprintf(fp," accepted");
								break;
								case shape_reject:
									fprintf(fp," rejected");
								break;
								default:
									g_error("Shape: Unknown %d", (int)cstype);
							}
						}
						free(csd);
					} else {
						fprintf(fp," length out of bounds");
					}
					PList3dFree(list);
				}
			}
		}
	}
}


ShapeDescriptor3d*PList3d2ShapeDescriptor3d(PList3d*list)
{
	ShapeDescriptor3d*sd;
	PList3d*cur;
	double mu000, mu000_gamma, signv, valv;
	double a, b, c;
	int i_min, i_max, j_min, j_max, k_min, k_max, i_mean, j_mean, k_mean;
	sd = (ShapeDescriptor3d*)malloc(sizeof(ShapeDescriptor3d));
	sd->m000 = 0.0;
	sd->m001 = 0.0;
	sd->m010 = 0.0;
	sd->m100 = 0.0;
	sd->xmean = 0.0;
	sd->ymean = 0.0;
	sd->zmean = 0.0;
	sd->mu002 = 0.0;
	sd->mu020 = 0.0;
	sd->mu200 = 0.0;
	sd->mu110 = 0.0;
	sd->mu101 = 0.0;
	sd->mu011 = 0.0;
	sd->eta002 = 0.0;
	sd->eta020 = 0.0;
	sd->eta200 = 0.0;
	sd->eta110 = 0.0;
	sd->eta101 = 0.0;
	sd->eta011 = 0.0;
	sd->I_1 = 0.0;
	sd->I_2 = 0.0;
	cur = list;
	while (cur) {
		sd->m000 += 1.0;
		sd->m100 += (double)cur->column;
		sd->m010 += (double)cur->row;
		sd->m001 += (double)cur->plane;
		cur = cur->next;
	}
	sd->xmean = sd->m100 / sd->m000;
	sd->ymean = sd->m010 / sd->m000;
	sd->zmean = sd->m001 / sd->m000;
	cur = list;
	i_min = (int)cur->row;
	j_min = (int)cur->column;
	k_min = (int)cur->plane;
	i_max = (int)cur->row;
	j_max = (int)cur->column;
	k_max = (int)cur->plane;
	i_mean = (int)(sd->ymean + 0.5);
	j_mean = (int)(sd->xmean + 0.5);
	k_mean = (int)(sd->zmean + 0.5);
	while (cur) {
		sd->mu002 += ( cur->plane - sd->zmean ) * ( cur->plane - sd->zmean );
		sd->mu020 += ( cur->row - sd->ymean ) * ( cur->row - sd->ymean );
		sd->mu200 += ( cur->column - sd->xmean ) * ( cur->column - sd->xmean );
		sd->mu110 += ( cur->column - sd->xmean ) * ( cur->row - sd->ymean );
		sd->mu101 += ( cur->column - sd->xmean ) * ( cur->plane - sd->zmean );
		sd->mu011 += ( cur->row - sd->ymean ) * ( cur->plane - sd->zmean );
		if ( cur->plane == k_mean ) {
			if ( i_min > (int)cur->row )
				i_min = (int)cur->row;
			if ( j_min > (int)cur->column )
				j_min = (int)cur->column;
			if ( i_max < (int)cur->row )
				i_max = (int)cur->row;
			if ( j_max < (int)cur->column )
				j_max = (int)cur->column;
		}
		if ( cur->row == i_mean ) {
			if ( k_min > (int)cur->plane )
				k_min = (int)cur->plane;
			if ( k_max < (int)cur->plane )
				k_max = (int)cur->plane;
		}
		cur = cur->next;
	}
	if ( k_mean - k_min > k_max - k_mean ) {
		c = k_mean - k_min;
	} else {
		c = k_max - k_mean;
	}
	if ( i_mean - i_min > i_max - i_mean ) {
		b = i_mean - i_min;
	} else {
		b = i_max - i_mean;
	}
	if ( j_mean - j_min > j_max - j_mean ) {
		a = j_mean - j_min;
	} else {
		a = j_max - j_mean;
	}
	sd->ellipsoid_axis_a = a;
	sd->ellipsoid_axis_b = b;
	sd->ellipsoid_axis_c = c;
	sd->i_mean = i_mean;
	sd->j_mean = j_mean;
	sd->k_mean = k_mean;
	sd->i_min = i_min;
	sd->j_min = j_min;
	sd->k_min = k_min;
	sd->i_max = i_max;
	sd->j_max = j_max;
	sd->k_max = k_max;
	mu000 = sd->m000;
	mu000_gamma = pow(mu000, 1.0 / 3.0) * mu000;
	sd->eta002 = sd->mu002 / mu000_gamma;
	sd->eta020 = sd->mu020 / mu000_gamma;
	sd->eta200 = sd->mu200 / mu000_gamma;
	sd->eta110 = sd->mu110 / mu000_gamma;
	sd->eta101 = sd->mu101 / mu000_gamma;
	sd->eta011 = sd->mu011 / mu000_gamma;
	sd->I_1 = sd->eta002 + sd->eta020 + sd->eta200;
	sd->I_2 = sd->eta020 * sd->eta002 + sd->eta200 * sd->eta002 + sd->eta200 * sd->eta020 - sd->eta011 * sd->eta011 - sd->eta101 * sd->eta101 - sd->eta110 * sd->eta110;
	sd->I_3 = sd->eta002 * sd->eta200 * sd->eta020 + 2 * sd->eta110 * sd->eta011 * sd->eta101 - sd->eta101 * sd->eta101 * sd->eta020 - sd->eta011 * sd->eta011 * sd->eta200 - sd->eta110 * sd->eta110 * sd->eta002;
	sd->J_1 = sd->I_1;
	signv = ( sd->I_2 > 0 ) ? 1.0 : -1.0;
	valv = ( sd->I_2 > 0 ) ? sd->I_2 : -sd->I_2;
	sd->J_2 = signv * sqrt(valv);
	signv = ( sd->I_3 > 0 ) ? 1.0 : -1.0;
	valv = ( sd->I_3 > 0 ) ? sd->I_3 : -sd->I_3;
	sd->J_3 = signv * pow(valv , 1.0 / 3.0);
	return sd;
}

ShapeDescriptor3d*CharVolume2ShapeDescriptor3d(CharVolume*cv, int connectivity)
{
	int column, row, plane, dummy;
	PList3d*list;
	ShapeDescriptor3d*sd;
	list = (PList3d*)NULL;
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &dummy, 0, EmptyPixel);
					break;
				}
			}
		}
	}
	if ( !list)
		g_error("SD null pointer");
	sd = PList3d2ShapeDescriptor3d(list);
	PList3dFree(list);
	return sd;
}

double DistanceShape3d(ShapeDescriptor3d*sd, ShapeDescriptor3d*csd)
{
	return ( sd->I_1 - csd->I_1 ) * ( sd->I_1 - csd->I_1 ) + ( sd->I_2 - csd->I_2 ) * ( sd->I_2 - csd->I_2 ) + ( sd->I_3 - csd->I_3 ) * ( sd->I_3 - csd->I_3 );
}


double DistanceShape3d_1(ShapeDescriptor3d*sd, ShapeDescriptor3d*csd)
{
	return ( sd->J_1 - csd->J_1 ) * ( sd->J_1 - csd->J_1 ) + ( sd->J_2 - csd->J_2 ) * ( sd->J_2 - csd->J_2 ) + ( sd->J_3 - csd->J_3 ) * ( sd->J_3 - csd->J_3 );
}


CharVolume*CharVolumeCopy(CharVolume*cv)
{
	int column, row,plane;
	CharVolume*ci;
	ci = CharVolumeClone(cv);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(ci,cv)
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				ci->voxels[column][row][plane] = cv->voxels[column][row][plane];
			}
		}
	}
	return ci;
}

CharVolume*CharVolumeInvert(CharVolume*cv)
{
	int column, row, plane;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(cv)
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				cv->voxels[column][row][plane] = FilledPixel - cv->voxels[column][row][plane];
			}
		}
	}
	return cv;
}


void ShapeDescriptor3dPrint(GKeyFile*gkf, ShapeDescriptor3d*s, char*grp)
{
	char *buf;
	buf = g_strdup_printf("%13.6f", s->m000);
	g_key_file_set_value(gkf, grp, "m000", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->m100);
	g_key_file_set_value(gkf, grp, "m100", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->m010);
	g_key_file_set_value(gkf, grp, "m010", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->m001);
	g_key_file_set_value(gkf, grp, "m001", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->xmean);
	g_key_file_set_value(gkf, grp, "xmean", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ymean);
	g_key_file_set_value(gkf, grp, "ymean", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->zmean);
	g_key_file_set_value(gkf, grp, "zmean", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu002);
	g_key_file_set_value(gkf, grp, "mu002", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu020);
	g_key_file_set_value(gkf, grp, "mu020", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu200);
	g_key_file_set_value(gkf, grp, "mu200", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu110);
	g_key_file_set_value(gkf, grp, "mu110", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu101);
	g_key_file_set_value(gkf, grp, "mu101", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu011);
	g_key_file_set_value(gkf, grp, "mu011", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta002);
	g_key_file_set_value(gkf, grp, "eta002", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta020);
	g_key_file_set_value(gkf, grp, "eta020", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta200);
	g_key_file_set_value(gkf, grp, "eta200", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta110);
	g_key_file_set_value(gkf, grp, "eta110", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta101);
	g_key_file_set_value(gkf, grp, "eta101", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta011);
	g_key_file_set_value(gkf, grp, "eta011", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_1);
	g_key_file_set_value(gkf, grp, "I_1", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_2);
	g_key_file_set_value(gkf, grp, "I_2", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_3);
	g_key_file_set_value(gkf, grp, "I_3", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->J_1);
	g_key_file_set_value(gkf, grp, "J_1", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->J_2);
	g_key_file_set_value(gkf, grp, "J_2", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->J_3);
	g_key_file_set_value(gkf, grp, "J_3", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ellipsoid_axis_a);
	g_key_file_set_value(gkf, grp, "EllipsoidalAxisA", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ellipsoid_axis_b);
	g_key_file_set_value(gkf, grp, "EllipsoidalAxisB", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ellipsoid_axis_c);
	g_key_file_set_value(gkf, grp, "EllipsoidalAxisC", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->i_mean);
	g_key_file_set_value(gkf, grp, "RowMean", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->j_mean);
	g_key_file_set_value(gkf, grp, "ColumnMean", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->k_mean);
	g_key_file_set_value(gkf, grp, "PlaneMean", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->i_min);
	g_key_file_set_value(gkf, grp, "RowMin", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->j_min);
	g_key_file_set_value(gkf, grp, "ColumnMin", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->k_min);
	g_key_file_set_value(gkf, grp, "PlaneMin", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->i_max);
	g_key_file_set_value(gkf, grp, "RowMax", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->j_max);
	g_key_file_set_value(gkf, grp, "ColumnMax", buf);
	g_free(buf);
	buf = g_strdup_printf("%d", s->k_max);
	g_key_file_set_value(gkf, grp, "PlaneMax", buf);
	g_free(buf);
}

ShapeDescriptor3d*shape_descriptor_3d_read(GKeyFile*gkf, char*grp, GError**gerror)
{
	ShapeDescriptor3d*s;
	s = (ShapeDescriptor3d*)malloc(sizeof(ShapeDescriptor3d));
	s->m000 = g_key_file_get_double(gkf, grp, "m000", gerror);
	if ( (*gerror) )
		return s;
	s->m100 = g_key_file_get_double(gkf, grp, "m100", gerror);
	if ( (*gerror) )
		return s;
	s->m010 = g_key_file_get_double(gkf, grp, "m010", gerror);
	if ( (*gerror) )
		return s;
	s->m001 = g_key_file_get_double(gkf, grp, "m001", gerror);
	if ( (*gerror) )
		return s;
	s->xmean = g_key_file_get_double(gkf, grp, "xmean", gerror);
	if ( (*gerror) )
		return s;
	s->ymean = g_key_file_get_double(gkf, grp, "ymean", gerror);
	if ( (*gerror) )
		return s;
	s->zmean = g_key_file_get_double(gkf, grp, "zmean", gerror);
	if ( (*gerror) )
		return s;
	s->mu002 = g_key_file_get_double(gkf, grp, "mu002", gerror);
	if ( (*gerror) )
		return s;
	s->mu020 = g_key_file_get_double(gkf, grp, "mu020", gerror);
	if ( (*gerror) )
		return s;
	s->mu200 = g_key_file_get_double(gkf, grp, "mu200", gerror);
	if ( (*gerror) )
		return s;
	s->mu110 = g_key_file_get_double(gkf, grp, "mu110", gerror);
	if ( (*gerror) )
		return s;
	s->mu101 = g_key_file_get_double(gkf, grp, "mu101", gerror);
	if ( (*gerror) )
		return s;
	s->mu011 = g_key_file_get_double(gkf, grp, "mu011", gerror);
	if ( (*gerror) )
		return s;
	s->eta002 = g_key_file_get_double(gkf, grp, "eta002", gerror);
	if ( (*gerror) )
		return s;
	s->eta020 = g_key_file_get_double(gkf, grp, "eta020", gerror);
	if ( (*gerror) )
		return s;
	s->eta200 = g_key_file_get_double(gkf, grp, "eta200", gerror);
	if ( (*gerror) )
		return s;
	s->eta110 = g_key_file_get_double(gkf, grp, "eta110", gerror);
	if ( (*gerror) )
		return s;
	s->eta101 = g_key_file_get_double(gkf, grp, "eta101", gerror);
	if ( (*gerror) )
		return s;
	s->eta011 = g_key_file_get_double(gkf, grp, "eta011", gerror);
	if ( (*gerror) )
		return s;
	s->I_1 = g_key_file_get_double(gkf, grp, "I_1", gerror);
	if ( (*gerror) )
		return s;
	s->I_2 = g_key_file_get_double(gkf, grp, "I_2", gerror);
	if ( (*gerror) )
		return s;
	s->I_3 = g_key_file_get_double(gkf, grp, "I_3", gerror);
	if ( (*gerror) )
		return s;
	s->J_1 = g_key_file_get_double(gkf, grp, "J_1", gerror);
	if ( (*gerror) )
		return s;
	s->J_2 = g_key_file_get_double(gkf, grp, "J_2", gerror);
	if ( (*gerror) )
		return s;
	s->J_3 = g_key_file_get_double(gkf, grp, "J_3", gerror);
	if ( (*gerror) )
		return s;
	s->ellipsoid_axis_a = g_key_file_get_double(gkf, grp, "EllipsoidalAxisA", gerror);
	if ( (*gerror) )
		return s;
	s->ellipsoid_axis_b = g_key_file_get_double(gkf, grp, "EllipsoidalAxisB", gerror);
	if ( (*gerror) )
		return s;
	s->ellipsoid_axis_c = g_key_file_get_double(gkf, grp, "EllipsoidalAxisC", gerror);
	if ( (*gerror) )
		return s;
	s->i_mean = g_key_file_get_integer(gkf, grp, "RowMean", gerror);
	if ( (*gerror) )
		return s;
	s->j_mean = g_key_file_get_integer(gkf, grp, "ColumnMean", gerror);
	if ( (*gerror) )
		return s;
	s->k_mean = g_key_file_get_integer(gkf, grp, "PlaneMean", gerror);
	if ( (*gerror) )
		return s;
	s->i_min = g_key_file_get_integer(gkf, grp, "RowMin", gerror);
	if ( (*gerror) )
		return s;
	s->j_min = g_key_file_get_integer(gkf, grp, "ColumnMin", gerror);
	if ( (*gerror) )
		return s;
	s->k_min = g_key_file_get_integer(gkf, grp, "PlaneMin", gerror);
	if ( (*gerror) )
		return s;
	s->i_max = g_key_file_get_integer(gkf, grp, "RowMax", gerror);
	if ( (*gerror) )
		return s;
	s->j_max = g_key_file_get_integer(gkf, grp, "ColumnMax", gerror);
	if ( (*gerror) )
		return s;
	s->k_max = g_key_file_get_integer(gkf, grp, "PlaneMax", gerror);
	if ( (*gerror) )
		return s;
	return s;
}

void CharVolumeQuTransform(GKeyFile*gkf, int center, int percent, ShapeDescriptor3d*mask, double minvolume, double maxvolume, GError**gerror)
{
	double xmean, ymean, zmean, xrange, yrange, zrange;
	double x, y, z, volume;
	char**grps, *buf;
	gsize size;
	int i;
	xmean = mask->xmean;
	ymean = mask->ymean;
	zmean = mask->zmean;
	xrange = (double)(mask->j_max - mask->j_min);
	if ( xrange <= 0 )
		xrange = 1;
	yrange = (double)(mask->i_max - mask->i_min);
	if ( yrange <= 0 )
		yrange = 1;
	zrange = (double)(mask->k_max - mask->k_min);
	if ( zrange <= 0 )
		zrange = 1;
	grps = g_key_file_get_groups(gkf, &size);
	if ( grps ) {
		for ( i = 0; i < (int)size; i++ ) {
			if ( g_str_has_prefix(grps[i], "Shape:")) {
				volume = g_key_file_get_double(gkf, grps[i], "m000", gerror);
				if ( minvolume > volume || volume > maxvolume ) {
					if ( !g_key_file_remove_group (gkf, (const gchar*)grps[i], gerror) ) {
						g_strfreev(grps);
						return;
					}
				} else {
					x = g_key_file_get_double(gkf, grps[i], "xmean", gerror);
					if ( (*gerror) ) {
						g_strfreev(grps);
						return;
					}
					y = g_key_file_get_double(gkf, grps[i], "ymean", gerror);
					if ( (*gerror) ) {
						g_strfreev(grps);
						return;
					}
					z = g_key_file_get_double(gkf, grps[i], "zmean", gerror);
					if ( (*gerror) ) {
						g_strfreev(grps);
						return;
					}
					if ( center ) {
						x -= xmean;
						y -= ymean;
						z -= zmean;
					}
					if ( percent ) {
						x *= 100.0 / xrange;
						y *= 100.0 / yrange;
						z *= 100.0 / zrange;
					}
					buf = g_strdup_printf("%13.6f", x);
					g_key_file_set_value(gkf, grps[i], "xmean", buf);
					g_free(buf);
					buf = g_strdup_printf("%13.6f", y);
					g_key_file_set_value(gkf, grps[i], "ymean", buf);
					g_free(buf);
					buf = g_strdup_printf("%13.6f", z);
					g_key_file_set_value(gkf, grps[i], "zmean", buf);
					g_free(buf);
				}
			}
		}
		g_strfreev(grps);
	}
}

CharVolume*CharVolumeQuMap(GKeyFile*gkf, CharVolume*mask, int connectivity, char*channel, int index, GError**gerror)
{
	double x, y, z, p;
	CharVolume*cv;
	char**grps;
	gsize size;
	int row, column, plane;
	PList3d*list;
	int SegmentLength;
	int intensity, i;
	unsigned char pixel;
	grps = g_key_file_get_groups(gkf, &size);
	if ( grps ) {
		cv = CharVolumeClone(mask);
		for ( i = 0; i < (int)size; i++ ) {
			if ( g_str_has_prefix(grps[i], "Shape:")) {
				x = g_key_file_get_double(gkf, grps[i], "xmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				y = g_key_file_get_double(gkf, grps[i], "ymean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				z = g_key_file_get_double(gkf, grps[i], "zmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				row = (int) ( y + 0.5 );
				column = (int) ( x + 0.5 );
				plane = (int) ( z + 0.5 );
				shape_descriptor_get_data_field(gkf, grps[i], channel, &p, index, gerror);
				intensity = (int) ( p + 0.5 );
				if ( intensity > 255 )
					intensity = 255;
				if ( intensity < 0 )
					intensity = 0;
				pixel = (unsigned char) intensity;
				list = IntensitySegment3dModified(mask, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &SegmentLength, 0, EmptyPixel);
				MarkPlist3d(list, cv, pixel);
				PList3dFree(list);
			}
		}
		g_strfreev(grps);
	}
	return cv;
}

gint grps_func (gconstpointer a, gconstpointer b, gpointer user_data)
{
	char*grp_1 = (char*)a;
	char*grp_2 = (char*)b;
	double p_1, p_2;
	GKeyFile*gkf = (GKeyFile*)user_data;
	GError*gerror = NULL;
	char*channel = g_key_file_get_string(gkf, "default", "channel", &gerror);
	int index = g_key_file_get_integer(gkf, "default", "index", &gerror);
	int ascending = g_key_file_get_integer(gkf, "default", "ascending", &gerror);
	int retval = (ascending == 1) ? 1 : -1;
	shape_descriptor_get_data_field(gkf, grp_1, channel, &p_1, index, &gerror);
	shape_descriptor_get_data_field(gkf, grp_2, channel, &p_2, index, &gerror);
	if (p_1 < p_2) {
		return -retval;
	} else if (p_1 == p_2) {
		return 0;
	}
	return retval;
}

CharVolume*CharVolumeQuSort(GKeyFile*gkf, CharVolume*mask, int connectivity, char*channel, int index, char*refch, char*newch, int ascending, GError**gerror)
{
	double x, y, z, p;
	CharVolume*cv;
	char**grps;
	gsize size;
	int row, column, plane;
	PList3d*list;
	int SegmentLength;
	int intensity, i;
	GHashTable *gtab, *gtabv;
	GList*garr;
	unsigned char pixel;
	grps = g_key_file_get_groups(gkf, &size);
	gtab = g_hash_table_new (g_int_hash, g_int_equal);
	g_key_file_set_value(gkf, "default", "channel", channel);
	g_key_file_set_integer(gkf, "default", "index", index);
	g_key_file_set_integer(gkf, "default", "ascending", ascending);
	if ( grps ) {
		cv = CharVolumeClone(mask);
		for ( i = 0; i < (int)size; i++ ) {
			if ( g_str_has_prefix(grps[i], "Shape:")) {
				x = g_key_file_get_double(gkf, grps[i], "xmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				y = g_key_file_get_double(gkf, grps[i], "ymean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				z = g_key_file_get_double(gkf, grps[i], "zmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				row = (int) ( y + 0.5 );
				column = (int) ( x + 0.5 );
				plane = (int) ( z + 0.5 );
				shape_descriptor_get_data_field(gkf, grps[i], refch, &p, 0, gerror);
				intensity = (int) ( p + 0.5 );
				if (intensity > 0) {
					shape_descriptor_get_data_field(gkf, grps[i], channel, &p, index, gerror);
					garr = g_hash_table_lookup (gtab, &intensity);
					garr = g_list_insert_sorted_with_data (garr, grps[i], (GCompareDataFunc) grps_func, gkf);
					g_hash_table_insert (gtab, &intensity, garr);
				}
			}
		}
		for ( i = 0; i < (int)size; i++ ) {
			if ( g_str_has_prefix(grps[i], "Shape:")) {
				x = g_key_file_get_double(gkf, grps[i], "xmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				y = g_key_file_get_double(gkf, grps[i], "ymean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				z = g_key_file_get_double(gkf, grps[i], "zmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return cv;
				}
				row = (int) ( y + 0.5 );
				column = (int) ( x + 0.5 );
				plane = (int) ( z + 0.5 );
				shape_descriptor_get_data_field(gkf, grps[i], refch, &p, 0, gerror);
				intensity = (int) ( p + 0.5 );
				if (intensity > 0) {
					garr = g_hash_table_lookup (gtab, &intensity);
					intensity = g_list_index (garr, grps[i]) + 1;
					g_key_file_set_integer(gkf, grps[i], newch, intensity);
					if ( intensity > 255 )
						intensity = 255;
					if ( intensity < 0 )
						intensity = 0;
				}
				pixel = (unsigned char) intensity;
				list = IntensitySegment3dModified(mask, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &SegmentLength, 0, EmptyPixel);
				MarkPlist3d(list, cv, pixel);
				PList3dFree(list);
			}
		}
		g_strfreev(grps);
	}
	return cv;
}

void CharVolumeQuPrint(GKeyFile*gkf, char*filename)
{
	FILE*fp;
	char**grps;
	int ngrps;
	char**ckeys;
	int nckeys;
	int i, j, k;
	char**list;
	int len;
	gsize size;
	GError*gerror = NULL;
	fp = fopen(filename, "w");
	if ( !fp )
		g_error("Can't open %s", filename);
	grps = g_key_file_get_groups(gkf, &size);
	ngrps = (int)size;
	if ( grps ) {
		j = 0;
		if ( g_str_has_prefix(grps[j], "Shape:")) {
			ckeys = g_key_file_get_keys(gkf, grps[j], &size, &gerror);
			nckeys = (int)size;
			fprintf(fp, "Number");
			if ( ckeys ) {
				for ( i = 0; i < nckeys; i++ ) {
					list = g_key_file_get_string_list(gkf, grps[j], ckeys[i], &size, &gerror);
					len = (int)size;
					if ( len == 1 ) {
						fprintf(fp, ",%s", ckeys[i]);
					} else if (g_str_has_prefix(ckeys[i], "vert")) {
						for ( k = 0; k < len; k++ ) {
							fprintf(fp, ",%s_%d", ckeys[i], k);
						}
					} else {
						for ( k = 0; k < len; k++ ) {
							if ( k == 0 ) {
								fprintf(fp, ",%s_mean", ckeys[i]);
							} else if ( k == 1 ) {
								fprintf(fp, ",%s_var", ckeys[i]);
							} else if ( k == 2 ) {
								fprintf(fp, ",%s_stdev", ckeys[i]);
							} else if ( k == 3 ) {
								fprintf(fp, ",%s_max", ckeys[i]);
							} else if ( k == 4 ) {
								fprintf(fp, ",%s_min", ckeys[i]);
							} else if ( k == 5 ) {
								fprintf(fp, ",%s_median", ckeys[i]);
							} else if ( k == 6 ) {
								fprintf(fp, ",%s_muc", ckeys[i]);
							} else if ( k == 7 ) {
								fprintf(fp, ",%s_notempty", ckeys[i]);
							} else {
								fprintf(fp, ",%s_%d", ckeys[i], k);
							}
						}
					}
					g_strfreev(list);
				}
				g_strfreev(ckeys);
			}
			fprintf(fp, "\n");
		}
		for ( j = 0; j < ngrps; j++ ) {
			if ( g_str_has_prefix(grps[j], "Shape:")) {
				ckeys = g_key_file_get_keys(gkf, grps[j], &size, &gerror);
				nckeys = (int)size;
				fprintf(fp, "%d", j);
				if ( ckeys ) {
					for ( i = 0; i < nckeys; i++ ) {
						list = g_key_file_get_string_list(gkf, grps[j], ckeys[i], &size, &gerror);
						len = (int)size;
						for ( k = 0; k < len; k++ ) {
							fprintf(fp, ",%s", list[k]);
						}
						g_strfreev(list);
					}
					g_strfreev(ckeys);
				}
				fprintf(fp, "\n");
			}
		}
		g_strfreev(grps);
	}
	fclose(fp);
}

void ShapeDescriptor3dDelete (ShapeDescriptor3d*sd)
{
	free(sd);
}

void CharVolumePrintText(CharVolume*cv, FILE*f, int connectivity, VolumetricFormat format)
{
	int column, row, plane, counter;
	char *temp, *buffer;
	FILE *tmpfile;
	VolumetricFormatWriteHeaderType vfh;
	VolumetricFormatWriteBodyType vfb;
	VolumetricFormatWriteFooterType vff;
	switch ( format ) {
		case XYZ_VOLUMETRIC_FORMAT:
			vfh = xyzWriteHeader;
			vfb = xyzWriteBody;
			vff = xyzWriteFooter;
		break;
		case VRML_VOLUMETRIC_FORMAT:
			vfh = vrmlWriteHeader;
			vfb = vrmlWriteBody;
			vff = vrmlWriteFooter;
		break;
		case VRML_VOLUMETRIC_FORMAT_SPHERE:
			vfh = vrmlSphereWriteHeader;
			vfb = vrmlSphereWriteBody;
			vff = vrmlSphereWriteFooter;
		break;
		default:
			g_error("CharVolumePrintText: unknown format");
	}
	temp = pr_get_name_temp();
	tmpfile = fopen( temp, "w");               /* ... and open it for writing */
	if ( !tmpfile )
		g_error("CharVolumePrintText: error opening temporary file");
	counter = 0;
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					double dist;
					int SegmentLength;
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &SegmentLength, 0, EmptyPixel);
fprintf(stdout,"\n orig: %d", SegmentLength);
/*					if ( counter == 3 ) {*/
					vfb( (void*) list, tmpfile, (void*) cv);
/*					column = cv->columns;
					row = cv->rows;
					plane = cv->planes;
					}*/
					counter++;
					PList3dFree(list);
				}
			}
		}
	}
	CharVolumeDelete(cv);
	fclose(tmpfile);
	vfh( (void*) (&counter), f);
	tmpfile = fopen( temp, "r");
	if ( !tmpfile )
		g_error("CharVolumePrintText: error opening temporary file for reading");
	buffer = (char *)calloc(MAX_RECORD, sizeof(char));
	while ( buffer = fgets( buffer, MAX_RECORD, tmpfile) ) {
		fprintf( f, "%s", buffer);
	}
	fclose(tmpfile);
	free(buffer);
	remove(temp);
	free(temp);
	vff( (void*) (&counter), f);
}


void xyzWriteHeader( void* ptr, FILE*fp)
{
	fprintf( fp, "%d\n\n", *((int*)ptr));
}

void xyzWriteBody( void* ptr, FILE*fp, void* ptr2)
{
	ShapeDescriptor3d*sd;
	CharVolume*cv;
	cv = (CharVolume*)ptr2;
	sd = PList3d2ShapeDescriptor3d( (PList3d*)ptr );
	fprintf( fp, " %5.3f", 0.5 - sd->xmean / (double)cv->columns);
	fprintf( fp, " %5.3f", 0.5 - sd->ymean / (double)cv->columns);
	fprintf( fp, " %5.3f", 0.5 - sd->zmean * (double)cv->rows / (double)cv->columns / (double)cv->planes);
	fprintf( fp, "\n");
	free(sd);
}

void xyzWriteFooter( void* ptr, FILE*fp)
{
	return;
}

void vrmlWriteHeader( void* ptr, FILE*fp)
{
static const char vrmlHeader[] =
"#VRML V2.0 utf8\n"
"Transform {\n"
"	children [\n"
"		NavigationInfo { headlight TRUE }\n"
/*"		DirectionalLight {\n"
"			direction 0 0 -1\n"
"		}\n"*/;
	fprintf( fp, "%s", vrmlHeader);
}

void vrmlWriteBody( void* ptr, FILE*fp, void* ptr2)
{
static const char vrmlElementStart[] =
"		Transform {\n"
"			children [\n"
"				Shape {\n"
"					appearance Appearance {\n"
"						material Material {\n"
"							diffuseColor 1 0 0\n"
"							transparency 0.000\n"
"							shininess 0.234\n"
"							specularColor 0.850 0.850 0.850}\n"
"					}\n"
"					geometry IndexedFaceSet {\n"
"						coord Coordinate{\n"
"							point [\n";
static const char vrmlElementMid[] =
"							]\n"
"						}\n"
"						coordIndex [\n";
static const char vrmlElementEnd[] =
"						]\n"
"						solid FALSE\n"
"					}\n"
"				}\n"
"			]\n"
"		}\n";
	CharVolume*cv;
	PList3d*list, *p;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n, n1;
	int k_1, k_2, k_3;
	int connectivity;
	PList3d*trList, *newlist;
	connectivity = 26;
	cv = (CharVolume*)ptr2;
	list = (PList3d*)ptr;
	fprintf( fp, "%s", vrmlElementStart);
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
/*	newlist = PList3dReduce(list, delta, connectivity);*/
	newlist = list;
	p = newlist;
	while ( p ) {
		fprintf( fp, "%5.3f %5.3f %5.3f\n", 0.5 - (double)p->column / (double)cv->columns, 0.5 - (double)p->row / (double)cv->columns, 0.5 - (double)p->plane /** (double)cv->rows*/ / (double)cv->columns /* /(double)cv->planes*/);
		p = p->next;
	}
	fprintf( fp, "%s", vrmlElementMid);
	p = newlist;
	k_1 = 0;
	trList = (PList3d*)NULL;
	while ( p ) {
/*		fprintf( stdout, "sss0 %d : %d  %d %d \n", k_1, p->column, p->row, p->plane);*/
		for ( n = 0; n < next; n+=3 ) {
			int co = p->column + ext[n];
			int r = p->row + ext[n + 1];
			int po = p->plane + ext[n + 2];
			k_2 = -1;
			if ( 0 <= co && co < cv->columns && 0 <= r && r < cv->rows && 0 <= po && po <= cv->planes )
				k_2 = plist3dHas(newlist, co, r, po);
/*			fprintf( stdout, "sss %d : %d  %d %d \n", k_2, co,r,po);
			if ( k_2 != -1 )fprintf( stdout, "should n : %d %d \n", k_2, areNeighbors(newlist, k_1, k_2, connectivity));*/
			if ( k_2 != -1 ) {
				for ( n1 = 0; n1 < next; n1+=3 ) {
					int co1 = p->column + ext[n1];
					int r1 = p->row + ext[n1 + 1];
					int p1 = p->plane + ext[n1 + 2];
					k_3 = -1;
					if ( 0 <= co1 && co1 < cv->columns && 0 <= r1 && r1 < cv->rows && 0 <= p1 && p1 <= cv->planes )
						k_3 = plist3dHas(newlist, co1, r1, p1);
					if ( k_3 != -1  && areNeighbors(newlist, k_2, k_3, connectivity) ) {
						if ( plist3dCheckTriangle(trList, k_1, k_2, k_3, newlist) == -1 ) {
							PList3d*ptr_cur;
							ptr_cur = trList;
							trList = (PList3d*)malloc(sizeof(PList3d));
							trList->column = k_1;
							trList->row = k_2;
							trList->plane = k_3;
							trList->next = ptr_cur;
							fprintf( fp, "%d %d %d -1\n", k_1, k_2, k_3);
						}
					}
/*					if ( k_3 != -1) fprintf( stdout, "k_3: %d %d %d \n", k_1, k_2, k_3);*/
				}
			}
		}
		p = p->next;
		k_1++;
	}
	PList3dFree(trList);
	fprintf( fp, "%s", vrmlElementEnd);
}

void vrmlWriteFooter( void* ptr, FILE*fp)
{
static const char vrmlFooter[] =
"	]\n"
"}\n";
	fprintf( fp, "%s", vrmlFooter);
}

void vrmlSphereWriteHeader( void* ptr, FILE*fp)
{
static const char vrmlShereHeader[] =
"#VRML V2.0 utf8\n"
"Transform {\n"
"	children [\n"
"		NavigationInfo { headlight FALSE }\n"
"		DirectionalLight {\n"
"			direction 0 0 -1\n"
"		}\n";
	fprintf( fp, "%s", vrmlShereHeader);
}

void vrmlSphereWriteBody( void* ptr, FILE*fp, void* ptr2)
{
static const char vrmlShereElement[] =
"		Transform {\n"
"			translation %5.3f %5.3f %5.3f\n"
"			children [\n"
"				Shape {\n"
"					geometry Sphere { radius %5.3f }\n"
"					appearance Appearance {\n"
"						material Material { diffuseColor 1 0 0 }\n"
"					}\n"
"				}\n"
"			]\n"
"		}\n";
	ShapeDescriptor3d*sd;
	CharVolume*cv;
	double radius = 0.045;
	cv = (CharVolume*)ptr2;
	sd = PList3d2ShapeDescriptor3d( (PList3d*)ptr );
	fprintf( fp, vrmlShereElement, 0.5 - sd->xmean / (double)cv->columns, 0.5 - sd->ymean / (double)cv->columns, 0.5 - sd->zmean * (double)cv->rows / (double)cv->columns / (double)cv->planes, radius);
	free(sd);
}

void vrmlSphereWriteFooter( void* ptr, FILE*fp)
{
static const char vrmlFooter[] =
"	]\n"
"}\n";
	fprintf( fp, "%s", vrmlFooter);
}

int plist3dHas(PList3d*list, int c, int r, int po)
{
	int k, kk;
	PList3d *p;
	p = list;
	kk = 0;
	while ( p && ! (p->column == c && p->row == r && p->plane == po) ) {
		p = p->next;
		kk++;
	}
	if ( p )
		return kk;
	return -1;
}


int plist3dHasAny(PList3d*list, int c, int r, int po)
{
	int k, kk;
	PList3d *p;
	p = list;
	kk = 0;
	while ( p && ! (p->column == c && p->row == r && p->plane == po) && ! (p->column == po && p->row == r && p->plane == c) && ! (p->column == c && p->row == po && p->plane == r) && ! (p->column == po && p->row == c && p->plane == r) && ! (p->column == r && p->row == po && p->plane == c) && ! (p->column == r && p->row == c && p->plane == po) ) {
		p = p->next;
		kk++;
	}
	if ( p )
		return kk;
	return -1;
}

int plist3dCheckTriangle(PList3d*trList, int c, int r, int po, PList3d*list)
{
	PList3d *p;
	p = trList;
	while ( p ) {
/* this triangle is already in list */
		if ( (p->column == c && p->row == r && p->plane == po) && ! (p->column == po && p->row == r && p->plane == c) && ! (p->column == c && p->row == po && p->plane == r) && ! (p->column == po && p->row == c && p->plane == r) && ! (p->column == r && p->row == po && p->plane == c) && ! (p->column == r && p->row == c && p->plane == po) ) {
			return 0;
/* if two of the three vertices of two triangles coinside then we need to include new triangle if and only if triangles are in different planes */
		} else if ( p->column == c && p->row == r ) {
			if ( areOnePlane(list, c, r, p->plane, po) )
				return 0;
			else
				return -1;
		} else if ( p->column == c && p->row == po ) {
			if ( areOnePlane(list, c, po, p->plane, r) )
				return 0;
			else
				return -1;
		} else if ( p->column == c && p->plane == r ) {
			if ( areOnePlane(list, c, r, p->row, po) )
				return 0;
			else
				return -1;
		} else if ( p->column == c && p->plane == po ) {
			if ( areOnePlane(list, c, po, p->row, r) )
				return 0;
			else
				return -1;
		} else if ( p->column == r && p->row == c ) {
			if ( areOnePlane(list, r, c, p->plane, po) )
				return 0;
			else
				return -1;
		} else if ( p->column == r && p->row == po ) {
			if ( areOnePlane(list, r, po, p->plane, c) )
				return 0;
			else
				return -1;
		} else if ( p->column == r && p->plane == c ) {
			if ( areOnePlane(list, r, c, p->row, po) )
				return 0;
			else
				return -1;
		} else if ( p->column == r && p->row == po ) {
			if ( areOnePlane(list, r, po, p->plane, c) )
				return 0;
			else
				return -1;
		} else if ( p->column == po && p->row == c ) {
			if ( areOnePlane(list, po, c, p->plane, r) )
				return 0;
			else
				return -1;
		} else if ( p->column == po && p->row == r ) {
			if ( areOnePlane(list, po, r, p->plane, c) )
				return 0;
			else
				return -1;
		} else if ( p->column == po && p->plane == c ) {
			if ( areOnePlane(list, po, c, p->row, r) )
				return 0;
			else
				return -1;
		} else if ( p->column == po && p->row == r ) {
			if ( areOnePlane(list, po, r, p->plane, c) )
				return 0;
			else
				return -1;
		} else if ( p->row == c && p->plane == r ) {
			if ( areOnePlane(list, c, r, p->column, po) )
				return 0;
			else
				return -1;
		} else if ( p->row == c && p->plane == po ) {
			if ( areOnePlane(list, c, po, p->column, r) )
				return 0;
			else
				return -1;
		} else if ( p->row == r && p->plane == c ) {
			if ( areOnePlane(list, r, c, p->column, po) )
				return 0;
			else
				return -1;
		} else if ( p->row == r && p->plane == po ) {
			if ( areOnePlane(list, r, po, p->column, c) )
				return 0;
			else
				return -1;
		} else if ( p->row == po && p->plane == c ) {
			if ( areOnePlane(list, po, c, p->column, r) )
				return 0;
			else
				return -1;
		} else if ( p->row == po && p->plane == r ) {
			if ( areOnePlane(list, po, r, p->column, c) )
				return 0;
			else
				return -1;
		}
		p = p->next;
	}
	return -1;
}

int areOnePlane(PList3d*list, int v1, int v2, int v3, int v4)
{
	int Q;
	PList3d*p, *p1, *p2, *p3, *p4;
	double x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;
	double a, b, c, d;
	int n;
	n = 0;
	p = list;
	p1 = (PList3d*)NULL;
	p2 = (PList3d*)NULL;
	p3 = (PList3d*)NULL;
	p4 = (PList3d*)NULL;
	while ( p ) {
		if ( n == v1 ) p1 = p;
		if ( n == v2 ) p2 = p;
		if ( n == v3 ) p3 = p;
		if ( n == v4 ) p4 = p;
		n++;
		p = p->next;
	}
	if ( !p1 || !p2 || !p3 || !p4)
		g_error("areOnePlane null pointer ");
	Q = 0;
	x1 = (double)p1->column;
	y1 = (double)p1->row;
	z1 = (double)p1->plane;
	x2 = (double)p2->column;
	y2 = (double)p2->row;
	z2 = (double)p2->plane;
	x3 = (double)p3->column;
	y3 = (double)p3->row;
	z3 = (double)p3->plane;
	x4 = (double)p4->column;
	y4 = (double)p4->row;
	z4 = (double)p4->plane;
	d = ( y2 - y3 ) * ( x1 - x2 ) - ( y1 - y2 ) * ( x2 - x3 );
	if ( d != 0 )
		a = ( ( y2 - y3 ) * ( z1 - z2 ) - ( y1 - y2 ) * ( z2 - z3 ) ) / d;
	else if ( x1 == x2 && x2 == x3 )
		a = 0;
	else if ( x1 != x2 )
		a = ( z1 - z2 ) / ( x1 - x2 );
	else if ( x2 != x3 )
		a = ( z2 - z3 ) / ( x2 - x3 );
	d = ( x2 - x3 ) * ( y1 - y2 ) - ( x1 - x2 ) * ( y2 - y3 );
	if ( d != 0 )
		b = ( ( x2 - x3 ) * ( z1 - z2 ) - ( x1 - x2 ) * ( z2 - z3 ) ) / d;
	else if ( y1 == y2 && y2 == y3 )
		b = 0;
	else if ( y1 != y2 )
		b = ( z1 - z2 ) / ( y1 - y2 );
	else if ( y2 != y3 )
		b = ( z2 - z3 ) / ( y2 - y3 );
	c = z3 - a * x3 - b * y3;
	d = fabs( z4 - a * x4 - b* y4 - c );
	if ( d == 0 ) {
		Q = 1;
		double a1x, a2x, a1y, a2y, b1x, b2x, b1y, b2y, t1, t2;
		b1x = x1;
		b1y = y1;
		a1x = x2 - b1x;
		a1y = y2 - b1y;
		b2x = x3;
		b2y = y3;
		a2x = x4 - b2x;
		a2y = y4 - b2y;
		t1 = ( ( a2y * b2x - a2x * b2y ) - ( a2y * b1x - a2x * b1y ) ) / ( a2y * a1x - a2x * a1y );
		t2 = ( ( a1y * b1x - a1x * b1y ) - ( a1y * b2x - a1x * b2y ) ) / ( a1y * a2x - a1x * a2y );
		if ( 0 <= t1 && t1 <= 1 && 0<=t2 && t2 <= 1 )
			Q = 0;
	}
/*	fprintf(stdout, "\n %f %f %f %f == %d", a, b, c, d, Q);*/
	return Q;
}

PList3d*PList3dReduce(PList3d*list, int delta, int connectivity)
{
	PList3d*newlist, *cur;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	int a, b;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	b = 1;
	newlist = (PList3d*)malloc(sizeof(PList3d));
	newlist->row = list->row;
	newlist->column = list->column;
	newlist->plane = list->plane;
	newlist->next = (PList3d*)NULL;
	for ( cur = newlist; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=3 ) {
			for ( a = delta; a > 0; a -= 1 ) {
				int r = a * ext[n] + cur->row;
				int c = a * ext[n + 1] + cur->column;
				int p = a * ext[n + 2] + cur->plane;
				if ( IsLabeled3d(newlist, c, r, p) ) {
					break;
				} else if ( IsLabeled3d(list, c, r, p) ) {
					PList3d*prev, *s;
					prev = cur;
					s = newlist;
					while ( prev ) {
						s = prev;
						prev = prev->next;
					}
					if ( ! (s->next = (PList3d*)malloc(sizeof(PList3d)) ) )
						g_error("PList3dGo can't allocate element");
					s->next->column = c;
					s->next->row = r;
					s->next->plane = p;
					s->next->next = (PList3d*)NULL;
					b++;
					break;
/*fprintf(stdout," a=%d", a);
fprintf(stdout,"\n %d %d %d", a, n, b);*/
				}
			}
		}
	}
	PList3dFree(list);
fprintf(stdout,"\n reduced %d", b);
fflush(stdout);
	return newlist;
}

int areNeighbors(PList3d*list, int k_1, int k_3, int connectivity)
{
	PList3d*pK_1, *pK_3, *cur;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	pK_1 = (PList3d*)NULL;
	pK_3 = (PList3d*)NULL;
	for ( cur = list, n = 0; cur; cur = cur->next, n++ ) {
		if ( n == k_1 ) pK_1 = cur;
		if ( n == k_3 ) pK_3 = cur;
	}
	if ( !pK_1 || !pK_3 )
		g_error("areNeighbors error");
/*	fprintf( stdout, "sss1 : %d: %d  %d %d \n", k_3, pK_3->column, pK_3->row, pK_3->plane);*/
	for ( n = 0; n < next; n+=3 ) {
		int r = ext[n] + pK_1->row;
		int c = ext[n + 1] + pK_1->column;
		int p = ext[n + 2] + pK_1->plane;
		if ( c == pK_3->column && r == pK_3->row && p == pK_3->plane )
			return 1;
	}
	return 0;
}

CharVolume*CharVolumeGErosionSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype)
{
	char *file;
	file = pr_get_name_temp();
	StructElem3d( file, vsize, hsize, zsize, vhtype, ztype);
	dp = CharVolumeGErosionWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharVolume*CharVolumeGErosionWithSE(CharVolume*dp, char *file)
{
	uint32 column, row, plane;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharVolume*dpm;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 3.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharVolumeClone(dp);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,dpm,pattern,npattern) private(np)
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++) {
				unsigned char newval;
				newval = FilledPixel;
				for ( np=0; np<npattern; np++ ) {
					register int indexc = column + pattern[3*np];
					register int indexr = row + pattern[3*np+1];
					register int indexz = plane + pattern[3*np+2];
					if (  -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows && -1 < indexz && indexz < dp->planes ) {
						unsigned char Intensity = dp->voxels[indexc][indexr][indexz];
						if ( Intensity < newval )
							newval = Intensity;
					}
				}
				dpm->voxels[column][row][plane] = newval;
			}
		}
	}
	CharVolumeDelete(dp);
	return dpm;
}

CharVolume*CharVolumeGDilationSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype)
{
	char *file;
	file = pr_get_name_temp();
	StructElem3d( file, vsize, hsize, zsize, vhtype, ztype);
	dp = CharVolumeGDilationWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageGDilationSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharVolume*CharVolumeGDilationWithSE(CharVolume*dp, char *file)
{
	uint32 column, row, plane;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharVolume*dpm;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 3.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharVolumeClone(dp);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,dpm,pattern,npattern) private(np)
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++) {
				unsigned char newval;
				newval = EmptyPixel;
				for ( np=0; np<npattern; np++ ) {
					register int indexc = column + pattern[3*np];
					register int indexr = row + pattern[3*np+1];
					register int indexz = plane + pattern[3*np+2];
					if (  -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows && -1 < indexz && indexz < dp->planes ) {
						unsigned char Intensity = dp->voxels[indexc][indexr][indexz];
						if ( Intensity > newval )
							newval = Intensity;
					}
				}
				dpm->voxels[column][row][plane] = newval;
			}
		}
	}
	CharVolumeDelete(dp);
	return dpm;
}

void CharVolumeGErosionColorWithSE(CharVolume*ir, CharVolume*ig, CharVolume*ib, char *file, CharVolume**or, CharVolume**og, CharVolume**ob)
{
	uint32 column, row, plane, ncolumns, nrows, nplanes;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	int lnp;
	double lnorm;
	CharVolume*r, *g, *b;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 3.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	r = CharVolumeClone(ir);
	g = CharVolumeClone(ig);
	b = CharVolumeClone(ib);
	ncolumns = ir->columns;
	nrows = ir->rows;
	nplanes = ir->planes;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(ir,ig,ib,r,g,b,ncolumns,nrows,nplanes,pattern,npattern) private(np,lnp,lnorm)
	for ( column = 0; column < ncolumns; column++ ) {
		for ( row = 0; row < nrows; row++ ) {
			for ( plane = 0; plane < nplanes; plane++) {
				double score;
				unsigned char rnewval;
				unsigned char gnewval;
				unsigned char bnewval;
				score = npattern * FilledPixel * FilledPixel;
				for ( np = 0; np < npattern; np++ ) {
					register int indexc = column + pattern[ 3 * np ];
					register int indexr = row + pattern[ 3 * np + 1 ];
					register int indexz = plane + pattern[ 3 * np + 2 ];
					double score_k = 0;
					if ( -1 < indexc && indexc < ncolumns && -1 < indexr && indexr < nrows && -1 < indexz && indexz < nplanes ) {
						unsigned char rIntensity = ir->voxels[indexc][indexr][indexz];
						unsigned char gIntensity = ig->voxels[indexc][indexr][indexz];
						unsigned char bIntensity = ib->voxels[indexc][indexr][indexz];
						double norm = sqrt ( rIntensity * rIntensity + gIntensity * gIntensity + bIntensity * bIntensity );
						score_k = 0;
						for ( lnp = 0; lnp < npattern; lnp++ ) {
							register int indc = column + pattern[ 3 * lnp ];
							register int indr = row + pattern[ 3 * lnp + 1 ];
							register int indz = plane + pattern[ 3 * lnp + 2 ];
							if ( -1 < indc && indc < ncolumns && -1 < indr && indr < nrows && -1 < indz && indz < nplanes ) {
								double dot = 0;
								lnorm = sqrt ( ir->voxels[indc][indr][indz] * ir->voxels[indc][indr][indz] + ig->voxels[indc][indr][indz] * ig->voxels[indc][indr][indz] + ib->voxels[indc][indr][indz] * ib->voxels[indc][indr][indz] );
								dot = rIntensity * ir->voxels[indc][indr][indz] + gIntensity * ig->voxels[indc][indr][indz] + bIntensity * ib->voxels[indc][indr][indz];
								score_k += 1.0 / cos ( dot / norm / lnorm );
							}
						}
						if ( score_k < score ) {
							score = score_k;
							rnewval = rIntensity;
							gnewval = gIntensity;
							bnewval = bIntensity;
						}
					}
				}
				r->voxels[column][row][plane] = rnewval;
				g->voxels[column][row][plane] = gnewval;
				b->voxels[column][row][plane] = bnewval;
			}
		}
	}
	(*or) = r;
	(*og) = g;
	(*ob) = b;
}

CharVolume*CharVolumeReconstruct(CharVolume*mask, CharVolume*marker, int connectivity)
{
	int index, i, j, k;
	uint8 ***labels;
	CheapFifo *queue;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext6_min[9] = {0, -1, 0,
					0, 0, -1,
				-1, 0, 0};
	int next6_min = 9;
	int ext6_plus[9] = {0, 1, 0,
					0, 0, 1,
				1, 0, 0};
	int next6_plus = 9;
	int *ext;
	int next;
	int n;
	int *ext_min;
	int next_min;
	int *ext_plus;
	int next_plus;
	int kk;
/*	if ( connectivity != 6 )
		g_error("CharVolumeReconstruct connectivity %d not supported", connectivity);*/
	if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
		next_min = next6_min;
		ext_min = ext6_min;
		next_plus = next6_plus;
		ext_plus = ext6_plus;
	} else if ( connectivity == 8 ) {
		next = next6;
		ext = ext6;
		next_min = next6_min;
		ext_min = ext6_min;
		next_plus = next6_plus;
		ext_plus = ext6_plus;
	}
	for ( i = 0; i < marker->columns; i++ ) {
		for ( j = 0; j < marker->rows; j++ ) {
			for ( k = 0; k < marker->planes; k++ ) {
				unsigned char newval = marker->voxels[i][j][k];
				for ( n = 0; n < next_plus; n += 3) {
					int c, r, z;
					unsigned char curval;
					c = ext_plus[n] + i;
					r = ext_plus[n+1] + j;
					z = ext_plus[n + 2] + k;
/*					index1 = z * ( marker->rows * marker->columns ) + r * marker->columns + c;*/
					if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) && ( 0 <= z ) && ( z < marker->planes ) ) {
						curval = marker->voxels[c][r][z];
						if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) && newval < curval ) {
							newval = curval;
						}
					}
				}
				marker->voxels[i][j][k] = ( newval < mask->voxels[i][j][k] ) ? newval : mask->voxels[i][j][k];
			}
		}
	}
	labels = (uint8***)calloc(marker->columns, sizeof(uint8**));
	for ( i = 0; i < marker->columns; i++) {
		labels[i] = (uint8**)calloc(marker->rows, sizeof(uint8*));
		for ( j = 0; j < marker->rows; j++) {
			labels[i][j] = (uint8*)calloc(marker->planes, sizeof(uint8));
			for ( k = 0; k < marker->planes; k++) {
				labels[i][j][k] = 0;
			}
		}
	}
	queue = CheapFifoNew();
	for ( i = marker->columns - 1; i >= 0; i-- ) {
		for ( j = marker->rows - 1; j >= 0; j-- ) {
			for ( k = marker->planes - 1; k >= 0; k-- ) {
				unsigned char newval;
				index = k * ( marker->columns * marker->rows) + j * marker->columns + i;
				newval = marker->voxels[i][j][k];
				for ( n = 0; n < next_min; n += 3) {
					int c, r, z;
					unsigned char curval;
					c = ext_min[n] + i;
					r = ext_min[n+1] + j;
					z = ext_min[n+2] + k;
					if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) ) {
						curval = marker->voxels[c][r][z];
						if ( newval < curval ) {
							newval = curval;
						}
					}
				}
				newval = ( newval < mask->voxels[i][j][k] ) ? newval : mask->voxels[i][j][k];
				marker->voxels[i][j][k] = newval;
				for ( n = 0; n < next_min; n += 3) {
					int c, r, z;
					unsigned char curval;
					c = ext_min[n] + i;
					r = ext_min[n+1] + j;
					z = ext_min[n+2] + k;
					if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) ) {
						curval = marker->voxels[c][r][z];
						if ( /*&& labels[c][r] == 0*/ curval < newval && curval < mask->voxels[c][r][z] ) {
							labels[i][j][k] = 1;
							CheapFifoAdd(queue, index);
							break;
						}
					}
				}
			}
		}
	}
/*	return marker;
	CheapFifoAddFictitious(queue);*/
	kk = 0;
	while ( !CheapFifoEmpty(queue) ) {
		int index2;
/*fprintf(stdout,"\n %d size %d", kk, queue->size);
fflush(stdout);
kk++;*/
		index = CheapFifoRemove(queue);
		index2 = (int)( index % ( marker->columns * marker->rows) );
		i = (int)( index2 % marker->columns );
		j = (int)( (double)( index2 - i ) / (double)marker->columns );
		k = (int)( (double)(index - index2) / (double)( marker->columns * marker->rows ) );
/*fprintf(stdout," index %d ", index);*/
		labels[i][j][k] = 0;
		for ( n = 0; n < next; n += 3) {
			int c, r, z, index1;
			c = ext[n] + i;
			r = ext[n+1] + j;
			z = ext[n+2] + k;
			if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) && ( 0 <= z ) && ( z < marker->planes ) && labels[c][r][z] == 0 && marker->voxels[c][r][z] < marker->voxels[i][j][k] && mask->voxels[c][r][z] != marker->voxels[c][r][z] ) {
/*fprintf(stdout,"\n changed %d from %u", index1, marker->pixels[index1]);*/
				marker->voxels[c][r][z] = ( marker->voxels[i][j][k] < mask->voxels[c][r][z] ) ? marker->voxels[i][j][k] : mask->voxels[c][r][z];
				index1 = z * ( marker->columns * marker->rows ) + r * marker->columns + c;
				CheapFifoAdd(queue, index1);
/*fprintf(stdout,"to %u", marker->pixels[index1]);*/
				labels[c][r][z] = 1;
			}
		}
	}
	for ( i = 0; i < marker->columns; i++) {
		for ( j = 0; j < marker->rows; j++) {
			free(labels[i][j]);
		}
		free(labels[i]);
	}
	free(labels);
	CheapFifoDelete(queue);
	return marker;
}

CharVolume*CharVolumeCreate(char*ID)
{
	CharVolume*cv;
	cv = (CharVolume*)malloc(sizeof(CharVolume));
/*	if ( g_str_has_suffix(ID, "lsm") )
		grf_read_char_volume_lsm(ID, &cv->voxels, &cv->rows, &cv->columns, &cv->planes, 0);
	else*/
	grf_read_char_volume(ID, &cv->voxels, &cv->rows, &cv->columns, &cv->planes);
	return cv;
}

CharVolume*CharVolumeCreateEmpty(char*ID)
{
	CharVolume*cv;
	uint32 rows, columns, planes;
	grf_read_dim_volume(ID, &rows, &columns, &planes);
	cv = NewCharVolume(columns, rows, planes);
	return cv;
}

CharVolume*CharVolumeCreateChannel(char*ID, int channel)
{
	CharVolume*cv;
	cv = (CharVolume*)malloc(sizeof(CharVolume));
/*	if ( g_str_has_suffix(ID, "lsm") )*/
		grf_read_char_volume_lsm(ID, &cv->voxels, &cv->rows, &cv->columns, &cv->planes, channel);
/*	else
		grf_read_char_volume(ID, &cv->voxels, &cv->rows, &cv->columns, &cv->planes);*/
	return cv;
}


CharVolume*CharVolumeMaxChannel(char*ID, int nchannels, int *channels)
{
	CharVolume*cv;
	cv = (CharVolume*)malloc(sizeof(CharVolume));
/*	if ( g_str_has_suffix(ID, "lsm") )*/
		grf_read_max_char_volume_lsm(ID, &cv->voxels, &cv->rows, &cv->columns, &cv->planes, nchannels, channels);
/*	else
		grf_read_char_volume(ID, &cv->voxels, &cv->rows, &cv->columns, &cv->planes);*/
	return cv;
}


/*
CharImage*CharVolumeReadPlane(char*ID, int plane)
{
	int i, j;
	CharImage*ci;
	uint32 columns, rows, planes;
	grf_read_dim_volume(ID, &rows, &columns, &planes);
	ci = CharImageNew(columns, rows);
	for ( i = 0; i < cv->columns; i++) {
		for ( j = 0; j < cv->rows; j++) {
			ci->pixels[ j * ci->columns + i]= cv->voxels[i][j][plane];
		}
	}
	return ci;
}


CharImage*CharVolumeReadRow(char*ID, int row)
{
	int i, j;
	CharImage*ci;
	ci = CharImageNew(cv->columns, cv->planes);
	for ( i = 0; i < cv->columns; i++) {
		for ( j = 0; j < cv->planes; j++) {
			ci->pixels[ j * ci->columns + i]= cv->voxels[i][row][j];
		}
	}
	return ci;
}

CharImage*CharVolumeReadColumn(char*ID, int column)
{
	int i, j;
	CharImage*ci;
	ci = CharImageNew( cv->planes, cv->rows);
	for ( i = 0; i < cv->rows; i++) {
		for ( j = 0; j < cv->planes; j++) {
			ci->pixels[ i * ci->columns + j]= cv->voxels[column][i][j];
		}
	}
	return ci;
}
*/

CharVolume*CharVolumeCHolSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype)
{
	char *file;
	file = pr_get_name_temp();
	StructElem3d( file, vsize, hsize, zsize, vhtype, ztype);
	dp = CharVolumeCHolWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharVolume*CharVolumeCHolWithSE(CharVolume*dp, char *file)
{
	uint32 column, row, plane;
	unsigned char Intensity;
	CharVolume*dpm;
	int cond;
	dpm = CharVolumeCopy(dp);
	dp = CharVolumeInvert(dp);
	row = 0;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,row) private(Intensity)
	for ( plane = 0; plane < dp->planes; plane++) {
		for ( column = 0; column < dp->columns; column++ ) {
			Intensity = dp->voxels[column][row][plane];
			if ( Intensity != EmptyPixel ) {
				PList3d*list;
				int SegmentLength;
				list = IntensitySegment3dModified(dp, Intensity, row, column, plane, 6, dp->columns * dp->rows * dp->planes, &SegmentLength, 0, EmptyPixel);
				PList3dFree(list);
			}
		}
	}
	row = dp->rows - 1;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,row) private(Intensity)
	for ( plane = 0; plane < dp->planes; plane++) {
		for ( column = 0; column < dp->columns; column++ ) {
			Intensity = dp->voxels[column][row][plane];
			if ( Intensity != EmptyPixel ) {
				PList3d*list;
				int SegmentLength;
				list = IntensitySegment3dModified(dp, Intensity, row, column, plane, 6, dp->columns * dp->rows * dp->planes, &SegmentLength, 0, EmptyPixel);
				PList3dFree(list);
			}
		}
	}
	column = 0;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,column) private(Intensity)
	for ( plane = 0; plane < dp->planes; plane++) {
		for ( row = 0; row < dp->rows; row++ ) {
			Intensity = dp->voxels[column][row][plane];
			if ( Intensity != EmptyPixel ) {
				PList3d*list;
				int SegmentLength;
				list = IntensitySegment3dModified(dp, Intensity, row, column, plane, 6, dp->columns * dp->rows * dp->planes, &SegmentLength, 0, EmptyPixel);
				PList3dFree(list);
			}
		}
	}
	column = dp->columns - 1;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,column) private(Intensity)
	for ( plane = 0; plane < dp->planes; plane++) {
		for ( row = 0; row < dp->rows; row++ ) {
			Intensity = dp->voxels[column][row][plane];
			if ( Intensity != EmptyPixel ) {
				PList3d*list;
				int SegmentLength;
				list = IntensitySegment3dModified(dp, Intensity, row, column, plane, 6, dp->columns * dp->rows * dp->planes, &SegmentLength, 0, EmptyPixel);
				PList3dFree(list);
			}
		}
	}
	plane = 0;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,plane) private(Intensity)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			Intensity = dp->voxels[column][row][plane];
			if ( Intensity != EmptyPixel ) {
				PList3d*list;
				int SegmentLength;
				list = IntensitySegment3dModified(dp, Intensity, row, column, plane, 6, dp->columns * dp->rows * dp->planes, &SegmentLength, 0, EmptyPixel);
				PList3dFree(list);
			}
		}
	}
	plane = dp->planes - 1;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,plane) private(Intensity)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			Intensity = dp->voxels[column][row][plane];
			if ( Intensity != EmptyPixel ) {
				PList3d*list;
				int SegmentLength;
				list = IntensitySegment3dModified(dp, Intensity, row, column, plane, 6, dp->columns * dp->rows * dp->planes, &SegmentLength, 0, EmptyPixel);
				PList3dFree(list);
			}
		}
	}
	cond = 1;
	while ( cond ) {
		dp = CharVolumeConditionalGDilationWithSE(dp, file, dpm, &cond);
	}
	CharVolumeDelete(dpm);
	return dp;
}

CharVolume*CharVolumeConditionalGDilationWithSE(CharVolume*dp, char *file, CharVolume*ci, int*cond)
{
	uint32 column, row, plane;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharVolume*dpm;
	FILE*fp;
	int lcond = 0;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 3.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharVolumeCopy(dp);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,ci,pattern,dpm,npattern) reduction(||:lcond) private(np) //private(column,row,np)
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			for ( plane = 0; plane < dp->planes; plane++) {
/*if (row==0 && plane == 0 ) printf("Нить %d of %d выполнила итерацию %d\n", omp_get_thread_num(), omp_get_num_threads(), column);*/
				if ( ci->voxels[column][row][plane] != EmptyPixel ) {
					unsigned char newval;
					unsigned char oldval = dpm->voxels[column][row][plane];
					newval = EmptyPixel;
					for ( np=0; np<npattern; np++ ) {
						register int indexc = column + pattern[3*np];
						register int indexr = row + pattern[3*np+1];
						register int indexz = plane + pattern[3*np+2];
						if (  -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows && -1 < indexz && indexz < dp->planes ) {
							unsigned char Intensity = dp->voxels[indexc][indexr][indexz];
							if ( Intensity > newval )
								newval = Intensity;
						}
					}
					if ( oldval != newval ) {
						dpm->voxels[column][row][plane] = newval;
						lcond = 1;
					}
				}
			}
		}
	}
	free(pattern);
	CharVolumeDelete(dp);
	*cond = lcond;
	return dpm;
}

CharVolume*CharVolumeBPLHeqWithSE(CharVolume*dp, char *file)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageLHeqWithSE(ci, file);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPGErosionWithSE(CharVolume*dp, char *file)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageGErosionWithSE(ci, file);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPGDilationWithSE(CharVolume*dp, char *file)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageGDilationWithSE(ci, file);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPGCloseWithSE(CharVolume*dp, char *file, int repetitions)
{
	uint32 plane;
	CharImage*ci;
	int i;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		for	( i = 0; i < repetitions; i++ ) {
			ci = CharImageGDilationWithSE(ci, file);
			ci = CharImageGErosionWithSE(ci, file);
		}
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPGOpenWithSE(CharVolume*dp, char *file, int repetitions)
{
	uint32 plane;
	CharImage*ci;
	int i;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		for	( i = 0; i < repetitions; i++ ) {
			ci = CharImageGErosionWithSE(ci, file);
			ci = CharImageGDilationWithSE(ci, file);
		}
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPAnisoDiff(CharVolume*dp, int iterations, double lambda, double threshold, char*func)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageAnisoDiff(ci, iterations, lambda, threshold, func);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;

}

CharVolume*CharVolumeBPCHolWithSE(CharVolume*dp, char *file)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageCHolWithSE(ci, file);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPHeqM(CharVolume*dp, CharVolume*mask)
{
	uint32 plane;
	CharImage*ci, *mi;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		mi = CharVolumeGetPlane(mask, plane);
		ci = CharImageHistogramEquilizeWithMask(ci, mi);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
		CharImageDelete(mi);
	}
	return dp;
}

CharVolume*CharVolumeBPHeq(CharVolume*dp)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageHistogramEquilize(ci, 0, 0, ci->rows, ci->columns);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPNormalize(CharVolume*dp)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageHistogramNormalize(ci);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPCrimminsDespekle(CharVolume*dp, int repetitions)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		CharImageCrimminsDespekle(ci, repetitions);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPReconstruct(CharVolume*dp, CharVolume*mask, int connectivity)
{
	uint32 plane;
	CharImage*ci, *mi;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		mi = CharVolumeGetPlane(mask, plane);
		mi = CharImageReconstruct(ci, mi, connectivity);
		CharVolumeSetPlane(dp, mi, plane);
		CharImageDelete(ci);
		CharImageDelete(mi);
	}
	return dp;
}

CharVolume*CharVolumeBPShenCastan92(CharVolume*dp, double a1, double a2, double low, double high, uint32 window, uint32 segment, int connectivity)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageEdgeShenCastan92(ci, a1, a2, low, high, window, segment, connectivity);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPLHBGWithSE(CharVolume*dp, char *file)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageLHBGWithSE(ci, file);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

CharVolume*CharVolumeBPShapeSelect(CharVolume*dp, ShapeDescriptor*sd, double eps, CheckShapeType cstype, int connectivity, int MaxSegment, int MinSegment)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		ci = CharImageShapeSelect(ci, sd, eps, cstype, connectivity, MaxSegment, MinSegment);
		CharVolumeSetPlane(dp, ci, plane);
		CharImageDelete(ci);
	}
	return dp;
}

void CharVolumeGWaterShed(CharVolume*di, int connectivity)
{
	int ***labels;
	unsigned char ***distances;
	unsigned char IntensityLevel;
	int current_dist;
	int index1, index2;
	uint32 i, j, k, size;
	int current_label;
	unsigned char HMIN, HMAX;
	GQueue *queue;
	Element *elem;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("NewCharVoxelStructure connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
/* Instead of cvs */
	HMIN = FilledPixel;
	HMAX = EmptyPixel;
	size = di->planes * di->columns * di->rows;
	labels = (int***)calloc(di->columns, sizeof(int**));
	distances = (unsigned char***)calloc(di->columns, sizeof(unsigned char**));
	for ( i = 0; i < di->columns; i++) {
		labels[i] = (int**)calloc(di->rows, sizeof(int*));
		distances[i] = (unsigned char**)calloc(di->rows, sizeof(unsigned char*));
		for ( j = 0; j < di->rows; j++) {
			labels[i][j] = (int*)calloc(di->planes, sizeof(int));
			distances[i][j] = (unsigned char*)calloc(di->planes, sizeof(unsigned char));
			for ( k = 0; k < di->planes; k++) {
				labels[i][j][k] = INIT;
				distances[i][j][k] = 0;
				IntensityLevel = di->voxels[i][j][k];
				if ( IntensityLevel > HMAX )
					HMAX = IntensityLevel;
				if ( IntensityLevel < HMIN )
					HMIN = IntensityLevel;
			}
		}
	}
/**/
	sv = di;
	sv = (CharVolume*)NULL;
	current_label = 0;
	current_dist = 0;
	queue = g_queue_new();
	for ( IntensityLevel = HMIN; IntensityLevel <= HMAX; IntensityLevel++ ) {
/*fprintf(stdout,"\n level: %d label: %d", (int)IntensityLevel, current_label);
fflush(stdout);*/
		for ( i = 0; i < di->columns; i++) {
			for ( j = 0; j < di->rows; j++) {
				for ( k = 0; k < di->planes; k++) {
					if ( di->voxels[i][j][k] == IntensityLevel ) {
						labels[i][j][k] = MASK;
						for ( n = 0; n < next; n += 3) {
							int c, r, p;
							c = ext[n] + i;
							r = ext[n+1] + j;
							p = ext[n+2] + k;
							if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
								if ( labels[c][r][p] >= WSHED ) {
									distances[i][j][k] = 1;
									elem = elemenet_new(i, j, k);
									g_queue_push_head(queue, elem);
								}
							}
						}
					}
				}
			}
		}
/* end for */
		current_dist = 1;
		elem = elemenet_new(FICTITIOUS, FICTITIOUS, FICTITIOUS);
		g_queue_push_head(queue, elem);
		while (1) {
			int flag  = 0;
			elem = g_queue_pop_tail(queue);
/*fprintf(stdout,"\n level: %d dist: %d pix: %d", IntensityLevel, (int)current_dist, pix);
fflush(stdout);*/
			if ( elem->row == FICTITIOUS ) {
				if ( g_queue_is_empty(queue) ) {
					break;
				} else {
					elem = elemenet_new(FICTITIOUS, FICTITIOUS, FICTITIOUS);
					g_queue_push_head(queue, elem);
					current_dist++;
					elem = g_queue_pop_tail(queue);
				}
			}
			i = elem->column;
			j = elem->row;
			k = elem->plane;
			element_free(elem);
			for ( n = 0; n < next; n += 3) {
				int c, r, p;
				c = ext[n] + i;
				r = ext[n+1] + j;
				p = ext[n+2] + k;
				if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
					if ( distances[c][r][p] <= current_dist && labels[c][r][p] >= WSHED ) {
						if ( labels[c][r][p] > WSHED ) {
							if ( labels[i][j][k] == MASK || labels[i][j][k] == WSHED ) {
								if ( !flag ) labels[i][j][k] = labels[c][r][p];
							} else if ( labels[i][j][k] != labels[c][r][p] ) {
								labels[i][j][k] = WSHED;
								flag = 1;
							}
						} else if ( labels[i][j][k] == MASK ) {
							labels[i][j][k] = WSHED;
						}
					} else if ( labels[c][r][p] == MASK && distances[c][r][p] == 0 ) {
						distances[c][r][p] = current_dist + 1;
						elem = elemenet_new(p, r, c);
						g_queue_push_head(queue, elem);
					}
				}
			}
		}/* end while */
		for ( i = 0; i < di->columns; i++) {
			for ( j = 0; j < di->rows; j++) {
				for ( k = 0; k < di->planes; k++) {
					if ( di->voxels[i][j][k] == IntensityLevel ) {
						distances[i][j][k] = 0;
						if ( labels[i][j][k] == MASK ) {
							current_label++;
							labels[i][j][k] = current_label;
							elem = elemenet_new(i, j, k);
							g_queue_push_head(queue, elem);
							while ( !g_queue_is_empty(queue) ) {
								elem = g_queue_pop_tail(queue);
								i = elem->column;
								j = elem->row;
								k = elem->plane;
								element_free(elem);
								for ( n = 0; n < next; n += 3) {
									int c, r, p;
									c = ext[n] + i;
									r = ext[n+1] + j;
									p = ext[n+2] + k;
									if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
										if ( labels[c][r][p] == MASK ) {
											elem = elemenet_new(c, r, p);
											g_queue_push_head(queue, elem);
											labels[c][r][p] = current_label;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}/* end main for */
	for ( i = 0; i < di->columns; i++) {
		for ( j = 0; j < di->rows; j++) {
			for ( k = 0; k < di->planes; k++) {
				int AllNeighWSHED = 1;
				for ( n = 0; n < next; n += 3) {
					int c, r, p;
					c = ext[n] + i;
					r = ext[n+1] + j;
					p = ext[n+2] + k;
					if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) && ( 0 <= p ) && ( p < di->planes ) ) {
						if ( labels[c][r][p] != WSHED ) {
							AllNeighWSHED = 0;
							break;
						}
					}
				}
				di->voxels[i][j][k] = ( labels[i][j][k] == WSHED && !AllNeighWSHED ) ? EmptyPixel:FilledPixel;
			}
		}
	}
	g_queue_foreach(queue, (GFunc)element_free, NULL);
	g_queue_free(queue);
	for ( i = 0; i < di->columns; i++) {
		for ( j = 0; j < di->rows; j++) {
			free(labels[i][j]);
			free(distances[i][j]);
		}
		free(labels[i]);
		free(distances[i]);
	}
	free(labels);
	free(distances);
}

double CharVolumeThresholdHist1(CharVolume*dp)
{
	int index, k;
	double *p;
	double omega;
	double mu0, mu1;
	double criterion;
	double dummy;
	double denom;
	double threshold;
	uint32 MaxNumber;
	int histogram[256];
	uint32 i;
	uint32 j;
	uint32 kk;
	MaxNumber = dp->columns * dp->rows * dp->planes;
	for ( index = 0; index < 256; index++) {
		histogram[index] = 0;
	}
	for ( i = 0; i < dp->columns; i++ )
		 for ( i = 0; i < dp->rows; i++ )
			for ( i = 0; i < dp->planes; i++ ) {
				histogram[dp->voxels[i][j][k]]++;
	}
	p = (double*)calloc(256,sizeof(double));
	for ( index = 0; index < 256; index++) {
		p[index] = (double)histogram[index] / (double)MaxNumber;
	}
	threshold = computeOtsu(p, 256);
	free(p);
	return threshold;
}

void CharVolumeThreshold(CharVolume*dp, double threshold)
{
	uint32 i;
	uint32 j;
	uint32 k;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(dp,threshold)
	for ( i = 0; i < dp->columns; i++ )
		 for ( j = 0; j < dp->rows; j++ )
			for ( k = 0; k < dp->planes; k++ ) {
				if ( dp->voxels[i][j][k] < (unsigned char)threshold )
					dp->voxels[i][j][k] = EmptyPixel;
				else
					dp->voxels[i][j][k] = FilledPixel;
			}
}

int qwd (const void*x, const void*y)
{
	int **z1, **z2, x1, x2, y1, y2;
	z1 = (int**)x;
	x1 = z1[0][0];
	y1 = z1[0][1];
	z2 = (int**)y;
	x2 = z2[0][0];
	y2 = z2[0][1];
/*	fprintf(stderr,"%d %d %d %d\n", x1, x2, y1, y2);*/
	if ( x1 < x2 )
		return -1;
	else if ( x1 > x2 )
		return 1;
	return 0;
}

CharVolume*CharVolumeUnbent(CharVolume*mask, int xoffset, int yoffset)
{
	int i, ui;
	int j, uj;
	int k, ii;
	CharVolume*dp;
	int n, xn, yn, done, m, done_through, **z;
	dp = CharVolumeClone(mask);
	ui = (int)((double)dp->columns / 2.0 );
	z = (int**)calloc (dp->columns, sizeof(int*));
	for ( i = 0; i < dp->columns; i++ ) {
		z[i] = (int*)calloc (2, sizeof(int*));
	}
	for ( j = 0; j < dp->rows; j++ ) {
		done_through = 0;
		m = 0;
		while ( m < dp->planes && !done_through) {
			n = 0;
			done = 0;
			for ( k = dp->planes; k >= 0 && done == 0; k-- ) {
				xn = 0;
				yn = 0;
				for ( i = 0; i < dp->columns && ( xn == 0 || yn == 0 ); i++ ) {
					if ( ( k < dp->planes - yoffset || i < xoffset ) && xn == 0 && mask->voxels[i][j][k] == (unsigned char)FilledPixel ) {
						z[n][0] = i;
						z[n][1] = k;
						n++;
						mask->voxels[i][j][k] = EmptyPixel;
						if ( k > 0 && mask->voxels[i][j][k - 1] != EmptyPixel )
							xn = 1;
					}
					ii = dp->columns - 1 - i;
					if ( ( k < dp->planes - yoffset || ii > xoffset ) && yn == 0 && mask->voxels[ii][j][k] == (unsigned char)FilledPixel ) {
						z[n][0] = ii;
						z[n][1] = k;
						n++;
						mask->voxels[ii][j][k] = EmptyPixel;
						if ( k > 0 && mask->voxels[ii][j][k - 1] != EmptyPixel )
							yn = 1;
					}
				}
				if ( ( xn == 0 && yn == 0 ) || n >= dp->columns )
					done = 1;
			}
			qsort((void**)z, n, sizeof(int*), qwd);
			uj = -1;
			k = dp->planes;
			for ( i = 0; i < n; i++) {
				if ( z[i][0] == xoffset ) { /*&& z[i][1] < k ) {*/
					k = z[i][1];
					uj = i;
				}
			}
			if ( uj == -1 ) {
				g_warning("no peak at row %d and column %d from %d points", j, xoffset, n);
				n = 0;
			} else {
				g_warning("found peak on %d row at %d/%d point with %d plane", j, uj, n, k);
			}
			for ( i = 0; i < n; i++) {
				k = i + xoffset - uj;
				if ( k < 0 )
					g_error("negative index %d %d %d %d %d", j, k, i, ui, uj);
				if ( k >= dp->columns )
					g_error("exceeded %d %d %d %d %d", j, k, i, ui, uj);
				dp->voxels[k][j][m] = FilledPixel;
				fprintf (stdout,"%i,%i,%i %i,%i,%i\n", z[i][0], j, z[i][1], k, j, m);
			}
//			if ( m == 0 )fprintf (stdout,"row = %d -- %d\n", j, n);
			m++;
			if ( n == 0 )
				done_through = 1;
		}
	}
	for ( i = 0; i < dp->columns; i++ )
		free (z[i]);
	free (z);
	return dp;
}

CharVolume*CharVolumeCUnbent(CharVolume*mask, char*log)
{
	int i, ui;
	int j, uj;
	int k, uk;
	CharVolume*dp;
	char*record, *d;
	FILE*fp;
	fp = fopen(log, "r");
	if ( !fp )
		g_error("Can't open %s for reading", log);
	dp = CharVolumeClone(mask);
	record = (char*)calloc (MAX_RECORD, sizeof(char));
	d = record;
	while ( ( record = fgets (record, MAX_RECORD, fp) ) ) {
		sscanf(record, "%d,%d,%d %d,%d,%d", &i, &j, &k, &ui, &uj, &uk);
		dp->voxels[ui][uj][uk] = mask->voxels[i][j][k];
	}
//	free (d);
	return dp;
}

CharVolume*CharVolumeRestack(CharVolume*ci, int step)
{
	uint32 column, row, plane;
	uint32 skip;
	uint32 rcolumns, rrows, rplanes, rplane;
	CharVolume*resized;
	rcolumns = ci->columns;
	rrows = ci->rows;
	skip = (uint32)step;
	rplanes = 1;
	for ( plane = skip; plane < ci->planes - 1; plane += skip )
		rplanes++;
	rplanes++;
	resized = NewCharVolume( rcolumns, rrows, rplanes );
	plane = 0;
	rplane = 0;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(ci,resized,rrows,rcolumns,plane)
	for ( row = 0; row < rrows; row++ ) {
		for ( column = 0; column < rcolumns; column++ ) {
			resized->voxels[column][row][plane] = ci->voxels[column][row][plane];
		}
	}
	rplane++;
	for ( plane = skip; plane < ci->planes - 1; plane += skip ) {
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(ci,resized,rrows,rcolumns,plane,rplane)
		for ( row = 0; row < rrows; row++ ) {
			for ( column = 0; column < rcolumns; column++ ) {
				resized->voxels[column][row][rplane] = ci->voxels[column][row][plane];
			}
		}
		rplane++;
	}
	plane = rplanes - 1;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(ci,resized,rrows,rcolumns,plane,rplane)
	for ( row = 0; row < rrows; row++ ) {
		for ( column = 0; column < rcolumns; column++ ) {
			resized->voxels[column][row][rplane] = ci->voxels[column][row][ci->planes - 1];
		}
	}
	rplane++;
	return resized;
}

#ifndef G_OS_WIN32
#include <gts.h>

void gts_func(gdouble **gts_a, GtsCartesianGrid gts_grid, guint i, gpointer data)
{
	uint32 column, row, plane;
	int k, j;
	CharVolume*cv = (CharVolume*)data;
	plane = (uint32)i;
	for ( k = 0, row = (uint32)gts_grid.y; k < (uint32)gts_grid.ny && row < cv->rows; k++, row += (uint32)gts_grid.dy ) {
		for ( j = 0, column = (uint32)gts_grid.x; j < (uint32)gts_grid.nx && column < cv->columns; j++, column += (uint32)gts_grid.dx ) {
			if ( cv->voxels[column][row][plane] == FilledPixel ) {
				gts_a[j][k] = (gdouble)FilledPixel;
			} else {
				gts_a[j][k] = (gdouble)EmptyPixel;
			}
		}
	}
}

gint gts_vertex_scale_func(gpointer item, gpointer data)
{
	GtsVertex*p = (GtsVertex*)item;
	GtsMatrix*m = (GtsMatrix*)data;
	gts_point_transform(GTS_POINT(p), m);
	return 0;
}

void gts_surface_scale(GtsSurface *surface, double scale_column, double scale_row, double scale_plane)
{
	GtsMatrix*scale_matrix;
	GtsVector scale_vector;
	scale_vector[0] = scale_column;
	scale_vector[1] = scale_row;
	scale_vector[2] = scale_plane;
	scale_matrix = gts_matrix_scale(NULL, scale_vector);
	gts_surface_foreach_vertex(surface, (GtsFunc) gts_vertex_scale_func, (gpointer) scale_matrix);
	gts_matrix_destroy(scale_matrix);
}

void CharVolumeIsosurface(CharVolume*cv, double max_cost, gdouble minangle, char*output_file, char*format, double scale_column, double scale_row, double scale_plane)
{
	GtsCartesianGrid gts_grid;
	FILE *fptr;
	GtsSurface *gts_surface;
	gdouble gts_iso;
	gdouble stop_data;
/* gts grid */
	gts_grid.nx = cv->columns;
	gts_grid.ny = cv->rows;
	gts_grid.nz = cv->planes;
	gts_grid.x = 0.0;
	gts_grid.y = 0.0;
	gts_grid.z = 0.0;
	gts_grid.dx = 1.0;
	gts_grid.dy = 1.0;
	gts_grid.dz = 1.0;
/* gts surface */
	gts_surface = gts_surface_new(gts_surface_class(), gts_face_class(), gts_edge_class(), gts_vertex_class());
/* triangulate mask */
	gts_iso = (gdouble)FilledPixel;
	gts_isosurface_cartesian(gts_surface, gts_grid, (GtsIsoCartesianFunc) gts_func, (gpointer) cv, gts_iso);
/* reduce the number of triangles */
	fprintf(stderr, "Orig vertex number: %d, edge number: %d\n", gts_surface_vertex_number(gts_surface), gts_surface_edge_number(gts_surface) );
	stop_data = max_cost;
	gts_surface_coarsen (gts_surface, NULL, NULL, NULL, NULL, (GtsStopFunc)gts_coarsen_stop_cost, (gpointer)(&stop_data), minangle);
	fprintf(stderr, "Opti vertex number: %d, edge number: %d\n", gts_surface_vertex_number(gts_surface), gts_surface_edge_number(gts_surface) );
	gts_surface_scale(gts_surface, scale_column, scale_row, scale_plane);
	fptr = fopen(output_file, "w");
	if ( !strcmp(format, "oogl") ) {
		gts_surface_write_oogl (gts_surface, fptr);
	} else if ( !strcmp(format, "vtk") ) {
		gts_surface_write_vtk (gts_surface, fptr);
	} else if ( !strcmp(format, "ooglb") ) {
		gts_surface_write_oogl_boundary (gts_surface, fptr);
	} else if ( !strcmp(format, "gts") ) {
		gts_surface_write (gts_surface, fptr);
	} else {
		fclose(fptr);
		g_error("Wrong format: %s", format);
	}
	fclose(fptr);
	gts_object_destroy((GtsObject*)gts_surface);
}

gdouble gts_key_func_cost (gpointer item, gpointer data)
{
	GtsEdge*e = (GtsEdge*)item;
	GtsSegment*pp = GTS_SEGMENT(e);
	GtsPoint*p1 = GTS_POINT(pp->v1);
	GtsPoint*p2 = GTS_POINT(pp->v2);
	gdouble T = gts_point_distance (p1, p2);
	return T;
}

void CharVolumeIsosurfaceFull(CharVolume*cv, double max_cost, gdouble minangle, char*output_file, char*format, double scale_column, double scale_row, double scale_plane, double xx, double yy, double zz, char*hint)
{
	GtsCartesianGrid gts_grid;
	FILE *fptr;
	GtsSurface *gts_surface;
	gdouble gts_iso;
	gdouble stop_data;
/* gts grid */
	gts_grid.x = 0.0;
	gts_grid.y = 0.0;
	gts_grid.z = 0.0;
	gts_grid.dx = floor( xx * (double)cv->columns / 100.0 + 0.5 );
	gts_grid.dy = floor( yy * (double)cv->rows / 100.0 + 0.5 );
	gts_grid.dz = floor( zz * (double)cv->planes / 100.0 + 0.5 );
	gts_grid.nx = (guint)floor( (double)cv->columns / gts_grid.dx + 0.5 );
	gts_grid.ny = (guint)floor( (double)cv->rows / gts_grid.dy + 0.5 );
	gts_grid.nz = (guint)floor( (double)cv->planes / gts_grid.dz + 0.5 );
/* gts surface */
	gts_surface = gts_surface_new(gts_surface_class(), gts_face_class(), gts_edge_class(), gts_vertex_class());
/* triangulate mask */
	gts_iso = (gdouble)FilledPixel;
	if ( !strcmp(hint, "cartesian") ) {
		gts_isosurface_cartesian(gts_surface, gts_grid, (GtsIsoCartesianFunc) gts_func, (gpointer) cv, gts_iso);
	} else if ( !strcmp(hint, "tetra") ) {
		gts_isosurface_tetra(gts_surface, gts_grid, (GtsIsoCartesianFunc) gts_func, (gpointer) cv, gts_iso);
	} else if ( !strcmp(hint, "tetra_bounded") ) {
		gts_isosurface_tetra_bounded(gts_surface, gts_grid, (GtsIsoCartesianFunc) gts_func, (gpointer) cv, gts_iso);
	} else if ( !strcmp(hint, "tetra_bcl") ) {
		gts_isosurface_tetra_bcl(gts_surface, gts_grid, (GtsIsoCartesianFunc) gts_func, (gpointer) cv, gts_iso);
	}
/* reduce the number of triangles */
	fprintf(stderr, "Orig vertex number: %d, edge number: %d\n", gts_surface_vertex_number(gts_surface), gts_surface_edge_number(gts_surface) );
	stop_data = max_cost;
	gts_surface_coarsen (gts_surface, (GtsKeyFunc)gts_key_func_cost, NULL, (GtsCoarsenFunc)gts_segment_midvertex, NULL, (GtsStopFunc)gts_coarsen_stop_cost, (gpointer)(&stop_data), minangle);
	fprintf(stderr, "Opti vertex number: %d, edge number: %d\n", gts_surface_vertex_number(gts_surface), gts_surface_edge_number(gts_surface) );
	gts_surface_scale(gts_surface, scale_column, scale_row, scale_plane);
	fptr = fopen(output_file, "w");
	if ( !strcmp(format, "oogl") ) {
		gts_surface_write_oogl (gts_surface, fptr);
	} else if ( !strcmp(format, "vtk") ) {
		gts_surface_write_vtk (gts_surface, fptr);
	} else if ( !strcmp(format, "ooglb") ) {
		gts_surface_write_oogl_boundary (gts_surface, fptr);
	} else if ( !strcmp(format, "gts") ) {
		gts_surface_write (gts_surface, fptr);
	} else {
		fclose(fptr);
		g_error("Wrong format: %s", format);
	}
	fclose(fptr);
	gts_object_destroy((GtsObject*)gts_surface);
}


gint gts_grid_func(gpointer item, gpointer data)
{
	GtsVertex*p = (GtsVertex*)item;
	GtsCartesianGrid*m = (GtsCartesianGrid*)data;
	GtsPoint*pp = GTS_POINT(p);
	if ( pp->x > m->nx ) {
		m->nx = pp->x;
	}
	if ( pp->y > m->ny ) {
		m->ny = pp->y;
	}
	if ( pp->z > m->nz ) {
		m->nz = pp->z;
	}
	return 0;
}

gint gts_volume_func(gpointer item, gpointer data)
{
	GtsEdge*e = (GtsEdge*)item;
	CharVolume*cv = (CharVolume*)data;
	GtsSegment*pp = GTS_SEGMENT(e);
	GtsPoint*p1 = GTS_POINT(pp->v1);
	GtsPoint*p2 = GTS_POINT(pp->v2);
	double x1, y1, z1, x2, y2, z2, ax, bx, ay, by, az, bz, t, T, dt, x, y, z;
	int i, n, c, r, p;
	unsigned char Intensity = FilledPixel;
	T = gts_point_distance (p1, p2);
	if ( T < 1e-16 ) {
		return 0;
	}
	x1 = p1->x;
	y1 = p1->y;
	z1 = p1->z;
	x2 = p2->x;
	y2 = p2->y;
	z2 = p2->z;
	bx = x1;
	by = y1;
	bz = z1;
	ax = ( x2 - bx ) / T;
	ay = ( y2 - by ) / T;
	az = ( z2 - bz ) / T;
	n = (int) ( 2.0 * T + 0.5 );
	dt = T / (double)n;
	for ( i = 0, t = 0; i < n; i++, t += dt ) {
		x = ax * t + bx;
		c = (int)( x + 0.5 );
		y = ay * t + by;
		r = (int)( y + 0.5 );
		z = az * t + bz;
		p = (int)( z + 0.5 );
		if ( 0 <= c && c < cv->columns && 0 <= r && r < cv->rows && 0 <= p && p < cv->planes ) {
			cv->voxels[c][r][p] = Intensity;
		}
	}
	x = ax * T + bx;
	c = (int)( x + 0.5 );
	y = ay * T + by;
	r = (int)( y + 0.5 );
	z = az * T + bz;
	p = (int)( z + 0.5 );
	if ( 0 <= c && c < cv->columns && 0 <= r && r < cv->rows && 0 <= p && p < cv->planes ) {
		cv->voxels[c][r][p] = Intensity;
	}
	return 0;
}

CharVolume*Isosurface2CharVolume(char*input_file)
{
	CharVolume*cv;
	GtsCartesianGrid gts_grid;
	guint read_res;
	GtsSurface *gts_surface;
	GtsFile *gts_file;
	FILE*fp;
	fp = fopen(input_file, "r");
	if ( !fp ) {
		return NULL;
	}
	gts_file = gts_file_new(fp);
	gts_surface = gts_surface_new(gts_surface_class(), gts_face_class(), gts_edge_class(), gts_vertex_class());
	read_res = gts_surface_read (gts_surface, gts_file);
	if ( read_res != 0 ) {
		fclose(fp);
		gts_file_destroy(gts_file);
		gts_object_destroy((GtsObject*)gts_surface);
		return NULL;
	}
/* gts grid */
	gts_grid.nx = 0.0;
	gts_grid.ny = 0.0;
	gts_grid.nz = 0.0;
	gts_grid.x = 0.0;
	gts_grid.y = 0.0;
	gts_grid.z = 0.0;
	gts_grid.dx = 1.0;
	gts_grid.dy = 1.0;
	gts_grid.dz = 1.0;
	gts_surface_foreach_vertex (gts_surface, (GtsFunc) gts_grid_func, (gpointer) (&gts_grid));
	cv = NewCharVolume((uint32) ( gts_grid.nx + 1 ), (uint32) ( gts_grid.ny + 1 ), (uint32) ( gts_grid.nz + 1 ));
	gts_surface_foreach_edge (gts_surface, (GtsFunc) gts_volume_func, (gpointer) cv);
	gts_object_destroy((GtsObject*)gts_surface);
	return cv;
}

#endif

CharVolume*CharVolumePump(CharImage*distmap, CharImage*ci)
{
	CharVolume*cv;
	unsigned char max_intensity;
	int i, j, k;
	max_intensity = CharImageMaxLevel(distmap);
	cv = NewCharVolume((uint32) distmap->columns, (uint32) distmap->rows, (uint32) max_intensity + 1 );
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(ci,cv,distmap) private(k)
	for ( i = 0; i < distmap->columns; i++ ) {
		for ( j = 0; j < distmap->rows; j++ ) {
			k = (int)distmap->pixels[ j * distmap->columns + i ];
			if ( k > 0 ) {
				if ( ci != NULL ) {
					cv->voxels[i][j][k] = ci->pixels[ j * ci->columns + i ];
				} else {
					cv->voxels[i][j][k] = FilledPixel;
				}
			}
		}
	}
	return cv;
}

CharVolume*CharVolumeMultiSlice(CharVolume*distmap, ShapeDescriptor3d*sd, double ns, int direction)
{
	CharVolume*cv;
	int intensity, *levels;
	int i, j, k, nlevels, n;
	int step;
	cv = CharVolumeClone(distmap);
	switch ( direction ) {
		case 1:
			step = (int)( (double)ns * (double)( sd->i_max - sd->i_min ) / 100.0 + 0.5);
			break;
		case 2:
			step = (int)( (double)ns * (double)( sd->j_max - sd->j_min ) / 100.0 + 0.5);
			break;
		case 3:
			step = (int)( (double)ns * (double)( sd->k_max - sd->k_min ) / 100.0 + 0.5);
			break;
	}
	levels = (int*)calloc(256, sizeof(int));
	nlevels = 0;
	for ( n = 0, intensity = 1; n < 256; n++, intensity += step ) {
		levels[n] = intensity;
		nlevels++;
	}
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(cv,distmap,levels,nlevels) private(n)
	for ( i = 0; i < distmap->columns; i++ ) {
		for ( j = 0; j < distmap->rows; j++ ) {
			for ( k = 0; k < distmap->planes; k++ ) {
				for ( n = 0; n < nlevels; n++ ) {
					if ( (int)distmap->voxels[i][j][k] == levels[n] ) {
						cv->voxels[i][j][k] = FilledPixel;
					}
				}
			}
		}
	}
	free(levels);
	return cv;
}

CharVolume*CharVolumeMultiSliceGCP(CharVolume*distmap, ShapeDescriptor3d*sd, double ns, int direction, int ns_direction, double ns_rows, double ns_columns, double ns_planes)
{
	CharVolume*cv;
	int intensity, *levels;
	int i, j, k, nlevels, n;
	int i_step, j_step, k_step, i_min, i_max, j_min, j_max, k_min, k_max;
	int step, got_max, got_min;
	cv = CharVolumeClone(distmap);
	switch ( direction ) {
		case 1:
			step = (int)( (double)ns * (double)( sd->i_max - sd->i_min ) / 100.0 + 0.5);
			break;
		case 2:
			step = (int)( (double)ns * (double)( sd->j_max - sd->j_min ) / 100.0 + 0.5);
			break;
		case 3:
			step = (int)( (double)ns * (double)( sd->k_max - sd->k_min ) / 100.0 + 0.5);
			break;
	}
	levels = (int*)calloc(256, sizeof(int));
	nlevels = 0;
	for ( n = 0, intensity = 1; n < 256; n++, intensity += step ) {
		levels[n] = intensity;
		nlevels++;
	}
	i_step = (int)( (double)ns_rows * (double)( 2 * sd->ellipsoid_axis_b ) / 100.0 + 0.5);
	j_step = (int)( (double)ns_columns * (double)( 2 * sd->ellipsoid_axis_a ) / 100.0 + 0.5);
	k_step = (int)( (double)ns_planes * (double)( 2 * sd->ellipsoid_axis_c ) / 100.0 + 0.5);
	i_min = -sd->ellipsoid_axis_b + sd->i_mean;
	i_max = sd->ellipsoid_axis_b + sd->i_mean;
	j_min = -sd->ellipsoid_axis_a + sd->j_mean;
	j_max = sd->ellipsoid_axis_a + sd->j_mean;
	k_min = -sd->ellipsoid_axis_c + sd->k_mean;
	k_max = sd->ellipsoid_axis_c + sd->k_mean;
	if ( ns_direction == 1 ) {
		for ( k = k_min; k <= k_max; k++ ) {
			got_min = 0;
			got_max = 0;
			for ( i = i_min; i <= i_max; i += i_step ) {
				for ( j = j_min; j <= j_max ; j++) {
					for ( n = 0; n < nlevels; n++ ) {
						if ( (int)distmap->voxels[j][i][k] == levels[n] ) {
							cv->voxels[j][i][k] = FilledPixel;
							if ( i == i_min ) {
								got_min++;
							}
							if ( i == i_max ) {
								got_max++;
							}
						}
					}
				}
			}
			if ( got_max == 0 ) {
				i = i_max;
				for ( j = j_min; j <= j_max ; j++) {
					for ( n = 0; n < nlevels; n++ ) {
						if ( (int)distmap->voxels[j][i][k] == levels[n] ) {
							cv->voxels[j][i][k] = FilledPixel;
							got_max++;
						}
					}
				}
				if ( got_max == 0 ) {
					i = sd->i_max;
					j = sd->j_mean;
					cv->voxels[j][i][k] = FilledPixel;
				}
			}
			if ( got_min == 0 ) {
				i = sd->i_min;
				j = sd->j_mean;
				cv->voxels[j][i][k] = FilledPixel;
			}
		}
	} else if ( ns_direction == 2 ) {
		for ( k = k_min; k <= k_max; k++ ) {
			got_min = 0;
			got_max = 0;
			for ( i = i_min; i <= i_max; i++) {
				for ( j = j_min; j <= j_max ; j += j_step) {
					for ( n = 0; n < nlevels; n++ ) {
						if ( (int)distmap->voxels[j][i][k] == levels[n] ) {
							cv->voxels[j][i][k] = FilledPixel;
							if ( j == j_min ) {
								got_min++;
							}
							if ( j == j_max ) {
								got_max++;
							}
						}
					}
				}
			}
			if ( got_max == 0 ) {
				j = j_max;
				for ( i = i_min; i <= i_max ; i++) {
					for ( n = 0; n < nlevels; n++ ) {
						if ( (int)distmap->voxels[j][i][k] == levels[n] ) {
							cv->voxels[j][i][k] = FilledPixel;
							got_max++;
						}
					}
				}
				if ( got_max == 0 ) {
					j = sd->j_max;
					i = sd->i_mean;
					cv->voxels[j][i][k] = FilledPixel;
				}
			}
			if ( got_min == 0 ) {
				j = sd->j_min;
				i = sd->i_mean;
				cv->voxels[j][i][k] = FilledPixel;
			}
		}
	} else if ( ns_direction == 3 ) {
		if ( distmap->planes > 2 ) {
			for ( k = k_min; k <= k_max; k += k_step ) {
				for ( i = i_min; i <= i_max; i++ ) {
					for ( j = j_min; j <= j_max ; j++) {
						for ( n = 0; n < nlevels; n++ ) {
							if ( (int)distmap->voxels[j][i][k] == levels[n] ) {
								cv->voxels[j][i][k] = FilledPixel;
							}
						}
					}
				}
			}
			k = k_max;
			for ( i = i_min; i <= i_max; i++ ) {
				for ( j = j_min; j <= j_max ; j++) {
					for ( n = 0; n < nlevels; n++ ) {
						if ( (int)distmap->voxels[j][i][k] == levels[n] ) {
							cv->voxels[j][i][k] = FilledPixel;
						}
					}
				}
			}
		}
	}
	free(levels);
	return cv;
}

void CharVolumeQu(CharVolume*cv, CharVolume*ci, GKeyFile*gkf, char*tag, int connectivity, int adding)
{
	int column, row, plane, counter;
	char *grp, *buf;
	Measurements3d *data;
	counter = 0;
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					double dist;
					int SegmentLength;
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &SegmentLength, 0, EmptyPixel);
					grp = g_strdup_printf("Shape:%d", counter);
					if ( !adding ) {
						ShapeDescriptor3d*sd;
						sd = PList3d2ShapeDescriptor3d(list);
						ShapeDescriptor3dPrint(gkf, sd, grp);
						ShapeDescriptor3dDelete (sd);
					}
					data = Plist3d2Measurements3d(SegmentLength, list, ci);
					Measurements3dPrint(gkf, data, grp, tag);
					g_free(grp);
					Measurements3dDelete (data);
					counter++;
					PList3dFree(list);
				}
			}
		}
	}
	CharVolumeDelete(cv);
}

Measurements3d*Plist3d2Measurements3d(int length, PList3d*list, CharVolume*ci)
{
	Measurements3d*data;
	int i, med, notempty;
	double mean, mean2, max, min, median, dot, muc;
	unsigned char *p;
	PList3d*cur;
	notempty = 0;
	mean = 0;
	mean2 = 0;
	max = 0;
	min = 65536;
	median = 0;
	p = (unsigned char*)calloc(length, sizeof(unsigned char));
	for (cur = list, i = 0; cur; cur = cur->next, i++) {
		dot = ci->voxels[cur->column][cur->row][cur->plane];
		if ( dot > EmptyPixel ) notempty++;
		mean += dot;
		mean2 += dot * dot;
		if ( dot > max )
			max = dot;
		if ( dot < min )
			min = dot;
		p [i] = dot;
	}
	muc = mean;
	qsort((void*)p, length, sizeof(unsigned char), com1);
	med = (int)( 0.5 * (double)length + 0.5 ) - 1;
	median = p [med];
	free(p);
	if ( length > 1 )
		dot = mean2 / ( length - 1.0 ) - mean * mean / ( length * ( length - 1.0 ) );
	else
		dot = 0;
	data = (Measurements3d*)malloc(sizeof(Measurements3d));
	if ( length > 1 )
		data->mean = mean / length;
	data->notempty = notempty;
	data->var = dot;
	data->stdev = sqrt(dot);
	data->max = max;
	data->min = min;
	data->median = median;
	data->muc = muc;
	return data;
}

void Measurements3dDelete (Measurements3d*data)
{
	free(data);
}

void Measurements3dPrint(GKeyFile*gkf, Measurements3d*data, char*grp, char*tag)
{
	char *buf;
	buf = g_strdup_printf("%13.6f;%13.6f;%13.6f;%13.6f;%13.6f;%13.6f;%13.6f;%d;", data->mean, data->var, data->stdev, data->max, data->min, data->median, data->muc, data->notempty);
	g_key_file_set_value(gkf, grp, tag, buf);
	g_free(buf);
}

int comPList3d(const void *a, const void *b)
{
	PList3d**x;
	PList3d**y;
	int resid = 0;
	x = (PList3d**)a;
	y = (PList3d**)b;
	if ( (*x)->theta > (*y)->theta ) resid = 1;
	if ( (*x)->theta < (*y)->theta ) resid = -1;
	return resid;
}

VertDat*Plist3d2VertDat_old(int length, PList3d*list, CharVolume*ci, double xmean, double ymean, int ncycles, int max_vert, double alpha)
{
	VertDat*data;
	int i, blength;
	double x, y;
	double theta, radius;
	PList3d*cur;
	PList3d**parr;
	int npattern = 8;
	int pattern[16] = {	0, 1,
						0, -1,
						1, 0,
						1, 1,
						1, -1,
						-1, 0,
						-1, 1,
						-1, -1 };
	parr = (PList3d**)calloc(length, sizeof(PList3d*));
	blength = 0;
	for (cur = list, i = 0; cur; cur = cur->next, i++) {
		int inside = 1;
		for (int np = 0; np < npattern; np++) {
			int indexc = cur->column + pattern[2*np];
			int indexr = cur->row + pattern[2*np+1];
			if ( -1 < indexc && indexc < ci->columns && - 1 < indexr && indexr < ci->rows ) {
				if ( ci->voxels[indexc][indexr][cur->plane] < FilledPixel ) {
					inside = 0;
					break;
				}
			}
		}
		if (inside == 0) {
			x = (double)cur->column - xmean;
			y = ymean - (double)cur->row;
			radius = sqrt( y * y + x * x );
			if ( x == 0 )
				if ( y > 0 )
					theta = 0.5 * MY_PI_CONST;
				else if ( y < 0 )
					theta = -0.5 * MY_PI_CONST;
				else
					theta = 0;
			else if ( x > 0 )
				theta = atan( y / x );
			else
				if ( y > 0 )
					theta = 0.5 * MY_PI_CONST + 0.5 * MY_PI_CONST - atan( y / -x );
				else if ( y < 0 )
					theta = -0.5 * MY_PI_CONST - 0.5 * MY_PI_CONST + atan( -y / -x );
				else
					theta = -1.0 * MY_PI_CONST;
			cur->x = x;
			cur->y = y;
			cur->theta = theta;
			cur->radius = radius;
			cur->val = radius;
			parr[blength] = cur;
			blength++;
		}
	}
	qsort((void*)parr, blength, sizeof(PList3d*), comPList3d);

	data = (VertDat*)malloc(sizeof(VertDat));
	data->n_vert = 0;
	data->row_vert = NULL;
	data->column_vert = NULL;
	int skip = 0;
	int n_points = 0;
	int last_cycle = ncycles;
	for (int nc = 1; nc < ncycles + 1; nc++) {
		n_points = 0;
		double mean = 0, mean2 = 0;
		for (i = 0; i < blength; i += nc) {
			int r1 = i + nc;
			int r2 = i - nc;
			while (r2 < 0) r2 = blength + r2;
			while (r1 > blength - 1) r1 = r1 - blength;
			parr[i]->vald1 = parr[r1]->val - parr[r2]->val;
			parr[i]->vald2 = parr[r2]->val - 2 * parr[i]->val + parr[r1]->val;
/*			parr[i]->val = 0.25 * (parr[r2]->val + 2 * parr[i]->val + parr[r1]->val);*/
			parr[i]->val = (2.0 * parr[r2]->val + 3.0 * parr[i]->val + 2.0 * parr[r1]->val)/7.0;
			n_points++;
			mean += parr[i]->val;
			mean2 += parr[i]->val * parr[i]->val;
		}
/*		last_cycle = (nc > 1) ? nc - 1:1;
		if (n_points < 12) break;
	}*/
		last_cycle = nc;

		if (data->row_vert != NULL) {
			free(data->row_vert);
			data->row_vert = NULL;
		}
		if (data->column_vert != NULL) {
			free(data->column_vert);
			data->column_vert = NULL;
		}
		data->n_vert = 0;

		mean /= (n_points > 0) ? (double)n_points:1.0;
		mean2 -= n_points * mean * mean;
		mean2 /= (n_points > 0) ? (double)n_points:1.0;
		mean2 = sqrt(mean2);

		for (i = 0; i < blength; i += last_cycle) {
			fprintf(stdout, "%d,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%d,%d\n", i, parr[i]->theta, parr[i]->radius, parr[i]->val, parr[i]->vald1, parr[i]->vald2, parr[i]->row, parr[i]->column, parr[i]->y, parr[i]->x);
		}
		fprintf(stdout, "%d,%d,%d,%13.6f,%13.6f\n", blength, n_points, last_cycle, mean, mean2);

		for (i = 0; i < blength; i += last_cycle) {
			if (skip == 1) {
				skip = 0;
				continue;
			}
			int r1 = i + last_cycle;
			int r2 = i - last_cycle;
			while (r2 < 0) r2 = blength + r2;
			while (r1 > blength - 1) r1 = r1 - blength;
			if (parr[r1]->vald1 * parr[r2]->vald1 < 0 && parr[i]->vald2 < 0 && parr[i]->val > (mean - alpha * mean2)) {
				data->row_vert = (int*)realloc(data->row_vert, (data->n_vert + 1) * sizeof(int));
				data->column_vert = (int*)realloc(data->column_vert, (data->n_vert + 1) * sizeof(int));
				data->column_vert[data->n_vert] = parr[i]->column;
				data->row_vert[data->n_vert] = parr[i]->row;
				data->n_vert++;
				skip = 1;
				fprintf(stdout, "%d,%d,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%d,%d\n", data->n_vert, i, parr[i]->theta, parr[i]->radius, parr[i]->val, parr[i]->vald1, parr[i]->vald2, parr[i]->row, parr[i]->column, parr[i]->y, parr[i]->x);
			}
		}
		data->cycle = last_cycle;
		data->n_points = n_points;
		if (data->n_vert < max_vert) break;
	}

	free(parr);
	return data;
}

void CharVolumeVu(CharVolume*cv, CharVolume*ci, GKeyFile*gkf, char*tag, int connectivity, int ncycles, int max_vert, double alpha)
{
	int column, row, plane, counter;
	char *grp, *buf;
	VertDat *data;
	counter = 0;
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					double dist;
					int SegmentLength;
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &SegmentLength, 0, EmptyPixel);
					grp = g_strdup_printf("Shape:%d", counter);
					double xmean = g_key_file_get_double (gkf, grp, "xmean", NULL);
					double ymean = g_key_file_get_double (gkf, grp, "ymean", NULL);
					data = Plist3d2VertDat(SegmentLength, list, cv, xmean, ymean, ncycles, max_vert, alpha);
					data->max_vert = max_vert;
					VertDatPrint(gkf, data, grp, tag);
					g_free(grp);
					VertDatDelete (data);
					counter++;
					PList3dFree(list);
				}
			}
		}
	}
	CharVolumeDelete(cv);
}

VertDat*Plist3d2VertDat(int length, PList3d*list, CharVolume*ci, double xmean, double ymean, int ncycles, int max_vert, double alpha)
{
	VertDat*data;
	int min_vert = 3;
	int i, blength, j;
	double x, y;
	double theta, radius;
	PList3d*cur;
	PList3d**parr;
	blength = 360;
	parr = (PList3d**)calloc(blength, sizeof(PList3d*));
	for (i = 0; i < blength; i++) {
		parr[i] = NULL;
	}
	for (cur = list, i = 0; cur; cur = cur->next, i++) {
		x = (double)cur->column - xmean;
		y = ymean - (double)cur->row;
		radius = sqrt( y * y + x * x );
		if ( x == 0 )
			if ( y > 0 )
				theta = 0.5 * MY_PI_CONST;
			else if ( y < 0 )
				theta = -0.5 * MY_PI_CONST;
			else
				theta = 0;
		else if ( x > 0 )
			theta = atan( y / x );
		else
			if ( y > 0 )
				theta = 0.5 * MY_PI_CONST + 0.5 * MY_PI_CONST - atan( y / -x );
			else if ( y < 0 )
				theta = -0.5 * MY_PI_CONST - 0.5 * MY_PI_CONST + atan( -y / -x );
			else
				theta = -1.0 * MY_PI_CONST;
		cur->x = x;
		cur->y = y;
		cur->theta = 180.0 * theta/((double)MY_PI_CONST) + 180.0;
		cur->radius = radius;
		cur->val = radius;
		for (i = 0; i < blength; i++) {
			if ((double)i <= cur->theta && cur->theta < (double)(i + 1)) {
				if (parr[i] != NULL) {
					if (parr[i]->radius < cur->radius)
						parr[i] = cur;
				} else {
					parr[i] = cur;
				}
			}
		}
	}

	data = (VertDat*)malloc(sizeof(VertDat));
	data->n_vert = 0;
	data->row_vert = NULL;
	data->column_vert = NULL;
	int skip = 0;
	int n_points = 0;
	int last_cycle = ncycles;
	for (int nc = 1; nc < ncycles + 1; nc++) {
		n_points = 0;
		double mean = 0, mean2 = 0;
		for (i = 0; i < blength; i += nc) {
			int r1 = i + nc;
			int r2 = i - nc;
			while (r2 < 0) r2 = blength + r2;
			while (r1 > blength - 1) r1 = r1 - blength;
			if (parr[i] == NULL || parr[r1] == NULL || parr[r2] == NULL) continue;
			parr[i]->vald1 = parr[r1]->val - parr[r2]->val;
			parr[i]->vald2 = parr[r2]->val - 2 * parr[i]->val + parr[r1]->val;
			parr[i]->val = 0.25 * (parr[r2]->val + 2 * parr[i]->val + parr[r1]->val);
/*			parr[i]->val = (2.0 * parr[r2]->val + 3.0 * parr[i]->val + 2.0 * parr[r1]->val)/7.0;*/
			n_points++;
			mean += parr[i]->val;
			mean2 += parr[i]->val * parr[i]->val;
			parr[i]->inc = 0;
		}
/*		last_cycle = (nc > 1) ? nc - 1:1;
		if (n_points < 12) break;
	}*/
		last_cycle = nc;

		if (data->row_vert != NULL) {
			free(data->row_vert);
			data->row_vert = NULL;
		}
		if (data->column_vert != NULL) {
			free(data->column_vert);
			data->column_vert = NULL;
		}
		data->n_vert = 0;

		mean /= (n_points > 0) ? (double)n_points:1.0;
		mean2 -= n_points * mean * mean;
		mean2 /= (n_points > 0) ? (double)n_points:1.0;
		mean2 = sqrt(mean2);

		for (i = 0; i < blength; i += last_cycle) {
			if (parr[i] == NULL) continue;
			fprintf(stdout, "%d,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%d,%d\n", i, parr[i]->theta, parr[i]->radius, parr[i]->val, parr[i]->vald1, parr[i]->vald2, parr[i]->row, parr[i]->column, parr[i]->y, parr[i]->x);
		}
		fprintf(stdout, "%d,%d,%d,%13.6f,%13.6f\n", blength, n_points, last_cycle, mean, mean2);

		skip = 0;
		for (i = 0; i < blength; i += last_cycle) {
			if (skip == 1) {
				skip = 0;
				continue;
			}
			int r1 = i + last_cycle;
			int r2 = i - last_cycle;
			while (r2 < 0) r2 = blength + r2;
			while (r1 > blength - 1) r1 = r1 - blength;
			if (parr[i] == NULL || parr[r1] == NULL || parr[r2] == NULL) continue;
			if (parr[r1]->vald1 * parr[r2]->vald1 < 0 && parr[i]->vald2 < 0 && parr[i]->val > (mean - alpha * mean2)) {
				data->row_vert = (int*)realloc(data->row_vert, (data->n_vert + 1) * sizeof(int));
				data->column_vert = (int*)realloc(data->column_vert, (data->n_vert + 1) * sizeof(int));
				data->column_vert[data->n_vert] = parr[i]->column;
				data->row_vert[data->n_vert] = parr[i]->row;
				data->n_vert++;
				skip = 1;
				parr[i]->inc = 1;
				parr[r1]->inc = 1;
				parr[r2]->inc = 1;
				fprintf(stdout, "%d,%d,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%d,%d\n", data->n_vert, i, parr[i]->theta, parr[i]->radius, parr[i]->val, parr[i]->vald1, parr[i]->vald2, parr[i]->row, parr[i]->column, parr[i]->y, parr[i]->x);
			}
		}
		data->cycle = last_cycle;
		data->n_points = n_points;
		if (data->n_vert < max_vert) break;
	}
	if (data->n_vert < min_vert) {
		skip = 0;
		data->row_vert = (int*)realloc(data->row_vert, (data->n_vert + 1) * sizeof(int));
		data->column_vert = (int*)realloc(data->column_vert, (data->n_vert + 1) * sizeof(int));
		data->n_vert++;
		double max_val = 0;
		for (i = 0; i < blength; i += last_cycle) {
			int r1 = i + last_cycle;
			int r2 = i - last_cycle;
			while (r2 < 0) r2 = blength + r2;
			while (r1 > blength - 1) r1 = r1 - blength;
			if (parr[i] == NULL || parr[r1] == NULL || parr[r2] == NULL) continue;
			if (parr[r1]->vald1 * parr[r2]->vald1 < 0 && parr[i]->vald2 < 0 && parr[i]->inc == 0) {
				if (parr[i]->val > max_val) {
					data->column_vert[data->n_vert - 1] = parr[i]->column;
					data->row_vert[data->n_vert - 1] = parr[i]->row;
					max_val = parr[i]->val;
					skip = 1;
					j = i;
				}
			}
		}
		if (skip == 1) {
			fprintf(stdout, "%d,%d,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%13.6f,%d,%d\n", data->n_vert, j, parr[j]->theta, parr[j]->radius, parr[j]->val, parr[j]->vald1, parr[j]->vald2, parr[j]->row, parr[j]->column, parr[j]->y, parr[j]->x);
		} else {
			data->row_vert = (int*)realloc(data->row_vert, (data->n_vert - 1) * sizeof(int));
			data->column_vert = (int*)realloc(data->column_vert, (data->n_vert - 1) * sizeof(int));
			data->n_vert--;
		}
	}
	free(parr);
	return data;
}

void VertDatDelete (VertDat*data)
{
	free(data->row_vert);
	free(data->column_vert);
	free(data);
}

void VertDatPrint(GKeyFile*gkf, VertDat*data, char*grp, char*tag)
{
	GString*buf = g_string_new("");
	g_string_printf(buf, "%d;%d;%d;%d;", data->n_vert, data->max_vert, data->cycle, data->n_points);
	for (int i = 0; i < data->n_vert; i++) {
		g_string_append_printf(buf, "%d;%d;", data->row_vert[i], data->column_vert[i]);
	}
	for (int i = data->n_vert; i < data->max_vert; i++) {
		g_string_append_printf(buf, "%d;%d;", -99, -99);
	}
	g_key_file_set_value(gkf, grp, tag, buf->str);
	g_string_free(buf, TRUE);
}

void shape_descriptor_get_data_field(GKeyFile*gkf, char*grp, char*key, double*data, int idex, GError**gerror)
{
	char**list;
	gsize size;
	list = g_key_file_get_string_list(gkf, grp, key, &size, gerror);
	if ( (*gerror) || !list )
		return;
	(*data) = atof(list[idex]);
	g_strfreev(list);
}

CharVolume*CharVolumeGrid(CharVolume*ci, double ns_rows, double ns_columns, double ns_planes, double xmean, double ymean, double zmean)
{
	int i, j, k;
	int i_mean, j_mean, k_mean;
	int i_max, j_max, k_max;
	int i_min, j_min, k_min;
	int i_step, j_step, k_step;
	CharVolume*di, *dv;
	unsigned char intensity_marker_empty = 64;
	unsigned char intensity_marker_edge = 64;
	di = CharVolumeClone(ci);
	dv = CharVolumeClone(ci);
	i_mean = (int)(ymean + 0.5);
	j_mean = (int)(xmean + 0.5);
	k_mean = (int)(zmean + 0.5);
	k = k_mean;
	i_max = 0;
	j_max = 0;
	i_min = ci->rows;
	j_min = ci->columns;
	for ( i = 0; i < ci->rows; i++ ) {
		for ( j = 0; j < ci->columns ; j++) {
			if ( ci->voxels[j][i][k] == (unsigned char)FilledPixel )
			{
				if ( i_min > i )
					i_min = i;
				if ( j_min > j )
					j_min = j;
				if ( i_max < i )
					i_max = i;
				if ( j_max < j )
					j_max = j;
			}
		}
	}
	i_step = (int)( (double)ns_rows * (double)( i_max - i_min ) / 100.0 + 0.5);
	j_step = (int)( (double)ns_columns * (double)( j_max - j_min ) / 100.0 + 0.5);
	k_max = 0;
	k_min = ci->planes;
	i = i_mean;
	for ( k = 0; k < ci->planes; k++ ) {
		for ( j = j_min; j <= j_max ; j++) {
			if ( ci->voxels[j][i][k] == (unsigned char)FilledPixel )
			{
				if ( k_min > k )
					k_min = k;
				if ( k_max < k )
					k_max = k;
			}
		}
	}
	k_step = (int)( (double)ns_planes * (double)( k_max - k_min ) / 100.0 + 0.5);
	for ( k = k_min; k <= k_max; k++ ) {
		for ( i = i_min; i <= i_max; i += i_step ) {
			for ( j = j_min; j <= j_max ; j++) {
				di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
			}
		}
		for ( i = i_min; i <= i_max; i++) {
			for ( j = j_min; j <= j_max ; j += j_step) {
				dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
			}
		}
	}
	if ( ci->planes > 2 ) {
		for ( k = k_min; k <= k_max; k += k_step ) {
			for ( i = i_min; i <= i_max; i++ ) {
				for ( j = j_min; j <= j_max ; j++) {
					di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
				}
			}
		}
	}
	for ( k = 0; k < ci->planes; k++ ) {
		for ( i = 0; i < ci->rows; i++) {
			for ( j = 0; j < ci->columns ; j++) {
				di->voxels[j][i][k] += dv->voxels[j][i][k];
			}
		}
	}
	CharVolumeDelete(dv);
	di = CharVolumeInvert(di);
	i = 0;
	j = 0;
	k = 0;
	if ( di->voxels[j][i][k] == FilledPixel ) {
		PList3d*list;
		int SegmentLength;
		int connectivity = 6;
		list = IntensitySegment3dModified(di, FilledPixel, i, j, k, connectivity, di->rows * di->columns * di->planes, &SegmentLength, 0, (unsigned char)intensity_marker_empty);
		PList3dFree(list);
	}
	return di;
}

void vertex_mark_func(gpointer data, gpointer user_data)
{
	unsigned char intensity_marker_vertex = 255 - 16;
	GList*list_two = (GList*)user_data;
	unsigned char*pixel = (unsigned char*)data;
	gint index;
	index = g_list_index(list_two, (gconstpointer)pixel);
	if ( index > -1 ) {
		*pixel = intensity_marker_vertex;
		fprintf(stderr, "Vertex\n");
	}
}

CharVolume*CharVolumeGridElliptical(CharVolume*ci, double ns_rows, double ns_columns, double ns_planes, double offset, double xmean, double ymean, double zmean, double eps)
{
	int i, j, k;
	int i_mean, j_mean, k_mean;
	int i_max, j_max, k_max;
	int i_min, j_min, k_min;
	int i_step, j_step, k_step;
	int mu, lambda, nunu, nu, lala, iii1, iii2, jjj1, jjj2;
	double dot, a, b, c, a2, b2, c2, ii, kk, jj, ii2, kk2, jj2;
	CharVolume*di, *dv;
	unsigned char intensity_marker_empty = 64;
	unsigned char intensity_marker_edge = 64;
	unsigned char intensity_marker_edge_extra = 63;
	di = CharVolumeClone(ci);
	dv = CharVolumeClone(ci);
	i_mean = (int)(ymean + 0.5);
	j_mean = (int)(xmean + 0.5);
	k_mean = (int)(zmean + 0.5);
	k = k_mean;
	i_max = 0;
	j_max = 0;
	i_min = ci->rows;
	j_min = ci->columns;
	for ( i = 0; i < ci->rows; i++ ) {
		for ( j = 0; j < ci->columns ; j++) {
			if ( ci->voxels[j][i][k] == (unsigned char)FilledPixel )
			{
				if ( i_min > i )
					i_min = i;
				if ( j_min > j )
					j_min = j;
				if ( i_max < i )
					i_max = i;
				if ( j_max < j )
					j_max = j;
			}
		}
	}
	i_step = (int)( (double)ns_rows * (double)( i_max - i_min ) / 100.0 + 0.5);
	j_step = (int)( (double)ns_columns * (double)( j_max - j_min ) / 100.0 + 0.5);
	k_max = 0;
	k_min = ci->planes;
	i = i_mean;
	for ( k = 0; k < ci->planes; k++ ) {
		for ( j = j_min; j <= j_max ; j++) {
			if ( ci->voxels[j][i][k] == (unsigned char)FilledPixel )
			{
				if ( k_min > k )
					k_min = k;
				if ( k_max < k )
					k_max = k;
			}
		}
	}
	k_step = (int)( (double)ns_planes * (double)( k_max - k_min ) / 100.0 + 0.5);
	if ( k_mean - k_min > k_max - k_mean ) {
		c = k_mean - k_min;
	} else {
		c = k_max - k_mean;
	}
	if ( i_mean - i_min > i_max - i_mean ) {
		b = i_mean - i_min;
	} else {
		b = i_max - i_mean;
	}
	if ( j_mean - j_min > j_max - j_mean ) {
		a = j_mean - j_min;
	} else {
		a = j_max - j_mean;
	}
/*fprintf(stderr, "a,b,c %f,%f,%f\n", a, b ,c);
fprintf(stderr, "-a2,-b2,-c2 %f,%f,%f\n", -a * a, -b * b , -c * c);*/
	if ( fabs ( offset ) < eps ) {
		for ( k = 0; k < ci->planes; k++ ) {
			for ( i = i_min; i <= i_max; i++) {
				j = j_mean;
				di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
			}
			for ( j = j_min; j <= j_max ; j++) {
				i = i_mean;
				dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
			}
		}
		if ( ci->planes > 2 && k_min != k_mean && k_min != k_mean ) {
			k = k_mean;
			for ( i = i_min; i <= i_max; i++) {
				for ( j = j_min; j <= j_max ; j++) {
					di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
				}
			}
		}
	}
	for ( lambda = -c * c; lambda <= -c * c + 3 * k_max; lambda++ ) {
		for ( nu = -a * a + 1, nunu = ( 1.0 - offset ) * j_step; nu < -b * b + 1; nu += 1 ) {
			mu = -b * b;
			c2 = ( a * a + lambda * lambda );
			b2 = ( a * a + mu );
			a2 = ( a * a + nu );
			dot = ( a * a - b * b ) * ( a * a - c * c );
			jj2 = c2 * b2 * a2 / dot;
			jj = sqrt(jj2);
			if ( fabs ( jj - nunu ) < eps ) {
				nunu += j_step;
				jjj1 = (int)(jj + j_mean + 0.5);
				jjj2 = (int)(-jj + j_mean + 0.5);
				for ( mu = -b * b + 1; mu < -c * c; mu++ ) {
					c2 = ( b * b + lambda * lambda );
					b2 = ( b * b + mu );
					a2 = ( b * b + nu );
					dot = ( b * b - a * a ) * ( b * b - c * c );
					ii2 = c2 * b2 * a2 / dot;
					c2 = ( a * a + lambda * lambda );
					b2 = ( a * a + mu );
					a2 = ( a * a + nu );
					dot = ( a * a - b * b ) * ( a * a - c * c );
					jj2 = c2 * b2 * a2 / dot;
					c2 = ( c * c + lambda );
					b2 = ( c * c + mu );
					a2 = ( c * c + nu );
					dot = ( c * c - b * b ) * ( c * c - a * a );
					kk2 = c2 * b2 * a2 / dot;
					kk = sqrt(kk2);
					jj = sqrt(jj2);
					ii = sqrt(ii2);
					k = (int)(kk + k_mean + 0.5);
					if ( -1 < jjj1 && jjj1 < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[jjj1][i_mean - 1][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj1][i_mean][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj1][i_mean + 1][k] = (unsigned char)intensity_marker_edge;
					}
					if ( -1 < jjj2 && jjj2 < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[jjj2][i_mean - 1][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj2][i_mean][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj2][i_mean + 1][k] = (unsigned char)intensity_marker_edge;
					}
					j = (int)(jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
					k = (int)(kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
					k = (int)(kk + k_mean + 0.5);
					j = (int)(jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
					k = (int)(kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
					k = (int)(-kk + k_mean + 0.5);
					if ( -1 < jjj1 && jjj1 < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[jjj1][i_mean - 1][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj1][i_mean][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj1][i_mean + 1][k] = (unsigned char)intensity_marker_edge;
					}
					if ( -1 < jjj2 && jjj2 < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[jjj2][i_mean - 1][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj2][i_mean][k] = (unsigned char)intensity_marker_edge;
						di->voxels[jjj2][i_mean + 1][k] = (unsigned char)intensity_marker_edge;
					}
					j = (int)(jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
					k = (int)(-kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
					k = (int)(-kk + k_mean + 0.5);
					j = (int)(jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
					k = (int)(-kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						di->voxels[j][i][k] = (unsigned char)intensity_marker_edge;
					}
				}
			}
		}
		for ( mu = -b * b + 1, nunu = ( 1.0 - offset ) * i_step; mu < -c * c; mu += 1 ) {
			nu = -a * a;
			c2 = ( b * b + lambda * lambda );
			b2 = ( b * b + mu );
			a2 = ( b * b + nu );
			dot = ( b * b - a * a ) * ( b * b - c * c );
			ii2 = c2 * b2 * a2 / dot;
			ii = sqrt(ii2);
			if ( fabs ( ii - nunu ) < eps ) {
				nunu += i_step;
				iii1 = (int)(ii + i_mean + 0.5);
				iii2 = (int)(-ii + i_mean + 0.5);
				for ( nu = -a * a + 1; nu < -b * b + 1; nu++) {
					c2 = ( b * b + lambda * lambda );
					b2 = ( b * b + mu );
					a2 = ( b * b + nu );
					dot = ( b * b - a * a ) * ( b * b - c * c );
					ii2 = c2 * b2 * a2 / dot;
					c2 = ( a * a + lambda * lambda );
					b2 = ( a * a + mu );
					a2 = ( a * a + nu );
					dot = ( a * a - b * b ) * ( a * a - c * c );
					jj2 = c2 * b2 * a2 / dot;
					c2 = ( c * c + lambda );
					b2 = ( c * c + mu );
					a2 = ( c * c + nu );
					dot = ( c * c - b * b ) * ( c * c - a * a );
					kk2 = c2 * b2 * a2 / dot;
					kk = sqrt(kk2);
					jj = sqrt(jj2);
					ii = sqrt(ii2);
					k = (int)(kk + k_mean + 0.5);
					if ( -1 < iii1 && iii1 < ci->rows && -1 < k && k < ci->planes ) {
						dv->voxels[j_mean - 1][iii1][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean][iii1][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean + 1][iii1][k] = (unsigned char)intensity_marker_edge_extra;
					}
					if ( -1 < iii2 && iii2 < ci->rows && -1 < k && k < ci->planes ) {
						dv->voxels[j_mean - 1][iii2][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean][iii2][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean + 1][iii2][k] = (unsigned char)intensity_marker_edge_extra;
					}
					j = (int)(jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
					k = (int)(kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
					k = (int)(kk + k_mean + 0.5);
					j = (int)(jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
					k = (int)(kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
					k = (int)(-kk + k_mean + 0.5);
					if ( -1 < iii1 && iii1 < ci->rows && -1 < k && k < ci->planes ) {
						dv->voxels[j_mean - 1][iii1][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean][iii1][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean + 1][iii1][k] = (unsigned char)intensity_marker_edge_extra;
					}
					if ( -1 < iii2 && iii2 < ci->rows && -1 < k && k < ci->planes ) {
						dv->voxels[j_mean - 1][iii2][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean][iii2][k] = (unsigned char)intensity_marker_edge_extra;
						dv->voxels[j_mean + 1][iii2][k] = (unsigned char)intensity_marker_edge_extra;
					}
					j = (int)(jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
					k = (int)(-kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
					k = (int)(-kk + k_mean + 0.5);
					j = (int)(jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
					k = (int)(-kk + k_mean + 0.5);
					j = (int)(-jj + j_mean + 0.5);
					i = (int)(-ii + i_mean + 0.5);
					if ( -1 < i && i < ci->rows && -1 < j && j < ci->columns && -1 < k && k < ci->planes ) {
						dv->voxels[j][i][k] = (unsigned char)intensity_marker_edge_extra;
					}
				}
			}
		}
	}
	for ( k = 0; k < ci->planes; k++ ) {
		for ( i = 0; i < ci->rows; i++) {
			for ( j = 0; j < ci->columns ; j++) {
				di->voxels[j][i][k] += dv->voxels[j][i][k];
			}
		}
	}
	CharVolumeDelete(dv);
	di = CharVolumeInvert(di);
	i = 0;
	j = 0;
	k = 0;
	if ( di->voxels[j][i][k] == FilledPixel ) {
		PList3d*list;
		int SegmentLength;
		int connectivity = 6;
		list = IntensitySegment3dModified(di, FilledPixel, i, j, k, connectivity, di->rows * di->columns * di->planes, &SegmentLength, 0, (unsigned char)intensity_marker_empty);
		PList3dFree(list);
	}
	return di;
}

CharVolume*CharVolumeQuMark(CharVolume*cv, int connectivity)
{
	int column, row, plane, i, j, k;
	CharVolume*ci;
	Measurements3d *data;
	ci = CharVolumeClone(cv);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(cv,ci,connectivity) private(i,j,k)
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					double dist;
					int SegmentLength;
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &SegmentLength, 0, EmptyPixel);
					ShapeDescriptor3d*sd;
					sd = PList3d2ShapeDescriptor3d(list);
					i = sd->i_mean;
					j = sd->j_mean;
					k = sd->k_mean;
					ci->voxels[j][i][k] = FilledPixel;
					ShapeDescriptor3dDelete (sd);
					PList3dFree(list);
				}
			}
		}
	}
	return ci;
}

SuperImage*char_volume_mk_label(CharVolume*mask, int connectivity, double ep)
{
	SuperImage*labels;
	int row, column, plane;
	int counter, index, number;
	number = mask->columns * mask->rows * mask->planes;
	labels = SuperImageNewLabel(mask->columns * mask->rows, mask->planes, 16, ep);
	counter = 0;
	for ( column = 0; column < mask->columns; column++ ) {
		for ( row = 0; row < mask->rows; row++ ) {
			for ( plane = 0; plane < mask->planes; plane++ ) {
				if ( mask->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					int SegmentLength;
					list = IntensitySegment3dModified(mask, FilledPixel, row, column, plane, connectivity, number, &SegmentLength, 0, EmptyPixel);
					counter++;
					LabelDoubleByPList3d(list, labels->pixels, (double)counter, mask->rows, mask->columns, mask->planes);
					PList3dFree(list);
				}
			}
		}
	}
	return labels;
}

CharVolume* char_volume_erode_mask(CharVolume*mask, int connectivity, int maxiter, GError**gerror)
{
	double x, y, z, p;
	char*grp, *buf;
	gsize size;
	int row, column, plane;
	int counter, index, iter;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	CharVolume*cop, *tmp;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	cop = CharVolumeCopy(mask);
	counter = 1;
	iter = 0;
	while (counter == 1 && (maxiter < 0 || iter < maxiter)) {
		counter = 0;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(mask, cop, tmp, ext, next, counter) private(n)
		for ( column = 0; column < mask->columns; column++ ) {
			for ( row = 0; row < mask->rows; row++ ) {
				for ( plane = 0; plane < mask->planes; plane++ ) {
					cop->voxels[column][row][plane] = mask->voxels[column][row][plane];
				}
			}
		}
		tmp = mask;
		mask = cop;
		cop = tmp;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(mask, cop, tmp, ext, next, counter) private(n)
		for ( column = 0; column < mask->columns; column++ ) {
			for ( row = 0; row < mask->rows; row++ ) {
				for ( plane = 0; plane < mask->planes; plane++ ) {
					if ( cop->voxels[column][row][plane] == FilledPixel ) {
						int label1 = 0;
						int label2 = 0;
						int label1_next = 0;
						int label2_next = 0;
						for ( n = 0; n < next; n+=3 ) {
							register int indexr = row + ext[n];
							register int indexc = column + ext[n+1];
							register int indexp = plane + ext[n+2];
							if ( -1 < indexc && indexc < mask->columns && -1 < indexr && indexr < mask->rows && -1 < indexp && indexp < mask->planes ) {
								if ( cop->voxels[indexc][indexr][indexp] == FilledPixel) {
									label1++;
								} else {
									label2++;
								}
								if ( mask->voxels[indexc][indexr][indexp] == FilledPixel) {
									label1_next++;
								} else {
									label2_next++;
								}
							}
						}
						if (label1_next > 0 && label1 > 0 && label2 > 0) {
							mask->voxels[column][row][plane] = EmptyPixel;
							counter = 1;
						}
					}
				}
			}
		}
		iter++;
	}
/* FIXME
 * change to g_debug
 * */
	g_message("Thining used %d iterations from %d max", iter, maxiter);
	CharVolumeDelete(cop);
	return mask;
}

CharVolume* char_volume_dilate_mask(CharVolume*mask, int connectivity, int maxiter, GError**gerror)
{
	double x, y, z, p;
	char*grp, *buf;
	gsize size;
	int row, column, plane;
	PList3d*list;
	int SegmentLength;
	int label, i;
	SuperImage*labels, *cop, *tmp;
	int counter, number, ind, iter;
	int ext6[18] = {0, 0, 1,
					0, 0, -1,
					0, 1, 0,
					0, -1, 0,
					1, 0, 0,
					-1, 0, 0};
	int next6 = 18;
	int ext26[78] = {0, 0, 1,
				0, -1, 1,
				0, 1, 1,
				1, 0, 1,
				1, -1, 1,
				1, 1, 1,
				-1, 0, 1,
				-1, -1, 1,
				-1, 1, 1,
				0, 0, -1,
				0, -1, -1,
				0, 1, -1,
				1, 0, -1,
				1, -1, -1,
				1, 1, -1,
				-1, 0, -1,
				-1, -1, -1,
				-1, 1, -1,
				0, -1, 0,
				0, 1, 0,
				1, 0, 0,
				1, -1, 0,
				1, 1, 0,
				-1, 0, 0,
				-1, -1, 0,
				-1, 1, 0};
	int next26 = 78;
	int *ext;
	int next;
	int n;
	if ( connectivity != 26 && connectivity != 6 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 26 ) {
		next = next26;
		ext = ext26;
	} else if ( connectivity == 6 ) {
		next = next6;
		ext = ext6;
	}
	number = mask->columns * mask->rows * mask->planes;
	labels = char_volume_mk_label(mask, connectivity, -1.0);
	cop = SuperImageCopy(labels);
	counter = 1;
	iter = 0;
	while (counter == 1 && (maxiter < 0 || iter < maxiter)) {
		counter = 0;
#pragma omp parallel for schedule(static) default(none) shared(labels, cop)
		for ( i = 0; i < labels->number; i++ )
			cop->pixels[i] = labels->pixels[i];
		tmp = labels;
		labels = cop;
		cop = tmp;
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(mask, labels, cop, next, ext, counter) private(ind, n)
		for ( column = 0; column < mask->columns; column++ ) {
			for ( row = 0; row < mask->rows; row++ ) {
				for ( plane = 0; plane < mask->planes; plane++ ) {
					ind = column + row * mask->columns + plane * mask->rows * mask->columns;
					if ( cop->pixels[ind] < 1 ) {
						int label1 = 0;
						int label2 = 0;
						int label1_next = 0;
						int label2_next = 0;
						int index, label, label_next;
						for ( n = 0; n < next; n+=3 ) {
							register int indexr = row + ext[n];
							register int indexc = column + ext[n+1];
							register int indexp = plane + ext[n+2];
							if ( -1 < indexc && indexc < mask->columns && -1 < indexr && indexr < mask->rows && -1 < indexp && indexp < mask->planes ) {
								index = indexc + indexr * mask->columns + indexp * mask->rows * mask->columns;
								label = (int)( cop->pixels[index] + 0.5 );
								if ( label1 == 0 && label > 0) {
									label1 = label;
								} else if ( label > 0 && label != label1 && label2 == 0 ) {
									label2 = label;
								}
								label_next = (int)( labels->pixels[index] + 0.5 );
								if ( label1_next == 0 && label_next > 0) {
									label1_next = label_next;
								} else if ( label_next > 0 && label_next != label1_next && label2_next == 0 ) {
									label2_next = label_next;
								}
							}
						}
						if (label1 > 0 && label2 == 0 && label2_next == 0) {
							labels->pixels[ind] = label1;
							counter = 1;
						}
					}
				}
			}
		}
		iter++;
	}
/* FIXME
 * change to g_debug
 * */
	g_message("Thicken used %d iterations from %d max", iter, maxiter);
#pragma omp parallel for schedule(static) collapse(3) default(none) shared(mask, labels) private(ind)
	for ( column = 0; column < mask->columns; column++ ) {
		for ( row = 0; row < mask->rows; row++ ) {
			for ( plane = 0; plane < mask->planes; plane++ ) {
				ind = column + row * mask->columns + plane * mask->rows * mask->columns;
				if ( labels->pixels[ind] > 0 ) {
					mask->voxels[column][row][plane] = (unsigned char)((int)( labels->pixels[ind] + 0.5 ) % (FilledPixel - 1) + 1);
				} else {
					mask->voxels[column][row][plane] = EmptyPixel;
				}
			}
		}
	}
	SuperImageDelete(labels);
	SuperImageDelete(cop);
	return mask;
}

void char_volume_relabel_blobs(GKeyFile*gkf, CharVolume*mask, int connectivity, CharImage*landmarks_fixed, CharImage*landmarks, char*type, double alpha, GError**gerror)
{
	double x, y, z, p;
	char**grps, *buf;
	gsize size;
	int row, column, plane;
	PList3d*list;
	int SegmentLength;
	int label, i;
	SuperImage*labels, *labels_registered;
	int counter, index, number;
	number = mask->columns * mask->rows * mask->planes;
/*	labels = SuperImageNew(mask->columns * mask->rows, mask->planes, 16);
	counter = 0;
	for ( column = 0; column < mask->columns; column++ ) {
		for ( row = 0; row < mask->rows; row++ ) {
			for ( plane = 0; plane < mask->planes; plane++ ) {
				if ( mask->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					int SegmentLength;
					list = IntensitySegment3dModified(mask, FilledPixel, row, column, plane, connectivity, number, &SegmentLength, 0, EmptyPixel);
					counter++;
					LabelDoubleByPList3d(list, labels->pixels, (double)counter, mask->rows, mask->columns, mask->planes);
					PList3dFree(list);
				}
			}
		}
	}*/
	labels = char_volume_mk_label(mask, connectivity, 0.0);
	labels_registered = super_image_mlsreg(landmarks_fixed, landmarks, labels, type, alpha);
	SuperImageDelete(labels);
	grps = g_key_file_get_groups(gkf, &size);
	if ( grps ) {
		for ( i = 0; i < (int)size; i++ ) {
			if ( g_str_has_prefix(grps[i], "Shape:")) {
				x = g_key_file_get_double(gkf, grps[i], "xmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return;
				}
				y = g_key_file_get_double(gkf, grps[i], "ymean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return;
				}
				z = g_key_file_get_double(gkf, grps[i], "zmean", gerror);
				if ( (*gerror) ) {
					g_strfreev(grps);
					return;
				}
				row = (int) ( y + 0.5 );
				column = (int) ( x + 0.5 );
				plane = (int) ( z + 0.5 );
				index = row * mask->columns + column ;
				label = (int)( labels_registered->pixels[index] + 0.5 );
				buf = g_strdup_printf("%d", label);
				g_key_file_set_value(gkf, grps[i], "label", buf);
				g_free(buf);
			}
		}
		g_strfreev(grps);
	}
	SuperImageDelete(labels_registered);
}

void char_volume_label_mask(GKeyFile*gkf, CharVolume*mask, int connectivity, char*tag, CharVolume*cv, GError**gerror)
{
	double x, y, z, p;
	char*grp, *buf;
	gsize size;
	int row, column, plane;
	PList3d*list;
	int SegmentLength;
	int label, i;
	SuperImage*labels;
	int counter, index, number;
	number = mask->columns * mask->rows * mask->planes;
/*	labels = SuperImageNewLabel(mask->columns * mask->rows, mask->planes, 16, -1.0);
	counter = 0;
	for ( column = 0; column < mask->columns; column++ ) {
		for ( row = 0; row < mask->rows; row++ ) {
			for ( plane = 0; plane < mask->planes; plane++ ) {
				if ( mask->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					int SegmentLength;
					list = IntensitySegment3dModified(mask, FilledPixel, row, column, plane, connectivity, number, &SegmentLength, 0, EmptyPixel);
					counter++;
					LabelDoubleByPList3d(list, labels->pixels, (double)counter, mask->rows, mask->columns, mask->planes);
					PList3dFree(list);
				}
			}
		}
	}*/
	labels = char_volume_mk_label(mask, connectivity, -1.0);
	counter = 0;
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			for ( plane = 0; plane < cv->planes; plane++ ) {
				if ( cv->voxels[column][row][plane] == FilledPixel ) {
					PList3d*list;
					int SegmentLength;
					list = IntensitySegment3dModified(cv, FilledPixel, row, column, plane, connectivity, cv->columns * cv->rows * cv->planes, &SegmentLength, 0, EmptyPixel);
					grp = g_strdup_printf("Shape:%d", counter);
					index = list->column + list->row * cv->columns + list->plane * cv->rows * cv->columns;
					label = (int)( labels->pixels[index] + 0.5 );
					buf = g_strdup_printf("%d", label);
					g_key_file_set_value(gkf, grp, tag, buf);
					g_free(buf);
					counter++;
					g_free(grp);
					PList3dFree(list);
				}
			}
		}
	}
	CharVolumeDelete(cv);
	SuperImageDelete(labels);
}

CharVolume*CharVolumeBPHysThresh(CharVolume*dp, double low, double high, uint32 segment, int connectivity)
{
	uint32 plane, i;
	CharImage*ci;
	double*buffer;
	for ( plane = 0; plane < dp->planes; plane++) {
		ci = CharVolumeGetPlane(dp, plane);
		buffer = (double*)calloc(ci->number, sizeof(double));
		for ( i = 0; i < ci->number; i++ ) {
			buffer[i] = (double)(ci->pixels[i]);
		}
		HysteresisThreshold(ci, buffer, low, high, (uint32) segment, connectivity);
		CharVolumeSetPlane(dp, ci, plane);
		free(buffer);
	}
	CharImageDelete(ci);
	return dp;
}

CharVolume*CharVolumeBPCheapWaterShed(CharVolume*di, int repetitions)
{
	uint32 plane;
	CharImage*ci;
	for ( plane = 0; plane < di->planes; plane++) {
		ci = CharVolumeGetPlane(di, plane);
		CharImageCheapWaterShed(ci, repetitions);
		CharVolumeSetPlane(di, ci, plane);
	}
	CharImageDelete(ci);
	return di;
}

void CharVolumePBreak(CharVolume*di, int repetitions, CharVolume**di_1, CharVolume**di_2)
{
	uint32 column, row, plane;
	*di_1 = NewCharVolume(di->columns, di->rows, repetitions);
	*di_2 = NewCharVolume(di->columns, di->rows, di->planes - repetitions);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(di, di_1, di_2, repetitions) private(column, row, plane)
	for ( column = 0; column < di->columns; column++ ) {
		for ( row = 0; row < di->rows; row++ ) {
			for ( plane = 0; plane < repetitions; plane++) {
				(*di_1)->voxels[column][row][plane] = di->voxels[column][row][plane];
			}
			for ( plane = repetitions; plane < di->planes; plane++) {
				(*di_2)->voxels[column][row][plane - repetitions] = di->voxels[column][row][plane];
			}
		}
	}
}

CharVolume*CharVolumePUnite(CharVolume*di_1, CharVolume*di_2)
{
	uint32 column, row, plane;
	CharVolume*di;
	di = NewCharVolume(di_1->columns, di_1->rows, di_1->planes +  di_2->planes);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(di, di_1, di_2) private(column, row, plane)
	for ( column = 0; column < di->columns; column++ ) {
		for ( row = 0; row < di->rows; row++ ) {
			for ( plane = 0; plane < di_1->planes; plane++) {
				di->voxels[column][row][plane] = di_1->voxels[column][row][plane];
			}
			for ( plane = 0; plane < di_2->planes; plane++) {
				di->voxels[column][row][plane + di_1->planes] = di_2->voxels[column][row][plane];
			}
		}
	}
	return di;
}
