/***************************************************************************
                          Pattern.h  -  description
                             -------------------
    begin                : ðÔÎ íÁÊ 14 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PATTERN_H
#define PATTERN_H

#include <stdio.h>
#include <stdlib.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <float.h>
#include <string.h>

#ifndef MAX_RECORD
#define MAX_RECORD 255
#endif

typedef struct Range {
	double start;
	double end;
} Range;

typedef struct DataPoint {
	int nuc_number;
	double AP_coord;
	double DV_coord;
	double *prot_conc;
} DataPoint;

typedef struct Pattern {
	int nnucs;
	int ngenes;
	int*channel;
	char*gene_ids;
	Range*xrange;
	Range*yrange;
	DataPoint*array;
} Pattern;

Pattern*pattern_new(int nnucs, double xstart, double xend, double ystart, double yend);
void Pattern_delete(Pattern *p);
Range*Range_create(double start,double end);
void Range_delete(Range*p);
void Pattern_write(FILE*fp,Pattern*p);
int com(const void*a,const void*s);
int com_nuc(const void*a,const void*s);
double pattern_get_prot_conc_val(Pattern*p, int label, int channel);
Pattern *Pattern_create(FILE*fp, int channel);

#endif
