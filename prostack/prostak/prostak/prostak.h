/***************************************************************************
                          prostak.h  -  description
                             -------------------
    begin                : ��� ��� 21 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <math.h>
#include <prostak_funcs2d.h>
#include <prostak_funcs3d.h>

#include <prostak_vfuncs.h>

#ifndef PROSTAK_H
#define PROSTAK_H

#ifndef MAX_RECORD
#define MAX_RECORD 255

#endif

#define OPTS ":abhm:o:p:r:s:v"

int ParseCommandLine(int argc, char**argv);

#endif
