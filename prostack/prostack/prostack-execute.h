/***************************************************************************
 *            prostack-execute.h
 *
 *  Ср Март 11 09:51:05 2015
 *  Copyright  2015  kkozlov
 *  <user@host>
 ****************************************************************************/

#ifndef _PROSTACK_EXECUTE_H_
#define _PROSTACK_EXECUTE_H_
#include <Node.h>

gboolean mwdelete( GtkWidget *widget, gpointer data );
void trace_progress(Node*ne);
gboolean toolbar1_run_event( GtkWidget *widget, gpointer data );
gboolean toolbar1_cancel_event( GtkWidget *widget, gpointer data );
gboolean toolbar1_save_event( GtkWidget *widget, gpointer data );
gboolean toolbar1_save_as_event( GtkWidget *widget, gpointer data );
void fileSelectSave();
void bambu_set_title(int saved);
void notify_error(char*msg);
void make_optab();

#endif