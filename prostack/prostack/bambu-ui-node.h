/***************************************************************************
 *            UiNode.h
 *
 *  Fri May 12 14:28:58 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _UINODE_H
#define _UINODE_H

#ifdef __cplusplus
extern "C"
{
#endif

void uiPam(Node*node);
void cleanUiDef();

void uiFile(Node*node, GtkWidget*mw);
void uiSave(Node*node, GtkWidget*mw);
void setMountPoint(char*mountpoint);

void uiOus(Node*node, GtkWidget*mw);
void uiIns(Node*node, GtkWidget*mw);
void uiDisplay(Node*node);
void uiMacro(Node*node);
char*strlasttoken(const char*str, const char delim);
char*strfirsttokens(const char*str, const char delim);
void node_set_defaults(Node*node);

#ifdef __cplusplus
}
#endif

#endif /* _UINODE_H */
