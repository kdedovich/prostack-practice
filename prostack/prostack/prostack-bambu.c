/* This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <bambu-interface.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <gio/gio.h>
#include <bambu-ui-node.h>
#include <Kimono.h>

#include "prostack-dbus-generated.h"

#define OPTS ":hPtv"

int ParseCommandLine(int argc, char**argv);

static Node *scratchNode;
static Node *ctrlNode;
static short isSetLogger = 1;
static short Pegas = 0;
static OlapServer *oss;

static ProStackInterface *pro_stack_interface_proxy;

static const char help[]     =

"Usage: prostack-bambu [options] [workspace]\n\n"

"Argument:\n"
"  [workspace]          input data file\n\n"

"Options:\n"
"  -h                  prints this help message\n"
"  -p <val>            parameter\n"
"  -r <n>              repetitions\n"
"  -s <x,x1,y,y1>      set geometry\n"
"  -v                  print version and compilation date\n"

"Please report bugs to <kozlov@spbcas.ru>. Thank you!\n";

static const char verstring[] =

"compiled by:      %s\n"
"         on:      %s\n"
"      using:      %s\n"
"      flags:      %s\n"
"       date:      %s at %s\n";

static const char usage[]    =
"Usage: komet   [-h]\n"
"  -p <val>            parameter\n"
"  -r <n>              repetitions\n"
"  -s <x,x1,y,y1>      set geometry\n"
"                 [-v]\n";

#define LOCALE_DIR "locale"

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif

int ParseCommandLine(int argc, char**argv)
{
	int         c;                     /* used to parse command line options */
/* external declarations for command line option parsing (unistd.h) */
	extern char *optarg;                     /* command line option argument */
	extern int  optind;                /* pointer to current element of argv */
	extern int  optopt;               /* contain option character upon error */
	optarg = NULL;
	while( (c = getopt(argc, argv, OPTS)) != -1 ) {
		switch(c) {
			case 'h':                                            /* -h help option */
				g_error(help);
			break;
			case 'P':
				Pegas = 1;
			break;
			case 't':
				isSetLogger = 0;
			break;
			case ':':
				g_error("bambu: need an argument for option -%c", optopt);
			break;
			case '?':
			default:
				g_error("bambu: unrecognized option -%c", optopt);
		}
	}

	return optind;
}


void l(char*msg)
{
	fprintf(stdout,"\n %s", msg);
	fflush(stdout);
}

void mod1Node()
{
	gchar **ret;
	gboolean gret;
	GError *error = NULL;
	gret = pro_stack_interface_call_get_node_sync (pro_stack_interface_proxy, &ret, NULL, &error);
	if ( gret == FALSE ) {
		g_warning("Unable to connect to dbus: %s", error->message);
		g_error_free(error);
		ctrlNode->type = file;
		ctrlNode->pam->name = strcpy(ctrlNode->pam->name, "empty");
		setMod1Node(NULL);
	} else {
		if ( !strcmp(ret[0], "pam") ) {
			oss->hname = strcpy(oss->hname, ret[2]);
			oss->port = atoi(ret[3]);
			oss->proto = strcpy(oss->proto, ret[4]);
			oss->address = strcpy(oss->address, ret[5]);
			oss->nicname = strcpy(oss->nicname, ret[6]);
			if ( !strcmp(oss->proto, "rests") ) {
				oss->login = strcpy(oss->login, ret[7]);
				oss->password = strcpy(oss->password, ret[8]);
			}
			ctrlNode->oServer = oss;
			ctrlNode->type = pam;
			ctrlNode->pam->name = strcpy(ctrlNode->pam->name, ret[1]);
		} else if ( !strcmp(ret[0], "display") ) {
			oss->hname = strcpy(oss->hname, ret[3]);
			oss->port = atoi(ret[4]);
			oss->proto = strcpy(oss->proto, ret[5]);
			oss->address = strcpy(oss->address, ret[6]);
			oss->nicname = strcpy(oss->nicname, ret[7]);
			if ( !strcmp(oss->proto, "rests") ) {
				oss->login = strcpy(oss->login, ret[8]);
				oss->password = strcpy(oss->password, ret[9]);
			}
			ctrlNode->oServer = oss;
			ctrlNode->type = display;
			ctrlNode->id = (char*)realloc(ctrlNode->id, MAX_RECORD * sizeof(char));
			ctrlNode->id = strcpy(ctrlNode->id, ret[2]);
		} else if ( !strcmp(ret[0], "file") ) {
			oss->hname = strcpy(oss->hname, ret[1]);
			oss->port = atoi(ret[2]);
			oss->proto = strcpy(oss->proto, ret[3]);
			oss->address = strcpy(oss->address, ret[4]);
			oss->nicname = strcpy(oss->nicname, ret[5]);
			if ( !strcmp(oss->proto, "rests") ) {
				oss->login = strcpy(oss->login, ret[6]);
				oss->password = strcpy(oss->password, ret[7]);
			}
			ctrlNode->oServer = oss;
			ctrlNode->type = file;
			ctrlNode->pam->name = strcpy(ctrlNode->pam->name, "empty");
		}
		g_strfreev(ret);
	}
}

static void close_bambu_signal_handler (GObject *object, GParamSpec *pspec, gpointer user_data);

int init_gdbus_connection()
{
	GError *gerror = NULL;
	pro_stack_interface_proxy = pro_stack_interface_proxy_new_for_bus_sync (
	           G_BUS_TYPE_SESSION,
	           G_DBUS_PROXY_FLAGS_NONE,
				"ru.spbstu.sysbio.ProStack", /* name*/
				"/ru/spbstu/sysbio/ProStack", /* object_path */
				NULL,
				&gerror);
	if ( !pro_stack_interface_proxy ) {
		if ( gerror ) {
			g_warning("Unable to connect to dbus: %s", gerror->message);
			g_error_free(gerror);
		}
		return 0;
	}
	setMod1Node(mod1Node);
	g_signal_connect (pro_stack_interface_proxy, "close-bambu-signal", G_CALLBACK (close_bambu_signal_handler), NULL);
	return 1;
}

static void close_bambu_signal_handler (GObject *object, GParamSpec *pspec, gpointer user_data)
{
	g_print("Received signal and it says\n");
/*	g_idle_add (mwdelete, NULL);*/
	gdk_threads_add_idle (mwdelete, NULL);
}

int main (int argc, char *argv[])
{
	int optind; /* returned by getopt from ParseCommandLine: index of the first non-option argument */
	GtkWidget *window1;
	Workspace*ws;
	int wsWidth;
	int wsHeight;
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, subst_data_dir(LOCALE_DIR));
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	setlocale (LC_ALL, "");
	g_set_application_name ( _("Bambu!"));
	gtk_init (&argc, &argv);
	setError(l);
	setErrorNode(l);
	setErrorOlapServer(l);
	setErrorLon(l);
	setTracer(traceNe);
	setNotifyNode(notifyNode);
	if ( isSetLogger ) {
		setLoggerNode(l);
		setLoggerOlapServer(l);
	}
	kimono_init(&argc, &argv, NULL);
	optind = ParseCommandLine(argc, argv);
	if (!init_gdbus_connection()) {
		g_error("bambu: no kimono-gui");
	}
	oss = newOlapServerV();
	oss->nicname = (char*)calloc(MAX_RECORD, sizeof(char));
	oss->proto = (char*)calloc(MAX_RECORD, sizeof(char));
	oss->address = (char*)calloc(MAX_RECORD, sizeof(char));
	oss->login = (char*)calloc(MAX_RECORD, sizeof(char));
	oss->password = (char*)calloc(MAX_RECORD, sizeof(char));
	ctrlNode = newNode();
	ctrlNode->type = file;
	ctrlNode->pam = newOlapPAM("empty");
	setCtrlNodeInterface(ctrlNode);
	setOuter(disp);
	if ( argc - optind > 1 ) {
		g_error(usage);
	}
	gtk_window_set_default_icon_from_file (subst_data_dir("/icons/gnome/48x48/mimetypes/gnome-mime-application-x-prostack.png"), NULL);
	if ( argc - optind < 1 ) {
		ws = workspaceEmpty();
		wsWidth = 500;
		wsHeight = 400;
		window1 = create_window1 (ws, wsWidth, wsHeight, NULL);
	}
	if ( argc - optind == 1 ) {
		char *scheme;
		char *filename = (char*)NULL;
		scheme = g_uri_parse_scheme((const char*)argv[optind]);
		if ( !scheme && !g_path_is_absolute((const char*)argv[optind]) ) {
			char*gcd = g_get_current_dir();
			filename = g_build_filename(gcd, argv[optind], NULL);
			g_free(gcd);
		} else {
			filename = g_strdup(argv[optind]);
		}
		if ( scheme )
			g_free(scheme);
		ws = workspace_from_file((const char*)filename);
		if ( !ws ) {
			ws = workspaceEmpty();
			wsWidth = 500;
			wsHeight = 400;
		} else {
			getBbWorkspace(ws, &wsWidth, &wsHeight, Pegas);
		}
		if ( filename == NULL ) {
			g_warning("Filename is NULL for argv[%d] %s", optind, argv[optind]);
			scheme = g_uri_parse_scheme((const char*)argv[optind]);
			if ( !scheme && !g_path_is_absolute((const char*)argv[optind]) ) {
				char*gcd = g_get_current_dir();
				filename = g_build_filename(gcd, argv[optind], NULL);
				g_free(gcd);
			} else {
				filename = g_strdup(argv[optind]);
			}
		}
		window1 = create_window1 (ws, wsWidth, wsHeight, filename);
		if ( filename )
			g_free(filename);
	}
	scratchNode = (Node*)NULL;
	gtk_widget_show (window1);
	setError(winError2);
	setErrorNode(winError2);
	setErrorOlapServer(winError2);
	setErrorLon(winError2);
	redrawWorkspace();
	gtk_main ();
	kimono_shutdown();
	return 0;
}
