/***************************************************************************
 *            UiNode.c
 *
 *  Fri May 12 14:28:23 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <math.h>
#include <bambu-interface.h>
#include <Kimono.h>
#include <Workspace.h>
#include <Node.h>
#include <bambu-ui-node.h>

static UiDef *ud;
static Node*ne;

static GtkWidget**labels;
static GtkWidget**eLabels;
static GtkWidget*Caption;
static GtkWidget*Timeout;
static GtkWidget*eTimeout;
static GtkWidget*Lab;
static GtkWidget*eLab;

static char*folder;


/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif


void uiFile(Node*node, GtkWidget*mw)
{
	GtkWidget *eLabel, *eEntry, *eHbox, *dbox;
	GtkDialog *dialog;
	char*filename;
	int flag;
	gint response;
	gchar *omero_browser_name;
	gchar *omero_browser_path = (gchar*)NULL;
	flag = 0;
	filename = node->file;
	omero_browser_name = g_strdup("omero_browser");
	if ( node->type == file ) {
		int pam_type;
		char*pam_id = NULL;
		int use_metaname = 0;
		char*pam_metaname = NULL;
		omero_browser_path = kimono_get_pam_exec (omero_browser_name, -1, &pam_type, &pam_id, use_metaname, &pam_metaname);
		if ( pam_id != NULL ) {
			g_free(pam_id);
		}
	}
	g_free(omero_browser_name);
	if ( omero_browser_path == NULL ) {
		dialog = (GtkDialog *)gtk_file_chooser_dialog_new (_("Select file"), (GtkWindow*)mw, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
	} else {
		dialog = (GtkDialog *)gtk_file_chooser_dialog_new (_("Select file"), (GtkWindow*)mw, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, "network-workgroup", 1, NULL);
	}
	dbox = gtk_dialog_get_content_area (dialog);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	if ( folder ) {
		gtk_file_chooser_set_current_folder_uri( ( GtkFileChooser *)dialog, (const gchar *)folder);
	}
	if ( filename && strlen(filename) > 0 && !g_str_has_prefix((const gchar*)filename, "omero://")) {
		char*scheme = g_uri_parse_scheme(filename);
		if ( !scheme ) {
#ifdef G_OS_WIN32
			filename = g_strdup_printf("file:///%s", filename);
#else
			filename = g_strdup_printf("file://%s", filename);
#endif
		} else {
			g_free(scheme);
		}
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), filename);
	}
	eLabel = gtk_label_new(_("Label"));
	eEntry = gtk_entry_new();
	gtk_widget_show(eLabel);
	gtk_widget_show(eEntry);
	eHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
	gtk_widget_show(eHbox);
	gtk_box_pack_start(GTK_BOX(eHbox), eLabel, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(eHbox), eEntry, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(dbox), eHbox, FALSE, TRUE, 1);
	gtk_entry_set_text( GTK_ENTRY (eEntry), node->label);
	gtk_editable_set_editable(GTK_EDITABLE (eEntry), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eEntry), TRUE);
	response = gtk_dialog_run (dialog);
	if ( response == GTK_RESPONSE_ACCEPT ) {
		char *newfilename;
		char *newfolder;
		newfilename = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
		newfolder = gtk_file_chooser_get_current_folder_uri(GTK_FILE_CHOOSER (dialog));
		if ( newfolder ) {
			if ( folder )
				g_free(folder);
			folder = g_strdup(newfolder);
			g_free(newfolder);
		}
		g_mutex_lock(&(node->statusMutex));
		if ( node->status != waiting )
			flag = 1;
		if ( newfilename ) {
			if ( node->file )
				g_free(node->file);
			node->file = g_strdup(newfilename);
			if ( node->id )
				g_free(node->id);
			node->id = g_path_get_dirname(newfilename);
			g_free(newfilename);
		}
		drawNodeLabel(node, 0);
		node->label = strcpy(node->label, (const char*)gtk_entry_get_text(GTK_ENTRY (eEntry)));
		drawNodeLabel(node, 1);
		g_mutex_unlock(&(node->statusMutex));
		gtk_widget_destroy ((GtkWidget *)dialog);
		bambu_set_title(0);
	} else if ( omero_browser_path != NULL && response == 1 ) {
		GError*gerror = NULL;
		GtkWidget *hLabel, *hEntry, *hHbox;
		GtkWidget *pLabel, *pEntry, *pHbox;
		GtkWidget *lLabel, *lEntry, *lHbox;
		GtkWidget *wLabel, *wEntry, *wHbox;
		gchar*standard_output, **margv;
		char*cmd;
		int flaggs, len, d;
		gint exit_status;
		gtk_widget_destroy ((GtkWidget *)dialog);
		dialog = (GtkDialog *)gtk_dialog_new_with_buttons (_("Enter connection details"), (GtkWindow*)mw, (GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT), "_Cancel", GTK_RESPONSE_CANCEL, "_OK", GTK_RESPONSE_ACCEPT, NULL);
/*host*/
		hLabel = gtk_label_new(_("Host"));
		hEntry = gtk_entry_new();
		gtk_widget_show(hLabel);
		gtk_widget_show(hEntry);
		hHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
		gtk_widget_show(hHbox);
		gtk_box_pack_start(GTK_BOX(hHbox), hLabel, FALSE, TRUE, 1);
		gtk_box_pack_start(GTK_BOX(hHbox), hEntry, FALSE, TRUE, 1);
		dbox = gtk_dialog_get_content_area (dialog);
		gtk_box_pack_start(GTK_BOX(dbox), hHbox, FALSE, TRUE, 1);
		gtk_entry_set_text( GTK_ENTRY (hEntry), "omero.local");
		gtk_editable_set_editable(GTK_EDITABLE (hEntry), TRUE );
		gtk_entry_set_activates_default(GTK_ENTRY (hEntry), TRUE);
/*port*/
		pLabel = gtk_label_new(_("Port"));
		pEntry = gtk_entry_new();
		gtk_widget_show(pLabel);
		gtk_widget_show(pEntry);
		pHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
		gtk_widget_show(pHbox);
		gtk_box_pack_start(GTK_BOX(pHbox), pLabel, FALSE, TRUE, 1);
		gtk_box_pack_start(GTK_BOX(pHbox), pEntry, FALSE, TRUE, 1);
		dbox = gtk_dialog_get_content_area (dialog);
		gtk_box_pack_start(GTK_BOX(dbox), pHbox, FALSE, TRUE, 1);
		gtk_entry_set_text( GTK_ENTRY (pEntry), "4063");
		gtk_editable_set_editable(GTK_EDITABLE (pEntry), TRUE );
		gtk_entry_set_activates_default(GTK_ENTRY (pEntry), TRUE);
/*user*/
		lLabel = gtk_label_new(_("User"));
		lEntry = gtk_entry_new();
		gtk_widget_show(lLabel);
		gtk_widget_show(lEntry);
		lHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
		gtk_widget_show(lHbox);
		gtk_box_pack_start(GTK_BOX(lHbox), lLabel, FALSE, TRUE, 1);
		gtk_box_pack_start(GTK_BOX(lHbox), lEntry, FALSE, TRUE, 1);
		dbox = gtk_dialog_get_content_area (dialog);
		gtk_box_pack_start(GTK_BOX(dbox), lHbox, FALSE, TRUE, 1);
		gtk_entry_set_text( GTK_ENTRY (lEntry), "omerouser");
		gtk_editable_set_editable(GTK_EDITABLE (lEntry), TRUE );
		gtk_entry_set_activates_default(GTK_ENTRY (lEntry), TRUE);
/*pass*/
		wLabel = gtk_label_new(_("Password"));
		wEntry = gtk_entry_new();
		gtk_widget_show(wLabel);
		gtk_widget_show(wEntry);
		wHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
		gtk_widget_show(wHbox);
		gtk_box_pack_start(GTK_BOX(wHbox), wLabel, FALSE, TRUE, 1);
		gtk_box_pack_start(GTK_BOX(wHbox), wEntry, FALSE, TRUE, 1);
		dbox = gtk_dialog_get_content_area (dialog);
		gtk_box_pack_start(GTK_BOX(dbox), wHbox, FALSE, TRUE, 1);
		gtk_entry_set_text( GTK_ENTRY (wEntry), "omerouser");
		gtk_editable_set_editable(GTK_EDITABLE (wEntry), TRUE );
		gtk_entry_set_activates_default(GTK_ENTRY (wEntry), TRUE);
		gtk_entry_set_visibility (GTK_ENTRY (wEntry), FALSE);
		gtk_dialog_set_default_response(dialog, GTK_RESPONSE_ACCEPT);
		if ( gtk_dialog_run (dialog) == GTK_RESPONSE_ACCEPT ) {
			gchar*host = g_strdup((const char*)gtk_entry_get_text(GTK_ENTRY (hEntry)));
			gchar*port = g_strdup((const char*)gtk_entry_get_text(GTK_ENTRY (pEntry)));
			gchar*user = g_strdup((const char*)gtk_entry_get_text(GTK_ENTRY (lEntry)));
			gchar*pass = g_strdup((const char*)gtk_entry_get_text(GTK_ENTRY (wEntry)));
			guint ml;
			if ( filename && strlen(filename) > 0 && g_str_has_prefix((const gchar*)filename, "omero://") ) {
				margv = g_strsplit(filename, "/", MAX_RECORD);
				ml = g_strv_length(margv);
				if ( margv && ml > 2 ) {
					cmd = g_strdup_printf("%s %s %s %s %s %s %s %s", omero_browser_path, host, port, user, pass, margv[ml - 3], margv[ml - 2], margv[ml - 1]);
				}
				if ( margv ) {
					g_strfreev(margv);
				}
			} else {
				cmd = g_strdup_printf("%s %s %s %s %s", omero_browser_path, host, port, user, pass);
			}
			g_free(host);
			g_free(port);
			g_free(user);
			g_free(pass);
			gtk_widget_destroy ((GtkWidget *)dialog);
			if ( !g_shell_parse_argv(cmd, &len, &margv, &gerror) ) {
				if ( gerror ) {
					g_warning("g_shell_parse failed for %s\nwith %s\at UiNode.c:234", cmd, gerror->message);
					g_error_free(gerror);
				}
			} else {
				for ( d = 0; margv[d]; d++)
					g_print("omero_browser arg:%d %s\n", d, margv[d]);
				flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDERR_TO_DEV_NULL;
/*				flaggs = G_SPAWN_STDERR_TO_DEV_NULL;*/
				if ( !g_spawn_sync((const gchar*)NULL, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, &standard_output, NULL, &exit_status, &gerror) ) {
					if ( gerror ) {
						g_warning("g_spawn_sync failed on %s with %s\nat UiNode.c:243", cmd, gerror->message);
						g_error_free(gerror);
					}
				} else {
					g_strfreev(margv);
					if ( !g_shell_parse_argv(standard_output, &len, &margv, &gerror) ) {
						if ( gerror ) {
							g_warning("g_shell_parse failed for %s\nwith %s\at UiNode.c:250", standard_output, gerror->message);
							g_error_free(gerror);
						}
					} else {
						for ( d = 0; margv[d]; d++)
							g_print("omero_browser output:%d %s\n", d, margv[d]);
						if ( len == 2 && g_str_has_prefix((const gchar*)margv[0], "OK" ) ) {
							g_mutex_lock(&(node->statusMutex));
							if ( node->status != waiting )
								flag = 1;
							if ( node->file )
								g_free(node->file);
							node->file = g_strdup_printf("omero://%s", margv[1]);
							if ( node->id )
								g_free(node->id);
							node->id = g_path_get_dirname(margv[1]);
							g_mutex_unlock(&(node->statusMutex));
							bambu_set_title(0);
						} else if ( len >= 2 && g_str_has_prefix((const gchar*)margv[0], "ERROR" ) ) {
							dialog = (GtkDialog *)gtk_message_dialog_new ((GtkWindow*)mw,
                                 GTK_DIALOG_DESTROY_WITH_PARENT,
                                 GTK_MESSAGE_ERROR,
                                 GTK_BUTTONS_CLOSE,
                                 standard_output);
							gtk_dialog_run (dialog);
							gtk_widget_destroy ((GtkWidget*)dialog);
						}
						g_strfreev(margv);
					}
					if (standard_output)
						g_free(standard_output);
				}
			}
			g_free(cmd);
			g_free(omero_browser_path);
		} else {
			gtk_widget_destroy ((GtkWidget *)dialog);
		}
	} else {
		gtk_widget_destroy ((GtkWidget *)dialog);
	}
	if ( flag ) undoDownSrtream(node);
}

void uiSave(Node*node, GtkWidget*mw)
{
	GtkWidget *eLabel, *eEntry, *eHbox, *dbox;
	GtkDialog *dialog;
	char*filename;
	int flag;
	gint response;
	flag = 0;
	filename = node->id;
	dialog = (GtkDialog *)gtk_file_chooser_dialog_new (_("Select file"), (GtkWindow*)mw, GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel", GTK_RESPONSE_CANCEL, "Save _As", GTK_RESPONSE_ACCEPT, NULL);
	dbox = gtk_dialog_get_content_area (dialog);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	if ( folder ) {
		gtk_file_chooser_set_current_folder_uri( ( GtkFileChooser *)dialog, (const gchar *)folder);
	}
	if ( filename && strlen(filename) > 0 && !g_str_has_prefix((const gchar*)filename, "omero://")) {
		char*scheme = g_uri_parse_scheme(filename);
		char*fn;
		if ( !scheme ) {
#ifdef G_OS_WIN32
			filename = g_strdup_printf("file:///%s", filename);
#else
			filename = g_strdup_printf("file://%s", filename);
#endif
		} else {
			g_free(scheme);
		}
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), filename);
		fn = g_path_get_basename(filename);
		gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER (dialog), fn);
		g_free(fn);
	}
	eLabel = gtk_label_new(_("Label"));
	eEntry = gtk_entry_new();
	gtk_widget_show(eLabel);
	gtk_widget_show(eEntry);
	eHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
	gtk_widget_show(eHbox);
	gtk_box_pack_start(GTK_BOX(eHbox), eLabel, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(eHbox), eEntry, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(dbox), eHbox, FALSE, TRUE, 1);
	gtk_entry_set_text( GTK_ENTRY (eEntry), node->label);
	gtk_editable_set_editable(GTK_EDITABLE (eEntry), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eEntry), TRUE);
	response = gtk_dialog_run (dialog);
	if ( response == GTK_RESPONSE_ACCEPT ) {
		char *newfilename;
		char *newfolder;
		newfilename = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
		newfolder = gtk_file_chooser_get_current_folder_uri(GTK_FILE_CHOOSER (dialog));
		if ( newfolder ) {
			if ( folder )
				g_free(folder);
			folder = g_strdup(newfolder);
			g_free(newfolder);
		}
		g_mutex_lock(&(node->statusMutex));
		if ( node->status != waiting )
			flag = 1;
		if ( newfilename ) {
			if ( node->id )
				g_free(node->id);
			node->id = g_strdup(newfilename);
			g_free(newfilename);
		}
		drawNodeLabel(node, 0);
		node->label = strcpy(node->label, (const char*)gtk_entry_get_text(GTK_ENTRY (eEntry)));
		drawNodeLabel(node, 1);
		g_mutex_unlock(&(node->statusMutex));
		gtk_widget_destroy ((GtkWidget *)dialog);
		bambu_set_title(0);
	} else {
		gtk_widget_destroy ((GtkWidget *)dialog);
	}
	if ( flag ) undoDownSrtream(node);
}

void uiIns(Node*node, GtkWidget*mw)
{
	GtkWidget *eLabel, *eEntry, *eHbox, *dbox;
	GtkDialog *dialog;
	char*filename;
	int flag;
	filename = (char*)NULL;
	ne = node;
	flag = 0;
	dialog = (GtkDialog *)gtk_file_chooser_dialog_new (_("Select file"), (GtkWindow*)mw, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
	dbox = gtk_dialog_get_content_area (dialog);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	if ( folder )
		gtk_file_chooser_set_current_folder_uri( ( GtkFileChooser *)dialog, (const gchar *)folder);
	if ( ne->file && strlen(ne->file) > 0 ) {
		char*scheme = g_uri_parse_scheme(ne->file);
		if ( !scheme ) {
#ifdef G_OS_WIN32
			filename = g_strdup_printf("file:///%s", ne->file);
#else
			filename = g_strdup_printf("file://%s", ne->file);
#endif
		} else {
			filename = g_strdup(ne->file);
			g_free(scheme);
		}
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), filename);
		g_free(filename);
	}
	eLabel = gtk_label_new(_("Label"));
	eEntry = gtk_entry_new();
	gtk_widget_show(eLabel);
	gtk_widget_show(eEntry);
	eHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
	gtk_widget_show(eHbox);
	gtk_box_pack_start(GTK_BOX(eHbox), eLabel, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(eHbox), eEntry, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(dbox), eHbox, FALSE, TRUE, 1);
	gtk_entry_set_text( GTK_ENTRY (eEntry), ne->label);
	gtk_editable_set_editable(GTK_EDITABLE (eEntry), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eEntry), TRUE);
	if (gtk_dialog_run (dialog) == GTK_RESPONSE_ACCEPT) {
		char *newfilename;
		char *newfolder;
		newfilename = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
		newfolder = gtk_file_chooser_get_current_folder_uri(GTK_FILE_CHOOSER (dialog));
		if ( newfolder ) {
			if ( folder )
				g_free(folder);
			folder = g_strdup(newfolder);
			g_free(newfolder);
		}
		g_mutex_lock(&(ne->statusMutex));
		if ( ne->status != waiting )
			flag = 1;
		if ( newfilename ) {
			if ( ne->file )
				g_free(ne->file);
			ne->file = g_strdup(newfilename);
			g_free(newfilename);
		}
		drawNodeLabel(ne, 0);
		ne->label = strcpy(ne->label, (const char*)gtk_entry_get_text(GTK_ENTRY (eEntry)));
		drawNodeLabel(ne, 1);
		g_mutex_unlock(&(ne->statusMutex));
		bambu_set_title(0);
	}
	gtk_widget_destroy ((GtkWidget *)dialog);
	if ( flag ) undoDownSrtream(ne);
}

void uiOus(Node*node, GtkWidget*mw)
{
	GtkWidget *eLabel, *eEntry, *eHbox, *dbox;
	GtkDialog *dialog;
	char*filename;
	filename = (char*)NULL;
	ne = node;
	filename = ne->file;
	dialog = (GtkDialog *)gtk_file_chooser_dialog_new (_("Select file"),
				(GtkWindow*)mw,
				GTK_FILE_CHOOSER_ACTION_SAVE,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Save", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	dbox = gtk_dialog_get_content_area (dialog);
	if ( folder )
		gtk_file_chooser_set_current_folder_uri( ( GtkFileChooser *)dialog, (const gchar *)folder);
	if ( filename && strlen(filename) > 0 ) {
		char*scheme = g_uri_parse_scheme(filename);
		char*fn;
		if ( !scheme ) {
#ifdef G_OS_WIN32
			filename = g_strdup_printf("file:///%s", filename);
#else
			filename = g_strdup_printf("file://%s", filename);
#endif
		} else {
			g_free(scheme);
		}
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), filename);
		fn = g_path_get_basename(filename);
		gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER (dialog), fn);
		g_free(fn);
	}
	eLabel = gtk_label_new(_("Label"));
	eEntry = gtk_entry_new();
	gtk_widget_show(eLabel);
	gtk_widget_show(eEntry);
	eHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
	gtk_widget_show(eHbox);
	gtk_box_pack_start(GTK_BOX(eHbox), eLabel, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(eHbox), eEntry, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(dbox), eHbox, FALSE, TRUE, 1);
	gtk_entry_set_text( GTK_ENTRY (eEntry), ne->label);
	gtk_editable_set_editable(GTK_EDITABLE (eEntry), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eEntry), TRUE);
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *newfilename;
		char *newfolder;
		newfilename = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
		newfolder = gtk_file_chooser_get_current_folder_uri(GTK_FILE_CHOOSER (dialog));
		if ( newfolder ) {
			if ( folder )
				g_free(folder);
			folder = g_strdup(newfolder);
			g_free(newfolder);
		}
		g_mutex_lock(&(ne->statusMutex));
		if ( newfilename ) {
			if ( ne->file )
				g_free(ne->file);
			ne->file = g_strdup(newfilename);
			g_free(newfilename);
		}
		drawNodeLabel(ne, 0);
		ne->label = strcpy(ne->label, (const char*)gtk_entry_get_text(GTK_ENTRY (eEntry)));
		drawNodeLabel(ne, 1);
		g_mutex_unlock(&(ne->statusMutex));
		bambu_set_title(0);
	}
	gtk_widget_destroy ((GtkWidget *)dialog);
}

void uiPam(Node*node)
{
	int i, j, need_undo;
	int length, static_length;
	GtkWidget*dialog;
	GtkWidget *table;
	GtkWidget *dialog_vbox3;
	GtkWidget *scrolledwindow2;
	GtkWidget *viewport2;
	GtkWidget *use_meta_label, *use_meta_toggle;
	char*pattern;
	char*sample;
	gchar*dialog_title;
	dialog_title = g_strdup_printf("%s [%d]", node->pam->name, node->ID);
	dialog = create_option_dialog (dialog_title);
	dialog_vbox3 = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
	gtk_widget_show (dialog_vbox3);
	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy  ( ( GtkScrolledWindow *)scrolledwindow2,
                                             GTK_POLICY_AUTOMATIC,
                                             GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolledwindow2);
	gtk_box_pack_start (GTK_BOX (dialog_vbox3), scrolledwindow2, TRUE, TRUE, 0);
	viewport2 = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport2);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), viewport2);
	ne = node;
	ud = getUiDef(ne);
	static_length = 2;
	length = static_length + 1;
	if ( ud ) {
		length += ud->length;
	}
	table = gtk_grid_new ();
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (viewport2), table);
	Timeout = gtk_label_new(_("Timeout"));
	gtk_widget_show (Timeout);
	gtk_grid_attach (GTK_GRID (table), Timeout, 0, 0, 1, 1);
	eTimeout = gtk_entry_new();
	sample = (char*)calloc(MAX_RECORD, sizeof(char));
	sprintf(sample, "%d", ne->timeDelay);
	gtk_entry_set_text( GTK_ENTRY (eTimeout), sample);
	free(sample);
	gtk_editable_set_editable(GTK_EDITABLE (eTimeout), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eTimeout), TRUE);
	gtk_widget_show (eTimeout);
	gtk_grid_attach (GTK_GRID (table), eTimeout, 1, 0, 1, 1);
	Lab = gtk_label_new(_("Label"));
	gtk_widget_show (Lab);
	gtk_grid_attach (GTK_GRID (table), Lab, 0, 1, 1, 1);
	eLab = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY (eLab), ne->label );
	gtk_editable_set_editable(GTK_EDITABLE (eLab), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eLab), TRUE);
	gtk_widget_show (eLab);
	gtk_grid_attach (GTK_GRID (table), eLab, 1, 1, 1, 1);
	if ( ud ) {
		labels = (GtkWidget**)calloc(ud->length, sizeof(GtkWidget*));
		eLabels = (GtkWidget**)calloc(ud->length, sizeof(GtkWidget*));
		pattern = (char*)calloc(MAX_RECORD, sizeof(char));
		for( i = 0; i < ud->length; i++ ) {
			sprintf(pattern, "$%d", i + 1);
			sample = outstrsubst(node->id, ud->format, pattern);
			labels[i] = gtk_label_new(ud->opts[i].label);
			gtk_widget_show (labels[i]);
			gtk_grid_attach (GTK_GRID (table), labels[i], 0, static_length + i, 1, 1);
			if ( !strcmp(ud->opts[i].type, "bool") ) {
				eLabels[i] = gtk_check_button_new();
				gtk_widget_show (eLabels[i]);
				gtk_grid_attach (GTK_GRID (table), eLabels[i], 1, static_length + i, 1, 1);
				if ( sample && !strcmp(sample, ud->opts[i].value[0]) ) {
					gtk_toggle_button_set_active((GtkToggleButton *)eLabels[i], TRUE);
				} else {
					gtk_toggle_button_set_active((GtkToggleButton *)eLabels[i], FALSE);
				}
			} else if  ( !strcmp(ud->opts[i].type, "choice") ) {
				eLabels[i] = gtk_combo_box_text_new();
				for ( j = 0; j < ud->opts[i].lval; j++ ) {
					gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT(eLabels[i]), ud->opts[i].value[j]);
					if ( sample && g_strcmp0 (sample, ud->opts[i].value[j]) == 0) {
						gtk_combo_box_set_active (GTK_COMBO_BOX(eLabels[i]) ,j);
					}
				}
				gtk_widget_show (eLabels[i]);
				gtk_grid_attach (GTK_GRID (table), eLabels[i], 1, static_length + i, 1, 1);
			} else {
				eLabels[i] = gtk_entry_new();
				if ( !sample )
					gtk_entry_set_text( GTK_ENTRY (eLabels[i]), (const gchar *)ud->opts[i].value[0] );
				else
					gtk_entry_set_text( GTK_ENTRY (eLabels[i]), (const gchar *)sample );
				gtk_editable_set_editable(GTK_EDITABLE (eLabels[i]), TRUE );
				gtk_entry_set_activates_default(GTK_ENTRY (eLabels[i]), TRUE);
				gtk_widget_show (eLabels[i]);
				gtk_grid_attach (GTK_GRID (table), eLabels[i], 1, static_length + i, 1, 1);
			}
			if ( sample )
				free(sample);
		}
		free(pattern);
	}
	use_meta_label = gtk_label_new(_("Autoadjust"));
	gtk_widget_show (use_meta_label);
	gtk_grid_attach (GTK_GRID (table), use_meta_label, 0, length - 1, 1, 1);
	use_meta_toggle = gtk_check_button_new();
	gtk_widget_show (use_meta_toggle);
	gtk_grid_attach (GTK_GRID (table), use_meta_toggle, 1, length - 1, 1, 1);
	if ( ne->use_metaname == 1 ) {
		gtk_toggle_button_set_active((GtkToggleButton *)use_meta_toggle, TRUE);
	} else {
		gtk_toggle_button_set_active((GtkToggleButton *)use_meta_toggle, FALSE);
	}
	need_undo = 0;
	if ( gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT ) {
		g_mutex_lock(&(ne->statusMutex));
		if ( ne->status != waiting ) {
			need_undo = 1;
		}
		if ( gtk_toggle_button_get_active((GtkToggleButton *)use_meta_toggle) ) {
			ne->use_metaname = 1;
		} else {
			ne->use_metaname = 0;
		}
		ne->timeDelay = atoi((char*)gtk_entry_get_text(GTK_ENTRY ( eTimeout )));
		drawNodeLabel(ne, 0);
		ne->label = strcpy(ne->label, (const char*)gtk_entry_get_text(GTK_ENTRY (eLab)));
		drawNodeLabel(ne, 1);
		if ( ud ) {
			if ( ne->id )
				free( ne->id );
			ne->id = (char*)calloc(MAX_RECORD, sizeof(char));
			ne->id = strcpy(ne->id, ud->format);
			ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
			pattern = (char*)calloc(MAX_RECORD, sizeof(char));
			for( i = 0; i < ud->length; i++ ) {
				sprintf(pattern, "$%d", i + 1);
				if ( !strcmp(ud->opts[i].type, "bool") ) {
					if ( gtk_toggle_button_get_active((GtkToggleButton *)eLabels[i]) )
						sample = ud->opts[i].value[0];
					else
						sample = ud->opts[i].value[1];
				} else if  ( !strcmp(ud->opts[i].type, "choice") ) {
					sample = (char*)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(eLabels[i]));
				} else {
					sample = (char*)gtk_entry_get_text(GTK_ENTRY (eLabels[i]));
				}
				g_strstrip(sample);
				instrsubst(ne->id, pattern, sample);
			}
			g_mutex_unlock(&(ne->statusMutex));
			free(pattern);
			deleteUiDef(ud);
			free(eLabels);
			free(labels);
		} else {
			g_mutex_unlock(&(ne->statusMutex));
		}
		bambu_set_title(0);
	} else {
		if ( ud ) {
			deleteUiDef(ud);
			free(eLabels);
			free(labels);
		}
	}
	gtk_widget_destroy(dialog);
	if ( need_undo ) undoDownSrtream(ne);
}

void uiDisplay(Node*node)
{
	int need_undo;
	int length;
	GtkWidget*dialog;
	GtkWidget *table;
	GtkWidget *dialog_vbox3;
	GtkWidget *scrolledwindow2;
	GtkWidget *viewport2;
	char*sample;
	dialog = create_option_dialog (_("display"));
	dialog_vbox3 = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
	gtk_widget_show (dialog_vbox3);
	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy  ( ( GtkScrolledWindow *)scrolledwindow2,
                                             GTK_POLICY_AUTOMATIC,
                                             GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolledwindow2);
	gtk_box_pack_start (GTK_BOX (dialog_vbox3), scrolledwindow2, TRUE, TRUE, 0);
	viewport2 = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport2);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), viewport2);
	ne = node;
	length = 3;
	table = gtk_grid_new ();
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (viewport2), table);
	Caption = gtk_label_new(_("Options"));
	gtk_widget_show (Caption);
	gtk_grid_attach (GTK_GRID (table), Caption, 0, 0, 2, 1);
	Timeout = gtk_label_new(_("Timeout"));
	gtk_widget_show (Timeout);
	gtk_grid_attach (GTK_GRID (table), Timeout, 0, 1, 1, 1);
	eTimeout = gtk_entry_new();
	sample = (char*)calloc(MAX_RECORD, sizeof(char));
	sprintf(sample, "%d", ne->timeDelay);
	gtk_entry_set_text( GTK_ENTRY (eTimeout), sample);
	free(sample);
	gtk_editable_set_editable(GTK_EDITABLE (eTimeout), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eTimeout), TRUE);
	gtk_widget_show (eTimeout);
	gtk_grid_attach (GTK_GRID (table), eTimeout, 1, 1, 1, 1);
	Lab = gtk_label_new(_("Label"));
	gtk_widget_show (Lab);
	gtk_grid_attach (GTK_GRID (table), Lab, 0, 2, 1, 1);
	eLab = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY (eLab), ne->label );
	gtk_editable_set_editable(GTK_EDITABLE (eLab), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eLab), TRUE);
	gtk_widget_show (eLab);
	gtk_grid_attach (GTK_GRID (table), eLab, 1, 2, 1, 1);
	if ( gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT ) {
		g_mutex_lock(&(ne->statusMutex));
		if ( ne->status != waiting )
			need_undo = 1;
		ne->timeDelay = atoi((char*)gtk_entry_get_text(GTK_ENTRY ( eTimeout )));
		drawNodeLabel(ne, 0);
		ne->label = strcpy(ne->label, (const char*)gtk_entry_get_text(GTK_ENTRY (eLab)));
		drawNodeLabel(ne, 1);
		g_mutex_unlock(&(ne->statusMutex));
		bambu_set_title(0);
	}
	gtk_widget_destroy(dialog);
	if ( need_undo ) undoDownSrtream(ne);
}

void uiMacro(Node*node)
{
	int length;
	GtkWidget*dialog;
	GtkWidget *table;
	GtkWidget *dialog_vbox3;
	GtkWidget *scrolledwindow2;
	GtkWidget *viewport2;
	dialog = create_option_dialog (_("macro"));
	dialog_vbox3 = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
	gtk_widget_show (dialog_vbox3);
	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy  ( ( GtkScrolledWindow *)scrolledwindow2,
                                             GTK_POLICY_AUTOMATIC,
                                             GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolledwindow2);
	gtk_box_pack_start (GTK_BOX (dialog_vbox3), scrolledwindow2, TRUE, TRUE, 0);
	viewport2 = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport2);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), viewport2);
	ne = node;
	length = 1;
	table = gtk_grid_new ();
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (viewport2), table);
	Caption = gtk_label_new(_("Options"));
	gtk_widget_show (Caption);
	gtk_grid_attach (GTK_GRID (table), Caption, 0, 0, 2, 1);
	Lab = gtk_label_new(_("Label"));
	gtk_widget_show (Lab);
	gtk_grid_attach (GTK_GRID (table), Lab, 0, 1, 1, 1);
	eLab = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY (eLab), ne->label );
	gtk_editable_set_editable(GTK_EDITABLE (eLab), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eLab), TRUE);
	gtk_widget_show (eLab);
	gtk_grid_attach (GTK_GRID (table), eLab, 1, 1, 1, 1);
	if ( gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT ) {
		g_mutex_lock(&(ne->statusMutex));
		drawNodeLabel(ne, 0);
		ne->label = strcpy(ne->label, (const char*)gtk_entry_get_text(GTK_ENTRY (eLab)));
		drawNodeLabel(ne, 1);
		g_mutex_unlock(&(ne->statusMutex));
		bambu_set_title(0);
	}
	gtk_widget_destroy(dialog);
}


char*strlasttoken(const char*str, const char delim)
{
	char*lasttoken;
	int i, j, k, l;
	if ( !str )
		g_error("Empty string in strlasttoken!\n");
	i = strlen(str);
	l = 0;
	for( j = i; j > 0; j--) {
		if ( str[j] == delim ) {
			k = j;
			l = 1;
			break;
		}
	}
	if ( l )
		k++;
	else
		k = 0;
	if ( k > i ) {
		lasttoken = (char*)calloc(MAX_RECORD, sizeof(char));
		lasttoken = strcpy(lasttoken, "");
	} else {
		lasttoken = (char*)calloc(MAX_RECORD, sizeof(char));
		lasttoken = strcpy(lasttoken, &str[k]);
	}
	return lasttoken;
}

char*strfirsttokens(const char*str, const char delim)
{
	char*firsttoken;
	int i, j, k, l;
	if ( !str )
		g_error("Empty string in strlasttoken!\n");
	i = strlen(str);
	l = 0;
	k = i - 1;
	for( j = i - 1; j >= 0; j--) {
		if ( str[j] == delim ) {
			k = j;
			l = 1;
			break;
		}
	}
	if ( l )
		k--;
	if ( k < 0 ) {
		firsttoken = (char*)calloc(MAX_RECORD, sizeof(char));
		firsttoken = strcpy(firsttoken, "");
	} else {
		firsttoken = (char*)calloc(MAX_RECORD, sizeof(char));
		firsttoken = strncpy(firsttoken, str, k + 1);
	}
	return firsttoken;
}
