/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin Kozlov 2009-2013 <kkozlov@prostack.ru>
 *
 * main.c is free software.
 *
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * main.c is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with main.c.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <config.h>
#include <glib.h>
#include <gio/gio.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <ctype.h>

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif

/* For testing propose use the local (not installed) glade file */
#define GTK_BUILDER_FILE "/prostack/glade/prostack.glade"
#define KIMONO_DOCS_FILE "/prostack/html/ProStackUserGuide4.html"
#define KIMONO_EXAMPLES_DIR "/prostack/examples"
#define LOCALE_DIR "locale"

#include "prostack-dbus-generated.h"

#include <Workspace.h>
#include <OlapServer.h>
#include <Node.h>
#include <Kimono.h>

/*
 For ShellExecute:
*/
#ifdef G_OS_WIN32
#include <windows.h>
#endif

static GString *gumethod;
static GString *gudisplay;
static GHashTable *Displays;
static int what_tree;
static OlapServer*os;
static GList*servers;
static GtkWidget*combobox1;

//static KimonoObject *obj;
static ProStackInterface *pro_stack_interface;
static gboolean is_master = TRUE;

gboolean kimono_gui_start_bambu();

static gboolean pro_stack_interface_get_node (ProStackInterface *obj, GDBusMethodInvocation  *invocation, gpointer user_data)
{
	gchar **response;
	switch (what_tree) {
		case -1:
			response = g_new (gchar *, 9);
			response[0] = g_strdup ("file");
			response[1] = g_strdup (os->hname);
			response[2] = g_strdup_printf("%d", os->port);
			response[3] = g_strdup (os->proto);
			response[4] = g_strdup (os->address);
			response[5] = g_strdup (os->nicname);
			if ( !strcmp(os->proto, "rests") ) {
				response[6] = g_strdup (os->login);
				response[7] = g_strdup (os->password);
			} else {
				response[6] = g_strdup ("");
				response[7] = g_strdup ("");
			}
			response[8] = NULL;
		break;
		case 0:
			response = g_new (gchar *, 10);
			response[0] = g_strdup ("pam");
			response[1] = g_strdup (gumethod->str);
			response[2] = g_strdup (os->hname);
			response[3] = g_strdup_printf("%d", os->port);
			response[4] = g_strdup (os->proto);
			response[5] = g_strdup (os->address);
			response[6] = g_strdup (os->nicname);
			if ( !strcmp(os->proto, "rests") ) {
				response[7] = g_strdup (os->login);
				response[8] = g_strdup (os->password);
			} else {
				response[7] = g_strdup ("");
				response[8] = g_strdup ("");
			}
			response[9] = NULL;
		break;
		case 1:
			response = g_new (gchar *, 11);
			response[0] = g_strdup ("display");
			response[1] = g_strdup (gudisplay->str);
			response[2] = g_strdup ((const gchar*)g_hash_table_lookup(Displays, gudisplay->str));
			response[3] = g_strdup (os->hname);
			response[4] = g_strdup_printf("%d", os->port);
			response[5] = g_strdup (os->proto);
			response[6] = g_strdup (os->address);
			response[7] = g_strdup (os->nicname);
			if ( !strcmp(os->proto, "rests") ) {
				response[8] = g_strdup (os->login);
				response[9] = g_strdup (os->password);
			} else {
				response[8] = g_strdup ("");
				response[9] = g_strdup ("");
			}
			response[10] = NULL;
		break;
	}
	pro_stack_interface_complete_get_node(obj, invocation, (const gchar *const *)response);
	g_strfreev(response);
	return TRUE;
}

gboolean pro_stack_interface_get_display (ProStackInterface *obj, GDBusMethodInvocation  *invocation, gpointer user_data)
{
	gchar **response;
	response = g_new (gchar *, 3);
	response[0] = g_strdup (gudisplay->str);
	response[1] = g_strdup ((const gchar*)g_hash_table_lookup(Displays, gudisplay->str));
	response[2] = NULL;
	pro_stack_interface_complete_get_display(obj, invocation, (const gchar *const *)response);
	g_strfreev(response);
	return TRUE;
}


#define MAXCHARS 255

static GtkBuilder *gtkbuild;
static GtkWidget *kimdow;
static GtkWidget *kimsplash;
static GtkTreeStore *kim_store;

void fetch_servers();
void put_servers(OlapServer*os, GtkComboBox*cb);
void servers_remove_default(OlapServer*los, gpointer data);
void put_hierarhy(OlapServer*os);
void make_interface();
void make_menu();
static void on_new_clicked(GtkWidget *widget, gpointer data);
gboolean kim_fetch_func(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, OlapServer*os);
gboolean kim_find_func(GtkTreeModel *model, GtkTreePath *gpath, GtkTreeIter *iter, char*key);

/* Prototype for selection handler callback */
static void tree_selection_changed_cb (GtkTreeSelection *selection, gpointer data);

/* Setup the selection handler */
static GtkTreeSelection *kim_select;

static GtkTreeSelection *kim_select_pam;
static void pam_selection_changed_cb (GtkTreeSelection *selection, gpointer data);

static void button1_clicked_cb (GtkButton *button, gpointer data);
static void button2_clicked_cb (GtkButton *button, gpointer data);

void put_displays();
void make_displays();

static GtkTreeSelection *kim_select_display;
static void display_selection_changed_cb (GtkTreeSelection *selection, gpointer data);

static void server_selection_changed_cb (GtkComboBox *box, gpointer data);
int server_find_nicname(OlapServer*los, char*nicname);

static char*current_folder;
static char*current_file;
void run_bambu(char*filename, int log, char*current);
void run_komet(char*filename, int log, char*current);
void run_komet_batch(char*filename, int log, char*current, char*extra_args);

/*static GList*bambus;*/
void kill_bambu(GPid*pid, gpointer data);
static gboolean on_all_close();

GtkWidget*create_aboutdialog(void);
void on_about1_activate(GtkMenuItem *menuitem, gpointer user_data);
static gboolean button_press_event( GtkTreeView *tree_view, GdkEventButton *event );

gboolean on_guide_activate(GtkMenuItem *menuitem, gpointer user_data);
gboolean on_examples_activate(GtkMenuItem *menuitem, gpointer user_data);
gboolean add_operations();
gboolean on_add_oper_apfile_change();
gboolean add_oper_radio_toggled();
void kimono_gui_add_operation(gchar*Name, gchar*metaname, int metaweight, gchar*Executable, int type, gchar*uidescr, gchar*message, gchar*inputs, gchar*outputs, gchar*ostring, char*str);
/*gint recent_compare_func (gconstpointer a, gconstpointer b);*/
static char*email = NULL;
static int config_changed;

GtkWidget*create_main_window (void)
{
	GtkWidget *window;
	GtkWidget *w;
	GError*gerror = NULL;
	gtkbuild = gtk_builder_new ();
	if ( !gtk_builder_add_from_file (gtkbuild, subst_data_dir(GTK_BUILDER_FILE), &gerror)) {
		g_warning ("Couldn't load %s builder file: %s", GTK_BUILDER_FILE, gerror->message);
		g_error_free (gerror);
		gerror = NULL;
	}
/* This is important */
	gtk_builder_connect_signals (gtkbuild, NULL);
	window = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
	g_signal_connect(G_OBJECT (window), "delete_event", G_CALLBACK (on_all_close), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "button1"));
	g_signal_connect (G_OBJECT (w), "clicked", G_CALLBACK (button1_clicked_cb), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "button2"));
	g_signal_connect (G_OBJECT (w), "clicked", G_CALLBACK (button2_clicked_cb), NULL);
	return window;
}
/*
gint recent_compare_func (gconstpointer a, gconstpointer b)
{
	GtkRecentInfo*ra = (GtkRecentInfo*)a;
	GtkRecentInfo*rb = (GtkRecentInfo*)b;
	time_t ta, tb;
	ta = gtk_recent_info_get_visited (ra);
	tb = gtk_recent_info_get_visited (rb);
	if ( ta < tb )
		return -1;
	else if ( ta > tb )
		return 1;
	return 0;
}
*/
/*
void add_to_recent(char *uri)
{
	GtkRecentManager *manager;
	GtkRecentData *data;
	data = (GtkRecentData *)g_new(GtkRecentData, 1);
	data->display_name = NULL;
//	a UTF-8 encoded string, containing the name of the recently used resource to be displayed, or NULL;
	data->description = NULL;
//	a UTF-8 encoded string, containing a short description of the resource, or NULL;
	data->mime_type = g_strdup("application/x-prostack");
//	the MIME type of the resource;
	data->app_name = g_strdup("prostack");
//	the name of the application that is registering this recently used resource;
	data->app_exec = g_strdup("prostack %f");
//	command line used to launch this resource; may contain the "%f" and "%u" escape characters which will be expanded to the resource file path and URI respectively when the command line is retrieved;
	data->groups = (char**)g_new(char*, 2);
//	a vector of strings containing groups names;
	data->groups[0] = g_strdup("prostack");
	data->groups[1] = NULL;
	data->is_private = FALSE;
//	whether this resource should be displayed only by the applications that have registered it or not.
	manager = gtk_recent_manager_get_default ();
	if ( !gtk_recent_manager_has_item(manager, (const gchar*)uri) ) {
		gint maxritems;
		gint nritems;
		GList*ritems = NULL;
/*		maxritems = gtk_recent_manager_get_limit (manager);*/
/* FIXME
		maxritems = 10;
		ritems = gtk_recent_manager_get_items (manager);
		if ( ritems != NULL ) {
			nritems = g_list_length (ritems);
			if ( maxritems > 0 && nritems > 0 && nritems >= maxritems ) {
				GtkRecentInfo*rinfo_to_remove;
				gchar*uri_to_remove;
				GError *gerror = NULL;
				ritems = g_list_sort (ritems, (GCompareFunc) recent_compare_func);
				rinfo_to_remove = (GtkRecentInfo*)g_list_nth_data (ritems, 0);
				uri_to_remove = (gchar*)gtk_recent_info_get_uri (rinfo_to_remove);
				if ( !gtk_recent_manager_remove_item (manager, (const gchar *)uri_to_remove, &gerror) ) {
					if ( gerror != NULL ) {
						g_warning(gerror->message);
						g_clear_error(&gerror);
					}
				}
			}
			g_list_foreach (ritems, (GFunc)gtk_recent_info_unref, NULL);
			g_list_free (ritems);
		}
		gtk_recent_manager_add_full (manager, uri, data);
	}
	g_free(data->mime_type);
	g_free(data->app_name);
	g_free(data->app_exec);
	g_strfreev(data->groups);
	g_free(data);
}
*/
gboolean kimono_gui_init(gpointer data)
{
	config_changed = 0;
	fetch_servers();
	make_interface();
	kimono_gui_start_bambu();
/*	if ( current_file ) {
		char *uri;
		char *bname;
		char *fname;
		bname = g_path_get_basename(current_file);
		fname = g_build_filename(current_folder, bname, NULL);
#ifdef G_OS_WIN32
		uri = g_strdup_printf("file:///%s", fname);
#else
		uri = g_strdup_printf("file://%s", fname);
#endif
		g_free(bname);
		g_free(fname);
		add_to_recent(uri);
		run_bambu(uri, 0, current_folder);
		g_free(uri);
		g_free(current_file);
		current_file = NULL;
	}*/
	return FALSE;
}

gboolean kimono_gui_start_bambu()
{
	if ( current_file ) {
		char *uri;
		char *bname;
		char *fname;
		bname = g_path_get_basename(current_file);
		fname = g_build_filename(current_folder, bname, NULL);
#ifdef G_OS_WIN32
		uri = g_strdup_printf("file:///%s", fname);
#else
		uri = g_strdup_printf("file://%s", fname);
#endif
		g_free(bname);
		g_free(fname);
/*		add_to_recent(uri);*/
		run_bambu(uri, 0, current_folder);
		g_free(uri);
		g_free(current_file);
		current_file = NULL;
	}
	return FALSE;
}

static void
on_bus_acquired (GDBusConnection *connection,
                 const gchar     *name,
                 gpointer         user_data)
{
	GError *gerror = NULL;
	g_print ("Acquired a message bus connection %s\n", name);
	pro_stack_interface = pro_stack_interface_skeleton_new ();
	g_signal_connect (pro_stack_interface, "handle-get-node", G_CALLBACK (pro_stack_interface_get_node), NULL);
	g_signal_connect (pro_stack_interface, "handle-get-display", G_CALLBACK (pro_stack_interface_get_display), NULL);
	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (pro_stack_interface), connection, "/ru/spbstu/sysbio/ProStack", &gerror)) {
		g_warning(gerror->message);/* handle error */
	}
}

static void
on_name_acquired (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
	g_print ("Acquired the name %s\n", name);
	is_master = TRUE;
	kimdow = create_main_window ();
	gtk_widget_hide(kimdow);
/* This is important */
	kimsplash = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window1"));
	gtk_widget_show(kimsplash);
	g_idle_add(kimono_gui_init, NULL);
}

static void
on_name_lost (GDBusConnection *connection,
              const gchar     *name,
              gpointer         user_data)
{
	g_print ("Lost the name %s\n", name);
	if ( connection == NULL ) {
		g_print ("Connection null %s\n", name);
	} else {
		g_print ("Good connection, but %s already in use\n", name);
		is_master = FALSE;
		kimono_gui_start_bambu();
	}
	exit(EXIT_SUCCESS);
}


int main (int argc, char *argv[])
{
	guint owner_id;
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, subst_data_dir(LOCALE_DIR));
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	gumethod = g_string_new("");
/*	g_setenv("LANG", setlocale (LC_ALL, ""), TRUE);*/
	setlocale (LC_ALL, "");
	g_set_application_name ( _("ProStack"));
	gtk_init (&argc, &argv);
	kimono_init(&argc, &argv, NULL);
	current_folder = g_get_current_dir();
	current_file = NULL;
	if ( argc > 1 ) {
		current_file = g_strdup(argv[argc - 1]);
		if ( !g_file_test ((const gchar *)current_file, G_FILE_TEST_EXISTS) ) {
			g_free(current_file);
			current_file = NULL;
		}
	}

	owner_id = g_bus_own_name (G_BUS_TYPE_SESSION,  "ru.spbstu.sysbio.ProStack",
                             G_BUS_NAME_OWNER_FLAGS_NONE,
                             on_bus_acquired,
                             on_name_acquired,
                             on_name_lost,
                             NULL,
                             NULL);

	gtk_main ();
	kimono_shutdown();
	g_bus_unown_name (owner_id);
	return 0;
}

void fetch_servers()
{
	GError*error = NULL;
	GKeyFile*gkf;
	char*config_path;
	char**grps;
	int i;
	gsize ngrps;
	OlapServer*los;
	os = NULL;
	config_path = g_build_filename (door_get_config_home(), "prostack.rc", NULL);
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_file(gkf, config_path, G_KEY_FILE_NONE, &error ) ) {
		g_warning("Can't read %s: %s", config_path, error->message);
		g_free(config_path);
		os = os_init_default();
		servers = g_list_insert(servers, os, 0);
		email = g_strdup("anonymous@yandex.ru");
		config_changed = 1;
		return;
	}
	g_free(config_path);
	grps = g_key_file_get_groups(gkf, &ngrps);
	if ( grps ) {
		for ( i = 0; i < ngrps; i++ ) {
			if ( !strcmp(grps[i],"Settings") ) {
				email = g_key_file_get_string (gkf, grps[i], "email", &error);
			} else if ( !strcmp(grps[i],"Default") ) {
				os = olapserverParse(gkf, grps[i], i);
				os->is_default = 1;
				servers = g_list_insert(servers, os, 0);
			} else {
				los = olapserverParse(gkf, grps[i], i);
				servers = g_list_append(servers, los);
			}
		}
		g_strfreev(grps);
	}
	g_key_file_free(gkf);
	if ( !os ) {
		os = os_init_default();
		servers = g_list_insert(servers, os, 0);
		config_changed = 1;
	}
	if (email == NULL) {
		email = g_strdup("anonymous@yandex.ru");
		config_changed = 1;
	}
	return;
}

void put_hierarhy(OlapServer*os)
{
	GtkWidget *tree;
	GtkTreeIter iter1;  /* Parent iter */
//	GtkTreeIter iter2;  /* Child iter  */
//	GtkTreeViewColumn *kim_column;
//	GtkCellRenderer *kim_renderer;
//	int done;
	char**concepts;
//	char**tokens;
//	int ntokens;
//	int i;
	char**concept;
	int j;
	int door_result;
	Node*sproxy;
/*	tree = glade_xml_get_widget(kim_gxml, "treeview1");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview1"));
	kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
	gtk_tree_store_clear(kim_store);
	sproxy = proxy_sys_node("concepts");
	sproxy->oServer = os;
	sproxy->label = strcpy(sproxy->label, "Methods");
	door_result = door_call("GET", sproxy);
	if ( door_result == -1 ) {
		GtkWidget*d, *w;
		char*buf;
		proxy_sys_node_del(sproxy);
		buf = g_strdup_printf("Can't contact server %s://%s:%d%s", os->proto, os->hname, os->port, os->address);
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
		d = gtk_message_dialog_new ((GtkWindow*)w,
									(GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
									GTK_MESSAGE_ERROR,
									GTK_BUTTONS_CLOSE,
									_(buf));
		gtk_dialog_run (GTK_DIALOG (d));
		gtk_widget_destroy(d);
		g_free(buf);
		return;
	}
	concepts = g_strsplit(sproxy->buf, "\n", MAXTOKENS);
	free(sproxy->buf);
	for ( j = 0; concepts[j]; j++) {
		concept = g_strsplit(concepts[j], "|", 2);
		if ( concept[0] && concept[1] ) {
			gtk_tree_store_append (kim_store, &iter1, NULL);  /* Acquire a top-level iterator */
			gtk_tree_store_set (kim_store, &iter1, 0, concept[0], 1, concept[1], -1);
			g_strfreev(concept);
		}
	}
	g_strfreev(concepts);
	gtk_tree_model_foreach(GTK_TREE_MODEL (kim_store), (GtkTreeModelForeachFunc)kim_fetch_func, (gpointer) os);
/*	tree = glade_xml_get_widget(kim_gxml, "treeview2");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
	kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
	gtk_tree_store_clear(kim_store);
	proxy_sys_node_set_mode(sproxy, "methods");
	door_call("GET", sproxy);
	concepts = g_strsplit(sproxy->buf, "\n", MAXTOKENS);
	free(sproxy->buf);
	proxy_sys_node_del(sproxy);
	for ( j = 0; concepts[j]; j++) {
		concept = g_strsplit(concepts[j], "|", 2);
		if ( concept[0] && concept[1] ) {
			gtk_tree_store_append (kim_store, &iter1, NULL);
			gtk_tree_store_set (kim_store, &iter1, 0, concept[0], 1, concept[1], -1);
			g_strfreev(concept);
		}
	}
	g_strfreev(concepts);
}

static void pam_selection_changed_cb (GtkTreeSelection *selection, gpointer data)
{
/*	char*request;
	char*response;*/
//	char *req;
	GtkTreeIter iter;
	GtkTreeModel *model;
	gchar *concept;
	GtkWidget *view;
	GtkTextBuffer*buffer;
	Node*sproxy;
	if ( gtk_tree_selection_get_selected (selection, &model, &iter) ) {
		gtk_tree_model_get (model, &iter, 0, &concept, -1);
		concept = g_strchomp(concept);
		concept = g_strdelimit(concept, " ", '_');
		gumethod = g_string_assign(gumethod, concept);
		sproxy = proxy_sys_node("pam");
		sproxy->label = strcpy(sproxy->label, concept);
		sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
		sproxy->oServer = os;
		sproxy->file = strcpy(sproxy->file, "message");
		door_call("GET", sproxy);
		view = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "textview1"));
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
		gtk_text_buffer_set_text (buffer, sproxy->buf, -1);
		free(sproxy->buf);
		proxy_sys_node_del(sproxy);
		g_free (concept);
		what_tree = 0;
	}
}

static void tree_selection_changed_cb (GtkTreeSelection *selection, gpointer data)
{
	GtkTreeIter iter1;
/*	char*request;
	char*response;
*/	char**concepts;
//	int i;
	char**cpt;
	int j;
//	char*req;
	GtkTreeIter iter;
	GtkTreeModel *model;
	gchar *concept;
	GtkWidget *tree;
	Node*sproxy;
	if ( gtk_tree_selection_get_selected (selection, &model, &iter) ) {
		gtk_tree_model_get (model, &iter, 0, &concept, -1);
//		g_print ("You selected a book by %s\n", concept);
		concept = g_strchomp(concept);
		concept = g_strdelimit(concept, " -", '_');

/*
		req = (char*)calloc(MAXCHARS, sizeof(char));
		sprintf(req, "-m %s", concept);
		request = requestSYS("PAM", req);
		free(req);
		response = comm(os, request);
		free(request);
		concepts = g_strsplit(response, "\n", MAXTOKENS);
		free(response);
*/

		sproxy = proxy_sys_node("methods");
		sproxy->oServer = os;
		sproxy->label = strcpy(sproxy->label, concept);
		door_call("GET", sproxy);
		if ( sproxy->buf ) {
			concepts = g_strsplit(sproxy->buf, "\n", MAXTOKENS);
			free(sproxy->buf);
/*			tree = glade_xml_get_widget(kim_gxml, "treeview2");*/
			tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
			kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
			gtk_tree_store_clear(kim_store);
			for ( j = 0; concepts[j]; j++) {
				cpt = g_strsplit(concepts[j], "|", 2);
				if ( cpt[0] && cpt[1] ) {
					gtk_tree_store_append (kim_store, &iter1, NULL);
					gtk_tree_store_set (kim_store, &iter1, 0, cpt[0], 1, cpt[1], -1);
					g_strfreev(cpt);
				}
			}
			g_strfreev(concepts);
		}
		proxy_sys_node_del(sproxy);
		g_free (concept);
	}
}

gboolean kim_fetch_func(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, OlapServer*os)
{
	GtkTreeIter iter1;
	char**concepts;
	int /*i,*/ j;
	char**concept;
	char*cpt;
	Node*sproxy;
	if ( gtk_tree_model_iter_has_child( model, iter) )
		return TRUE;
	gtk_tree_model_get (model, iter, 0, &cpt, -1);
	cpt = g_strchomp(cpt);
	cpt = g_strdelimit(cpt, " -", '_');
	sproxy = proxy_sys_node("concepts");
	sproxy->oServer = os;
	sproxy->label = strcpy(sproxy->label, cpt);
	g_free(cpt);
	door_call("GET", sproxy);
	if ( sproxy->buf && strncmp(sproxy->buf, "N/A", 3) ) {
		concepts = g_strsplit(sproxy->buf, "\n", MAXTOKENS);
		if ( sproxy->buf ) g_free(sproxy->buf);
		proxy_sys_node_del(sproxy);
		for ( j = 0; concepts[j]; j++) {
			concept = g_strsplit(concepts[j], "|", 2);
			if ( concept[0] && concept[1] ) {
				gtk_tree_store_append (kim_store, &iter1, iter);  /* Acquire a top-level iterator */
				gtk_tree_store_set (kim_store, &iter1, 0, concept[0], 1, concept[1], -1);
				g_strfreev(concept);
			}
		}
		g_strfreev(concepts);
	} else {
		g_free(sproxy->buf);
		proxy_sys_node_del(sproxy);
	}
	return FALSE;
}

static void on_new_clicked(GtkWidget *widget, gpointer data)
{
	int log;
	log = 0;
	run_bambu(NULL, log, current_folder);
}

static gboolean on_open_clicked(GtkWidget *widget, gpointer data)
{
	int log;
	char *str;
	GtkWidget*dialog,*w;
	log = 0;
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
	dialog = gtk_file_chooser_dialog_new ("Select file",
				(GtkWindow*)w,
				GTK_FILE_CHOOSER_ACTION_OPEN,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Open", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), current_folder);
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		str = gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (dialog));
		if ( str ) {
			g_free(current_folder);
			current_folder = str;
		}
		str = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
/*		add_to_recent(str);*/
		run_bambu(str, log, current_folder);
		g_free(str);
	}
	gtk_widget_destroy (dialog);
	return TRUE;
}

static gboolean on_exec_clicked(GtkWidget *widget, gpointer data)
{
	int log;
	char *str;
	GtkWidget*dialog, *w;
	log = 0;
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
	dialog = gtk_file_chooser_dialog_new ("Select file",
				(GtkWindow*)w,
				GTK_FILE_CHOOSER_ACTION_OPEN,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Open", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), current_folder);
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		str = gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (dialog));
		if ( str ) {
			g_free(current_folder);
			current_folder = str;
		}
		str = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
		run_execute(str, log, current_folder);
		g_free(str);
	}
	gtk_widget_destroy (dialog);
	return TRUE;
}

static gboolean on_apply_clicked(GtkWidget *widget, gpointer data)
{
	char*folder, *efolder, *oemail;
	char*wksp;
	char*mountpoint, *emp;
	char*serv_str;
	char*cmd;
	GtkWidget*dialog, *w;
	folder = NULL;
	wksp = NULL;
	serv_str = NULL;
	mountpoint = NULL;
	efolder = NULL;
	emp = NULL;
	oemail = NULL;
	dialog = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "dialog3"));
	if (email != NULL) {
		oemail = g_strdup(email);
		gtk_entry_set_text(GTK_ENTRY(gtk_builder_get_object (gtkbuild, "entry12")), email);
	}
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
		folder = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (gtk_builder_get_object (gtkbuild, "filechooserbutton3")));
		wksp = gtk_file_chooser_get_uri(GTK_FILE_CHOOSER (gtk_builder_get_object (gtkbuild, "filechooserbutton2")));
		if ( email ) g_free(email);
		email = g_strdup(gtk_entry_get_text(GTK_ENTRY(gtk_builder_get_object (gtkbuild, "entry12"))));
		serv_str = g_strdup(gtk_entry_get_text(GTK_ENTRY(gtk_builder_get_object (gtkbuild, "entry13"))));
		mountpoint = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (gtk_builder_get_object (gtkbuild, "filechooserbutton4")));
//g_print("%s=%d\n%s=%d\n%s=%d\n%s=%d\n%s=%d\n",folder, strlen(folder), wksp, strlen(wksp), email, strlen(email), serv_str, strlen(serv_str), mountpoint, strlen(mountpoint));
		if ( !folder || !wksp || strlen(email) < 3 || strlen(serv_str) < 4 || !mountpoint ) {
			GtkWidget*d;
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "dialog3"));
			d = gtk_message_dialog_new ((GtkWindow*)w,
                                 GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "Fill all entries!");
			gtk_dialog_run (GTK_DIALOG (d));
			gtk_widget_destroy (d);
			if ( email ) {
				g_free(email);
			}
			if ( oemail ) {
				email = g_strdup(oemail);
				g_free(oemail);
			}
			if ( folder ) g_free(folder);
			if ( folder ) g_free(folder);
			if ( wksp ) g_free(wksp);
			if ( serv_str ) g_free(serv_str);
			if ( mountpoint ) g_free(mountpoint);
			gtk_widget_hide (dialog);
			return TRUE;
		}
		if (g_strcmp0(email, oemail) != 0) {
			config_changed = 1;
		}
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "checkbutton2"));
		efolder = g_shell_quote ((const gchar *)folder);
		emp = g_shell_quote ((const gchar *)mountpoint);
		if (gtk_toggle_button_get_active((GtkToggleButton *)w)) {
			cmd = g_strdup_printf("-w -p %s,%s,%s,%s", email, emp, efolder, serv_str);
		} else {
			cmd = g_strdup_printf("-p %s,%s,%s,%s", email, emp, efolder, serv_str);
		}
		run_komet_batch(wksp, 0, current_folder, cmd);
		g_free(cmd);
		if ( oemail ) g_free(oemail);
		if ( folder ) g_free(folder);
		if ( efolder ) g_free(efolder);
		if ( wksp ) g_free(wksp);
		if ( serv_str ) g_free(serv_str);
		if ( mountpoint ) g_free(mountpoint);
		if ( emp ) g_free(emp);
	}
	gtk_widget_hide (dialog);
	return TRUE;
}

void remove_oper()
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	Node*sproxy;
	if ( gtk_tree_selection_get_selected (kim_select_pam, &model, &iter) ) {
		char*curr;
		gtk_tree_model_get (model, &iter, 0, &curr, -1);
		curr = g_strchomp(curr);
		if ( !strcmp(gumethod->str, curr) ) {
//			char *orig_key, *value;
			sproxy = proxy_sys_node("methods");
			sproxy->oServer = os;
			sproxy->label = strcpy(sproxy->label, gumethod->str);
			door_call("DELETE", sproxy);
			if ( strncmp(sproxy->buf, "OK", 2) ) {
				GtkWidget*d, *w;
				w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
				d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "FAILURE: '%s'",
                                  sproxy->buf);
				gtk_dialog_run (GTK_DIALOG (d));
				gtk_widget_destroy (d);
			} else {
				gtk_tree_store_remove ((GtkTreeStore*)model, &iter);  /* Acquire a top-level iterator */
			}
			g_free(sproxy->buf);
			proxy_sys_node_del(sproxy);
		}
		g_free(curr);
	}
}

void remove_disp()
{
	GtkTreeModel *model;
	GtkTreeIter iter, piter, citer;
	int nc, i;
	int kc, j;
	void *orig_key, *value;
	char*curr;
	char*pcurr;
	char*ccurr;
	config_changed = 1;
//	g_print("remove disp");
	if ( gtk_tree_selection_get_selected (kim_select_display, &model, &iter) ) {
		gtk_tree_model_get (model, &iter, 0, &curr, -1);
		nc = gtk_tree_model_iter_n_children(model, &iter);
		if ( nc == 0 ) {
			if ( gtk_tree_model_iter_parent(model, &piter, &iter) ) {
				gtk_tree_model_get (model, &piter, 0, &pcurr, -1);
				gtk_tree_store_remove ((GtkTreeStore*)model, &iter);
				kimono_delete_screen(gudisplay->str, pcurr);
				g_free(pcurr);
			}
			if ( g_hash_table_lookup_extended(Displays, gudisplay->str, &orig_key, &value) ) {
				g_hash_table_remove(Displays, gudisplay->str);
				g_free((char*)orig_key);
				g_free((char*)value);
			}
		} else {
			if ( gtk_tree_model_iter_parent(model, &piter, &iter) ) {
				for ( i = 0; i < nc; i++ ) {
					if ( gtk_tree_model_iter_nth_child(model, &citer, &iter, i) ) {
						gtk_tree_model_get (model, &citer, 0, &ccurr, -1);
						gtk_tree_store_remove ((GtkTreeStore*)model, &citer);
						kimono_delete_screen(ccurr, curr);
						if ( g_hash_table_lookup_extended(Displays, ccurr, &orig_key, &value) ) {
							g_hash_table_remove(Displays, ccurr);
							g_free((char*)orig_key);
							g_free((char*)value);
						}
						g_free(ccurr);
					}
				}
				gtk_tree_store_remove ((GtkTreeStore*)model, &iter);
			} else {
				for ( i = 0; i < nc; i++ ) {
					if ( gtk_tree_model_iter_nth_child(model, &citer, &iter, i) ) {
						kc = gtk_tree_model_iter_n_children(model, &citer);
						gtk_tree_model_get (model, &citer, 0, &ccurr, -1);
						for ( j = 0; j < kc; j++ ) {
							if ( gtk_tree_model_iter_nth_child(model, &piter, &citer, j) ) {
								gtk_tree_model_get (model, &piter, 0, &pcurr, -1);
								gtk_tree_store_remove ((GtkTreeStore*)model, &piter);
								kimono_delete_screen(pcurr, ccurr);
								if ( g_hash_table_lookup_extended(Displays, pcurr, &orig_key, &value) ) {
									g_hash_table_remove(Displays, pcurr);
									g_free((char*)orig_key);
									g_free((char*)value);
								}
								g_free(pcurr);
							}
						}
						g_free(ccurr);
						gtk_tree_store_remove ((GtkTreeStore*)model, &citer);
					}
				}
				gtk_tree_store_remove ((GtkTreeStore*)model, &iter);
			}
		}
		g_free(curr);
	}
}

void remove_server()
{
//	GtkWidget*w;
	OlapServer*los;
	config_changed = 1;
//	g_print("remove server");
	los = os;
	servers = g_list_first(servers);
	os = (OlapServer*)g_list_nth_data(servers, 1);
	if ( os ) {
		int pos;
/*		w = glade_xml_get_widget(kim_gxml, "combobox1");*/
/*		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "combobox1"));*/
		pos = gtk_combo_box_get_active((GtkComboBox*)combobox1);
		gtk_combo_box_text_remove((GtkComboBoxText*)combobox1, pos);
		gtk_combo_box_set_active((GtkComboBox*)combobox1, 0);
		servers = g_list_remove(servers, los);
		deleteOlapServer(los);
	} else {
		os = los;
	}
	put_hierarhy(os);
}

void add_oper_with_name(char*uri)
{
	int flag/*, j*/;
	char*in, *mname, *out, *pl, *ap/*, *record*/, *curr, *ui2, *txt;
//	char**concepts;
	char**tokens;
//	int ntokens;
	int i;
	GtkWidget*tree;
	GtkTreeIter iter;
	GtkTreeModel*model;
	Node*sproxy;

g_warning("add_oper_with_name:%s", uri);

	mname = g_path_get_basename(uri);
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
	kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
	gtk_tree_model_foreach(GTK_TREE_MODEL (kim_store), (GtkTreeModelForeachFunc)kim_find_func, (gpointer) mname);
	if ( gtk_tree_selection_get_selected (kim_select_pam, &model, &iter) ) {
		gtk_tree_model_get (GTK_TREE_MODEL (kim_store), &iter, 0, &curr, -1);
		curr = g_strchomp(curr);
		if ( !strcmp(mname, curr) ) {
			GtkWidget*d, *w;
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
			d = gtk_message_dialog_new ((GtkWindow*)w,
            	                      GTK_DIALOG_DESTROY_WITH_PARENT,
                	                  GTK_MESSAGE_ERROR,
                    	              GTK_BUTTONS_CLOSE,
                        	          "Module '%s' exists in registry",
                            	      mname);
			gtk_dialog_run (GTK_DIALOG (d));
			gtk_widget_destroy (d);
			g_free(mname);
			g_free(curr);
			return;
		}
		g_free(curr);
	}
	curr = g_strdup_printf("%s.in", mname);
	in = g_build_filename(uri, curr, NULL);
	g_free(curr);
	curr = g_strdup_printf("%s.out", mname);
	out = g_build_filename(uri, curr, NULL);
	g_free(curr);
	curr = g_strdup_printf("%s.ap", mname);
	ap = g_build_filename(uri, curr, NULL);
	g_free(curr);
	curr = g_strdup_printf("%s.pl", mname);
	pl = g_build_filename(uri, curr, NULL);
	g_free(curr);
	curr = g_strdup_printf("%s.ui2", mname);
	ui2 = g_build_filename(uri, curr, NULL);
	g_free(curr);
	curr = g_strdup_printf("%s.txt", mname);
	txt = g_build_filename(uri, curr, NULL);
	g_free(curr);
	flag = 1;
	if ( 0 != g_access(in, F_OK ) ) flag = 0;
	if ( 0 != g_access(out, F_OK ) ) flag = 0;
	if ( 0 != g_access(pl, F_OK ) ) flag = 0;
	if ( 0 != g_access(ap, F_OK ) ) flag = 0;
	if ( !flag ) {
		GtkWidget*d, *w;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
		d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "The folder '%s' doesn't contain neccessary files",
                                  uri);
		gtk_dialog_run (GTK_DIALOG (d));
		gtk_widget_destroy (d);
		g_free(mname);
		g_free(in);
		g_free(out);
		g_free(ap);
		g_free(pl);
		g_free(txt);
		g_free(ui2);
		return;
	}
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview1"));
	kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
	if ( gtk_tree_selection_get_selected (kim_select, &model, &iter) ) {
		GtkTreePath *gtp;
		GString*spath;
		GString*dpath;
		gtp = gtk_tree_model_get_path (GTK_TREE_MODEL(kim_store), &iter);
		curr = gtk_tree_path_to_string(gtp);
		gtk_tree_path_free(gtp);
		tokens = g_strsplit(curr, ":", MAXTOKENS);
		g_free(curr);
		dpath = g_string_new("");
		spath = g_string_new("");
		dpath = g_string_assign(dpath, tokens[0]);
		gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(kim_store), &iter, dpath->str);
		gtk_tree_model_get (GTK_TREE_MODEL(kim_store), &iter, 0, &curr, -1);
		g_strstrip(curr);
		spath = g_string_assign(spath, curr);
		g_free(curr);
		for ( i = 1; tokens[i]; i++ ) {
			dpath = g_string_append(dpath, ":");
			spath = g_string_append(spath, ",");
			g_string_append_printf(dpath, "%s", tokens[i]);
			gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(kim_store), &iter, dpath->str);
			gtk_tree_model_get (GTK_TREE_MODEL(kim_store), &iter, 0, &curr, -1);
			g_strstrip(curr);
			g_string_append_printf(spath, "%s", curr);
			g_free(curr);
		}
		g_strfreev(tokens);
		sproxy = proxy_sys_node("methods");
		sproxy->oServer = os;
		sproxy->label = strcpy(sproxy->label, mname);
		sproxy->id = (char*)realloc(sproxy->id, MAX_RECORD * sizeof(char) );
		sproxy->id = strcpy(sproxy->id, spath->str);
		sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
		sproxy->file = strcpy(sproxy->file, uri);
		door_call("PUT", sproxy);
		g_string_free(dpath, TRUE);
		g_string_free(spath, TRUE);
		if ( strncmp(sproxy->buf, "OK", 2) ) {
			GtkWidget*d, *w;
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
			d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "FAILURE: '%s'",
                                  sproxy->buf);
			gtk_dialog_run (GTK_DIALOG (d));
			gtk_widget_destroy (d);
		} else {
	//		GError*error = NULL;
			tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
			kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
			gtk_tree_store_append (kim_store, &iter, NULL);  /* Acquire a top-level iterator */
			gtk_tree_store_set (kim_store, &iter, 0, mname, 1, g_get_user_name(), -1);
		}
		g_free(sproxy->buf);
		proxy_sys_node_del(sproxy);
	}
	g_free(mname);
	g_free(in);
	g_free(out);
	g_free(ap);
	g_free(pl);
	g_free(txt);
	g_free(ui2);
}

void add_oper()
{
	GtkWidget /**dialog,*/ *w;
//	char *str;
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window2"));
	gtk_widget_show_all(w);
/*	dialog = gtk_file_chooser_dialog_new ("Select module folder",
				(GtkWindow*)w,
				GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), current_folder);
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		str = gtk_file_chooser_get_uri(GTK_FILE_CHOOSER (dialog));
		if ( g_str_has_prefix(str, "file://") ) {
			str[0] = ' ';
			str[1] = ' ';
			str[2] = ' ';
			str[3] = ' ';
			str[4] = ' ';
			str[5] = ' ';
			str[6] = ' ';
			str = g_strchug(str);
		}
		add_oper_with_name(str);
		g_free(str);
	}
	gtk_widget_destroy (dialog);*/
}

void add_disp()
{
	GtkWidget*dialog;
	//char *str;
	config_changed = 1;
	dialog = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "dialog2"));
	while (1) {
		if (gtk_dialog_run (GTK_DIALOG (dialog) )== GTK_RESPONSE_OK) {
			GtkWidget *w;
			char*dname, *dopt, *dexec, *dext, *dtype;
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "filechooserbutton1"));
			dexec = (char*)gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (w));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry11"));
			dname = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY (w)));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry9"));
			dopt = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry10"));
			dext = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry8"));
			dtype = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			if ( strlen(dname) < 2 || strlen(dext) < 2 || strlen(dtype) < 2 || !dexec ) {
				GtkWidget*d, *w;
				w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "dialog2"));
				d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "Fill all entries!");
				gtk_dialog_run (GTK_DIALOG (d));
				gtk_widget_destroy (d);
				if ( dname ) g_free(dname);
				if ( dopt ) g_free(dopt);
				if ( dexec ) g_free(dexec);
				if ( dext ) g_free(dext);
				if ( dtype ) g_free(dtype);
			} else {
				char*cmd;
				char**adext;
				int i;
				adext = g_strsplit(dext, ",", MAXTOKENS);
				for ( i = 0; adext[i]; i++) {
					cmd = g_strdup_printf("%s|%s|%s|%s|%s", dname, dtype, adext[i], dexec, dopt);
					kimono_put_screen(cmd);
					g_free(cmd);
				}
				g_strfreev(adext);
				if ( dname ) g_free(dname);
				if ( dopt ) g_free(dopt);
				if ( dexec ) g_free(dexec);
				if ( dext ) g_free(dext);
				if ( dtype ) g_free(dtype);
				gtk_widget_hide (dialog);
				put_displays();
				break;
			}
		} else {
			gtk_widget_hide (dialog);
			break;
		}
	}
}

void kimono_gui_add_operation(gchar*Name, gchar*metaname, int metaweight, gchar*Executable, int type, gchar*uidescr, gchar*message, gchar*inputs, gchar*outputs, gchar*ostring, char*str)
{
	char*curr;
//	char**concepts;
	char**tokens;
//	int ntokens;
	int i;
	GtkWidget*tree;
	GtkTreeIter iter;
	GtkTreeModel*model;
	GError*gerror = NULL;
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview1"));
	kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
	if ( gtk_tree_selection_get_selected (kim_select, &model, &iter) ) {
		GtkTreePath *gtp;
		GString*spath;
		GString*dpath;
		int res;
		gtp = gtk_tree_model_get_path (GTK_TREE_MODEL(kim_store), &iter);
		curr = gtk_tree_path_to_string(gtp);
		gtk_tree_path_free(gtp);
		tokens = g_strsplit(curr, ":", MAXTOKENS);
		g_free(curr);
		dpath = g_string_new("");
		spath = g_string_new("");
		dpath = g_string_assign(dpath, tokens[0]);
		gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(kim_store), &iter, dpath->str);
		gtk_tree_model_get (GTK_TREE_MODEL(kim_store), &iter, 0, &curr, -1);
		g_strstrip(curr);
		spath = g_string_assign(spath, curr);
		g_free(curr);
		for ( i = 1; tokens[i]; i++ ) {
			dpath = g_string_append(dpath, ":");
			spath = g_string_append(spath, ",");
			g_string_append_printf(dpath, "%s", tokens[i]);
			gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(kim_store), &iter, dpath->str);
			gtk_tree_model_get (GTK_TREE_MODEL(kim_store), &iter, 0, &curr, -1);
			g_strstrip(curr);
			g_string_append_printf(spath, "%s", curr);
			g_free(curr);
		}
		g_strfreev(tokens);
		res = workspace_to_methods(Name, metaname, metaweight, Executable, type, uidescr, message, inputs, outputs, ostring, spath->str, str, os, &gerror);
		g_string_free(dpath, TRUE);
		g_string_free(spath, TRUE);
		if ( res != 0 ) {
			GtkWidget*d, *w;
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
			if ( gerror != NULL ) {
				d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "FAILURE: '%s'",
                                  gerror->message);
				g_clear_error (&gerror);
			} else {
				d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "FAILURE: '%s'",
                                  "Given values are invalid");
			}
			gtk_dialog_run (GTK_DIALOG (d));
			gtk_widget_destroy (d);
		} else {
			tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
			kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
			gtk_tree_store_append (kim_store, &iter, NULL);  /* Acquire a top-level iterator */
			gtk_tree_store_set (kim_store, &iter, 0, Name, 1, g_get_user_name(), -1);
		}
	} else {
		GtkWidget*d, *w;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
		d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "FAILURE: '%s'",
                                  "Select concept which new operation belongs to");
		gtk_dialog_run (GTK_DIALOG (d));
		gtk_widget_destroy (d);
	}
}

gboolean add_operations()
{
	GtkWidget*w;
	char*str = NULL;
	gchar*Name;
	gchar*metaname;
	int metaweight;
	gchar*Executable;
	gchar*uidescr;
	gchar*message;
	gchar*inputs = NULL, *outputs = NULL, *ostring = NULL;
	GtkTextBuffer *buf;
	GtkTextIter start, end;
	int type;
//	GError*gerror = NULL;
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "name_add_oper"));
	Name = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "metaname_add_oper"));
	metaname = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "metaweight_add_oper"));
	metaweight = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(w));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "radiobutton4"));
	if ( gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(w))) { /* ProStack Scenario */
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "fc_add_oper"));
		str = gtk_file_chooser_get_uri(GTK_FILE_CHOOSER (w));
		Executable = g_strdup_printf("komet -o %s", Name);
		type = 2;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "inp_add_oper"));
		inputs = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "out_add_oper"));
		outputs = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "os_add_oper"));
		ostring = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "radiobutton5"));
	if ( gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(w))) { /* Fiji Macro */
		gchar*wrapper = NULL;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "fiji_fc"));
		str = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (w));
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "fiji_batch_opt"));
		ostring = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "fiji_macro_w"));
		wrapper = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
		Executable = g_strdup_printf("%s -%s %s %s", str, ostring, wrapper, Name);
		g_free(wrapper);
		type = 4;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "fiji_inp"));
		inputs = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "fiji_outp"));
		outputs = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "radiobutton6"));
	if ( gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(w))) { /* Just command */
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "com_add_oper"));
		Executable = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
		type = 1;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "com_inp_add_oper"));
		inputs = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "com_out_add_oper"));
		outputs = g_strdup( gtk_entry_get_text(GTK_ENTRY(w)));
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "ui_add_oper"));
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW(w));
	gtk_text_buffer_get_start_iter (buf, &start);
	gtk_text_buffer_get_end_iter (buf, &end);
	uidescr = gtk_text_buffer_get_text (buf, &start, &end, FALSE);
	if ( strlen(uidescr) == 0 ) {
		g_free(uidescr);
		uidescr = g_strdup("N/A");
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "descr_add_oper"));
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW(w));
	gtk_text_buffer_get_start_iter (buf, &start);
	gtk_text_buffer_get_end_iter (buf, &end);
	message = gtk_text_buffer_get_text (buf, &start, &end, FALSE);
	kimono_gui_add_operation(Name, metaname, metaweight, Executable, type, uidescr, message, inputs, outputs, ostring, str);
	g_free(Name);
	g_free(metaname);
	g_free(Executable);
	g_free(uidescr);
	g_free(message);
	g_free(inputs);
	g_free(outputs);
	if ( ostring != NULL ) {
		g_free(ostring);
	}
	if ( str != NULL ) {
		g_free(str);
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window2"));
	gtk_widget_hide(w);
	return TRUE;
}

gboolean on_add_oper_apfile_change()
{
	GtkWidget*w;
	char*str = NULL;
	gchar*uidescr;
//	gchar*message;
	gchar*inputs, *outputs, *ostring;
	GtkTextBuffer *buf;
//	int type;
	int res;
	GError*gerror = NULL;
/* ProStack Scenario */
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "fc_add_oper"));
	str = gtk_file_chooser_get_uri(GTK_FILE_CHOOSER (w));
//	type = 2;
	res = workspace_get_compilation_infos_from_file((const char*)str, &uidescr, &inputs, &outputs, &ostring, &gerror);
	if ( res != 0 ) {
		GtkWidget*d;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
		if ( gerror != NULL ) {
			d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "FAILURE: '%s'",
                                  gerror->message);
			g_clear_error (&gerror);
		} else {
			d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "FAILURE: '%s'",
                                  "Didn't get compilation info from this scenario");
		}
		gtk_dialog_run (GTK_DIALOG (d));
		gtk_widget_destroy (d);
		if ( str != NULL ) {
			g_free(str);
		}
		return TRUE;
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "ui_add_oper"));
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW(w));
	gtk_text_buffer_set_text (buf, uidescr, strlen(uidescr));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "inp_add_oper"));
	gtk_entry_set_text(GTK_ENTRY(w), inputs);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "out_add_oper"));
	gtk_entry_set_text(GTK_ENTRY(w), outputs);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "os_add_oper"));
	gtk_entry_set_text(GTK_ENTRY(w), ostring);
	g_free(uidescr);
	g_free(inputs);
	g_free(outputs);
	g_free(ostring);
	g_free(str);
	return TRUE;
}

gboolean add_oper_radio_toggled()
{
	GtkWidget*w, *v;
	v = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "radiobutton4"));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "table_pros"));
	gtk_widget_set_sensitive (w, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(v)));
	v = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "radiobutton5"));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "table_fiji"));
	gtk_widget_set_sensitive (w, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(v)));
	v = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "radiobutton6"));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "table_com"));
	gtk_widget_set_sensitive (w, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(v)));
	return TRUE;
}

void add_server()
{
	GtkWidget*dialog;
//	char *str;
	config_changed = 1;
	dialog = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "dialog1"));
	while (1) {
		if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
			GtkWidget *w;
			char*sname, *shost, *sport, *sproto, *saddress;
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry4"));
			sname = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry5"));
			shost = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry6"));
			sport = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry7"));
			saddress = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry14"));
			sproto = g_strdup((char*)gtk_entry_get_text(GTK_ENTRY(w)));
			if ( strlen(sname) < 2 || strlen(sproto) < 2 || strlen(sport) < 2 || strlen(shost) < 2 || strlen(saddress) < 1 ) {
				GtkWidget*d;
				w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "dialog1"));
				d = gtk_message_dialog_new ((GtkWindow*)w,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "Fill all entries!");
				gtk_dialog_run (GTK_DIALOG (d));
				gtk_widget_destroy (d);
				if ( sname ) g_free(sname);
				if ( sport ) g_free(sport);
				if ( shost ) g_free(shost);
				if ( saddress ) g_free(saddress);
				if ( sproto ) g_free(sproto);
			} else {
				os = newOlapServer(shost, atoi(sport), 0);
				os->nicname = g_strdup(sname);
				os->address = g_strdup(saddress);
				os->proto = g_strdup(sproto);
				w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "checkbutton1"));
				if (gtk_toggle_button_get_active((GtkToggleButton *)w)) {
					os->is_default = 1;
					g_list_foreach(servers, (GFunc)servers_remove_default, NULL);
					servers = g_list_insert(servers, os, 0);
				} else {
					servers = g_list_append(servers, os);
				}
				gtk_combo_box_text_append_text((GtkComboBoxText*)combobox1, sname);
				if ( sname ) g_free(sname);
				if ( sport ) g_free(sport);
				if ( shost ) g_free(shost);
				if ( saddress ) g_free(saddress);
				if ( sproto ) g_free(sproto);
				break;
			}
		} else {
			break;
		}
	}
	gtk_widget_hide (dialog);
}

void save_servers()
{
	GError*error = NULL;
	GKeyFile*gkf;
	char*config_path;
	char*contents;
	gsize l;
//	OlapServer*los;
	os = NULL;
	config_path = g_build_filename (door_get_config_home(), "prostack.rc", NULL);
	gkf = g_key_file_new();
	g_key_file_set_string (gkf, "Settings", "email", email);
	g_list_foreach(servers, (GFunc)olapserverPrint, gkf);
	contents = g_key_file_to_data(gkf, &l, &error);
	if ( error ) {
		g_warning(error->message);
		g_error_free(error);
	} else {
		g_file_set_contents(config_path, contents, l, &error);
		if ( error ) {
			g_warning(error->message);
			g_error_free(error);
		}
		g_free(contents);
	}
	g_key_file_free(gkf);
	g_free(config_path);
}

void print_display(char*key, char*value, FILE*fp)
{
	fprintf(fp, "%s;%s;\n", key, value);
}

void save_displays()
{
	char*config_path;
	FILE*fp;
	config_path = g_build_filename (g_get_home_dir(), ".bambu", "dispconf", NULL);
	fp = fopen(config_path, "w");
	g_free(config_path);
	g_hash_table_foreach(Displays, (GHFunc)print_display, fp);
	fclose(fp);
}

void config_save()
{
	if (config_changed == 1) {
//		save_displays();
		save_servers();
	}
}

void config_remove(int flag)
{
	switch(flag) {
		case 0:
			remove_oper();
		break;
		case 1:
			config_changed = 1;
			remove_disp();
		break;
		case 2:
			config_changed = 1;
			remove_server();
		break;
	}
}

void config_add(int flag)
{
	switch(flag) {
		case 0:
			add_oper();
		break;
		case 1:
			config_changed = 1;
			add_disp();
		break;
		case 2:
			config_changed = 1;
			add_server();
		break;
	}
}

static void on_pop_item_activate(char*cmd)
{
	if ( !strcmp(cmd, "Add") )
		config_add(what_tree);
	else if ( !strcmp(cmd, "Remove") )
		config_remove(what_tree);
}
/*
static void on_recent_item_activated(GtkRecentChooser *chooser, gpointer user_data)
{
	char *uri;
	uri = gtk_recent_chooser_get_current_uri (chooser);
	run_bambu(uri, 0, current_folder);
	g_free(uri);
}
*/
void make_menu()
{
	GtkWidget*w;
/*	GtkWidget*recent_menu;
	GtkRecentFilter *grft;*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem1"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_new_clicked), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem2"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_open_clicked), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem3"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_exec_clicked), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem4"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_apply_clicked), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem5"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_all_close), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem10"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_about1_activate), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem8"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_guide_activate), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem6"));
	g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (on_examples_activate), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "menuitem7"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (on_pop_item_activate), g_strdup("Add"));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "menuitem8"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (on_pop_item_activate), g_strdup("Remove"));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem7"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (add_oper), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem12"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (remove_oper), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem14"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (add_disp), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem15"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (remove_disp), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem11"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (add_server), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem16"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (remove_server), NULL);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem17"));
	g_signal_connect_swapped (G_OBJECT (w), "activate", G_CALLBACK (config_save), NULL);
/*
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "imagemenuitem20"));
	recent_menu = gtk_recent_chooser_menu_new ();
	gtk_recent_chooser_set_sort_type (GTK_RECENT_CHOOSER(recent_menu), GTK_RECENT_SORT_MRU);
	gtk_recent_chooser_menu_set_show_numbers ((GtkRecentChooserMenu*)recent_menu, TRUE);
	gtk_recent_chooser_set_show_icons ((GtkRecentChooser*)recent_menu, FALSE);
	grft = gtk_recent_filter_new();
	gtk_recent_filter_add_mime_type( grft, "application/x-prostack");
	gtk_recent_chooser_set_filter((GtkRecentChooser *)recent_menu, grft);
	gtk_menu_item_set_submenu((GtkMenuItem *)w, (GtkWidget *)recent_menu);
	g_signal_connect (G_OBJECT (recent_menu), "item-activated", G_CALLBACK (on_recent_item_activated), NULL);
*/
	gtk_spin_button_set_increments (GTK_SPIN_BUTTON(gtk_builder_get_object (gtkbuild, "metaweight_add_oper")), 1, 10);
	gtk_spin_button_set_range (GTK_SPIN_BUTTON(gtk_builder_get_object (gtkbuild, "metaweight_add_oper")), 0, 65535);
	g_signal_connect (gtk_builder_get_object (gtkbuild, "ok_add_oper"), "clicked", G_CALLBACK (add_operations), NULL);
	g_signal_connect_swapped (gtk_builder_get_object (gtkbuild, "cancell_add_oper"), "clicked", G_CALLBACK (gtk_widget_hide), gtk_builder_get_object (gtkbuild, "window2"));
	g_signal_connect (gtk_builder_get_object (gtkbuild, "fc_add_oper"), "file-set", G_CALLBACK (on_add_oper_apfile_change), NULL);
	g_signal_connect (gtk_builder_get_object (gtkbuild, "radiobutton4"), "toggled", G_CALLBACK (add_oper_radio_toggled), NULL);
	g_signal_connect (gtk_builder_get_object (gtkbuild, "radiobutton5"), "toggled", G_CALLBACK (add_oper_radio_toggled), NULL);
	g_signal_connect (gtk_builder_get_object (gtkbuild, "radiobutton6"), "toggled", G_CALLBACK (add_oper_radio_toggled), NULL);
}

void make_hierarhy()
{
	GtkWidget *tree;
//	GtkTreeIter iter1;  /* Parent iter */
//	GtkTreeIter iter2;  /* Child iter  */
	GtkTreeViewColumn *kim_column;
	GtkCellRenderer *kim_renderer;
	kim_store = gtk_tree_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
/*	tree = glade_xml_get_widget(kim_gxml, "treeview1");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview1"));
	kim_select = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
	gtk_tree_selection_set_mode (kim_select, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (kim_select), "changed", G_CALLBACK (tree_selection_changed_cb), NULL);
	gtk_tree_view_set_model((GtkTreeView*)tree, GTK_TREE_MODEL (kim_store));
/* The view now holds a reference. We can get rid of our own reference */
	g_object_unref (G_OBJECT (kim_store));
	kim_renderer = gtk_cell_renderer_text_new ();
	kim_column = gtk_tree_view_column_new_with_attributes (_("Name"), kim_renderer, "text", 0, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), kim_column);
	kim_column = gtk_tree_view_column_new_with_attributes (_("Remark"), kim_renderer, "text", 1, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), kim_column);
	kim_store = gtk_tree_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
/*	tree = glade_xml_get_widget(kim_gxml, "treeview2");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
	g_signal_connect (G_OBJECT (tree), "button_press_event", G_CALLBACK (button_press_event), G_OBJECT (gtk_builder_get_object (gtkbuild, "menu4")));
	gtk_tree_view_set_model((GtkTreeView*)tree, GTK_TREE_MODEL (kim_store));
	g_object_unref (G_OBJECT (kim_store));
	kim_renderer = gtk_cell_renderer_text_new ();
	kim_column = gtk_tree_view_column_new_with_attributes (_("Operation"), kim_renderer, "text", 0, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), kim_column);
	kim_column = gtk_tree_view_column_new_with_attributes (_("Package"), kim_renderer, "text", 1, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), kim_column);
	kim_select_pam = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
	gtk_tree_selection_set_mode (kim_select_pam, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (kim_select_pam), "changed", G_CALLBACK (pam_selection_changed_cb), NULL);
}

void make_interface()
{
	GtkWidget*combobox;
//	kimdow = create_main_window ();
	make_hierarhy();
/*	combobox = glade_xml_get_widget(kim_gxml, "combobox1");*/
	combobox1 = gtk_combo_box_text_new ();
	gtk_widget_show(combobox1);
	combobox = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "alignment2"));
	gtk_container_add( GTK_CONTAINER(combobox), combobox1);
/*	combobox = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "combobox1"));*/
	g_list_foreach(servers, (GFunc)put_servers, combobox1);
	gtk_combo_box_set_active((GtkComboBox*)combobox1, 0);
	g_signal_connect (G_OBJECT (combobox1), "changed", G_CALLBACK (server_selection_changed_cb), NULL);
	put_hierarhy(os);
	what_tree = 1;
	make_displays();
	put_displays();
	what_tree = -1;
	make_menu();
	gtk_widget_hide (kimsplash);
	gtk_widget_show (kimdow);
}

static void server_selection_changed_cb (GtkComboBox *box, gpointer data)
{
	char*text;
	GList*gl;
//	GtkTreeView*tree;
	text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(box));
	gl = g_list_find_custom(servers, text, (GCompareFunc)server_find_nicname);
	os = (OlapServer*)g_list_nth_data(gl, 0);
	put_hierarhy(os);
	what_tree = -1;
	g_free(text);
}

int server_find_nicname(OlapServer*los, char*nicname)
{
	if ( !strcmp(los->nicname, nicname) )
		return 0;
	return 1;
}

void put_servers(OlapServer*los, GtkComboBox*cb)
{
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(cb), los->nicname);
}

void servers_remove_default(OlapServer*los, gpointer data)
{
	los->is_default = 0;
}

static void button1_clicked_cb(GtkButton *button, gpointer data)
{
	GtkWidget *w;
/*	w = glade_xml_get_widget(kim_gxml, "entry1");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry1"));
	gtk_entry_set_text( GTK_ENTRY (w), "");
/*	w = glade_xml_get_widget(kim_gxml, "entry2");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry2"));
	gtk_entry_set_text( GTK_ENTRY (w), "");
/*	w = glade_xml_get_widget(kim_gxml, "entry3");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry3"));
	gtk_entry_set_text( GTK_ENTRY (w), "");
	what_tree = -1;
}

static void button2_clicked_cb(GtkButton *button, gpointer data)
{
	GtkWidget *tree;
	GtkTreeIter iter1;
	GtkWidget *w;
	char**concepts;
//	int i;
	char**concept;
	int j;
	char*sample;
	Node*sproxy;
/*	w = glade_xml_get_widget(kim_gxml, "entry1");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry1"));
	sample = g_strdup(gtk_entry_get_text( GTK_ENTRY (w)));
	sample = g_strchomp(sample);
	if ( strlen(sample) <= 1 ) {
		g_free(sample);
		return;
	}
	sample = g_strdelimit(sample, " ", '+');
	sproxy = proxy_sys_node("search");
	sproxy->oServer = os;
	sproxy->label = strcpy(sproxy->label, sample);
	g_free(sample);
/*	w = glade_xml_get_widget(kim_gxml, "entry2");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry2"));
	sample = g_strdup(gtk_entry_get_text( GTK_ENTRY (w)));
	sample = g_strchomp(sample);
	sample = g_strdelimit(sample, " ", '_');
	if ( strlen(sample) > 1 ) {
		sproxy->id = (char*)realloc(sproxy->id, MAX_RECORD * sizeof(char) );
		sproxy->id = strcpy(sproxy->id, sample);
	}
	g_free(sample);
/*	w = glade_xml_get_widget(kim_gxml, "entry3");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry3"));
	sample = g_strdup(gtk_entry_get_text( GTK_ENTRY (w)));
	sample = g_strchomp(sample);
	sample = g_strdelimit(sample, " ", '_');
	if ( strlen(sample) > 1 ) {
		sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
		sproxy->file = strcpy(sproxy->file, sample);
	}
	g_free(sample);
	door_call("GET", sproxy);
	concepts = NULL;
	if ( sproxy->buf ) {
		concepts = g_strsplit(sproxy->buf, "\n", MAXTOKENS);
		free(sproxy->buf);
	}
	proxy_sys_node_del(sproxy);
/*	tree = glade_xml_get_widget(kim_gxml, "treeview2");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
	kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
	gtk_tree_store_clear(kim_store);
	for ( j = 0; concepts && concepts[j]; j++) {
		concept = g_strsplit(concepts[j], "|", 2);
		if ( concept[0] && concept[1] ) {
			gtk_tree_store_append (kim_store, &iter1, NULL);
			gtk_tree_store_set (kim_store, &iter1, 0, concept[0], 1, concept[1], -1);
			g_strfreev(concept);
		}
	}
	if ( concepts )
		g_strfreev(concepts);
}

void put_displays1()
{
	char *config_path;
	char *contents;
	char **displays;
	char **display;
	int j;
	gsize cl;
	GError *error = NULL;
	GtkWidget *tree;
	GtkTreeIter iter1;  /* Parent iter */
	GtkTreeViewColumn *kim_column;
	GtkCellRenderer *kim_renderer;
	kim_store = gtk_tree_store_new (1, G_TYPE_STRING);
/*	tree = glade_xml_get_widget(kim_gxml, "treeview3");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview3"));
	g_signal_connect (G_OBJECT (tree), "button_press_event", G_CALLBACK (button_press_event), G_OBJECT (gtk_builder_get_object (gtkbuild, "menu4")));
	kim_select_display = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
	gtk_tree_selection_set_mode (kim_select_display, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (kim_select_display), "changed", G_CALLBACK (display_selection_changed_cb), NULL);
	config_path = g_build_filename (g_get_home_dir(), ".bambu", "dispconf", NULL);
	if ( !g_file_get_contents(config_path, &contents, &cl, &error ) ) {
		g_error("Can't read dispconf: %s", error->message);
	}
	g_free(config_path);
//g_print ("displays %s\n", contents);
	Displays = g_hash_table_new(g_str_hash, g_str_equal);
	displays = g_strsplit(contents, "\n", MAXTOKENS);
	free(contents);
	for ( j = 0; displays[j]; j++) {
		display = g_strsplit(displays[j], ";", 2);
		if ( display[0] && display[1] ) {
			g_hash_table_insert(Displays, (gpointer) display[0], (gpointer) display[1]);
			gtk_tree_store_append (kim_store, &iter1, NULL);  /* Acquire a top-level iterator */
			gtk_tree_store_set (kim_store, &iter1, 0, display[0], -1);
//			g_strfreev(display);
		}
	}
	g_strfreev(displays);
	gtk_tree_view_set_model((GtkTreeView*)tree, GTK_TREE_MODEL (kim_store));
/* The view now holds a reference. We can get rid of our own reference */
	g_object_unref (G_OBJECT (kim_store));
	kim_renderer = gtk_cell_renderer_text_new ();
	kim_column = gtk_tree_view_column_new_with_attributes ("Name", kim_renderer, "text", 0, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), kim_column);
}

void make_displays()
{
	GtkWidget *tree;
//	GtkTreeIter iter1, iter2, iter3;  /* Parent iter */
	GtkTreeViewColumn *kim_column;
	GtkCellRenderer *kim_renderer;
//	GtkTreeModel *model;
	kim_store = gtk_tree_store_new (3, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN);
/*	tree = glade_xml_get_widget(kim_gxml, "treeview3");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview3"));
	kim_select_display = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
	gtk_tree_selection_set_mode (kim_select_display, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (kim_select_display), "changed", G_CALLBACK (display_selection_changed_cb), NULL);
	g_signal_connect (G_OBJECT (tree), "button_press_event", G_CALLBACK (button_press_event), G_OBJECT (gtk_builder_get_object (gtkbuild, "menu4")));
	Displays = g_hash_table_new(g_str_hash, g_str_equal);
	gtk_tree_view_set_model((GtkTreeView*)tree, GTK_TREE_MODEL (kim_store));
	g_object_unref (G_OBJECT (kim_store));
	kim_renderer = gtk_cell_renderer_text_new ();
	kim_column = gtk_tree_view_column_new_with_attributes (_("File Type"), kim_renderer, "text", 0, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), kim_column);
	kim_renderer = gtk_cell_renderer_toggle_new ();
	kim_column = gtk_tree_view_column_new_with_attributes (_("Is Default"), kim_renderer, "active", 1, "visible", 2, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), kim_column);
}

void put_displays()
{
	char *config_path;
	char *contents;
	char **displays;
	char **display;
	int cl, j;
//	GError *error = NULL;
	GtkWidget *tree;
	GtkTreeIter iter1, iter2, iter3;  /* Parent iter */
	GtkTreeModel *model;
/*	tree = glade_xml_get_widget(kim_gxml, "treeview3");*/
	tree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview3"));
	kim_store = (GtkTreeStore*)gtk_tree_view_get_model (GTK_TREE_VIEW(tree));
	gtk_tree_store_clear(kim_store);
	contents = kimono_get_screens();
	displays = g_strsplit(contents, "\n", MAXTOKENS);
//g_print("cl=%d", g_strv_length(displays));
	g_free(contents);
	for ( j = 0; displays[j]; j++) {
		display = g_strsplit(displays[j], "|", 6);
		cl = g_strv_length(display);
		if ( cl == 6 ) {
			if ( strcmp(display[5], "N/A") && strlen(display[5]) > 0 ) {
				config_path = g_strdup_printf("%s %s %%s %%s", display[3], display[5]);
			} else if ( strcmp(display[5], "N/A") && strlen(display[5]) == 0 ) {
				config_path = g_strdup_printf("%s %%s %%s", display[3]);
			} else {
				config_path = g_strdup_printf("%s %%s", display[3]);
			}
			g_hash_table_insert(Displays, (gpointer) g_strdup(display[0]), (gpointer) config_path);
			gtk_tree_selection_unselect_all(kim_select_display);
			gtk_tree_model_foreach(GTK_TREE_MODEL (kim_store), (GtkTreeModelForeachFunc)kim_find_func, (gpointer) display[1]);
			if ( !gtk_tree_selection_get_selected (kim_select_display, &model, &iter1) ) {
				gtk_tree_store_append (kim_store, &iter1, NULL);
				gtk_tree_store_set (kim_store, &iter1, 0, display[1], 2, FALSE, -1);
				gtk_tree_store_append (kim_store, &iter2, &iter1);
				gtk_tree_store_set (kim_store, &iter2, 0, display[2], 2, FALSE, -1);
				gtk_tree_store_append (kim_store, &iter3, &iter2);
				gtk_tree_store_set (kim_store, &iter3, 0, display[0], 1, atoi(display[4]), 2, TRUE, -1);
			} else {
				gtk_tree_selection_unselect_all(kim_select_display);
				gtk_tree_model_foreach(GTK_TREE_MODEL (kim_store), (GtkTreeModelForeachFunc)kim_find_func, (gpointer) display[2]);
				if ( !gtk_tree_selection_get_selected (kim_select_display, &model, &iter2) ) {
					gtk_tree_store_append (kim_store, &iter2, &iter1);
					gtk_tree_store_set (kim_store, &iter2, 0, display[2], 2, FALSE, -1);
				}
				gtk_tree_store_append (kim_store, &iter3, &iter2);
				gtk_tree_store_set (kim_store, &iter3, 0, display[0], 1, atoi(display[4]), 2, TRUE, -1);
			}
		}
		g_strfreev(display);
	}
	g_strfreev(displays);
	gtk_tree_view_collapse_all(GTK_TREE_VIEW(tree));
}


static void display_selection_changed_cb (GtkTreeSelection *selection, gpointer data)
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	gchar *concept;
//	GtkWidget *view;
//	GtkTextBuffer*buffer;
	if ( gtk_tree_selection_get_selected (selection, &model, &iter) && !gtk_tree_model_iter_has_child(model, &iter) ) {
		gtk_tree_model_get (model, &iter, 0, &concept, -1);
//		g_print ("You selected a display %s\n", concept);
		concept = g_strchomp(concept);
		gudisplay = g_string_assign(gumethod, concept);
		g_free (concept);
		what_tree = 1;
	}
}

void run_bambu(char*filename, int log, char*current)
{
	char*cmd;
	char*buf;
	char**margv;
/*	GPid child_pid;*/
	GError *gerror = NULL;
	int flaggs;
	int d;
	flaggs = G_SPAWN_SEARCH_PATH;
/*	flaggs |= G_SPAWN_FILE_AND_ARGV_ZERO;
	flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;*/
	if ( !log ) {
		flaggs |= G_SPAWN_STDOUT_TO_DEV_NULL;
		if ( filename ) {
			buf = g_shell_quote(filename);
			cmd = g_strdup_printf("prostack-bambu -t %s", buf);
			g_free(buf);
		} else {
			cmd = g_strdup_printf("prostack-bambu -t");
		}
	} else {
		if ( filename ) {
			buf = g_shell_quote(filename);
			cmd = g_strdup_printf("prostack-bambu --conf_dir=%s --database=%s --user=%s --default_port_num=%d %s", door_get_config_home(), door_get_db_name(), door_get_user(), door_get_config_port(), buf);
			g_free(buf);
		} else {
			cmd = g_strdup_printf("prostack-bambu --conf_dir=%s --database=%s --user=%s --default_port_num=%d", door_get_config_home(), door_get_db_name(), door_get_user(), door_get_config_port());
		}
	}
	if ( !g_shell_parse_argv(cmd, &d, &margv, &gerror) ) {
		if ( gerror ) {
			g_warning("g_shell_parse failed for %s\nwith %s", cmd, gerror->message);
			g_error_free(gerror);
			g_free(cmd);
			return;
		}
	}
/*
	for ( d = 0; margv[d]; d++)
		g_print("%d %s\n", d, margv[d]);
*/
	if ( !g_spawn_async (current, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, NULL /*&child_pid*/, &gerror) ) {
		g_warning("g_spawn_async failed with %s\nas %s", cmd, gerror->message);
		g_error_free(gerror);
	} else {
/*		bambus = g_list_append(bambus, (gpointer)child_pid);*/
	}
	g_free(cmd);
	g_strfreev(margv);
}

void run_execute(char*filename, int log, char*current)
{
	char*cmd;
	char*buf;
	char**margv;
	GError *gerror = NULL;
	int flaggs;
	int d;
	flaggs = G_SPAWN_SEARCH_PATH;
	if ( !filename ) {
		return;
	}
	buf = g_shell_quote(filename);
#ifdef G_OS_WIN32
	cmd = g_strdup_printf("prostack-execute %s", buf);
#else
	cmd = g_strdup_printf("prostack-execute --conf_dir=%s --database=%s --user=%s --default_port_num=%d %s", door_get_config_home(), door_get_db_name(), door_get_user(), door_get_config_port(), buf);
#endif
	g_free(buf);
	if ( !g_shell_parse_argv(cmd, &d, &margv, &gerror) ) {
		if ( gerror ) {
			g_warning("g_shell_parse failed for %s\nwith %s", cmd, gerror->message);
			g_error_free(gerror);
			g_free(cmd);
			return;
		}
	}
	g_message("run_execute: %s", cmd);
	if ( !g_spawn_async (current, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, NULL /*&child_pid*/, &gerror) ) {
		g_warning("g_spawn_async failed with %s\nas %s", cmd, gerror->message);
		g_error_free(gerror);
	}
	g_free(cmd);
	g_strfreev(margv);
}

void run_komet(char*filename, int log, char*current)
{
	char*cmd;
	char*buf;
//	char*buf1;
	char**margv;
	GPid child_pid;
	GError *gerror = NULL;
	int flaggs;
	int d;
	flaggs = G_SPAWN_SEARCH_PATH;
/*	flaggs |= G_SPAWN_FILE_AND_ARGV_ZERO;
	flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;*/
	if ( !filename ) {
		g_warning("run_komet: no file");
		return;
	}
	buf = g_shell_quote(filename);
#ifdef G_OS_WIN32
	if ( !log ) {
		flaggs |= G_SPAWN_STDOUT_TO_DEV_NULL;
		cmd = g_strdup_printf("komet -t %s", buf);
	} else {
		cmd = g_strdup_printf("komet %s", buf);
	}
#else
	if ( !log ) {
		flaggs |= G_SPAWN_STDOUT_TO_DEV_NULL;
		cmd = g_strdup_printf("komet --conf_dir=%s --database=%s --user=%s --default_port_num=%d -t %s", door_get_config_home(), door_get_db_name(), door_get_user(), door_get_config_port(), buf);
	} else {
		cmd = g_strdup_printf("komet --conf_dir=%s --database=%s --user=%s --default_port_num=%d %s", door_get_config_home(), door_get_db_name(), door_get_user(), door_get_config_port(), buf);
	}
#endif
	g_free(buf);
	if ( !g_shell_parse_argv(cmd, &d, &margv, &gerror) ) {
		if ( gerror ) {
			g_warning("g_shell_parse failed for %s\nwith %s", cmd, gerror->message);
			g_error_free(gerror);
			g_free(cmd);
			return;
		}
	}
	if ( !g_spawn_async (current, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, &child_pid, &gerror) ) {
		g_warning("g_spawn_async failed with %s\nas %s", cmd, gerror->message);
		g_error_free(gerror);
	}
	g_free(cmd);
	g_strfreev(margv);
}

void run_komet_batch(char*filename, int log, char*current, char*extra_args)
{
	char*cmd;
	char*buf;
	char**margv;
	GPid child_pid;
	GError *gerror = NULL;
	int flaggs;
	int d;
	flaggs = G_SPAWN_SEARCH_PATH;
	if ( !filename ) {
		g_warning("run_komet: no file");
		return;
	}
	buf = g_shell_quote(filename);
#ifdef G_OS_WIN32
	if ( !log ) {
		flaggs |= G_SPAWN_STDOUT_TO_DEV_NULL;
		cmd = g_strdup_printf("komet %s -t %s", extra_args, buf);
	} else {
		cmd = g_strdup_printf("komet %s %s", extra_args, buf);
	}
#else
	if ( !log ) {
		flaggs |= G_SPAWN_STDOUT_TO_DEV_NULL;
		cmd = g_strdup_printf("komet --conf_dir=%s --database=%s --user=%s --default_port_num=%d %s -t %s", door_get_config_home(), door_get_db_name(), door_get_user(), door_get_config_port(), extra_args, buf);
	} else {
		cmd = g_strdup_printf("komet --conf_dir=%s --database=%s --user=%s --default_port_num=%d %s %s", door_get_config_home(), door_get_db_name(), door_get_user(), door_get_config_port(), extra_args, buf);
	}
#endif
	g_free(buf);
	g_message("run_komet_batch: %s", cmd);
	if ( !g_shell_parse_argv(cmd, &d, &margv, &gerror) ) {
		if ( gerror ) {
			g_warning("g_shell_parse failed for %s\nwith %s", cmd, gerror->message);
			g_error_free(gerror);
			g_free(cmd);
			return;
		}
	}
	if ( !g_spawn_async (current, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, &child_pid, &gerror) ) {
		g_warning("g_spawn_async failed with %s\nas %s", cmd, gerror->message);
		g_error_free(gerror);
	}
	g_free(cmd);
	g_strfreev(margv);
}

static gboolean on_all_close()
{
	GtkWidget *dialog, *w;
	gint result;
	if ( config_changed == 1 ) {
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
		dialog = gtk_message_dialog_new (GTK_WINDOW (w),
			(GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_YES_NO,
			_("Configuration was modified. Do you want to save it?"));
		gtk_dialog_add_button(GTK_DIALOG (dialog), "_Cancel", GTK_RESPONSE_CANCEL);
		result = gtk_dialog_run (GTK_DIALOG (dialog));
		if (result == GTK_RESPONSE_YES) {
			config_save();
		} else if ( result == GTK_RESPONSE_CANCEL ) {
			gtk_widget_destroy (dialog);
			return TRUE;
		}
	}
	pro_stack_interface_emit_close_bambu_signal(pro_stack_interface);
	gtk_main_quit();
	return FALSE;
}

void kill_bambu(GPid*pid, gpointer data)
{
/*	g_spawn_close_pid((*pid));*/
}

GtkWidget*create_aboutdialog(void)
{
	GtkWidget *aboutdialog1;
	aboutdialog1 = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "aboutdialog1"));
	gtk_about_dialog_set_version (GTK_ABOUT_DIALOG (aboutdialog1), VERSION);
	return aboutdialog1;
}

void on_about1_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *adialog;
	int resp;
	adialog = create_aboutdialog();
	resp = gtk_dialog_run(GTK_DIALOG(adialog));
	gtk_widget_hide(adialog);
}

gboolean on_guide_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	GError*gerror = NULL;
	char*uri, *str;
	GtkWidget *dialog, *w;
#ifdef G_OS_WIN32
	int win32_error;
#endif
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
	str = subst_data_dir(KIMONO_DOCS_FILE);
	if ( !g_file_test ((const gchar*)str, G_FILE_TEST_EXISTS) ) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (w),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING,
				GTK_BUTTONS_CLOSE,
				_("Documentation is not installed"));
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
		g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
		gtk_widget_show (dialog);
		return TRUE;
	}
#ifdef G_OS_WIN32
/* gtk_show_uri doesn't seem to work on Win32, at least not for local files
 We use Windows API instead.
 TODO: Check it again and file a bug if necessary.*/
/*	uri = g_win32_locale_filename_from_utf8 ( (const gchar *) str );*/
/*	uri = g_utf8_to_utf16 ((const gchar *) str, -1, NULL, NULL, &gerror);*/
	uri = g_strdup_printf("%s", str);
	if ( gerror == NULL ) {
/*		win32_error = ShellExecute(0, "open", uri, 0, 0, SW_SHOW);*/
/*		win32_error = ShellExecute(NULL, "open", uri, "", NULL, SW_SHOWNORMAL);*/
		win32_error = ShellExecute(NULL, "open", uri, NULL, NULL, SW_SHOWNORMAL);
		if ( win32_error < 33 ) {
			dialog = gtk_message_dialog_new (GTK_WINDOW (w),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING,
				GTK_BUTTONS_CLOSE,
				_("Can't launch application with %s (%s): %d"), uri, str, win32_error);
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
			g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
			gtk_widget_show (dialog);
		}
	} else {
		dialog = gtk_message_dialog_new (GTK_WINDOW (w),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_WARNING,
			GTK_BUTTONS_CLOSE,
			gerror->message);
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
		g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
		gtk_widget_show (dialog);
		g_error_free(gerror);
		gerror = NULL;
	}
#else
	uri = g_strdup_printf("file://%s", str);
	if ( !g_app_info_launch_default_for_uri((const char *)uri, (GAppLaunchContext *)NULL, &gerror) ) {
		if ( gerror ) {
			dialog = gtk_message_dialog_new (GTK_WINDOW (w),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING,
				GTK_BUTTONS_CLOSE,
				gerror->message);
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
			g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
			gtk_widget_show (dialog);
			g_error_free(gerror);
			gerror = NULL;
		} else {
			dialog = gtk_message_dialog_new (GTK_WINDOW (w),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING,
				GTK_BUTTONS_CLOSE,
				_("Can't launch application"));
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
			g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
			gtk_widget_show (dialog);
		}
	}
#endif
	g_free(uri);
	return TRUE;
}

gboolean on_examples_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	int log;
	char *str, *str_folder;
	GtkWidget*dialog, *w;
	log = 0;
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
	str = subst_data_dir(KIMONO_EXAMPLES_DIR);
	if ( !g_file_test ((const gchar*)str, G_FILE_TEST_EXISTS) ) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (w),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING,
				GTK_BUTTONS_CLOSE,
				_("Examples are not installed"));
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
		g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
		gtk_widget_show (dialog);
		return TRUE;
	}
	dialog = gtk_file_chooser_dialog_new (_("Select .ap file in subdirectory"),
				(GtkWindow*)w,
				GTK_FILE_CHOOSER_ACTION_OPEN,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Open", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, TRUE);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), (const gchar*)str);
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		str = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
		str_folder = gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (dialog));
		run_bambu(str, log, str_folder);
		g_free(str);
		g_free(str_folder);
	}
	gtk_widget_destroy (dialog);
	return TRUE;
}

static gboolean button_press_event( GtkTreeView *tree_view, GdkEventButton *event )
{
	int x, y;
	int cell_x, cell_y;
	GtkTreePath *gtpath;
	GtkTreeViewColumn *gtvcolumn;
	char*dname, *dext;
	GdkEventButton *bevent = (GdkEventButton *) event;
	x = event->x;
	y = event->y;
	if ( gtk_tree_view_get_path_at_pos(tree_view, x, y, &gtpath, &gtvcolumn, &cell_x, &cell_y) ) {
		if ( gtk_tree_view_row_expanded(tree_view, gtpath) )
			gtk_tree_view_collapse_row( tree_view, gtpath);
		else
			gtk_tree_view_expand_to_path(tree_view, gtpath);
		gtk_widget_grab_focus((GtkWidget*)tree_view);
		gtk_tree_view_set_cursor(tree_view, gtpath, gtvcolumn, FALSE);
		if ( !strcmp(gtk_tree_view_column_get_title(gtvcolumn), _("Is Default") ) ) {
			GList *crlist;
			GtkCellRendererToggle *gcr;
			GtkTreeModel*model;
			GtkTreeIter iter;
/*			crlist = gtk_tree_view_column_get_cell_renderers(gtvcolumn);*/
			crlist = gtk_cell_layout_get_cells ((GtkCellLayout *)gtvcolumn);
			gcr = (GtkCellRendererToggle *)g_list_nth_data(crlist, 0);
			model = gtk_tree_view_get_model(tree_view);
			if ( gtk_cell_renderer_toggle_get_active (gcr) ) {
				if ( gtk_tree_model_get_iter(model, &iter, gtpath) ) {
					GtkTreeIter piter, citer;
					if ( gtk_tree_model_iter_parent(model, &piter, &iter) ) {
						int nc;
						nc = gtk_tree_model_iter_n_children(model, &piter);
						gtk_tree_model_get(model, &piter, 0, &dext, -1);
						if ( nc > 1 ) {
							int i;
							for ( i = 0; i < nc; i++ ) {
								if ( gtk_tree_model_iter_nth_child(model, &citer, &piter, i) ) {
									gtk_tree_store_set ((GtkTreeStore*)model, &citer, 1, TRUE, -1);
									gtk_tree_model_get(model, &citer, 0, &dname, -1);
									kimono_update_screen_default(dname, dext, TRUE);
									g_free(dname);
								}
							}
							gtk_tree_store_set ((GtkTreeStore*)model, &iter, 1, FALSE, -1);
							gtk_tree_model_get(model, &iter, 0, &dname, -1);
							kimono_update_screen_default(dname, dext, FALSE);
							g_free(dname);
						}
						g_free(dext);
					}
				}
			} else {
				if ( gtk_tree_model_get_iter(model, &iter, gtpath) ) {
					GtkTreeIter piter, citer;
					if ( gtk_tree_model_iter_parent(model, &piter, &iter) ) {
						int nc;
						nc = gtk_tree_model_iter_n_children(model, &piter);
						gtk_tree_model_get(model, &piter, 0, &dext, -1);
						if ( nc > 1 ) {
							int i;
							for ( i = 0; i < nc; i++ ) {
								if ( gtk_tree_model_iter_nth_child(model, &citer, &piter, i) ) {
									gtk_tree_store_set ((GtkTreeStore*)model, &citer, 1, FALSE, -1);
									gtk_tree_model_get(model, &citer, 0, &dname, -1);
									kimono_update_screen_default(dname, dext, FALSE);
									g_free(dname);
								}
							}
							gtk_tree_store_set ((GtkTreeStore*)model, &iter, 1, TRUE, -1);
							gtk_tree_model_get(model, &iter, 0, &dname, -1);
							kimono_update_screen_default(dname, dext, TRUE);
							g_free(dname);
						} else {
							gtk_tree_store_set ((GtkTreeStore*)model, &iter, 1, TRUE, -1);
							gtk_tree_model_get(model, &iter, 0, &dname, -1);
							kimono_update_screen_default(dname, dext, TRUE);
							g_free(dname);
						}
						g_free(dext);
					}
				}
			}
			g_list_free(crlist);
		}
	}
	if (event->button == 3 ) {
		gtk_menu_popup (GTK_MENU ( gtk_builder_get_object (gtkbuild, "menu4")), NULL, NULL, NULL, NULL, bevent->button, bevent->time);
	}
	return TRUE;
}

gboolean kim_find_func(GtkTreeModel *model, GtkTreePath *gpath, GtkTreeIter *iter, char*key)
{
//	GtkTreeIter iter1;
	GtkWidget *atree;
	GtkTreeSelection *selection;
	char*cpt;
	switch ( what_tree ) {
		case 0:
/*			tree = (GtkTreeView *)glade_xml_get_widget(kim_gxml, "treeview2");*/
			atree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview2"));
			selection = kim_select_pam;
		break;
		case 1:
/*			tree = (GtkTreeView *)glade_xml_get_widget(kim_gxml, "treeview3");*/
			atree = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "treeview3"));
			selection = kim_select_display;
		break;
	}
	gtk_tree_model_get (model, iter, 0, &cpt, -1);
	cpt = g_strchomp(cpt);
	if ( !strcmp(cpt, key) ) {
		gtk_tree_view_expand_to_path(GTK_TREE_VIEW(atree), gpath);
		gtk_widget_grab_focus(atree);
		gtk_tree_view_set_cursor(GTK_TREE_VIEW(atree), gpath, NULL, FALSE);
//		gtk_tree_selection_select_iter(selection, iter);
		return TRUE;
	}
	return FALSE;
}
