## Process this file with automake to produce Makefile.in

## Created by Anjuta

gladedir = $(datadir)/kimono-gui/glade
glade_DATA = kimono-gui.glade

INCLUDES = \
	-DPACKAGE_LOCALE_DIR=\""$(prefix)/$(DATADIRNAME)/locale"\" \
	-DPACKAGE_SRC_DIR=\""$(srcdir)"\" \
	-DPACKAGE_DATA_DIR=\""$(datadir)"\" \
	$(KIMONO_GUI_CFLAGS)

AM_CFLAGS =\
	 -Wall\
	 -g

bin_PROGRAMS = kimono-gui

kimono_gui_SOURCES = \
	callbacks.c \
	callbacks.h \
	main.c

kimono_gui_CFLAGS = \
	-I$(prefix)/include \
	-I$(prefix)/include/DOOR \
	-L$(prefix)/lib

kimono_gui_LDFLAGS = \
	-L$(prefix)/lib \
	-ldoor \
	-Wl,-rpath \
	$(prefix)/lib

kimono_gui_LDADD = $(KIMONO_GUI_LIBS)

BUILT_SOURCES = kimono-gui-glue.h

kimono-gui-glue.h: kimono-gui.xml
	dbus-binding-tool --prefix=kimono_object --mode=glib-server --output=kimono-gui-glue.h $(srcdir)/kimono-gui.xml

EXTRA_DIST = $(glade_DATA) \
	kimono-gui.xml 
