#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <config.h>
#include <gdk/gdkkeysyms.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <math.h>
#include <bambu-interface.h>
#include <Workspace.h>
#include <Node.h>
#include <Kimono.h>
#include <bambu-ui-node.h>


/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif

/* Backing pixmap for drawing area */
static cairo_surface_t *surface = NULL;
static GtkWidget *mw;
static GtkWidget *drawingarea1;
static PangoLayout *playout;
static Workspace*myWS;
static int myWSWidth;
static int myWSHeight;
static Node *selectedNode;
static int selectedInPort;
static int selectedOutPort;
static int manySelected;
static int Moved;
static GtkWidget *menu;

static GThread*threadWS;
static GThread*threadGUI;
//static pthread_attr_t thread_ws_attr;

static int event_x;
static int event_y;

static Node *copyNode;
static char*filename = (char*)NULL;
static char *dirname = NULL;
static char *basefilename = NULL;
static int filename_flag = 0;
static char*title;

static Node *ctrlNode;

static void (*mod1Node)();
static int mod1_node_enabled = 0;

static GtkWidget *undoButton;
static GtkWidget *redoButton;
static GtkWidget *runButton;
static GtkWidget *rewButton;
static GtkWidget *stopButton;
static GtkWidget *supernetButton;

static GList*undoList;
static GList*redoList;
static GList*operList;
static int ept_ID;

static int MACRO_NUMBER;
static int P_MACRO_NUMBER;

static int modified = 1;

static GdkRGBA normal_color, prelight_color, selected_color, active_color, intensive_color, white_color, black_color;

gint recent_compare_func (gconstpointer a, gconstpointer b);
void saveOutput();

void setMod1Node(void (*n)())
{
	if ( n ) {
		mod1Node = n;
		mod1_node_enabled = 1;
	} else {
		mod1Node = n;
		mod1_node_enabled = 0;
	}
}


void setCtrlNodeInterface(Node*ne)
{
	ctrlNode = ne;
}

GtkWidget*getDrawingArea()
{
	return drawingarea1;
}

GtkWidget*
create_window1 (Workspace*ws, int wsWidth, int wsHeight, char*name)
{
	GtkWidget *window1;
	GtkWidget *table1;
	GtkWidget *toolbar1;
	GtkIconSize tmp_toolbar_icon_size;
	GtkWidget *scrolledwindow1;
	GtkWidget *viewport1;
	GtkWidget *menu_items;
	GtkWidget *tButton;
	MACRO_NUMBER = 0;
	P_MACRO_NUMBER = -1;
	window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	mw = window1;
	title = (char*)calloc(MAX_RECORD, sizeof(char));
/*	if ( !filename ) {
		filename = (char*)calloc(MAX_RECORD, sizeof(char));
	}*/
	if (name != NULL) {
		filename = g_strdup(name);
		dirname = g_path_get_dirname (filename);
		basefilename = g_path_get_basename(filename);
		sprintf(title, "Bambu! - %s", filename);
		filename_flag = 1;
		bambu_set_title(1);
	} else {
		filename = g_strdup("New workspace");
		dirname = g_path_get_dirname ("");
		basefilename = g_path_get_basename("");
		sprintf(title, "Bambu! - New workspace");
		filename_flag = 0;
		bambu_set_title(0);
	}
	gtk_window_set_default_size (GTK_WINDOW (window1), 850, 400);
	g_signal_connect(window1, "delete_event", G_CALLBACK(mwdelete), NULL); /* dirty */
	table1 = gtk_grid_new ();
	gtk_widget_show (table1);
	gtk_container_add (GTK_CONTAINER (window1), table1);
/* toolbar */
	toolbar1 = gtk_toolbar_new ();
	gtk_widget_show (toolbar1);
/* close */
	tButton = gtk_tool_button_new (NULL, _("Options"));
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(toolbar1_properties_event), (gpointer)ws);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(tButton), 0);
/* close */
	tButton = gtk_tool_button_new (NULL, _("Close"));
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(mwdelete), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(tButton), 0);
/* close macro */
	supernetButton = gtk_tool_button_new (NULL, _("Close macro"));
	gtk_widget_show (supernetButton);
	g_signal_connect(supernetButton, "clicked", G_CALLBACK(toolbar1_close_macro_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(supernetButton), 0);
	gtk_widget_set_sensitive(supernetButton, FALSE);
/* make macro */
	tButton = gtk_tool_button_new (NULL, _("Make macro"));
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(toolbar1_macro_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(tButton), 0);
/* compile */
	tButton = gtk_tool_button_new (NULL, _("Compile"));
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(toolbar1_comp_event), NULL);
/* save as */
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(tButton), 0);
	tButton = gtk_tool_button_new (NULL, _("Save as"));
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(toolbar1_save_as_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(tButton), 0);
/* save */
	tButton = gtk_tool_button_new (NULL, _("Save"));
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(toolbar1_save_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(tButton), 0);
/* redo */
	redoButton = gtk_tool_button_new (NULL, _("Redo"));
	gtk_widget_show (redoButton);
	g_signal_connect(redoButton, "clicked", G_CALLBACK(toolbar1_redo_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(redoButton), 0);
	gtk_widget_set_sensitive(redoButton, FALSE);
	redoList = NULL;
	operList = NULL;
/* undo */
	undoButton = gtk_tool_button_new (NULL, _("Undo"));
	gtk_widget_show (undoButton);
	g_signal_connect(undoButton, "clicked", G_CALLBACK(toolbar1_undo_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(undoButton), 0);
	gtk_widget_set_sensitive(undoButton, FALSE);
	undoList = NULL;
/* rewind */
	rewButton = gtk_tool_button_new (NULL, _("Rew"));
	gtk_widget_show (rewButton);
	g_signal_connect(rewButton, "clicked", G_CALLBACK(toolbar1_rew_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(rewButton), 0);
/* stop */
	stopButton = gtk_tool_button_new (NULL, _("Stop"));
	gtk_widget_show (stopButton);
	g_signal_connect(stopButton, "clicked", G_CALLBACK(toolbar1_stop_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(stopButton), 0);
/* run */
	runButton = gtk_tool_button_new (NULL, _("Run"));
	gtk_widget_show (runButton);
	g_signal_connect(runButton, "clicked", G_CALLBACK(toolbar1_run_event), NULL);
	gtk_toolbar_insert (GTK_TOOLBAR(toolbar1), GTK_TOOL_BUTTON(runButton), 0);
/* table */
	gtk_grid_attach (GTK_GRID (table1), toolbar1, 0, 0, 1, 1);
	gtk_widget_set_size_request (toolbar1, 128, 30);
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar1), GTK_TOOLBAR_BOTH);
	tmp_toolbar_icon_size = gtk_toolbar_get_icon_size (GTK_TOOLBAR (toolbar1));
	gtk_toolbar_set_show_arrow ( GTK_TOOLBAR (toolbar1), TRUE);
	scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow1);
	gtk_grid_attach (GTK_GRID (table1), scrolledwindow1, 0, 1, 1, 1);
	gtk_widget_set_size_request (scrolledwindow1, 700, 370 );
	viewport1 = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport1);
	gtk_container_add (GTK_CONTAINER (scrolledwindow1), viewport1);
/*fprintf(stdout, " vp\n");
fflush(stdout);*/
	drawingarea1 = gtk_drawing_area_new ();
	gtk_widget_set_size_request (drawingarea1, wsWidth, wsHeight);
	gtk_widget_show (drawingarea1);
	gtk_widget_set_hexpand (drawingarea1, TRUE);
	gtk_widget_set_vexpand (drawingarea1, TRUE);
	gtk_container_add (GTK_CONTAINER (viewport1), drawingarea1);
/*fprintf(stdout, " da\n");
fflush(stdout);*/
	g_signal_connect (drawingarea1, "draw", G_CALLBACK(draw_event), NULL);
	g_signal_connect (drawingarea1,"configure-event", G_CALLBACK (configure_event), NULL);
	g_signal_connect (G_OBJECT (drawingarea1), "motion_notify_event",
		      G_CALLBACK(motion_notify_event), NULL);
	g_signal_connect (G_OBJECT (drawingarea1), "button_release_event",
		      G_CALLBACK(button_release_event), NULL);

  gtk_widget_set_events (drawingarea1, GDK_EXPOSURE_MASK
			 | GDK_LEAVE_NOTIFY_MASK
			 | GDK_BUTTON_PRESS_MASK
			 | GDK_BUTTON_RELEASE_MASK
			 | GDK_POINTER_MOTION_MASK
			 | GDK_POINTER_MOTION_HINT_MASK);

			menu = gtk_menu_new ();
/*			menu_items = gtk_menu_item_new_with_label ("Cut");
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);*/
/* Do something interesting when the menuitem is selected */
/*			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), "Cut");*/
/* Show the widget */
/*			gtk_widget_show (menu_items);*/
			menu_items = gtk_menu_item_new_with_label (_("Copy"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Copy"));
/* Show the widget */
			gtk_widget_show (menu_items);
			menu_items = gtk_menu_item_new_with_label (_("Paste"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Paste"));
/* Show the widget */
			gtk_widget_show (menu_items);
			menu_items = gtk_menu_item_new_with_label (_("Delete"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Delete"));
/* Show the widget */
			gtk_widget_show (menu_items);
/*			menu_items = gtk_menu_item_new_with_label ("Open");
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);*/
/* Do something interesting when the menuitem is selected */
/*			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), "Open");*/
/* Show the widget */
/*			gtk_widget_show (menu_items);*/
			menu_items = gtk_menu_item_new_with_label (_("Options"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Options"));
/* Show the widget */
			gtk_widget_show (menu_items);
/*			menu_items = gtk_menu_item_new_with_label ("UI");
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
 Do something interesting when the menuitem is selected
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), "UI");
 Show the widget
			gtk_widget_show (menu_items);*/
			menu_items = gtk_menu_item_new_with_label (_("Help"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Help"));
/* Show the widget */
			gtk_widget_show (menu_items);
/*			menu_items = gtk_menu_item_new_with_label ("New");
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), "New");
			gtk_widget_show (menu_items);*/
			menu_items = gtk_menu_item_new_with_label (_("View output"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("View output"));
/* Show the widget */
			gtk_widget_show (menu_items);
			menu_items = gtk_menu_item_new_with_label (_("Save output"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Save output"));
/* Show the widget */
			gtk_widget_show (menu_items);
			menu_items = gtk_menu_item_new_with_label (_("Clear"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Clear"));
/* Show the widget */
			gtk_widget_show (menu_items);
			menu_items = gtk_menu_item_new_with_label (_("Toggle VIP"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("VIP"));
/* Show the widget */
			gtk_widget_show (menu_items);
			menu_items = gtk_menu_item_new_with_label (_("Open macro"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Open macro"));
/* Show the widget */
			gtk_widget_show (menu_items);

			menu_items = gtk_menu_item_new_with_label (_("Destroy macro"));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
/* Do something interesting when the menuitem is selected */
			g_signal_connect_swapped (G_OBJECT (menu_items), "activate", G_CALLBACK (menuitem_response), g_strdup("Destroy macro"));
/* Show the widget */
			gtk_widget_show (menu_items);
/*			gtk_signal_connect (GTK_OBJECT (drawingarea1), "button_press_event",
		      (GtkSignalFunc) button_press_event, NULL);*/
			g_signal_connect_swapped (G_OBJECT (drawingarea1), "button_press_event",
                              G_CALLBACK (button_press_event),
                              G_OBJECT (menu));
	threadWS = NULL;
	myWS = ws;
	gdk_rgba_parse(&normal_color, "green");
	gdk_rgba_parse(&prelight_color, "pink");
	gdk_rgba_parse(&selected_color, "gray");
	gdk_rgba_parse(&active_color, "blue");
	gdk_rgba_parse(&intensive_color, "red");
	gdk_rgba_parse(&white_color, "white");
	gdk_rgba_parse(&black_color, "black");
	playout = gtk_widget_create_pango_layout (drawingarea1, "Text");
	myWS = ws;
	myWSWidth = wsWidth;
	myWSHeight = wsHeight;
	selectedNode = (Node*)NULL;
	manySelected = 0;
	Moved = 0;
	selectedInPort = -1;
	selectedOutPort = -1;
	ept_ID = 0;
	threadGUI = g_thread_self();
	return window1;
}

void drawWorkspace(Workspace*ws, GtkWidget *drawingarea1, int wsWidth, int wsHeight)
{
	ListOfNodes *curr;
	int i;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[0] == MACRO_NUMBER )
			drawNode(curr->node, drawingarea1, wsWidth, wsHeight, 1);
	}
}

void clearWorkspace(Workspace*ws, GtkWidget *drawingarea1, int wsWidth, int wsHeight)
{
	ListOfNodes *curr;
	Node*ne;
	int i, j, k;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[0] == MACRO_NUMBER ) {
			drawNode(curr->node, drawingarea1, wsWidth, wsHeight, 0);
			for ( j = 0; j < curr->node->nInConns; j++ ) {
				for ( k =0; k < curr->node->inConn[j].nConn; k++ ) {
					ne = nodeByID( ws->listOfNodes, curr->node->inConn[j].channel[k]->sourceNodeID);
					if ( ne->info->array[0] != MACRO_NUMBER ) {
						drawConnection3(curr->node->inConn[j].channel[k], 0);
					}
				}
			}
		}
	}
}


void drawSelectedNodes(Workspace*ws, GtkWidget *drawingarea1, int wsWidth, int wsHeight, int visible)
{
	ListOfNodes *curr;
	Node*ne;
	int i, j, k;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[ curr->node->info->n - 1 ] == 1 ) {
			drawNode(curr->node, drawingarea1, wsWidth, wsHeight, visible);
			for ( j = 0; j < curr->node->nInConns; j++ ) {
				for ( k = 0; k < curr->node->inConn[j].nConn; k++ ) {
					ne = nodeByID( ws->listOfNodes, curr->node->inConn[j].channel[k]->sourceNodeID);
					if ( ne->info->array[ ne->info->n - 1 ] != 1 ) {
						drawConnection3(curr->node->inConn[j].channel[k], visible);
					}
				}
			}
		}
	}
}


void drawNode(Node*node, GtkWidget *drawingarea1, int wsWidth, int wsHeight, int visible)
{
	int i, k;
	cairo_t *cr;
	cr = cairo_create (surface);
/*	if (!pixmap)
		fprintf(stdout,"meoe!");*/
	g_mutex_lock (&(node->statusMutex));
	if ( !visible ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
	} else if ( !node->info->array[ node->info->n - 1] ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&normal_color);
	} else {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&prelight_color);
	}
	g_mutex_unlock (&(node->statusMutex));
	cairo_rectangle (cr, node->info->array[2], node->info->array[1], node->info->array[3], node->info->array[4]);
	cairo_fill (cr);
	gtk_widget_queue_draw_area (drawingarea1, node->info->array[2], node->info->array[1], node->info->array[3], node->info->array[4]);
	if ( !visible ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
	} else {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&selected_color);
	}
	for ( i = 0; i < node->nInConns; i++) {
		cairo_rectangle (cr, node->info->array[2], node->info->array[1] + (i + 1) * 15 - 5, 10, 10);
		cairo_fill (cr);
		gtk_widget_queue_draw_area (drawingarea1, node->info->array[2], node->info->array[1]  + (i + 1) * 15 - 5, 10, 10);
	}
	if ( !visible ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
	} else {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&active_color);
	}
	for ( i = 0; i < node->nOutConns; i++) {
		cairo_rectangle (cr, node->info->array[2] + node->info->array[3] - 10, node->info->array[1] + (i + 1) * 15 - 5, 10, 10);
		cairo_fill (cr);
		gtk_widget_queue_draw_area (drawingarea1, node->info->array[2] + node->info->array[3] - 10, node->info->array[1]  + (i + 1) * 15 - 5, 10, 10);
		for ( k = 0; k < node->outConn[i].nConn; k++) {
			drawConnection3(node->outConn[i].channel[k], visible);
		}
	}
	drawNodeLabel(node, visible);
	if ( visible ) {
		traceNe(node);
	}
	if ( node->type == macro ) {
		if ( !visible ) {
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
		} else {
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&black_color);
		}
		cairo_rectangle (cr, node->info->array[2], node->info->array[1], 5, 5);
		cairo_fill (cr);
		gtk_widget_queue_draw_area (drawingarea1, node->info->array[2], node->info->array[1], 5, 5);
	}
	if ( node->vip == 1 ) {
		if ( !visible ) {
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
		} else {
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&black_color);
		}
		cairo_rectangle (cr, node->info->array[2] + node->info->array[3] - 5, node->info->array[1], 5, 5);
		cairo_fill (cr);
		gtk_widget_queue_draw_area (drawingarea1, node->info->array[2] + node->info->array[3] - 5, node->info->array[1], 5, 5);
	}
	cairo_destroy (cr);
}

void drawSelectedOutPort( Node*ne, int port, int state )
{
	cairo_t *cr;
	cr = cairo_create (surface);
	int k;
	if ( !state ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&active_color);
	} else {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&intensive_color);
	}
	cairo_rectangle (cr, ne->info->array[2] + ne->info->array[3] - 10, ne->info->array[1] + (port + 1) * 15 - 5, 10, 10);
	cairo_fill (cr);
	gtk_widget_queue_draw_area (drawingarea1, ne->info->array[2] + ne->info->array[3] - 10, ne->info->array[1]  + (port + 1) * 15 - 5, 10, 10);
	for ( k = 0; k < ne->outConn[port].nConn; k++) {
		drawConnection3(ne->outConn[port].channel[k], state + 1);
	}
	cairo_destroy (cr);
}

void drawSelectedInPort( Node*ne, int port, int state )
{
	cairo_t *cr;
	cr = cairo_create (surface);
	if ( !state ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&selected_color);
	} else {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&intensive_color);
	}
	cairo_rectangle (cr, ne->info->array[2], ne->info->array[1] + (port + 1) * 15 - 5, 10, 10);
	cairo_fill (cr);
	gtk_widget_queue_draw_area (drawingarea1, ne->info->array[2], ne->info->array[1]  + (port + 1) * 15 - 5, 10, 10);
	if ( ne->inConn[port].nConn > 0 ) {
		drawConnection3(ne->inConn[port].channel[0], state + 1);
	}
	cairo_destroy (cr);
}

void drawConnection(Connection*p, int xF, int yF, int visible)
{
	int xT, yT;
	Node *ne;
	ListOfNodes *curr;
	ne = (Node*)NULL;
	for (curr = myWS->listOfNodes; curr; curr = curr->next) {
		if ( curr->node->ID == p->destNodeID ) {
			ne = curr->node;
			break;
		}
	}
	if ( !ne )
		return;
	xT = ne->info->array[2];
	yT = ne->info->array[1] + p->destPortID * 15;
	drawConnection2(p, xF, yF, xT, yT, visible);
}

void drawConnection2(Connection*p, int xF, int yF, int xT, int yT, int visible)
{
	cairo_t *cr;
	cr = cairo_create (surface);
	if ( visible == 0 ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
	} else if ( visible == 1 ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&black_color);
	} else if ( visible == 2 ) {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&intensive_color);
	}
	cairo_set_line_width(cr, 2);
	if ( xT > xF ) {
		cairo_move_to(cr, xF, yF);
		cairo_line_to(cr, (xT + xF)/2, yF);
		cairo_stroke (cr);
		gtk_widget_queue_draw_area (drawingarea1, xF - 1, yF - 1, (xT - xF)/2 + 2, 3);
		cairo_move_to(cr, (xT + xF)/2, yF);
		cairo_line_to(cr, (xT + xF)/2, yT);
		cairo_stroke (cr);
		if (yT > yF) {
			gtk_widget_queue_draw_area (drawingarea1, (xT + xF)/2 - 1, yF - 1, 3, yT - yF + 2);
		} else {
			gtk_widget_queue_draw_area (drawingarea1, (xT + xF)/2 - 1, yT - 1, 3, yF - yT + 2);
		}
		cairo_move_to(cr, (xT + xF)/2, yT);
		cairo_line_to(cr, xT, yT);
		cairo_stroke (cr);
		gtk_widget_queue_draw_area (drawingarea1, (xT + xF)/2 - 1, yT - 1, (xT - xF)/2 + 2, 3);
	} else /*if ( yT > yF )*/ {
		cairo_move_to(cr, xF, yF);
		cairo_line_to(cr, xF + 5, yF);
		cairo_stroke (cr);
		gtk_widget_queue_draw_area (drawingarea1, xF -1, yF - 1, 6, 3);
		if (yT > yF) {
			cairo_move_to(cr, xF + 5, yF);
			cairo_line_to(cr, xF + 5, (yT + yF) / 2);
			cairo_stroke (cr);
			gtk_widget_queue_draw_area (drawingarea1, xF + 4, yF - 1, 3, (yT - yF) / 2 + 2);
		} else {
			cairo_move_to(cr, xF + 5, (yT + yF) / 2);
			cairo_line_to(cr, xF + 5, yF);
			cairo_stroke (cr);
			gtk_widget_queue_draw_area (drawingarea1, xF + 4, (yT + yF) / 2, 3, (yF - yT) / 2 + 2);
		}
		cairo_move_to(cr, xF + 5, (yT + yF) / 2);
		cairo_line_to(cr, xT - 5, (yT + yF) / 2);
		cairo_stroke (cr);
		gtk_widget_queue_draw_area (drawingarea1, xT - 5, (yT + yF) / 2 - 1, xF - xT + 10, 3);
		cairo_move_to(cr, xT - 5, (yT + yF) / 2);
		cairo_line_to(cr, xT - 5, yT);
		cairo_stroke (cr);
		if (yT > yF) {
			gtk_widget_queue_draw_area (drawingarea1, xT - 6, (yT + yF) / 2, 3, (yT - yF) / 2);
		} else {
			gtk_widget_queue_draw_area (drawingarea1, xT - 6, yT, 3, (yF - yT) / 2);
		}
		cairo_move_to(cr, xT - 5, yT);
		cairo_line_to(cr, xT , yT);
		cairo_stroke (cr);
		gtk_widget_queue_draw_area (drawingarea1, xT - 5, yT - 1, 5, 3);
	}
	cairo_destroy (cr);
}

void drawConnection3(Connection*p, int visible)
{
	int xF;
	int yF;
	int xT;
	int yT;
	int i, j;
	Node *ne1, *ne2;
	j = -2;
	if ( !( ne1 = nodeByID(myWS->listOfNodes, p->destNodeID) ) )
		return;
	i = ne1->info->array[0];
	if ( i == MACRO_NUMBER ) {
		xT = ne1->info->array[2];
		yT = p->destPortID * 15 + ne1->info->array[1];
		j++;
	}
	if ( !( ne2 = nodeByID(myWS->listOfNodes, p->sourceNodeID) ) )
		return;
	i = ne2->info->array[0];
	if ( i == MACRO_NUMBER ) {
		xF = ne2->info->array[2] + ne2->info->array[3];
		yF = ne2->info->array[1] + p->sourcePortID * 15;
		j++;
	}
	if ( j == 0 )
		drawConnection2(p, xF, yF, xT, yT, visible);
}

Node *getSelected(Workspace*ws, int x, int y, int *inPort, int *outPort, int *Conn)
{
	int i;
	Node *ne = (Node*)NULL;
	ListOfNodes *curr;
	*inPort = -1;
	*outPort = -1;
	*Conn = -1;
		for (curr = ws->listOfNodes; curr; curr = curr->next) {
			if ( curr->node->info->array[0] == MACRO_NUMBER && curr->node->info->array[2] <= x && x <= curr->node->info->array[2] + curr->node->info->array[3] && curr->node->info->array[1] <= y && y <= curr->node->info->array[1] + curr->node->info->array[4] ) {
				ne = curr->node;
				for ( i = 0; i < curr->node->nInConns; i++) {
					if ( curr->node->info->array[2] <= x && x <= curr->node->info->array[2] + 10 && curr->node->info->array[1] + (i + 1) * 15 - 5 <= y && y <= curr->node->info->array[1] + (i + 1) * 15 + 5 ) {
						*inPort = i;
						break;
					}
				}
				for ( i = 0; i < curr->node->nOutConns; i++) {
					if ( curr->node->info->array[2] + curr->node->info->array[3] - 10 <= x && x <= curr->node->info->array[2] + curr->node->info->array[3] && curr->node->info->array[1] + (i + 1) * 15 - 5 <= y && y <= curr->node->info->array[1] + (i + 1) * 15 + 5 ) {
						*outPort = i;
						break;
					}
				}
				break;
			}
		}
	return ne;
}

gboolean button_press_event( GtkWidget *widget, GdkEventButton *event )
{
	int x = event->x;
	int y = event->y;
	Node *ne;
	int inPort;
	int outPort;
	int Conn;
	EditorOperation*eop;
	if (event->button == 1 /*&& pixmap != NULL*/) {
		if ( (ne = getSelected(myWS, x, y, &inPort, &outPort, &Conn))) {
			if ( inPort == -1 && outPort == -1 && Conn == -1) {
				selectedInPort = -1;
				selectedOutPort = -1;
				selectedNode = ne;
			}
		}
		if ( event->type==GDK_2BUTTON_PRESS || event->type==GDK_3BUTTON_PRESS ) {
//			printf("I feel %s clicked with button %d\n", event->type==GDK_2BUTTON_PRESS ? "double" : "triple", event->button);
			if ( selectedNode ) {
				uiNode(selectedNode);
			}
		}
	} else if (event->button == 3 /*&& pixmap != NULL*/) {
		GdkEventButton *bevent = (GdkEventButton *) event;
		event_x = x;
		event_y = y;
		if ( (ne = getSelected(myWS, x, y, &inPort, &outPort, &Conn))) {
/*
			unselect all nodes*/
			clear_node_selections(myWS);
/**/
			selectedInPort = inPort;
			selectedOutPort = outPort;
			selectedNode = ne;
			ne->info->array[ ne->info->n - 1] = 1;
			drawNode(ne, drawingarea1, myWSWidth, myWSHeight, 1);
		}
		gtk_menu_popup (GTK_MENU (widget), NULL, NULL, NULL, NULL, bevent->button, bevent->time);
	} else if ( ( event->state & GDK_SHIFT_MASK ) & GDK_SHIFT_MASK ) {
		Node*ne, *ne2;
		int n;
		if ( mod1_node_enabled ) {
			mod1Node();
			ctrlNode->type = ins;
		}
		event_x = x;
		event_y = y;
		ne = placeNode();
		ne2 = dupNode(ne);
		ne2->ID = ne->ID;
		eop = newEditorOperation(ept_add_node, ne2->ID, (void*)ne2);
		n = g_list_position ( operList, undoList );
		n = ( n < 0 ) ? 0 : n;
		operList = g_list_insert ( operList, (gpointer)eop, n);
		undoList = g_list_nth(operList, n);
		gtk_widget_set_sensitive(undoButton, TRUE);
		bambu_set_title(0);
	} else if ( ( event->state & GDK_CONTROL_MASK ) & GDK_CONTROL_MASK ) {
		Node*ne, *ne2;
		int n;
		if ( mod1_node_enabled ) {
			mod1Node();
			ctrlNode->type = ous;
		}
		event_x = x;
		event_y = y;
		ne = placeNode();
		ne2 = dupNode(ne);
		ne2->ID = ne->ID;
		eop = newEditorOperation(ept_add_node, ne2->ID, (void*)ne2);
		n = g_list_position ( operList, undoList );
		n = ( n < 0 ) ? 0 : n;
		operList = g_list_insert ( operList, (gpointer)eop, n);
		undoList = g_list_nth(operList, n);
		gtk_widget_set_sensitive(undoButton, TRUE);
		bambu_set_title(0);
	} else if ( mod1_node_enabled && event->button == 2 ) {//(( event->state & GDK_MOD1_MASK ) & GDK_MOD1_MASK )) {
		Node*ne, *ne2;
		int n;
		mod1Node();
		event_x = x;
		event_y = y;
		ne = placeNode();
		ne2 = dupNode(ne);
		ne2->ID = ne->ID;
		eop = newEditorOperation(ept_add_node, ne2->ID, (void*)ne2);
		n = g_list_position ( operList, undoList );
		n = ( n < 0 ) ? 0 : n;
		operList = g_list_insert ( operList, (gpointer)eop, n);
		undoList = g_list_nth(operList, n);
		gtk_widget_set_sensitive(undoButton, TRUE);
		bambu_set_title(0);
	} else if (event->button == 2 /*&& pixmap != NULL*/) {
		Node*ne, *ne2;
		int n;
		event_x = x;
		event_y = y;
		ne = placeNode();
		ne2 = dupNode(ne);
		ne2->ID = ne->ID;
		eop = newEditorOperation(ept_add_node, ne2->ID, (void*)ne2);
		n = g_list_position ( operList, undoList );
		n = ( n < 0 ) ? 0 : n;
		operList = g_list_insert ( operList, (gpointer)eop, n);
		undoList = g_list_nth(operList, n);
		gtk_widget_set_sensitive(undoButton, TRUE);
		bambu_set_title(0);
	}
	return TRUE;
}

void menuitem_response( gchar *string )
{
	if ( !selectedNode )
		winError2("Node is not selected");/*^^*/
	if ( !strcmp((const char*)string, "View output") ) {
		if ( selectedNode && selectedOutPort != -1 && selectedOutPort < selectedNode->nOutConns && selectedNode->status == completed  && selectedNode->uid != NULL/*&& selectedNode->outConn[selectedOutPort].nConn > 0 && selectedNode->outConn[selectedOutPort].channel[0]->flag == 1 */)
			viewOutput();
		if ( selectedNode && selectedInPort != -1 && selectedInPort < selectedNode->nInConns) {
			int id = selectedNode->inConn[selectedInPort].channel[0]->sourceNodeID;
			Node *ne_id = nodeByID( myWS->listOfNodes, id);
			if (ne_id->status == completed  && ne_id->uid != NULL) {
				viewInput();
			}
		}
	} else if ( !strcmp((const char*)string, "Save output") ) {
		if ( selectedNode && selectedOutPort != -1 && selectedOutPort < selectedNode->nOutConns && selectedNode->status == completed  && selectedNode->uid != NULL/*&& selectedNode->outConn[selectedOutPort].nConn > 0 && selectedNode->outConn[selectedOutPort].channel[0]->flag == 1 */)
			saveOutput();
	} else if ( !strcmp((const char*)string, "Options") ) {
		if ( selectedNode ) {
			uiNode(selectedNode);
		}
	} else if ( !strcmp((const char*)string, "Delete") ) {
		if ( selectedNode ) {
			ept_ID++;
			if ( selectedNode->type == macro ) {
				int i;
				ListOfNodes*curr;
				for (curr = myWS->listOfNodes, i = 0; curr && i < myWS->numberOfNodes; curr = curr->next, i++) {
					if ( curr->node->info->array[0] == selectedNode->info->array[5] ) {
						menu_item_delete_node(curr->node);
						curr = myWS->listOfNodes;
					}
				}
				destroyMacro(selectedNode);
			} else {
				menu_item_delete_node(selectedNode);
			}
			selectedNode = NULL;
			bambu_set_title(0);
		}
	} else if ( !strcmp((const char*)string, "Copy") ) {
		if ( selectedNode ) {
			on_copy_activate(myWS);
		}
	} else if ( !strcmp((const char*)string, "Paste") ) {
		on_paste_activate(myWS);
	} else if ( !strcmp((const char*)string, "Help") ) {
		if ( selectedNode && selectedNode->type != macro )
			helpNode(selectedNode);
	} else if ( !strcmp((const char*)string, "Clear") ) {
		if ( selectedNode )
			undoDownSrtream(selectedNode);
	} else if ( !strcmp((const char*)string, "VIP") ) {
		if ( selectedNode ) {
			selectedNode->vip = 1 - selectedNode->vip;
			drawNode(selectedNode, drawingarea1, myWSWidth, myWSHeight, 1);
		}
	} else if ( !strcmp((const char*)string, "Open macro") ) {
		if ( selectedNode && selectedNode->type == macro )
			openMacro(selectedNode);
	} else if ( !strcmp((const char*)string, "Destroy macro") ) {
		if ( selectedNode && selectedNode->type == macro ) {
			ept_ID++;
			destroyMacro(selectedNode);
		}
	}
}

void menu_item_delete_node(Node*old_node)
{
	EditorOperation*eop;
	Node*ne;
	int n;
	int i, j, v;
	v = 0;
	for ( i = 0; i < old_node->nInConns; i++ ) {
		for ( j = 0; j < old_node->inConn[i].nConn; j++ ) {
			eop = newEditorOperation(ept_delete_connection, old_node->inConn[i].channel[j]->sourceNodeID, (void*)dupConnection(old_node->inConn[i].channel[j]));
			eop->vector = ept_ID;
			n = g_list_position ( operList, undoList );
			n = ( n < 0 ) ? 0 : n;
			operList = g_list_insert ( operList, (gpointer)eop, n);
			undoList = g_list_nth(operList, n);
			gtk_widget_set_sensitive(undoButton, TRUE);
			v++;
		}
	}
	for ( i = 0; i < old_node->nOutConns; i++ ) {
		for ( j = 0; j < old_node->outConn[i].nConn; j++ ) {
			eop = newEditorOperation(ept_delete_connection, old_node->ID, (void*)dupConnection(old_node->outConn[i].channel[j]));
			eop->vector = ept_ID;
			n = g_list_position ( operList, undoList );
			n = ( n < 0 ) ? 0 : n;
			operList = g_list_insert ( operList, (gpointer)eop, n);
			undoList = g_list_nth(operList, n);
			gtk_widget_set_sensitive(undoButton, TRUE);
			v++;
		}
	}
	n = old_node->ID;
	ne = dupNode(old_node);
	ne->ID = n;
	deleteNodeF(old_node);
	eop = newEditorOperation(ept_delete_node, ne->ID, (void*)ne);
	eop->vector = ept_ID;
	n = g_list_position ( operList, undoList );
	n = ( n < 0 ) ? 0 : n;
	operList = g_list_insert ( operList, (gpointer)eop, n);
	undoList = g_list_nth(operList, n);
	gtk_widget_set_sensitive(undoButton, TRUE);
}

void pasteNodeF(Node*node)
{
	Node*ne;
/*	EditorOperation*eop;
	ne = dupNode(copyNode);
	copyNode->nOutConns = 0;
	free(copyNode->outConn);
	copyNode->nInConns = 0;
	free(copyNode->inConn);
	deleteNode(copyNode);
	copyNode = (Node*)NULL;*/
	node->info->array[2] = event_x;
	node->info->array[1] = event_y;
	if ( node->type == macro ) {
		ListOfNodes*curr;
		Node *ne1, *ne2;
		Connection*conn;
		int i, j, k;
		clear_node_selections1(myWS);
		node->info->array[ node->info->n - 1 ] = 0;
		for (curr = myWS->listOfNodes, i = 0; curr && i < myWS->numberOfNodes; curr = curr->next, i++) {
			if ( curr->node->info->array[0] == node->info->array[5] ) {
				if ( curr->node->info->array[6] != -1 ) {
					ne = dupNode(curr->node);
					workspaceAddNode(myWS, ne);
					ne->info->array[ ne->info->n - 1 ] = 1;
					ne->info->array[0] = -1;
					ne->info->array[6] = curr->node->ID;
					curr->node->info->array[7] = ne->ID;
					curr->node->info->array[6] = -1;
					ne->info->array[2] += 10;
					ne->info->array[1] += 10;
				} else {
					ne = nodeByID( myWS->listOfNodes, curr->node->info->array[7]);
				}
				for ( j = 0; j < curr->node->nOutConns; j++ ) {
					for ( k = 0; k < curr->node->outConn[j].nConn; k++ ) {
						ne1 = nodeByID( myWS->listOfNodes, curr->node->outConn[j].channel[k]->destNodeID);
						if ( ne1->info->array[0] == node->info->array[5] ) {
							if ( ne1->info->array[6] != -1 ) {
								ne2 = dupNode( ne1 );
								workspaceAddNode(myWS, ne2);
								ne2->info->array[ ne2->info->n - 1 ] = 1;
								ne2->info->array[0] = -1;
								ne2->info->array[6] = ne1->ID;
								ne1->info->array[6] = -1;
								ne1->info->array[7] = ne2->ID;
								ne2->info->array[2] += 10;
								ne2->info->array[1] += 10;
							} else {
								ne2 = nodeByID(myWS->listOfNodes, ne1->info->array[7]);
							}
							conn = newConnection( ne->ID, j + 1, ne2->ID, curr->node->outConn[j].channel[k]->destPortID);
							installConnection(myWS->listOfNodes, conn);
						}
					}
				}
			}
		}
		workspaceAddMacro(myWS, node);
		setBBNode(node);
		clear_node_selections1(myWS);
		for (curr = myWS->listOfNodes, i = 0; curr && i < myWS->numberOfNodes; curr = curr->next, i++) {
			curr->node->info->array[7] = 0;
			curr->node->info->array[6] = 0;
		}
		redrawWorkspace();
	} else {
		drawNode(node, drawingarea1, myWSWidth, myWSHeight, 1);
		workspaceAddNode(myWS, node);
	}
}

void copyNodeF(Node*node)
{
	if ( copyNode ) {
		if ( copyNode->nOutConns ) {
			copyNode->nOutConns = 0;
			free(copyNode->outConn);
		}
		if ( copyNode->nInConns ) {
			copyNode->nInConns = 0;
			free(copyNode->inConn);
		}
		deleteNode(copyNode);
	}
	copyNode = dupNode(selectedNode);
}

void on_paste_activate(Workspace*wksp)
{
	GKeyFile*gkf;
	char *str;
	GtkClipboard *cb;
	Workspace*addon;
	GError*gerror = NULL;
	gkf = g_key_file_new();
	cb = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
	str = gtk_clipboard_wait_for_text(cb);
	if ( !str )
		return;
	if ( !g_key_file_load_from_data(gkf, (const gchar*) str, strlen(str), G_KEY_FILE_NONE, &gerror)) {
		g_warning("on_paste_activate error:%s", gerror->message);
		g_error_free(gerror);
		return;
	}
	g_free(str);
	addon = workspaceParse(gkf);
	if ( addon ) {
		clear_node_selections1(addon);
		workspaceTrimCoordinates(addon);
		workspaceNormalizeIDs(addon);
		workspaceCorrectMacroOnPaste(addon, MACRO_NUMBER);
		workspacePaste(wksp, addon, event_x, event_y);
		workspaceDeleteStruct(addon);
		redrawWorkspace();
	}
	g_key_file_free(gkf);
	return;
}

void on_copy_activate(Workspace*wksp)
{
	static GtkTargetEntry targets[] = {{"UTF8_STRING", 0, GDK_TARGET_STRING}};
	GtkClipboard *cb;
	cb = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
	gtk_clipboard_set_with_data(cb, targets, 1, cb_get_func, cb_clear_func, (gpointer)wksp);
}

void cb_get_func(GtkClipboard *cb, GtkSelectionData *sdata, guint info, gpointer data)
{
	GKeyFile*gkf;
	GError*gerror = NULL;
	char *str;
	Node *ne, *ne1;
	Workspace*wksp;
	ListOfNodes *curr;
	int i, j, k, n;
	gsize len;
	gkf = g_key_file_new();
	wksp = (Workspace*)data;
	workspacePrint(wksp, gkf);
	n = 0;
	for (curr = wksp->listOfNodes, i = 0; curr && i < wksp->numberOfNodes; curr = curr->next, i++) {
		ne = curr->node;
		if ( ne->info->array[ ne->info->n - 1] == 1) {
			nodePrint(ne, gkf);
			n++;
			if ( ne->type == macro ) {
				ListOfNodes*in_macro;
				Node *ne3;
				int ii, j, k;
				for (in_macro = wksp->listOfNodes, ii = 0; in_macro && ii < wksp->numberOfNodes; in_macro = in_macro->next, ii++) {
					ne3 = in_macro->node;
					if ( ne3->info->array[0] == ne->info->array[5] ) {
						if ( ne3->info->array[6] != -1 ) {
							nodePrint(ne3, gkf);
							n++;
							ne3->info->array[6] = -1;
							for ( j = 0; j < ne3->nOutConns; j++ ) {
								for ( k = 0; k < ne3->outConn[j].nConn; k++ ) {
									ne1 = nodeByID( wksp->listOfNodes, ne3->outConn[j].channel[k]->destNodeID);
									if ( ne1->info->array[0] == ne->info->array[5] ) {
										outPortPrint(ne3, j, k, gkf);
									}
								}
							}
						}
					}
				}
			}
			for ( j = 0; j < ne->nOutConns; j++ ) {
				for ( k = 0; k < ne->outConn[j].nConn; k++ ) {
					ne1 = nodeByID( wksp->listOfNodes, ne->outConn[j].channel[k]->destNodeID);
					if ( ne1->info->array[ne->info->n - 1] == 1 ) {
						outPortPrint(ne, j, k, gkf);
					}
				}
			}
		}
	}
	workspacePrintNNodes(wksp, gkf, n);
	str = g_key_file_to_data(gkf, &len, &gerror);
	if ( gerror ) {
		g_warning("node_print error:%s", gerror->message);
		g_error_free(gerror);
	}
	g_key_file_free(gkf);
	gtk_selection_data_set_text(sdata, str, len);
	g_free(str);
	for (curr = wksp->listOfNodes, i = 0; curr && i < wksp->numberOfNodes; curr = curr->next, i++) {
		curr->node->info->array[6] = 0;
	}
}

void cb_clear_func(GtkClipboard *cb, gpointer data)
{

}

void deleteNodeF(Node*node)
{
	int i, j;
	drawNode(node, drawingarea1, myWSWidth, myWSHeight, 0);
	for ( i = 0; i < node->nInConns; i++) {
		for ( j = 0; j < node->inConn[i].nConn; j++ ) {
			drawConnection3(node->inConn[i].channel[j], 0);
		}
	}
	g_mutex_lock(&(node->statusMutex));
	if ( node->status != waiting ) {
		g_mutex_unlock(&(node->statusMutex));
		undoDownSrtream(node);
	} else {
		g_mutex_unlock(&(node->statusMutex));
	}
	deleteNodeFromWS(myWS, node);
	redrawWorkspace();
}

gboolean nwdelete( GtkWidget *widget, gpointer data )
{
	gtk_widget_destroy((GtkWidget *)data);
	return FALSE;
}

/*
void setScratchNodeInterface(Node *node)
{
	scratchNode = node;
}

void setCopyNodeInterface(Node *node)
{
	copyNode = node;
}
*/
void uiNode(Node*ne)
{
	if ( ne->type == pam && strcmp(ne->pam->name,"save") ) {
		uiPam(ne);
	} else if ( ne->type == pam && !strcmp(ne->pam->name,"save") ) {
		uiSave(ne, mw);
	} else if ( ne->type == file ) {
		uiFile(ne, mw);
	} else if ( ne->type == ins ) {
		uiIns(ne, mw);
	} else if ( ne->type == ous ) {
		uiOus(ne, mw);
	} else if ( ne->type == display ) {
		uiDisplay(ne);
	} else if ( ne->type == macro ) {
		uiMacro(ne);
	}
}

void viewInput()
{
	GError *gerror = NULL;
	char*uri = NULL;
	int id = selectedNode->inConn[selectedInPort].channel[0]->sourceNodeID;
	Node *ne_id = nodeByID( myWS->listOfNodes, id);
	if ( !strcmp(ne_id->oServer->proto, "door") ) {
		if ( ne_id->type == ins )
			uri = g_strdup_printf("%s", ne_id->uid);
		else if ( ne_id->type == file )
			uri = g_strdup_printf("%s", ne_id->file);
		else if ( ne_id->type == pam ) {
			char*buf2 = door_get_config_path(ne_id->oServer->hname, ne_id->oServer->port, ne_id->oServer->proto);
			char*buf = g_strdup(selectedNode->inConn[selectedInPort].channel[selectedNode->inConn[selectedInPort].index]->file);
			uri = g_build_filename(buf2, buf, NULL);
			g_free(buf2);
			g_free(buf);
		}
	}
	if ( !uri ) {
		g_message("No uri");
		return;
	}
	g_message("uri: %s", uri);
	if ( !kimono_launch_default_for_uri((const char *)uri, &gerror) ) {
		if ( gerror ) {
			winError2(gerror->message);
			g_error_free(gerror);
			gerror = NULL;
		}
		if ( !g_app_info_launch_default_for_uri((const char *)uri, (GAppLaunchContext *)NULL, &gerror) ) {
			if ( gerror ) {
				winError2(gerror->message);
				g_error_free(gerror);
				gerror = NULL;
			} else {
				winError2(uri);
			}
		}
	}
	g_free(uri);
}

void viewOutput()
{
	GError *gerror = NULL;
	char*uri = NULL;
	if ( !strcmp(selectedNode->oServer->proto, "door") ) {
		if ( selectedNode->type == ins )
			uri = g_strdup_printf("%s", selectedNode->uid);
		else if ( selectedNode->type == file )
			uri = g_strdup_printf("%s", selectedNode->file);
		else if ( selectedNode->type == pam ) {
			char*buf, *buf2;
			char*ty;
			buf2 = door_get_config_path(selectedNode->oServer->hname, selectedNode->oServer->port, selectedNode->oServer->proto);
			ty = type2string(selectedNode->outConn[selectedOutPort].type);
			if ( selectedNode->outConn[selectedOutPort].type == any && selectedNode->nInConns > 0 ) {
				char**ext = g_strsplit(selectedNode->inConn[0].channel[selectedNode->inConn[0].index]->file, ".", MAXTOKENS);
				int next = g_strv_length(ext);
				ty = g_strdup(ext[next-1]);
				g_strfreev(ext);
			} else {
				ty = type2string2(selectedNode->outConn[selectedOutPort].type);
			}
			buf = g_strdup_printf("%s.out.%d.%s", selectedNode->uid, selectedOutPort, ty);
			uri = g_build_filename(buf2, buf, NULL);
			g_free(buf2);
			g_free(buf);
			free(ty);
		}
	}
	if ( !uri )
		return;
	if ( !kimono_launch_default_for_uri((const char *)uri, &gerror) ) {
		if ( gerror ) {
			winError2(gerror->message);
			g_error_free(gerror);
			gerror = NULL;
		}
		if ( !g_app_info_launch_default_for_uri((const char *)uri, (GAppLaunchContext *)NULL, &gerror) ) {
			if ( gerror ) {
				winError2(gerror->message);
				g_error_free(gerror);
				gerror = NULL;
			} else {
				winError2(uri);
			}
		}
	}
	g_free(uri);
}

void saveOutput()
{
	GtkWidget *dialog;
	GError *gerror = NULL;
	char*uri = NULL;
	if ( !strcmp(selectedNode->oServer->proto, "door") ) {
		if ( selectedNode->type == ins )
			uri = g_strdup_printf("%s", selectedNode->uid);
		else if ( selectedNode->type == file )
			uri = g_strdup_printf("%s", selectedNode->file);
		else if ( selectedNode->type == pam ) {
			char*buf, *buf2;
			char*ty;
			buf2 = door_get_config_path(selectedNode->oServer->hname, selectedNode->oServer->port, selectedNode->oServer->proto);
			ty = type2string(selectedNode->outConn[selectedOutPort].type);
			if ( selectedNode->outConn[selectedOutPort].type == any && selectedNode->nInConns > 0 ) {
				char**ext = g_strsplit(selectedNode->inConn[0].channel[selectedNode->inConn[0].index]->file, ".", MAXTOKENS);
				int next = g_strv_length(ext);
				ty = g_strdup(ext[next-1]);
				g_strfreev(ext);
			} else {
				ty = type2string2(selectedNode->outConn[selectedOutPort].type);
			}
			buf = g_strdup_printf("%s.out.%d.%s", selectedNode->uid, selectedOutPort, ty);
			uri = g_build_filename(buf2, buf, NULL);
			g_free(buf2);
			g_free(buf);
			free(ty);
		}
	}
	if ( !uri )
		return;
	dialog = gtk_file_chooser_dialog_new (_("Save output"),
				(GtkWindow*)mw,
				GTK_FILE_CHOOSER_ACTION_SAVE,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Save", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, TRUE);
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *newfilename;
		char*bufin, *bufout;
		gchar**margv;
		int argcp, flaggs;
		GString *command;
		newfilename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
		command = g_string_new("door_copy");
		bufout = g_shell_quote(newfilename);
		bufin = g_shell_quote(uri);
		g_string_append_printf(command, " %s %s", bufin, bufout);
		g_free(bufin);
		g_free(bufout);
		flaggs = G_SPAWN_SEARCH_PATH;
		if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
			if ( gerror ) {
				g_warning("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
				g_error_free(gerror);
			}
		} else {
			g_message("save_output: %s", command->str);
			if ( !g_spawn_async (NULL, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, NULL, &gerror) ) {
				if ( gerror ) {
					g_warning("g_spawn_sync failed for %s\nwith %s", command->str, gerror->message);
					g_error_free(gerror);
				}
				g_strfreev(margv);
			}
			g_string_free(command, TRUE);
			g_free (newfilename);
		}
	}
	gtk_widget_destroy (dialog);
	g_free(uri);
}


void helpNode(Node*ne)
{
	if ( selectedNode->type == pam ) {
		Node*sproxy;
		sproxy = proxy_sys_node("pam");
		sproxy->label = strcpy(sproxy->label, selectedNode->pam->name);
		sproxy->file = (char*)realloc(sproxy->file, MAX_RECORD * sizeof(char) );
		sproxy->oServer = selectedNode->oServer;
		sproxy->file = strcpy(sproxy->file, "message");
		door_call("GET", sproxy);
		winError2(sproxy->buf);
		free(sproxy->buf);
		proxy_sys_node_del(sproxy);
	}
}

gboolean button_release_event( GtkWidget *widget, GdkEventButton *event )
{
	int x = event->x;
	int y = event->y;
	Node *ne;
	int inPort;
	int outPort;
	int Conn;
	EditorOperation*eop;
	if (event->button == 1 /*&& pixmap != NULL*/) {
		if (!Moved) {
			if ( (ne = getSelected(myWS, x, y, &inPort, &outPort, &Conn))) {
				if ( inPort == -1 && outPort == -1 && Conn == -1) {
/*
			unselect all nodes if Ctrl is not pressed*/
/**/
					selectedInPort = -1;
					selectedOutPort = -1;
					if ( ne->info->array[ ne->info->n - 1] == 1) {
						if ( ! (( event->state & GDK_CONTROL_MASK ) & GDK_CONTROL_MASK ) ) {
							clear_node_selections(myWS);
						} else {
							ne->info->array[ ne->info->n - 1] = 0;
							selectedNode = findSelected(myWS);
						}
					} else {
						if ( ! (( event->state & GDK_CONTROL_MASK ) & GDK_CONTROL_MASK ) )
							clear_node_selections(myWS);
						ne->info->array[ ne->info->n - 1] = 1;
						selectedNode = ne;
					}
/*fprintf(stdout,"snode : %s %d\n", ne->label, ne->info->array[ ne->info->n - 1]);
fflush(stdout);*/
					drawNode(ne, drawingarea1, myWSWidth, myWSHeight, 1);
				} else if ( inPort != -1 && ne->info->array[ ne->info->n - 1] == 0 ) {
					if ( !selectedNode ) {
						selectedNode = ne;
						selectedInPort = inPort;
						drawSelectedInPort( selectedNode, selectedInPort, 1 );
					} else if ( selectedOutPort == -1 && selectedNode && selectedNode->ID != ne->ID ) {
						if ( selectedInPort != -1 ) {
							drawSelectedInPort( selectedNode, selectedInPort , 0 );
						}
						selectedNode = ne;
						selectedInPort = inPort;
						drawSelectedInPort( selectedNode, selectedInPort , 1 );
					} else if ( selectedNode && selectedNode->ID == ne->ID ) {
						if ( selectedOutPort != -1 ) {
							drawSelectedOutPort( selectedNode, selectedOutPort , 0);
						}
						if ( selectedInPort == inPort ) {
							drawSelectedInPort( selectedNode, selectedInPort , 0 );
							selectedNode = (Node*)NULL;
							selectedInPort = -1;
						} else {
							if ( selectedInPort != -1 ) {
								drawSelectedInPort( selectedNode, selectedInPort , 0 );
							}
							selectedNode = ne;
							selectedInPort = inPort;
							drawSelectedInPort( selectedNode, selectedInPort , 1 );
						}
					} else if ( selectedOutPort != -1 && selectedNode && selectedNode->ID != ne->ID ) {
						if ( ne->inConn[inPort].nConn == 0 ) {
							Connection*conn;
							int n;
							ept_ID++;
							conn = newConnection(selectedNode->ID, selectedOutPort + 1, ne->ID, inPort + 1);
							installConnection(myWS->listOfNodes, conn );
							drawSelectedOutPort( selectedNode, selectedOutPort , 0);
							eop = newEditorOperation(ept_add_connection, selectedNode->ID, (void*)dupConnection(conn));
							eop->vector = ept_ID;
							n = g_list_position ( operList, undoList );
							n = ( n < 0 ) ? 0 : n;
							operList = g_list_insert ( operList, (gpointer)eop, n);
							undoList = g_list_nth(operList, n);
							gtk_widget_set_sensitive(undoButton, TRUE);
							if ( ne->type == macro || selectedNode->type == macro ) {
								connect_macro(selectedNode, selectedOutPort, ne, inPort);
							}
							bambu_set_title(0);
						} else {
							int i;
							int n;
							Node *ne2;
							Connection*conn;
							int sourcePortID;
							for ( i = 0; i < ne->inConn[inPort].nConn; i++ ) {
								ne2 = nodeByID(myWS->listOfNodes, ne->inConn[inPort].channel[i]->sourceNodeID);
								sourcePortID = ne->inConn[inPort].channel[i]->sourcePortID - 1;
								if ( ne2->info->array[0] == ne->info->array[0] ) {
									drawConnection3(ne->inConn[inPort].channel[i], 0);
									break;
								}
							}
							ept_ID++;
							disconnect_1(ne, inPort);
							redrawWorkspace();
							if ( ne2->ID != selectedNode->ID || selectedOutPort != sourcePortID ) {
								conn = newConnection(selectedNode->ID, selectedOutPort + 1, ne->ID, inPort + 1);
								installConnection(myWS->listOfNodes,conn );
								drawSelectedOutPort( selectedNode, selectedOutPort , 0);
								eop = newEditorOperation(ept_add_connection, ne2->ID, (void*)dupConnection(conn));
								eop->vector = ept_ID;
								n = g_list_position ( operList, undoList );
								n = ( n < 0 ) ? 0 : n;
								operList = g_list_insert ( operList, (gpointer)eop, n);
								undoList = g_list_nth(operList, n);
								if ( ne->type == macro || selectedNode->type == macro ) {
									connect_macro(selectedNode, selectedOutPort, ne, inPort);
								}
								gtk_widget_set_sensitive(undoButton, TRUE);
							}
							bambu_set_title(0);
						}
						selectedNode = (Node*)NULL;
						selectedInPort = -1;
						selectedOutPort = -1;
					}
				} else if ( outPort != -1 && ne->info->array[ ne->info->n - 1] == 0 ) {
					if ( !selectedNode ) {
						selectedNode = ne;
						selectedOutPort = outPort;
						drawSelectedOutPort( selectedNode, selectedOutPort, 1 );
					} else if ( selectedInPort == -1 && selectedNode && selectedNode->ID != ne->ID ) {
						if ( selectedOutPort != -1 ) {
							drawSelectedOutPort( selectedNode, selectedOutPort , 0 );
						}
						selectedNode = ne;
						selectedOutPort = outPort;
						drawSelectedOutPort( selectedNode, selectedOutPort , 1 );
					} else if ( selectedNode && selectedNode->ID == ne->ID ) {
						if ( selectedInPort != -1 ) {
							drawSelectedInPort( selectedNode, selectedInPort , 0);
						}
						if ( selectedOutPort == outPort ) {
							drawSelectedOutPort( selectedNode, selectedOutPort , 0 );
							selectedNode = (Node*)NULL;
							selectedOutPort = -1;
						} else {
							if ( selectedOutPort != -1 ) {
								drawSelectedOutPort( selectedNode, selectedOutPort , 0 );
							}
							selectedNode = ne;
							selectedOutPort = outPort;
							drawSelectedOutPort( selectedNode, selectedOutPort , 1 );
						}
					} else if ( selectedInPort != -1 && selectedNode && selectedNode->ID != ne->ID ) {
						if ( selectedNode->inConn[selectedInPort].nConn == 0 ) {
							Connection*conn;
							int n;
							ept_ID++;
							conn = newConnection(ne->ID, outPort + 1, selectedNode->ID, selectedInPort + 1);
							installConnection(myWS->listOfNodes, conn );
							eop = newEditorOperation(ept_add_connection, ne->ID, (void*)dupConnection(conn));
							eop->vector = ept_ID;
							n = g_list_position ( operList, undoList );
							n = ( n < 0 ) ? 0 : n;
							operList = g_list_insert ( operList, (gpointer)eop, n);
							undoList = g_list_nth(operList, n);
							drawSelectedOutPort( ne, outPort , 0);
							if ( ne->type == macro || selectedNode->type == macro ) {
								connect_macro(ne, outPort, selectedNode, selectedInPort);
							}
							gtk_widget_set_sensitive(undoButton, TRUE);
							bambu_set_title(0);
						} else {
							int i;
							int n;
							Node *ne2;
							Connection*conn;
							for ( i = 0; i < selectedNode->inConn[selectedInPort].nConn; i++ ) {
								ne2 = nodeByID(myWS->listOfNodes, selectedNode->inConn[selectedInPort].channel[i]->sourceNodeID);
								if ( ne2->info->array[0] == selectedNode->info->array[0] ) {
									drawConnection3(selectedNode->inConn[selectedInPort].channel[i], 0);
									break;
								}
							}
							ept_ID++;
							disconnect_1(selectedNode, selectedInPort);
							redrawWorkspace();
							conn = newConnection(ne->ID, outPort + 1, selectedNode->ID, selectedInPort + 1);
							installConnection(myWS->listOfNodes, conn );
							eop = newEditorOperation(ept_add_connection, ne->ID, (void*)dupConnection(conn));
							eop->vector = ept_ID;
							n = g_list_position ( operList, undoList );
							n = ( n < 0 ) ? 0 : n;
							operList = g_list_insert ( operList, (gpointer)eop, n);
							undoList = g_list_nth(operList, n);
							drawSelectedOutPort( ne, outPort , 0);
							if ( ne->type == macro || selectedNode->type == macro ) {
								connect_macro(ne, outPort, selectedNode, selectedInPort);
							}
							gtk_widget_set_sensitive(undoButton, TRUE);
							bambu_set_title(0);
						}
						selectedNode = (Node*)NULL;
						selectedInPort = -1;
						selectedOutPort = -1;
					}
				}
			}
		} else {
			Moved = 0;
		}
	}
	return TRUE;
}

gboolean motion_notify_event( GtkWidget *widget, GdkEventMotion *event )
{
	int x, y;
	GdkModifierType state;
	int i, j;
	if (event->is_hint)
		gdk_window_get_device_position (event->window, event->device, &x, &y, &state);
	else {
		x = event->x;
		y = event->y;
		state = (GdkModifierType) event->state;
	}
	if (state & GDK_BUTTON1_MASK /*&& pixmap != NULL*/) {
		if ( selectedNode && selectedInPort == -1 && selectedOutPort == -1 ) {
			Moved = 1;
			drawNode(selectedNode, drawingarea1, myWSWidth, myWSHeight, 0);
			for ( i = 0; i < selectedNode->nInConns; i++) {
				for ( j = 0; j < selectedNode->inConn[i].nConn; j++ ) {
					drawConnection3(selectedNode->inConn[i].channel[j], 0);
				}
			}
			selectedNode->info->array[2] = x - selectedNode->info->array[3] / 2;
			selectedNode->info->array[1] = y - selectedNode->info->array[4] / 2;
			redrawWorkspace();
			bambu_set_title(0);
		}
	}
	return TRUE;
}

void redrawWorkspace()
{
	drawWorkspace(myWS, drawingarea1, myWSWidth, myWSHeight);
	gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)myWS);
}

/* Create a new backing pixmap of the appropriate size */
gboolean configure_event(GtkWidget *widget, GdkEventConfigure *event, gpointer data)
{
	cairo_t *cr;
	if (!surface) {
		surface = gdk_window_create_similar_surface (gtk_widget_get_window (widget),
                                               CAIRO_CONTENT_COLOR,
                                               gtk_widget_get_allocated_width (widget),
                                               gtk_widget_get_allocated_height (widget));
		cr = cairo_create (surface);
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
		cairo_paint (cr);
		cairo_destroy (cr);
	} else {
		cairo_surface_destroy (surface);
		surface = gdk_window_create_similar_surface (gtk_widget_get_window (widget),
                                               CAIRO_CONTENT_COLOR,
                                               gtk_widget_get_allocated_width (widget),
                                               gtk_widget_get_allocated_height (widget));
		cr = cairo_create (surface);
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
		cairo_paint (cr);
		cairo_destroy (cr);
		redrawWorkspace();
	}
	return TRUE;
}

/* Redraw the screen from the backing pixmap */
gboolean draw_event( GtkWidget *widget, cairo_t *cr, gpointer data)
{
	cairo_set_source_surface (cr, surface, 0, 0);
	cairo_paint (cr);
	return FALSE;
}

GtkWidget *create_option_dialog(char*name)
{
	GtkWidget *dialog = gtk_dialog_new_with_buttons (NULL, GTK_WINDOW (mw),
                                                  (GtkDialogFlags)(GTK_DIALOG_MODAL |
                                                  GTK_DIALOG_USE_HEADER_BAR |
                                                  GTK_DIALOG_DESTROY_WITH_PARENT),
                                                  "_OK", GTK_RESPONSE_ACCEPT,
                                                  NULL);
	GtkWidget *hbar = gtk_dialog_get_header_bar(GTK_DIALOG(dialog));
	gtk_header_bar_set_has_subtitle (hbar, TRUE);
	gtk_header_bar_set_subtitle (hbar, name);
	gtk_header_bar_set_title (hbar, _("Options"));
	gtk_header_bar_set_show_close_button (hbar, FALSE);
	GValue a = G_VALUE_INIT;
	g_value_init (&a, G_TYPE_INT);
	g_value_set_int (&a, GTK_PACK_START);
	GtkWidget *child = gtk_dialog_add_button (dialog, "_Cancel", GTK_RESPONSE_REJECT);
	gtk_container_child_set_property (GTK_CONTAINER(hbar), child, "pack-type", &a);
	gtk_widget_set_size_request (dialog, 400, 350);
	gtk_window_set_resizable(GTK_WINDOW (dialog), FALSE);
	gtk_dialog_set_default_response((GtkDialog *)dialog, GTK_RESPONSE_ACCEPT);
	return dialog;
}

gboolean toolbar1_properties_event( GtkWidget *widget, gpointer data )
{
	Workspace*ws = (Workspace*)data;
	int length = 0;
	GtkWidget *dialog;
	GtkWidget *table;
	GtkWidget *dialog_vbox3;
	GtkWidget *scrolledwindow2;
	GtkWidget *viewport2, *name_lab, *name_entry;
	GtkWidget *view;
	GtkTextBuffer *buffer;
	dialog = create_option_dialog (basefilename);
	dialog_vbox3 = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
	gtk_widget_show (dialog_vbox3);
	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy  ( ( GtkScrolledWindow *)scrolledwindow2,
                                             GTK_POLICY_AUTOMATIC,
                                             GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolledwindow2);
	gtk_box_pack_start (GTK_BOX (dialog_vbox3), scrolledwindow2, TRUE, TRUE, 0);
	viewport2 = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport2);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), viewport2);
	table = gtk_grid_new ();
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (viewport2), table);
	name_lab = gtk_label_new(_("Title"));
	gtk_widget_show (name_lab);
	gtk_grid_attach (GTK_GRID (table), name_lab, 0, length, 1, 1);
	name_entry = gtk_entry_new();
	if (ws->name != NULL && strlen(ws->name) > 0) {
		gtk_entry_set_text( GTK_ENTRY (name_entry), ws->name);
	} else {
		gtk_entry_set_text( GTK_ENTRY (name_entry), _("Untitled"));
	}
	gtk_editable_set_editable(GTK_EDITABLE (name_entry), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (name_entry), TRUE);
	gtk_widget_show (name_entry);
	gtk_grid_attach (GTK_GRID (table), name_entry, 1, length++, 1, 1);
	name_lab = gtk_label_new(_("Comments"));
	gtk_widget_show (name_lab);
	gtk_grid_attach (GTK_GRID (table), name_lab, 0, length++, 2, 1);
	view = gtk_text_view_new ();
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_widget_show (view);
	gtk_grid_attach (GTK_GRID (table), view, 0, length++, 2, 1);
	gtk_widget_set_hexpand (view, TRUE);
	gtk_widget_set_vexpand (view, TRUE);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(view), GTK_WRAP_WORD);
	gtk_text_view_set_editable (GTK_TEXT_VIEW(view), TRUE);
	if (ws->comment != NULL) {
		gtk_text_buffer_set_text (buffer, ws->comment, -1);
	}
	if ( gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT ) {
		if (ws->name != NULL ) {
			g_free(ws->name);
		}
		ws->name = g_strdup((const char*)gtk_entry_get_text(GTK_ENTRY (name_entry)));
		if (ws->comment != NULL) {
			g_free(ws->comment);
		}
		GtkTextIter start, end;
		gtk_text_buffer_get_iter_at_offset (buffer, &start, 0);
		gtk_text_buffer_get_iter_at_offset (buffer, &end, -1);
		ws->comment = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
		bambu_set_title(0);
	}
	gtk_widget_destroy(dialog);
}

gboolean toolbar1_run_event( GtkWidget *widget, gpointer data )
{
	GError*gerror = NULL;
	WorkspaceStatus wstatus;
	wstatus = workspace_get_status(myWS);
	if ( wstatus != WS_RUNNING ) {
		g_mutex_lock (&(myWS->stopMutex));
		myWS->clean = 0;
		g_mutex_unlock (&(myWS->stopMutex));
		workspace_set_status(myWS, WS_RUNNING);
		if ( ( threadWS = g_thread_try_new( "Run", (GThreadFunc)threadRunWS, (gpointer)myWS, &gerror ) ) == NULL ) {
			if ( gerror ) {
				winError2(gerror->message);
				g_error_free(gerror);
			} else {
				winError2(_("Can't run!"));
			}
			workspace_set_status(myWS, WS_FAILED);
		}
	} else {
		winError2(_("Workspace is running!"));/*^^*/
	}
	gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)myWS);
	return FALSE;
}

gboolean toolbar1_undo_event( GtkWidget *widget, gpointer data )
{
	EditorOperation*eop;
	Node*ne;
	Connection*conn;
	int n;
	int cur_ID, init_ID;
	eop = (EditorOperation*)undoList->data;
	init_ID = eop->vector;
	cur_ID = init_ID;
	while ( cur_ID == init_ID ) {
		switch ( eop->type ) {
			case ept_add_node:
				ne = (Node*)eop->data;
				n = ne->ID;
				ne = dupNode((Node*)eop->data);
				ne->ID = n;
				deleteNodeF(ne);
			break;
			case ept_delete_node:
				ne = (Node*)eop->data;
				n = ne->ID;
				ne = dupNode((Node*)eop->data);
				ne->ID = n;
				cur_ID = eop->vector;
				drawNode(ne, drawingarea1, myWSWidth, myWSHeight, 1);
				workspaceRestoreNode(myWS, ne);
			break;
			case ept_add_node_macro:
			break;
			case ept_delete_node_macro:
			break;
			case ept_add_connection:
				conn = (Connection*)eop->data;
				uninstallConnection(myWS->listOfNodes, conn);
				drawConnection3(conn, 1);
			break;
			case ept_delete_connection:
				if ( ! ( ne = nodeByID(myWS->listOfNodes, eop->ID) ) )
					return FALSE;
				conn = dupConnection((Connection*)eop->data);
				installConnection(myWS->listOfNodes, conn );
				drawSelectedOutPort( ne, conn->sourcePortID - 1, 0);
			break;
			default:
				winError2("wrong undo type");
		}
		redoList = undoList;
		undoList = g_list_next(undoList);
		if ( !undoList ) {
			gtk_widget_set_sensitive(undoButton, FALSE);
			break;
		}
		eop = (EditorOperation*)undoList->data;
		cur_ID = eop->vector;
	}
	gtk_widget_set_sensitive(redoButton, TRUE);
	redrawWorkspace();
//fprintf(stdout,"o = %d, u = %d, r = %d\n", g_list_length(operList), g_list_length(undoList), g_list_length(redoList));
//fflush(stdout);
	return FALSE;
}

gboolean toolbar1_redo_event( GtkWidget *widget, gpointer data )
{
	EditorOperation*eop;
	Node*ne;
	Connection*conn;
	int n;
	int cur_ID, init_ID;
	eop = (EditorOperation*)redoList->data;
	init_ID = eop->vector;
	cur_ID = init_ID;
	while ( cur_ID == init_ID ) {
		switch ( eop->type ) {
			case ept_add_node:
				ne = (Node*)eop->data;
				n = ne->ID;
				ne = dupNode(ne);
				ne->ID = n;
				drawNode(ne, drawingarea1, myWSWidth, myWSHeight, 1);
				workspaceRestoreNode(myWS, ne);
			break;
			case ept_delete_node:
				ne = (Node*)eop->data;
				n = ne->ID;
				ne = dupNode(ne);
				ne->ID = n;
				deleteNodeF(ne);
			break;
			case ept_add_node_macro:
			break;
			case ept_delete_node_macro:
			break;
			case ept_add_connection:
				if ( ! ( ne = nodeByID(myWS->listOfNodes, eop->ID) ) )
					return FALSE;
				conn = dupConnection((Connection*)eop->data);
				installConnection(myWS->listOfNodes, conn );
				drawSelectedOutPort( ne, conn->sourcePortID - 1, 0);
			break;
			case ept_delete_connection:
				conn = dupConnection((Connection*)eop->data);
				uninstallConnection(myWS->listOfNodes, conn);
				drawConnection3(conn, 1);
			break;
			default:
				winError2("wrong redo type");
		}
		undoList = redoList;
		redoList = g_list_previous (redoList);
		if ( !redoList ) {
			gtk_widget_set_sensitive(redoButton, FALSE);
			break;
		}
		eop = (EditorOperation*)redoList->data;
		cur_ID = eop->vector;
	}
	gtk_widget_set_sensitive(undoButton, TRUE);
	redrawWorkspace();
//fprintf(stdout,"o = %d, u = %d, r = %d\n", g_list_length(operList), g_list_length(undoList), g_list_length(redoList));
//fflush(stdout);
	return FALSE;
}

gboolean toolbar_set_sensitive(gpointer data)
{
	Workspace *ws = (Workspace *)data;
	WorkspaceStatus wstatus = workspace_get_status(ws);
	if (wstatus == WS_RUNNING) {
		gtk_widget_set_sensitive(rewButton, TRUE);
		gtk_widget_set_sensitive(stopButton, TRUE);
		gtk_widget_set_sensitive(runButton, FALSE);
	} else if (wstatus == WS_FAILED || wstatus == WS_TERMINATED || wstatus == WS_COMPLETED) {
		gtk_widget_set_sensitive(rewButton, TRUE);
		gtk_widget_set_sensitive(stopButton, FALSE);
		gtk_widget_set_sensitive(runButton, TRUE);
		if ( undoList )
			gtk_widget_set_sensitive(undoButton, TRUE);
		if ( redoList )
			gtk_widget_set_sensitive(redoButton, TRUE);
	} else if (wstatus == WS_WAITING) {
		gtk_widget_set_sensitive(rewButton, FALSE);
		gtk_widget_set_sensitive(stopButton, FALSE);
		gtk_widget_set_sensitive(runButton, TRUE);
		if ( undoList )
			gtk_widget_set_sensitive(undoButton, TRUE);
		if ( redoList )
			gtk_widget_set_sensitive(redoButton, TRUE);
	}
	return FALSE;
}

gpointer threadRunWS(gpointer *arg)
{
	Workspace *ws;
	WorkspaceStatus wstatus;
	ws = (Workspace*)arg;
	wstatus = WS_FAILED;
	workspace_set_status(myWS, WS_RUNNING);
	workspaceRun(ws);
	wstatus = workspace_get_status(myWS);
	g_warning("Status = %d", wstatus);
/*	if ( wstatus == WS_FAILED ) {
		if ( kounter_retries < retries_limit ) {
			for (curr = myWS->listOfNodes; curr; curr = curr->next) {
				g_mutex_lock (&(curr->node->statusMutex));
				if ( curr->node->status == failed ) {
					curr->node->status = waiting;
					traceNe(curr->node);
				}
				g_mutex_unlock (&(curr->node->statusMutex));
			}
		}
	}*/
	gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)ws);
	g_thread_exit (NULL);
}

gboolean toolbar1_stop_event( GtkWidget *widget, gpointer data )
{
	ListOfNodes *curr;
	WorkspaceStatus wstatus;
	wstatus = workspace_get_status(myWS);
	if ( wstatus != WS_RUNNING ) {
		winError2(_("Workspace wasn't run!"));
	} else if ( wstatus == WS_RUNNING ) {
		g_mutex_lock(&(myWS->stopMutex));
		myWS->clean = 1;
		g_mutex_unlock(&(myWS->stopMutex));
		g_thread_join(threadWS);
		for (curr = myWS->listOfNodes; curr; curr = curr->next) {
			g_mutex_lock (&(curr->node->statusMutex));
			if ( curr->node->status == running ) {
				curr->node->status = waiting;
				g_mutex_unlock (&(curr->node->statusMutex));
				traceNe(curr->node);
			} else {
				g_mutex_unlock (&(curr->node->statusMutex));
			}
		}
		threadWS = (GThread*)NULL;
		gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)myWS);
		winError2(_("Workspace stopped!"));
	}
	return FALSE;
}

gboolean toolbar1_rew_event( GtkWidget *widget, gpointer data )
{
	ListOfNodes *curr;
	WorkspaceStatus wstatus;
	wstatus = workspace_get_status(myWS);
	if ( wstatus == WS_RUNNING ) {
		g_mutex_lock(&(myWS->stopMutex));
		myWS->clean = 1;
		g_mutex_unlock(&(myWS->stopMutex));
		g_thread_join(threadWS);
	}
	workspaceRmTemp(myWS);
	for (curr = myWS->listOfNodes; curr; curr = curr->next) {
		g_mutex_lock (&(curr->node->statusMutex));
		if ( curr->node->status == completed )
			nodeCleanOutPut(curr->node);
		curr->node->status = waiting;
		g_mutex_unlock (&(curr->node->statusMutex));
		traceNe(curr->node);
	}
	workspace_set_status(myWS, WS_WAITING);
	gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)myWS);
	threadWS = (GThread*)NULL;
	return FALSE;
}

gboolean toolbar1_save_event( GtkWidget *widget, gpointer data )
{
	int error_occur = 0;
	if ( filename_flag == 1 ) {
		error_occur = workspace_to_file(myWS, filename);
		if ( error_occur == 1 ) {
			GtkWidget* dialog = gtk_message_dialog_new (GTK_WINDOW (mw),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING,
				GTK_BUTTONS_CLOSE,
				_("This file is read only. Use Save As"));
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
			g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
			gtk_widget_show (dialog);
			return FALSE;
		}
		bambu_set_title(1);
	} else {
		fileSelectSave();
	}
	return FALSE;
}

gboolean toolbar1_save_as_event( GtkWidget *widget, gpointer data )
{
	fileSelectSave();
	return FALSE;
}

gint recent_compare_func (gconstpointer a, gconstpointer b)
{
	GtkRecentInfo*ra = (GtkRecentInfo*)a;
	GtkRecentInfo*rb = (GtkRecentInfo*)b;
	time_t ta, tb;
	ta = gtk_recent_info_get_visited (ra);
	tb = gtk_recent_info_get_visited (rb);
	if ( ta < tb )
		return -1;
	else if ( ta > tb )
		return 1;
	return 0;
}


void add_to_recent(char *uri)
{
	GtkRecentManager *manager;
	GtkRecentData *data;
	data = (GtkRecentData *)g_new(GtkRecentData, 1);
	data->display_name = NULL;
//	a UTF-8 encoded string, containing the name of the recently used resource to be displayed, or NULL;
	data->description = NULL;
//	a UTF-8 encoded string, containing a short description of the resource, or NULL;
	data->mime_type = g_strdup("application/x-prostack");
//	the MIME type of the resource;
	data->app_name = g_strdup("kimono-gui");
//	the name of the application that is registering this recently used resource;
	data->app_exec = g_strdup("kimono-gui %f");
//	command line used to launch this resource; may contain the "%f" and "%u" escape characters which will be expanded to the resource file path and URI respectively when the command line is retrieved;
	data->groups = (char**)g_new(char*, 5);
//	a vector of strings containing groups names;
	data->groups[0] = g_strdup("prostack");
	data->groups[1] = g_strdup("bambu");
	data->groups[2] = g_strdup("kimono-gui");
	data->groups[3] = g_strdup("komet");
	data->groups[4] = NULL;
	data->is_private = FALSE;
//	whether this resource should be displayed only by the applications that have registered it or not.
	manager = gtk_recent_manager_get_default ();
	if ( !gtk_recent_manager_has_item(manager, (const gchar*)uri) ) {
		gint maxritems;
		gint nritems;
		GList*ritems = NULL;
		maxritems = 10;
		ritems = gtk_recent_manager_get_items (manager);
		if ( ritems != NULL ) {
			nritems = g_list_length (ritems);
			if ( maxritems > 0 && nritems > 0 && nritems >= maxritems ) {
				GtkRecentInfo*rinfo_to_remove;
				gchar*uri_to_remove;
				GError *gerror = NULL;
				ritems = g_list_sort (ritems, (GCompareFunc) recent_compare_func);
				rinfo_to_remove = (GtkRecentInfo*)g_list_nth_data (ritems, 0);
				uri_to_remove = (gchar*)gtk_recent_info_get_uri (rinfo_to_remove);
				if ( !gtk_recent_manager_remove_item (manager, (const gchar *)uri_to_remove, &gerror) ) {
					if ( gerror != NULL ) {
						g_warning(gerror->message);
						g_clear_error(&gerror);
					}
				}
			}
			g_list_foreach (ritems, (GFunc)gtk_recent_info_unref, NULL);
			g_list_free (ritems);
		}
		gtk_recent_manager_add_full (manager, uri, data);
	}
	g_free(data->mime_type);
	g_free(data->app_name);
	g_free(data->app_exec);
	g_strfreev(data->groups);
	g_free(data);
}

void fileSelectSave()
{
	GtkWidget *dialog;
	dialog = gtk_file_chooser_dialog_new (_("Save Workflow"),
				(GtkWindow*)mw,
				GTK_FILE_CHOOSER_ACTION_SAVE,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Save", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	if ( filename_flag == 1 && filename && strlen(filename) > 0 ) {
		char*scheme = g_uri_parse_scheme(filename);
		if ( !scheme ) {
			filename = g_strdup_printf("file://%s", filename);
		} else {
			g_free(scheme);
		}
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), filename);
	}
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *newfilename;
		newfilename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
		if ( filename ) g_free(filename);
		if ( dirname ) g_free(dirname);
		if ( basefilename ) g_free(basefilename);
		filename = g_strdup(newfilename);
		dirname = g_path_get_dirname (filename);
		basefilename = g_path_get_basename(filename);
		filename_flag = 1;
		if ( MACRO_NUMBER != 0 ) {
			Node*macro_node;
			macro_node = macro_node_by_number(myWS->listOfNodes, MACRO_NUMBER);
			workspaceRebuildMacro(myWS, macro_node);
			setBBNode(macro_node);
		}
		workspace_to_file(myWS, filename);
		bambu_set_title(1);
		g_free (newfilename);
		add_to_recent(filename);
	}
	gtk_widget_destroy (dialog);
}


gboolean toolbar1_comp_event( GtkWidget *widget, gpointer data )
{
	GtkWidget *dialog;
	char *newfilename;
	dialog = gtk_file_chooser_dialog_new (_("Select target name and location"),
				(GtkWindow*)mw,
				GTK_FILE_CHOOSER_ACTION_SAVE,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Save", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), _("Untitled"));
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT ) {
		newfilename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
		if ( MACRO_NUMBER != 0 ) {
			Node*macro_node;
			macro_node = macro_node_by_number(myWS->listOfNodes, MACRO_NUMBER);
			workspaceRebuildMacro(myWS, macro_node);
			setBBNode(macro_node);
		}
		workspace_to_module(myWS);
		workspace_to_file(myWS, newfilename);
		g_free (newfilename);
	}
	gtk_widget_destroy (dialog);
	return FALSE;
}

gboolean toolbar1_macro_event( GtkWidget *widget, gpointer data )
{
	Node*macro_node;
	if ( selectedNode ) {
		drawSelectedNodes(myWS, drawingarea1, myWSWidth, myWSHeight, 0);
		macro_node = newNode();
		macro_node->label = strcpy(macro_node->label, "macro");
		macro_node->oServer = dupOlapServer(selectedNode->oServer);
		macro_node->info = (VInfo*)malloc(sizeof(VInfo));
		macro_node->info->n = 14;
		macro_node->info->array = (int*)calloc(14, sizeof(int));
		macro_node->info->array[2] = selectedNode->info->array[2];
		macro_node->info->array[1] = selectedNode->info->array[1];
		macro_node->type = macro;
		macro_node->info->array[0] = MACRO_NUMBER;
		workspaceAddMacro(myWS, macro_node);
		setBBNode(macro_node);
		clear_node_selections1(myWS);
		redrawWorkspace();
		selectedInPort = -1;
		selectedOutPort = -1;
		selectedNode = (Node*)NULL;
	}
	return FALSE;
}

gboolean toolbar1_close_macro_event( GtkWidget *widget, gpointer data )
{
	Node*macro_node;
	if ( MACRO_NUMBER != 0 ) {
		clearWorkspace(myWS, drawingarea1, myWSWidth, myWSHeight);
		macro_node = macro_node_by_number(myWS->listOfNodes, MACRO_NUMBER);
		workspaceRebuildMacro(myWS, macro_node);
		setBBNode(macro_node);
		MACRO_NUMBER = macro_node->info->array[0];
		if ( MACRO_NUMBER != 0 ) {
			macro_node = macro_node_by_number(myWS->listOfNodes, MACRO_NUMBER);
			P_MACRO_NUMBER = macro_node->info->array[0];
		} else {
			gtk_widget_set_sensitive(supernetButton, FALSE);
		}
		clear_node_selections1(myWS);
		redrawWorkspace();
		selectedInPort = -1;
		selectedOutPort = -1;
		selectedNode = (Node*)NULL;
	}
	return FALSE;
}

gboolean trace_node(gpointer data)
{
	cr_rect *crr = (cr_rect*)data;
	cairo_t *cr;
	cr = cairo_create (surface);
	int x, y, dx, dy;
	NodeStatus statusOfNode;
	x = crr->x;
	y = crr->y;
	dx = crr->dx;
	dy = crr->dy;
	statusOfNode = crr->st;
/*	g_print("x = %d, y = %d, dx = %d, dy = %d, st = %d\n", x, y, dx, dy, statusOfNode);*/
	switch (statusOfNode) {
		case running:
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&active_color);
		break;
		case waiting:
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
		break;
		case completed:
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&prelight_color);
		break;
		case failed:
			gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&black_color);
		break;
	}
	cairo_rectangle (cr, x, y, dx, dy);
	cairo_fill (cr);
	gtk_widget_queue_draw_area (drawingarea1, x, y, dx, dy);
	cairo_destroy (cr);
	return FALSE;
}

void traceNe(Node*ne)
{
	int x, y, dx, dy;
	NodeStatus statusOfNode;
	NodeType ntp;
	int mnumber, j;
	dx = 6;
	dy = 10;
	g_mutex_lock (&(ne->statusMutex));
	statusOfNode = ne->status;
	ntp = ne->type;
	mnumber = ne->info->array[0];
	x = ne->info->array[2] + 12;
	y = ne->info->array[1] + 12;
	g_mutex_unlock (&(ne->statusMutex));
	j = 0;
	if ( MACRO_NUMBER != mnumber ) {
		ListOfNodes*curr;
		int i;
		if ( statusOfNode == completed ) {
			for( curr = myWS->listOfNodes, i = 0; curr && i < myWS->numberOfNodes; curr = curr->next, i++) {
				g_mutex_lock(&(curr->node->statusMutex));
				if ( curr->node->type != macro && mnumber == curr->node->info->array[0] && curr->node->status != completed ) {
					j = 1;
					g_mutex_unlock(&(curr->node->statusMutex));
					break;
				}
				g_mutex_unlock(&(curr->node->statusMutex));
			}

		}
		for( curr = myWS->listOfNodes, i = 0; curr && i < myWS->numberOfNodes; curr = curr->next, i++) {
			g_mutex_lock(&(curr->node->statusMutex));
			if ( curr->node->type == macro && mnumber == curr->node->info->array[5] ) {
				x = curr->node->info->array[2] + 12;
				y = curr->node->info->array[1] + 12;
				g_mutex_unlock(&(curr->node->statusMutex));
				if ( curr->node->info->array[0] != MACRO_NUMBER )
					j = 1;
				break;
			}
			g_mutex_unlock(&(curr->node->statusMutex));
		}
	}
	if ( MACRO_NUMBER != mnumber && mnumber == 0 )
		j = 1;
	if ( j == 0 ) {
		cr_rect *crr;
		crr = g_new0 ( cr_rect, 1);
		crr->x = x;
		crr->y = y;
		crr->dx = dx;
		crr->dy = dy;
		crr->st = statusOfNode;
		gdk_threads_add_idle (trace_node, crr);
	}
}

gboolean ewdelete( GtkWidget *widget, gpointer data )
{
	char*d;
	d = (char*)d;
	if ( d )
		free(d);
	return FALSE;
}

gboolean mwdelete( GtkWidget *widget, gpointer data )
{
	GtkWidget *dialog;
	gint result;
	int error_occur;
	if (modified == 1) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (mw),
			(GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_YES_NO,
			_("Save workflow?"));
		gtk_dialog_add_button(GTK_DIALOG (dialog), "_Cancel", GTK_RESPONSE_CANCEL);
		result = gtk_dialog_run (GTK_DIALOG (dialog));
		if (result == GTK_RESPONSE_YES) {
			if ( filename_flag == 1 ) {
				error_occur = workspace_to_file(myWS, filename);
			}
			if ( filename_flag == 0 || error_occur == 1 ) {
				fileSelectSave();
			}
		} else if ( result == GTK_RESPONSE_CANCEL ) {
			gtk_widget_destroy (dialog);
			return TRUE;
		}
	}
	gtk_main_quit();
	if ( myWS )
		workspaceRmTemp(myWS);
	return FALSE;
}

Node*placeNode()
{
	Node*ne;
	ne = newNode();
	switch (ctrlNode->type) {
		case pam:
			ne->label = strcpy(ne->label, (const char*)ctrlNode->pam->name);
		break;
		case file:
			ne->label = strcpy(ne->label, _("file"));
		break;
		case display:
			ne->label = strcpy(ne->label, _("display"));
		break;
		case ins:
			ne->label = strcpy(ne->label, _("from_file"));
		break;
		case ous:
			ne->label = strcpy(ne->label, _("to_file"));
		break;
	}
	ne->oServer = dupOlapServer(ctrlNode->oServer);
	ne->info = (VInfo*)malloc(sizeof(VInfo));
	ne->info->n = 14;
	ne->info->array = (int*)calloc(14, sizeof(int));
	ne->info->array[2] = event_x;
	ne->info->array[1] = event_y;
	ne->info->array[0] = MACRO_NUMBER;
	if ( ctrlNode->type == pam ) {
		ne->type= pam;
		ne->pam = newOlapPAM(ctrlNode->pam->name);
		setNodePorts(ne);
		node_set_defaults(ne);
	} else if ( ctrlNode->type == file ) {
		ne->type= file;
		ne->nOutConns = 1;
		ne->outConn = (Port*)calloc(1, sizeof(Port));
		ne->outConn[0].ID = 1;
		ne->outConn[0].nConn = 0;
		ne->outConn[0].channel = (Connection**)NULL;
		ne->outConn[0].label = (char*)calloc(6, sizeof(char));
		ne->outConn[0].label = strcpy(ne->outConn[0].label, "image");
		ne->outConn[0].type = tif;
	} else if ( ctrlNode->type == display ) {
		ne->type= display;
		ne->nInConns = 1;
		ne->inConn = (Port*)calloc(1, sizeof(Port));
		ne->inConn[0].ID = 1;
		ne->inConn[0].nConn = 0;
		ne->inConn[0].channel = (Connection**)NULL;
		ne->inConn[0].label = (char*)calloc(6, sizeof(char));
		ne->inConn[0].label = strcpy(ne->inConn[0].label, "image");
		ne->inConn[0].type = tif;
		ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
		ne->id = strcpy(ne->id, ctrlNode->id);
	} else if ( ctrlNode->type == ins ) {
		ne->type= ins;
//		ne->file = (char*)NULL;
		ne->nOutConns = 1;
		ne->outConn = (Port*)calloc(1, sizeof(Port));
		ne->outConn[0].ID = 1;
		ne->outConn[0].nConn = 0;
		ne->outConn[0].channel = (Connection**)NULL;
		ne->outConn[0].label = (char*)calloc(6, sizeof(char));
		ne->outConn[0].label = strcpy(ne->outConn[0].label, "image");
		ne->outConn[0].type = tif;
		ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
//		sprintf(ne->id, "%s/%s:%d", MPT, ne->oServer->hname, ne->oServer->port);
	} else if ( ctrlNode->type == ous ) {
		ne->type= ous;
		ne->nInConns = 1;
		ne->inConn = (Port*)calloc(1, sizeof(Port));
		ne->inConn[0].ID = 1;
		ne->inConn[0].nConn = 0;
		ne->inConn[0].channel = (Connection**)NULL;
		ne->inConn[0].label = (char*)calloc(6, sizeof(char));
		ne->inConn[0].label = strcpy(ne->inConn[0].label, "image");
		ne->inConn[0].type = tif;
		ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
//		sprintf(ne->id, "%s", MPT);
	}
	setBBNode(ne);
	drawNode(ne, drawingarea1, myWSWidth, myWSHeight, 1);
	workspaceAddNode(myWS, ne);
	ctrlNode->type = file;
	ctrlNode->pam->name = strcpy(ctrlNode->pam->name, "empty");
	return ne;
}

EditorOperation*newEditorOperation(EditorOperationType type, int ID, void *data)
{
	EditorOperation*ep;
	ep = (EditorOperation*)malloc(sizeof(EditorOperation));
	ep->type = type;
	ep->ID = ID;
	ep->data = data;
	ep->vector = 0;
	if ( redoList ) {
		cleanOperList();
		gtk_widget_set_sensitive(redoButton, FALSE);
	}
	return ep;
}

void undoDownSrtream(Node*ne)
{
	ListOfNodes*curr;
	WorkspaceStatus wstatus;
	gtk_widget_set_sensitive(stopButton, FALSE);
	wstatus = workspace_get_status(myWS);
	if ( wstatus == WS_RUNNING ) {
		g_mutex_lock(&(myWS->stopMutex));
		myWS->clean = 1;
		g_mutex_unlock(&(myWS->stopMutex));
		g_thread_join(threadWS);
	}
	listOfNodesPropagateStatus(myWS->listOfNodes, ne, waiting);
	for (curr = myWS->listOfNodes; curr; curr = curr->next) {
		g_mutex_lock (&(curr->node->statusMutex));
		if ( curr->node->status == running ) {
			curr->node->status = waiting;
			g_mutex_unlock (&(curr->node->statusMutex));
			traceNe(curr->node);
		} else {
			g_mutex_unlock (&(curr->node->statusMutex));
		}
	}
	gtk_widget_set_sensitive(runButton, TRUE);
	gtk_widget_set_sensitive(rewButton, TRUE);
	if ( undoList )
		gtk_widget_set_sensitive(undoButton, TRUE);
	if ( redoList )
		gtk_widget_set_sensitive(redoButton, TRUE);
}

GtkWidget*screenMesg(char*msg)
{
	GtkWidget*dialog, *label;
	dialog = gtk_window_new (GTK_WINDOW_POPUP);
	gtk_window_set_default_size (GTK_WINDOW (dialog), 100, 50);
	g_signal_connect(dialog, "delete_event", G_CALLBACK(gtk_widget_destroy), NULL);
	gtk_widget_show (dialog);
	label = gtk_label_new( (const char *)msg);
	gtk_container_add (GTK_CONTAINER (dialog), label);
	gtk_widget_show (label);
	return dialog;
}

void title_unsaved()
{
	sprintf(title, "Bambu! - %s", filename);
	gtk_window_set_title (GTK_WINDOW (mw), title);
}

void bambu_set_title(int saved)
{
	if ( saved == 1 ) {
		sprintf(title, "Bambu! - %s (%s)", filename, _("Saved"));
		modified = 0;
	} else {
		sprintf(title, "Bambu! - %s (%s)", filename, _("Unsaved"));
		modified = 1;
	}
	gtk_window_set_title (GTK_WINDOW (mw), title);
}

void lcf(gpointer data, gpointer user_data)
{
	EditorOperation*eop;
	Node*ne;
	Connection*conn;
	eop = (EditorOperation*)data;
	switch( eop->type ) {
		case ept_add_node:
			ne = (Node*)eop->data;
			if ( ne->nInConns > 0 )
				free( ne->inConn );
			if ( ne->nOutConns > 0 )
				free( ne->outConn );
			deleteNode(ne);
		break;
		case ept_delete_node:
			ne = (Node*)eop->data;
			if ( ne->nInConns > 0 )
				free( ne->inConn );
			if ( ne->nOutConns > 0 )
				free( ne->outConn );
			deleteNode(ne);
		break;
		case ept_change_node_id:
		break;
		case ept_change_node_file:
		break;
		case ept_add_connection:
			conn = (Connection*)eop->data;
			deleteConnection(conn);
		break;
		case ept_delete_connection:
			conn = (Connection*)eop->data;
			deleteConnection(conn);
		break;
		default:
			winError2("wrong undo/redo type");
	}
}

void cleanOperList()
{
	g_list_foreach(g_list_first(operList), lcf, NULL);
	g_list_free(operList);
	operList = NULL;
	redoList = NULL;
	undoList = NULL;
}

gboolean bambu_error(gpointer data)
{
	char*msg = (char*)data;
	GtkWidget*dialog;
	int we_are_stopping;
	dialog = gtk_message_dialog_new (GTK_WINDOW (mw),
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_WARNING,
		GTK_BUTTONS_CLOSE,
		msg,
		NULL);
 /* Destroy the dialog when the user responds to it (e.g. clicks a button) */
	g_signal_connect_swapped (dialog, "response",
		G_CALLBACK (gtk_widget_destroy),
		dialog);
	gtk_widget_show (dialog);
	return FALSE;
}

void winError2(char*msg)
{
	char*dmsg = g_strdup(msg);
/*	g_idle_add (bambu_error, dmsg);*/
	gdk_threads_add_idle (bambu_error, dmsg);
}

void drawNodeLabel(Node*node, int visible)
{
	int width, height;
	cairo_t *cr;
	cr = cairo_create (surface);
	gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&white_color);
	pango_layout_set_text (playout, (const gchar*)node->label, -1);
	pango_layout_get_pixel_size(playout, &width, &height);
	cairo_rectangle (cr, node->info->array[2], node->info->array[1] + 1 + node->info->array[4], width, height);
	cairo_fill (cr);
	if ( !visible ) {
		gtk_widget_queue_draw_area (drawingarea1, node->info->array[2], node->info->array[1] + 1 + node->info->array[4], width, height);
		cairo_destroy (cr);
		return;
	} else {
		gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&black_color);
		cairo_move_to (cr, node->info->array[2], node->info->array[1] + 1 + node->info->array[4]);
		pango_cairo_show_layout (cr, playout);
		gtk_widget_queue_draw_area (drawingarea1, node->info->array[2], node->info->array[1] + 1 + node->info->array[4], width, height);
		cairo_destroy (cr);
		return;
	}
}

void clear_node_selections(Workspace*ws)
{
	ListOfNodes *curr;
	int i;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[ curr->node->info->n - 1] == 1) {
			curr->node->info->array[ curr->node->info->n - 1] = 0;
			if ( curr->node->info->array[0] == MACRO_NUMBER )
				drawNode(curr->node, drawingarea1, myWSWidth, myWSHeight, 1);
		}
	}
	selectedInPort = -1;
	selectedOutPort = -1;
	selectedNode = (Node*)NULL;
}


void clear_node_selections1(Workspace*ws)
{
	ListOfNodes *curr;
	int i;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[ curr->node->info->n - 1] == 1) {
			curr->node->info->array[ curr->node->info->n - 1] = 0;
		}
	}
	selectedInPort = -1;
	selectedOutPort = -1;
	selectedNode = (Node*)NULL;
}


Node*findSelected(Workspace*ws)
{
	ListOfNodes *curr;
	int i;
	Node*ne;
	ne = (Node*)NULL;
	for (curr = ws->listOfNodes, i = 0; curr && i < ws->numberOfNodes; curr = curr->next, i++) {
		if ( curr->node->info->array[ curr->node->info->n - 1] == 1) {
			ne = curr->node;
			break;
		}
	}
	return ne;
}


void openMacro(Node*node)
{
	clear_node_selections(myWS);
	clearWorkspace(myWS, drawingarea1, myWSWidth, myWSHeight);
	P_MACRO_NUMBER = MACRO_NUMBER;
	MACRO_NUMBER = node->info->array[5];
	gtk_widget_set_sensitive(supernetButton, TRUE);
	redrawWorkspace();
}

void destroyMacro(Node*node)
{
	int n;
	Node*nne;
	EditorOperation*eop;
	drawSelectedNodes(myWS, drawingarea1, myWSWidth, myWSHeight, 0);
	clear_node_selections1(myWS);
	n = node->ID;
	nne = dupNode(node);
	nne->ID = n;
	eop = newEditorOperation(ept_delete_node_macro, nne->ID, (void*)nne);
	eop->vector = ept_ID;
	n = g_list_position ( operList, undoList );
	n = ( n < 0 ) ? 0 : n;
	operList = g_list_insert ( operList, (gpointer)eop, n);
	undoList = g_list_nth(operList, n);
	workspaceDeleteMacro(myWS, node);
	redrawWorkspace();
}


void connect_macro(Node*sNode, int sPort, Node*dNode, int dPort)
{
	Node*s_node, *d_node;
	Connection*conn;
	int s_port, d_port, flag;
	int n;
	EditorOperation*eop;
	s_node = sNode;
	s_port = sPort;
	d_node = dNode;
	d_port = dPort;
	flag = 0;
	while ( d_node->type == macro || s_node->type == macro ) {
		int sourceNodeID;
		int sourcePortID;
		int destNodeID;
		int destPortID;
		if ( d_node->type == macro ) {
			node_macro_get_inport(d_node, d_port, &destNodeID, &destPortID);
			flag = 1;
		} else {
			destNodeID = d_node->ID;
			destPortID = d_port;
			flag = 0;
		}
		while ( s_node->type == macro || flag == 1 ) {
			flag = 0;
			if ( s_node->type == macro ) {
				node_macro_get_outport(s_node, s_port, &sourceNodeID, &sourcePortID);
			} else {
				sourceNodeID = s_node->ID;
				sourcePortID = s_port;
			}
			conn = newConnection(sourceNodeID, sourcePortID + 1, destNodeID, destPortID + 1);
			installConnection(myWS->listOfNodes, conn );
			eop = newEditorOperation(ept_add_connection, sourceNodeID, (void*)dupConnection(conn));
			n = g_list_position ( operList, undoList );
			n = ( n < 0 ) ? 0 : n;
			operList = g_list_insert ( operList, (gpointer)eop, n);
			undoList = g_list_nth(operList, n);
			s_node = nodeByID(myWS->listOfNodes, sourceNodeID);
			s_port = sourcePortID;
		}
		d_node = nodeByID(myWS->listOfNodes, destNodeID);
		d_port = destPortID;
	}
}

void disconnect_1(Node*dNode, int dPort)
{
	Node*s_node, *d_node;
	int s_port, d_port, i, j;
	int n;
	EditorOperation*eop;
	int destNodeID;
	int destPortID;
	Node*destNode;
	destNode = dNode;
	destPortID = dPort;
	do {
		d_node = destNode;
		d_port = destPortID;
		g_mutex_lock (&(d_node->statusMutex));
		for ( i = 0; i < d_node->inConn[d_port].nConn; i++ ) {
			if ( d_node->status != waiting ) {
				g_mutex_lock (&(d_node->inConn[d_port].channel[i]->connMutex));
				d_node->inConn[d_port].channel[i]->flag = 0;
				g_mutex_unlock (&(d_node->inConn[d_port].channel[i]->connMutex));
			}
			s_node = nodeByID(myWS->listOfNodes, d_node->inConn[d_port].channel[i]->sourceNodeID);
			s_port = d_node->inConn[d_port].channel[i]->sourcePortID - 1;
			g_mutex_lock (&(s_node->statusMutex));
			for ( j = 0; j < s_node->outConn[s_port].nConn; j++ ) {
				if ( connection_equal( s_node->outConn[s_port].channel[j], d_node->inConn[d_port].channel[i] ) == 1 ) {
					s_node->outConn[s_port].channel[j] = s_node->outConn[s_port].channel[s_node->outConn[s_port].nConn - 1];
					break;
				}
			}
			s_node->outConn[s_port].nConn--;
			if ( s_node->outConn[s_port].nConn > 0 ) {
				s_node->outConn[s_port].channel = (Connection**)realloc(s_node->outConn[s_port].channel, s_node->outConn[s_port].nConn * sizeof(Connection*) );
			} else {
				free(s_node->outConn[s_port].channel);
				s_node->outConn[s_port].channel = (Connection**)NULL;
			}
			nodeCheckTerminal(s_node);
			g_mutex_unlock (&(s_node->statusMutex));
			eop = newEditorOperation(ept_delete_connection, d_node->inConn[d_port].channel[i]->sourceNodeID, (void*)dupConnection(d_node->inConn[d_port].channel[i]));
			eop->vector = ept_ID;
			n = g_list_position ( operList, undoList );
			n = ( n < 0 ) ? 0 : n;
			operList = g_list_insert ( operList, (gpointer)eop, n);
			undoList = g_list_nth(operList, n);
			deleteConnection(d_node->inConn[d_port].channel[i]);
		}
		d_node->inConn[d_port].nConn = 0;
		free(d_node->inConn[d_port].channel);
		d_node->inConn[d_port].channel = (Connection**)NULL;
		if ( d_node->status != waiting ) {
			g_mutex_unlock (&(d_node->statusMutex));
			listOfNodesPropagateStatus(myWS->listOfNodes, d_node, waiting);
		} else {
			g_mutex_unlock (&(d_node->statusMutex));
		}
		if ( d_node->type == macro ) {
			node_macro_get_inport(d_node, d_port, &destNodeID, &destPortID);
			destNode = nodeByID(myWS->listOfNodes, destNodeID);
		} else {
			destNodeID = d_node->ID;
			destPortID = d_port;
			destNode = d_node;
		}
	} while ( d_node->type == macro );
}

