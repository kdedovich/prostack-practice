#!/bin/bash

set -e

VERSION=3.0.8
ARCH=`uname -p`
RSH_CMD=ssh
host_host=urchin.spbcas.ru
host_user=kozlov
host_path="/var/www/html/downloads/softarchive/prostack"
host_template="prostack-main-?.?.?.tgz"

subdirs="door prostak quastack glaz kimono-gui bambu iapee cmove prutik"

cd $HOME

TARBALL=`$RSH_CMD $host_user@$host_host "cd $host_path; ls $host_template | tail -n 1"`
VERSION=`echo $TARBALL | sed -e "s/prostack-main-//g" -e "s/.tgz//g"`

TARGET="prostack-$VERSION-$ARCH.tgz"

target_test=`$RSH_CMD $host_user@$host_host "cd $host_path;find . -name $TARGET -print 2>/dev/null" 2>/dev/null`

if test -n "$target_test"
then
    echo "$host_user@$host_host:$host_path/$TARGET already exists. Aborting";
    exit;
fi

scp $host_user@$host_host:$host_path/$TARBALL $HOME

tar zxvf $HOME/$TARBALL -C prostack

cd prostack/prostack-main-$VERSION

export PKG_CONFIG_PATH=$HOME/lib/pkgconfig
export CC=gcc4
export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH
export PATH=$HOME/bin:$PATH
export CFLAGS="-O2"

for sds in $subdirs;
do
	cd $sds
	autoreconf -f -i
	cp ../../intltool-*.in .
        mv Makefile.am Makefile.am.tmp
        cat Makefile.am.tmp | sed -e "s/\$(docdir)/\${prefix}\/share\/doc/g" > Makefile.am
        mv Makefile.in Makefile.in.tmp
        cat Makefile.in.tmp | sed -e "s/\$(docdir)/\${prefix}\/share\/doc/g" > Makefile.in
	./configure --prefix=$HOME
	make
	make install
	cd ..
done

cd $HOME

host_template="prostack-omero-?.?.?.tgz"

subdirs="omep"

TARBALL=`$RSH_CMD $host_user@$host_host "cd $host_path; ls $host_template | tail -n 1"`
VERSION=`echo $TARBALL | sed -e "s/prostack-omero-//g" -e "s/.tgz//g"`

scp $host_user@$host_host:$host_path/$TARBALL $HOME

tar zxvf $HOME/$TARBALL -C prostack

cd prostack/prostack-omero-$VERSION

for sds in $subdirs;
do
	cd $sds
	autoreconf -f -i
	cp ../../intltool-*.in .
        mv Makefile.am Makefile.am.tmp
        cat Makefile.am.tmp | sed -e "s/\$(docdir)/\${prefix}\/share\/doc/g" > Makefile.am
        mv Makefile.in Makefile.in.tmp
        cat Makefile.in.tmp | sed -e "s/\$(docdir)/\${prefix}\/share\/doc/g" > Makefile.in
	./configure --prefix=$HOME
	make
        cat Makefile | sed -e "s/ src //g" > Makefile.1
        make -f Makefile.1 install
        mkdir -p $HOME/share/prostack/java
        cp -R build $HOME/share/prostack/java
	cd ..
done

cd $HOME

tar zcvf prostack/$TARGET `cat prostack/prostack-tar-file-list`
mkdir ProStack
tar zxvf prostack/$TARGET -C ProStack/
cp prostack/ProStack ProStack/bin
cp /usr/lib64/libexpat.so* ProStack/lib
cp -R /usr/share/mime/application ProStack/share/mime
cp -R /usr/share/mime/audio ProStack/share/mime
cp -R /usr/share/mime/image ProStack/share/mime
cp -R /usr/share/mime/inode ProStack/share/mime
cp -R /usr/share/mime/message ProStack/share/mime
cp -R /usr/share/mime/model ProStack/share/mime
cp -R /usr/share/mime/multipart ProStack/share/mime
cp -R /usr/share/mime/packages ProStack/share/mime
cp -R /usr/share/mime/text ProStack/share/mime
cp -R /usr/share/mime/video ProStack/share/mime
cp /usr/share/mime/XMLnamespaces ProStack/share/mime
cat /usr/share/mime/globs >> ProStack/share/mime/globs
cat /usr/share/mime/magic >> ProStack/share/mime/magic
cp -R /usr/share/icons/hicolor ProStack/share/icons
tar zcvf prostack/$TARGET `cat prostack/prostack-arch-file-list`
rm -fr ProStack

scp prostack/$TARGET $host_user@$host_host:$host_path
