:: environment setting for dbus clients
@echo off
:: set KIMONO_HOME=H:\Kimono

:: session bus address
set DBUS_SESSION_BUS_ADDRESS=tcp:host=localhost,port=12434

:: system bus address
set DBUS_SYSTEM_BUS_DEFAULT_ADDRESS=tcp:host=localhost,port=12434 

@echo starting local dbus daemon
start /B %KIMONO_HOME%\bin\dbus-daemon --session

set PATH=%KIMONO_HOME%;%KIMONO_HOME%\lib;%KIMONO_HOME%\bin;%KIMONO_HOME%\gnuplot\bin;%KIMONO_HOME%\ImageMagick;%KIMONO_HOME%\Perl\bin;%KIMONO_HOME%\Perl;%PATH%
set LD_LIBRARY_PATH=%KIMONO_HOME%\lib;%KIMONO_HOME%\bin;%KIMONO_HOME%\Perl\lib;%KIMONO_HOME%\Perl\bin;%LD_LIBRARY_PATH%

cls
start /wait kimono-gui %1
exit

