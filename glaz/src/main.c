/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin N.Kozlov 2007 <kozlov@spbcas.ru>
 * 
 * main.c is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * main.c is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with main.c.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <config.h>

#include <gtk/gtk.h>
/*#include <glade/glade.h>*/



/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif



#include "callbacks.h"

/* For testing propose use the local (not installed) glade file */
/*#define GLADE_FILE "glaz/glade/glaz.glade"*/
#define GTK_BUILDER_FILE "/glaz/glade/glaz.glade"
#define LOCALE_DIR "locale"
/*#define GLADE_FILE "glaz.glade"*/

#ifndef MAX_RECORD
#define MAX_RECORD 256
#endif

gboolean rconf(GtkBuilder *gtkbuild)
{
	double lower;
	double upper;
	double position;
	double max_size;
	GtkWidget*w, *u;
	u = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "eventbox1"));
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "vruler1"));
	gtk_ruler_get_range((GtkRuler *)w, &lower, &upper, &position, &max_size);
	upper = u->allocation.height;
	gtk_ruler_set_range((GtkRuler *)w, lower, upper, position, max_size);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "hruler1"));
	gtk_ruler_get_range((GtkRuler *)w, &lower, &upper, &position, &max_size);
	upper = u->allocation.width;
	gtk_ruler_set_range((GtkRuler *)w, lower, upper, position, max_size);
	gtk_ruler_set_range((GtkRuler *)w, lower, upper, position, max_size);
	return FALSE;
}

gboolean func(GtkBuilder *gtkbuild, GdkEventMotion*event)
{
	int x, y;
	char*buf;
	GdkModifierType state;
	GtkWidget*w;
	GdkPixbuf *pixbuf;
	int rowstride;
	int height;
	int n_channels;
	int width;
	guchar *pixels, *p;
	double lower;
	double upper;
	double position;
	double max_size;
	buf = (char*)calloc(128, sizeof(char));
	if (event->is_hint)
		gdk_window_get_pointer (event->window, &x, &y, &state);
	else {
		x = event->x;
		y = event->y;
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "image1"));
	pixbuf = gtk_image_get_pixbuf((GtkImage*)w);
	rowstride = gdk_pixbuf_get_rowstride (pixbuf);
	n_channels = gdk_pixbuf_get_n_channels (pixbuf);
	height = gdk_pixbuf_get_height((const GdkPixbuf *)pixbuf);
	width = gdk_pixbuf_get_width((const GdkPixbuf *)pixbuf);
	if ( x >= 0 && y < height && x < width && y >= 0 ) {
		pixels = gdk_pixbuf_get_pixels (pixbuf);
		p = pixels + y * rowstride + x * n_channels;
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry5"));
		if ( n_channels == 1 )
			sprintf(buf, "%d", (int)p[0]);
		else if ( n_channels == 3 )
			sprintf(buf, "%d,%d,%d", (int)p[0], (int)p[1], (int)p[2]);
		else if ( n_channels == 4 )
			sprintf(buf, "%d,%d,%d,%d", (int)p[0], (int)p[1], (int)p[2], (int)p[3]);
		gtk_entry_set_text((GtkEntry*)w, buf);
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "vruler1"));
		gtk_ruler_get_range((GtkRuler *)w, &lower, &upper, &position, &max_size);
		position = (double)y;
		gtk_ruler_set_range((GtkRuler *)w, lower, upper, position, max_size);
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "hruler1"));
		gtk_ruler_get_range((GtkRuler *)w, &lower, &upper, &position, &max_size);
		position = (double)x;
		gtk_ruler_set_range((GtkRuler *)w, lower, upper, position, max_size);
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry4"));
		sprintf(buf, "%d", x);
		gtk_entry_set_text((GtkEntry*)w, buf);
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry3"));
		sprintf(buf, "%d", y);
		gtk_entry_set_text((GtkEntry*)w, buf);
	}
	free(buf);
	return TRUE;
}

gboolean func_sav(char *input_image_file)
{
	GtkWidget*dialog;
	GError*gerror = NULL;
	dialog = gtk_file_chooser_dialog_new (_("Save output"),
				NULL,
				GTK_FILE_CHOOSER_ACTION_SAVE,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, TRUE);
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *newfilename;
		gchar**margv;
		int argcp, flaggs;
		GString *command;
		newfilename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
		command = g_string_new("door_copy");
		g_string_append_printf(command, " %s %s", input_image_file, newfilename);
		flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL;
		if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
			if ( gerror ) {
				g_warning("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
				g_error_free(gerror);
			}
		} else if ( !g_spawn_async (NULL, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, NULL, &gerror) ) {
			if ( gerror ) {
				g_warning("g_spawn_sync failed for %s\nwith %s", command->str, gerror->message);
				g_error_free(gerror);
			}
			g_strfreev(margv);
		}
		g_string_free(command, TRUE);
		g_free (newfilename);
	}
	gtk_widget_destroy (dialog);
	return TRUE;
}

/*
    * Mac OS X: _NSGetExecutablePath() (man 3 dyld)
    * Linux: readlink /proc/self/exe
    * Solaris: getexecname()
    * FreeBSD: sysctl CTL_KERN KERN_PROC KERN_PROC_PATHNAME -1
    * BSD with procfs: readlink /proc/curproc/file
    * Windows: GetModuleFileName() with hModule = NULL
*/

gchar*subst_data_dir(gchar*ORIGIN)
{
	static gchar *src_path;
	gchar*data_basename;
	char*runtime_install_prefix;
#ifdef G_OS_WIN32
	runtime_install_prefix = g_win32_get_package_installation_directory_of_module(NULL);
#else
	int ret = -1, ret_size = MAX_RECORD;
	char *buf;
	runtime_install_prefix = (char*)calloc(ret_size, sizeof(char));
#ifdef G_OS_DARWIN
	ret = _NSGetExecutablePath(runtime_install_prefix, &ret_size);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. On Mac OS X the size may be = %d", ret_size);
	runtime_install_prefix[ret_size - 1] = '\0';
#else
	ret = readlink("/proc/self/exe", runtime_install_prefix, ret_size - 1);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. The returned size may be = %d", ret);
	runtime_install_prefix[ret] = '\0';
#endif
	buf = g_path_get_dirname(runtime_install_prefix);
	g_free(runtime_install_prefix);
	runtime_install_prefix = g_path_get_dirname(buf);
#endif
	if ( runtime_install_prefix == NULL ) {
		return ORIGIN;
	}
	if ( src_path != NULL )
		g_free(src_path);
	if ( PACKAGE_DATA_DIR != NULL ) {
		data_basename = g_path_get_basename(PACKAGE_DATA_DIR);
	} else {
		return ORIGIN;
	}
	src_path = g_build_filename(runtime_install_prefix, data_basename, ORIGIN, NULL);
	g_free(data_basename);
	return src_path;
}

GtkWidget*
create_window (char*image_file, char*image_name)
{
	GtkWidget *window;
	GtkWidget *image;
	GtkWidget*w;
	GtkBuilder *gtkbuild;
	GError *gerror = NULL;
	GdkPixbuf *pixbuf;
	int bits_per_sample;
	int colorspace;
	int has_alpha;
	int height;
	int n_channels;
	int width;
	char*buf;
	gchar*image_title;
	double lower;
	double upper;
	double position;
	double max_size;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
#endif
	buf = (char*)calloc(128, sizeof(char));
	gtkbuild = gtk_builder_new ();
	if ( !gtk_builder_add_from_file (gtkbuild, subst_data_dir(GTK_BUILDER_FILE), &gerror)) {
		g_warning ("Couldn't load builder file: %s", gerror->message);
		g_error_free (gerror);
	}
	/* This is important */
	gtk_builder_connect_signals (gtkbuild, NULL);
	window = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
	image = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "image1"));
#ifdef G_OS_WIN32
	len = -1;
	converted = g_locale_to_utf8((const gchar *)image_file, len, &bytes_read, &bytes_written, &gerror);
	if ( gerror ) {
		g_error("Can't convert image name: %s", gerror->message);
	}
	gtk_image_set_from_file((GtkImage *)image, (const gchar *)converted);
	image_title = g_strdup_printf("%s:%s", image_name, converted);
	gtk_window_set_title((GtkWindow*)window, image_title);
#else
	gtk_image_set_from_file((GtkImage *)image, (const gchar *)image_file);
	image_title = g_strdup_printf("%s:%s", image_name, image_file);
	gtk_window_set_title((GtkWindow*)window, image_title);
#endif
	pixbuf = gtk_image_get_pixbuf((GtkImage *)image);
	if ( pixbuf == NULL ) {
#ifdef G_OS_WIN32
		g_error("Glaz: can't get pixbuf from %s", converted);
//		g_free(converted);
#else
		g_error("Glaz: can't get pixbuf from %s", image_file);
#endif
	}
	n_channels = gdk_pixbuf_get_n_channels((const GdkPixbuf *)pixbuf);
	height = gdk_pixbuf_get_height((const GdkPixbuf *)pixbuf);
	width = gdk_pixbuf_get_width((const GdkPixbuf *)pixbuf);
	gtk_widget_set_size_request ( window, width, (int)( 1.2 * height ) );
	sprintf(buf, "%d", height);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry1"));
	gtk_entry_set_text((GtkEntry*)w, buf);
	sprintf(buf, "%d", width);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "entry2"));
	gtk_entry_set_text((GtkEntry*)w, buf);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "vbox4"));
	gtk_widget_set_sensitive(w, FALSE);
	if ( n_channels == 1 ) {
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "vbox3"));
		gtk_widget_set_sensitive(w, FALSE);
	} else if ( n_channels == 3 ) {
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "rb_alpha"));
		gtk_widget_hide(w);
	} else if ( n_channels != 4 ) {
		g_error("Can't handle %d channels", n_channels);
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "vruler1"));
	gtk_ruler_get_range(GTK_RULER(w), &lower, &upper, &position, &max_size);
	lower = 0.0;
	upper = height;
	position = 0;
	gtk_ruler_set_range(GTK_RULER(w), lower, upper, position, max_size);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "hruler1"));
	gtk_ruler_get_range(GTK_RULER(w), &lower, &upper, &position, &max_size);
	lower = 0.0;
	upper = width;
	position = 0;
	gtk_ruler_set_range(GTK_RULER(w), lower, upper, position, max_size);
	free(buf);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "eventbox1"));
	g_signal_connect_swapped(G_OBJECT(w), "motion_notify_event", G_CALLBACK(func), gtkbuild);
	g_signal_connect_swapped(G_OBJECT(window), "configure_event", G_CALLBACK(rconf), gtkbuild);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "bn_sav"));
#ifdef G_OS_WIN32
	g_signal_connect_swapped(G_OBJECT(w), "clicked", G_CALLBACK(func_sav), converted);
#else
	g_signal_connect_swapped(G_OBJECT(w), "clicked", G_CALLBACK(func_sav), image_file);
#endif
	return window;
}


int
main (int argc, char *argv[])
{
	GtkWidget *window;
	gchar*image_file, *image_name;
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, subst_data_dir(LOCALE_DIR));
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	gtk_set_locale ();
	gtk_init (&argc, &argv);

	if ( argc <= 1 ) {
		g_error("No input file");
	} else if ( argc == 2 ) {
		image_file = g_strdup(argv[argc-1]);
		image_name = g_strdup(_("Image"));
	} else {
		image_file = g_strdup(argv[argc-1]);
		image_name = g_strdup(argv[argc-2]);
	}
	gtk_window_set_default_icon_from_file (subst_data_dir("/icons/gnome/48x48/mimetypes/gnome-mime-application-x-prostack.png"), NULL);
	window = create_window (image_file, image_name);
	gtk_widget_show (window);
	gtk_main ();
	g_free(image_file);
	g_free(image_name);
	return 0;
}
