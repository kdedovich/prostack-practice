#!/usr/bin/perl

use strict;

my $term;
my $columns;
my @column;
my $titles;
my @title;
my $i;
my $k;
my $infile;
my $outfile;
my $scfile;
my $lw;
my $pflag;
my $xcolumn;

if ( $#ARGV != 7 ) {
    print STDOUT "This is nplot2. Usage: xcolumn column,column,..,column title,title,...,title term lw pflag inFile outFile\n";
    exit(0);
}

$xcolumn = $ARGV[0];
$columns = $ARGV[1];
@column = split /\,/,$columns;
$titles = $ARGV[2];
@title = split /\,/,$titles;
$term = $ARGV[3];
$lw = $ARGV[4];
$pflag = $ARGV[5];
$infile = $ARGV[6];
$infile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;
$outfile = $ARGV[7];
$outfile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;


$scfile = $infile . ".sc";
open( OUT ,">$scfile");
select OUT;
#	printf("set key outside title \"Legend\" noautotitle\n");
	print("set key outside width 1 reverse\n");
	print("set term $term\n");
	print("set output \'$outfile\'\n");
	print("plot");
	for($i = 0; $i < $#column; $i++) {
		$k = $i + 1;
		print(" \'$infile\' using $xcolumn:$column[$i] title \"$title[$i]\" with $pflag lt $k lw $lw,"); 
	}
	$i = $#column;
	$k = $i + 1;
	print(" \'$infile\' using $xcolumn:$column[$i] title \"$title[$i]\" with $pflag lt $k lw $lw"); 
close(OUT);
system("gnuplot $scfile");
unlink $scfile;

