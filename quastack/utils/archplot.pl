#!/usr/bin/perl

use strict;                             # use strict c-style declarations
use File::Basename;                     # funcs to parse pathnames

my $filename;
my $output;
my @data;
my $rbins;
my $rmin;
my $rmax;
my $abins;
my $amin;
my $amax;
my @theta;
my @rho;
my @conc;
my @pix;
my $rstep;
my $astep;
my $i;
my $j;
my $k;
my $n;
my $r;
my $a;
my @line;

if ( $#ARGV != 7 ) {                      # too many arguments? -> error!
    print STDERR "Usage: archplot rmin rmax rbins amin amax abins datafile outfile\n";
    exit(1);
}

$rmin = $ARGV[0];
$rmax = $ARGV[1];
$rbins = $ARGV[2];
$amin = $ARGV[3];
$amax = $ARGV[4];
$abins = $ARGV[5];

$filename  = $ARGV[6];                    # assign arguments to variables
$filename =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
            { $1                        # understand it
                  ? (getpwnam($1))[7]
		  : ( $ENV{HOME} || $ENV{LOGDIR}
                       || (getpwuid($>))[7]
                     )
	    }ex;
$output  = $ARGV[7];                    # assign arguments to variables
$output  =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
            { $1                        # understand it
                  ? (getpwnam($1))[7]
		  : ( $ENV{HOME} || $ENV{LOGDIR}
                       || (getpwuid($>))[7]
                     )
	    }ex;
die "archplot: $filename not found!\n" if ( ! -e $filename );

#$output = $filename . ".$rmin.$rmax.$rbins.$amin.$amax.$abins";

open( IN , "<$filename" );
@data = <IN>;
close( IN );

$rstep = ($rmax - $rmin)/$rbins;
$astep = ($amax - $amin)/$abins;

$n = 0;
$r = $rmin;
for ($i = 0; $i < $rbins; $i++) {
	$a = $amin;
	for ($j = 0; $j < $abins; $j++) {
		$pix[$n] = 0;
		$conc[$n] = 0;
		$theta[$n] = 0;
		$rho[$n] = 0;
		for ($k = 0; $k <= $#data; $k++) {
			@line = split /\s+/,$data[$k];
#printf("%4d %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n", $k, $r,$line[1], $r + $rstep, $a,$line[2], $a + $astep);
			if ( $r < $line[1] && $line[1] <= $r + $rstep && $line[1] <= $rmax && $a < $line[2] && $line[2] <= $a + $astep && $line[2] <= $amax ) {
						$pix[$n]++;
						$conc[$n] += $line[3];
						$theta[$n] += $line[2];
						$rho[$n] += $line[1];
			}
		}
#printf("%4d %8.3f %8.3f %8.3f\n", $n, $rho[$n], $theta[$n], $conc[$n]);
#exit;
		$n++;
		$a = $a + $astep;
	}
	$r = $r + $rstep;
}

open( OUT , ">$output");
select OUT;
for ($i = 0; $i < $n; $i++) {
	if ( $pix[$i] > 1 ) {
		$conc[$i] /= $pix[$i];
		$theta[$i] /= $pix[$i];
		$rho[$i] /= $pix[$i];
	}
#	printf("%4d %8.3f %8.3f %8.3f\n", $i, $rho[$i], $theta[$i], $conc[$i]);
	printf("%4d %8.3f\n", $i, $conc[$i]);
}
close(OUT);



