#!/usr/bin/perl

use strict;

my $term;
my $columns;
my @column;
my $i;
my $k;
my $infile;
my $outfile;
my $scfile;
my $lw;
my $pflag;
my $xcolumn;

if ( $#ARGV != 6 ) {
    print STDOUT "This is nplot. Usage: xcolumn column,column,..,column term lw pflag inFile outFile\n";
    exit(0);
}

$xcolumn = $ARGV[0];
$columns = $ARGV[1];
@column = split /\,/,$columns;
$term = $ARGV[2];
$lw = $ARGV[3];
$pflag = $ARGV[4];
$infile = $ARGV[5];
$infile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;
$outfile = $ARGV[6];
$outfile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;


$scfile = $infile . ".sc";
open( OUT ,">$scfile");
select OUT;
	print("set nokey\n");
	print("set term $term\n");
	print("set output \'$outfile\'\n");
	print("plot");
	for($i = 0; $i < $#column; $i++) {
		$k = $i + 1;
		print(" \'$infile\' using $xcolumn:$column[$i] with $pflag lt $k lw $lw,"); 
	}
	$i = $#column;
	$k = $i + 1;
	print(" \'$infile\' using $xcolumn:$column[$i] with $pflag lt $k lw $lw"); 
close(OUT);
system("gnuplot $scfile");
unlink $scfile;

