INSERT OR REPLACE INTO Packages (Name, Version)  VALUES ('psocv','@PACKAGE_VERSION@');
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, segmentation, detect_edges, edges) VALUES ('canny','psocv','psocv -o canny', 0, "tif", "tif", "3;
Low threshold;double;10.0;
High threshold;double;20.0;
Aperture size;int;3;

-s $1,$2,$3;", "This operator implements the Canny algorithm for edge detection.

INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Low threshold - the minimal value of gradient magnitude,
High threshold - the maximal minimal value of gradient magnitude,
Aperture size - size of window to calculate the magnitude of gradient.", 'canny', 2, 1, 1, 1);
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, segmentation, detect_edges, edges) VALUES ('harris','psocv','psocv -o harris', 0, "tif", "tif", "3;
Block size;int;5;
Aperture size;int;3;
Free parameter;double;0.04;

-s $1,$2,$3;", "This operator implements the Harris algorithm for edge and corner detection.

INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Block size - size of window,
Aperture size - size of window to calculate the magnitude of gradient,
Free parameter - free parameter of the algorithm.", 'harris', 2, 1, 1, 1);
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, misc, utility) VALUES ('colorconv','psocv','psocv -o colorconv', 0, "tif", "tif", "1;
Code;choice;26;RGB2GRAY;GRAY2RGB;BGR2XYZ;RGB2XYZ;XYZ2BGR;XYZ2RGB;BGR2YCrCb;RGB2YCrCb;YCrCb2BGR;YCrCb2RGB;BGR2HSV;RGB2HSV;HSV2BGR;HSV2RGB;BGR2HLS;RGB2HLS;HLS2BGR;HLS2RGB;BGR2Lab;RGB2Lab;Lab2BGR;Lab2RGB;BGR2Luv;RGB2Luv;Luv2BGR;Luv2RGB;

-s $1;", "This operator implements the convertion between different color spaces.

INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Code - conversion code.", 'colorconv', 1, 1, 1);
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, misc, utility) VALUES ('colortrans','psocv','psocv -o colortrans', 0, "tif", "tif", "1;
Code;choice;10;HE;HE2;HDAB;FastRedFastBlueDAB;MethylGreenDAB;HEDAB;HAEC;Azan-Mallory;Alcian_blueH;HPAS;

-s $1;", "This operator implements the deconvolution of colors for known dyes.

INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Code - dyes code.", 'colortrans', 1, 1, 1);
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, misc, utility) VALUES ('color2trans','psocv','psocv -o colortrans', 0, "tif", "tif", "9;
MODx0;double;0.0;
MODy0;double;0.0;
MODz0;double;0.0;
MODx1;double;0.0;
MODy1;double;0.0;
MODz1;double;0.0;
MODx2;double;0.0;
MODy2;double;0.0;
MODz2;double;0.0;

-s Custom,$1,$2,$3,$4,$5,$6,$7,$8,$9;", "This operator implements the deconvolution of colors for known dyes.

INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
MODx0;MODy0;MODz0;MODx1;MODy1;MODz1;MODx2;MODy2;MODz2 - coefficients.", 'color2trans', 1, 1, 1);
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, morphology, extract, segmentation) VALUES ('pyrmeanshift','psocv','psocv -o pyrmeanshift', 0, "tif", "tif", "3;
The spatial window radius;double;0.0;
The color window radius;double;0.0;
Max level;int;0;

-s $1,$2,$3;", "This operator implements meanshift algorithm

INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
The spatial window radius;
The color window radius;
Max level.", 'meanshift', 1, 1, 1, 1);
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, morphology, extract, segmentation) VALUES ('pyrsegment','psocv','psocv -o pyrsegm', 0, "tif", "tif", "3;
Threshold 1;double;0.0;
Threshold 2;double;0.0;
Max level;int;0;

-s $1,$2,$3;", "This operator implements segmentation by pyramids algorithm

INPUT
1 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Threshold 1;
Threshold 2;
Max level.", 'pyrsegment', 1, 1, 1, 1);
INSERT OR REPLACE INTO Operators (name, implementor, executable, type, inputs, outputs, uidescription, message, metaname, metaweight, morphology, extract, segmentation) VALUES ('grabcut','psocv','psocvp -o grabcut', 1, "tif,tif", "tif", "3;
Threshold 1;double;0.0;
Threshold 2;double;0.0;
Iter count;int;0;

-s $1,$2,$3;", "This operator implements segmentation by grabcut algorithm

INPUT
1 image:.tif
2 image:.tif
OUTPUT
1 image:.tif
PARAMETERS
Threshold 1;
Threshold 2;
Iter count.", 'grabcut', 1, 1, 1, 1);
