/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin Kozlov 2010 <kozlov@spbcas.ru>
 * 
 * psocv is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * psocv is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fenv.h>
#include <math.h>
#include <glib.h>

#include <operations.h>

static gchar*oper;
static gchar*ostring;
static int rounding_mode_old, rounding_mode_new, rounding_mode_res;

static GOptionEntry entries[] = 
{
	{ "operation", 'o', 0, G_OPTION_ARG_STRING, &oper, "Operation", NULL },
	{ "options", 's', 0, G_OPTION_ARG_STRING, &ostring, "Options", NULL },
	{ NULL }
};

int main(int argc,char **argv)
{
	GError *gerror = NULL;
	GOptionContext *context;
	char*inputs_list = NULL, *outputs_list = NULL;
	char**input_pointers = NULL, **output_pointers = NULL;
	int n_input_pointers = 0, n_output_pointers = 0;
	context = g_option_context_new (" - Use OpenCV operations in ProStack");
	g_option_context_add_main_entries (context, entries, "psocv");
	if ( ! g_option_context_parse(context, &argc, &argv, &gerror) ) {
		g_warning(gerror->message);
		g_error_free(gerror);
		gerror = NULL;
	}
	g_option_context_free(context);
	if ( argc < 2 ) {
		g_error("No enough input files");
	}
	if ( oper == NULL ) {
		g_error("Operation not given");
	}
	rounding_mode_new = FE_TOWARDZERO;
	rounding_mode_old = fegetround();
	rounding_mode_res = fesetround(rounding_mode_new);
	inputs_list = g_strdup(argv[argc - 2]);
	input_pointers = g_strsplit(inputs_list, ",", MAX_RECORD);
	if ( input_pointers ) {
		n_input_pointers = g_strv_length(input_pointers);
	}
	outputs_list = g_strdup(argv[argc - 1]);
	output_pointers = g_strsplit(outputs_list, ",", MAX_RECORD);
	if ( output_pointers ) {
		n_output_pointers = g_strv_length(output_pointers);
	}
	if ( !g_strcmp0(oper, "canny") ) {
		psocv_canny(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !g_strcmp0(oper, "harris") ) {
		psocv_harris(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !g_strcmp0(oper, "split") ) {
		psocv_split(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !g_strcmp0(oper, "colorconv") ) {
		psocv_color_convert(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !g_strcmp0(oper, "colortrans") ) {
		psocv_color_transform(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !g_strcmp0(oper, "pyrmeanshift") ) {
		psocv_pyrmeanshift(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !g_strcmp0(oper, "pyrsegm") ) {
		psocv_pyrsegm(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else {
		g_error("psocv unknown operation %s", oper);
	}
	g_free(oper);
	g_free(ostring);
	rounding_mode_res = fesetround(rounding_mode_old);
	return (0);
}
