/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin N.Kozlov 2008 <kozlov@freya>
 * 
 * main.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * main.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>

#define  MAXTOKENS 256

static char*password;
static char*user;

static GOptionEntry entries[] = 
{
	{ "passowrd", 'p', 0, G_OPTION_ARG_STRING, &password, "password", "secret" },
	{ "user", 'u', 0, G_OPTION_ARG_STRING, &user, "user", "login" },
	{ NULL }
};

int main(int argc, char**argv)
{
	GError *gerror = NULL;
	GSpawnFlags flaggs;
	GString*command;
	GOptionContext *context;
	char**margv;
	char*standard_error = NULL;
	gint exit_status;
	int argcp;
	g_type_init();
	context = g_option_context_new ("URL input output");
	g_option_context_add_main_entries(context, entries, "cmove");
	if (!g_option_context_parse (context, &argc, &argv, &gerror)) {
		if ( gerror )
			g_error ("%s:option parsing failed: %s\n", g_get_prgname(), gerror->message);
	}
	g_option_context_free (context);
	flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL;
	command = g_string_new("curl");
	g_string_append(command, " -k");
	if ( user )
		g_string_append_printf(command, " -u %s", user);
	if ( password )
		g_string_append_printf(command, ":%s", password);
	if ( !g_access(argv[argc-2], F_OK) ) {
		g_string_append_printf(command, " --upload-file %s %s/%s", g_shell_quote(argv[argc-2]), argv[argc-3], g_path_get_basename(argv[argc-1]));
	} else {
		g_string_append_printf(command, " -o %s %s/%s", g_shell_quote(argv[argc-1]), argv[argc-3], g_path_get_basename(argv[argc-2]));
	}
	if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
		if ( gerror ) {
			g_warning("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
			g_error_free(gerror);
			return -1;
		}
	}
	g_string_free(command, TRUE);
	if ( !g_spawn_sync(g_get_current_dir(), margv, NULL, flaggs, NULL, NULL, NULL, &standard_error, &exit_status, &gerror) ) {
		g_warning("Error: ");
		if ( gerror ) {
			g_warning(gerror->message);
			g_error_free(gerror);
		}
	}
	g_fprintf(stderr, standard_error);
	g_strfreev(margv);
	return (exit_status);
}
