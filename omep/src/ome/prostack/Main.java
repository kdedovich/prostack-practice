/*
 * ome.prostack.Main
 *
 *------------------------------------------------------------------------------
 *  Copyright (C) 2009 ProStack LLC. All rights reserved.
 *
 *  This code is modified from the OmeroImageJ package by
 *  Konstantin Kozlov <kozlov@spbcas.ru>.
 *
 *  This code is licensed under GNU General Public License version 3.
 *  The original comments and notes are preserved for further reference.
 *
 *------------------------------------------------------------------------------
 *  Copyright (C) 2006 University of Dundee. All rights reserved.
 *
 *
 * 	This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *------------------------------------------------------------------------------
 */

package ome.prostack;

//Java imports

//Third-party libraries

//Application-internal dependencies

/** 
 * Application entry point.
 * This class implements the main method, which gets one or two optional
 * arguments to specify the path to the installation directory.
 * <p>If this argument doesn't specify an absolute path, then it'll be 
 * translated into an absolute path. Translation is system dependent -- in
 * many cases, the path is resolved against the user directory (typically the
 * directory in which the JVM was invoked).</p>
 *
 * @author  Jean-Marie Burel &nbsp;&nbsp;&nbsp;&nbsp;
 * 				<a href="mailto:j.burel@dundee.ac.uk">j.burel@dundee.ac.uk</a>
 * @author  <br>Andrea Falconi &nbsp;&nbsp;&nbsp;&nbsp;
 * 				<a href="mailto:a.falconi@dundee.ac.uk">
 * 					a.falconi@dundee.ac.uk</a>
 * @version 2.2 
 * <small>
 * (<b>Internal version:</b> $Revision: 6372 $ $Date: 2009-05-05 10:02:42 +0400 (Tue, 05 May 2009) $)
 * </small>
 * @since OME2.2
 */
public class Main 
{
	
	/**
	 * Main method.
	 * 
	 * @param args	Optional configuration file and path to the installation 
	 * 				directory. If not specified, then the user directory is 
	 * 				assumed and the <code>container.xml</code> is used.
	 */
	public static void main(String[] args) 
	{
		if ( args.length != 6 ) {
			System.err.println("Usage: programm <host> <port> <user> <password> <Project/dataset/image> <blob output file name>\nAll argumnets are mandatory\nSend bugs&comments to <kozlov@spbcas.ru>.");
			System.exit(1);
			return;
		}
		OmeroProStack op = new OmeroProStack(args[2], args[3], args[0], Integer.parseInt(args[1]));
		op.connect();
		String[] inputs = args[4].split("/");
		if ( inputs.length < 3 ) {
			System.err.println("Usage: programm <host> <port> <user> <password> <Project/dataset/image> <blob output file name>\nAll argumnets are mandatory\nSend bugs&comments to <kozlov@spbcas.ru>.");
			System.exit(1);
			return;
		}
/*		String[] outputs = args[5].split(",");
		if ( outputs.length != 2 ) {
			System.err.println("Usage: programm <host> <port> <user> <password> <Project/dataset/image> <blob output file name>,<stat output file name>\nAll argumnets are mandatory\nSend bugs&comments to <kozlov@spbcas.ru>.");
			return;
		}
		op.download(inputs[0], inputs[1], inputs[2], outputs[0], outputs[1]);*/
		op.download(inputs[inputs.length - 3], inputs[inputs.length - 2], inputs[inputs.length - 1], args[5]);
		System.out.println("Done");
		op.logout(true);
		System.exit(1);
		return;
	}
	
}

