/*
 * ome.prostack.OmeroProStack 
 *
 *------------------------------------------------------------------------------
 *  Copyright (C) 2009 ProStack LLC. All rights reserved.
 *
 *  This code is modified from the OmeroImageJ package by
 *  Konstantin Kozlov <kozlov@spbcas.ru>.
 *
 *  This code is licensed under GNU General Public License version 3.
 *  The original comments and notes are preserved for further reference.
 *
 *------------------------------------------------------------------------------
 *  Copyright (C) 2006-2009 University of Dundee. All rights reserved.
 *
 *
 * 	This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *------------------------------------------------------------------------------
 */
package ome.prostack;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.io.*;

//Application-internal dependencies
import ome.prostack.data.ServicesFactory;
import ome.prostack.data.DataService;
import ome.prostack.data.ImageObject;
import ome.prostack.data.*;
import org.openmicroscopy.shoola.util.ui.login.LoginCredentials;
import pojos.DataObject;
import pojos.DatasetData;
import pojos.ImageData;
import pojos.ProjectData;

/** 
 * Entry point of the <code>OMERO ImageJ</code> plugin.
 *
 * @author  Jean-Marie Burel &nbsp;&nbsp;&nbsp;&nbsp;
 * <a href="mailto:j.burel@dundee.ac.uk">j.burel@dundee.ac.uk</a>
 * @author Donald MacDonald &nbsp;&nbsp;&nbsp;&nbsp;
 * <a href="mailto:donald@lifesci.dundee.ac.uk">donald@lifesci.dundee.ac.uk</a>
 * @version 3.0
 * <small>
 * (<b>Internal version:</b> $Revision: $Date: $)
 * </small>
 * @since 3.0-Beta4
 */
public class OmeroProStack 
{
	/** The default port value. */
	private int PORT = 4063;
	private String host = "localhost";
	private String user = "Guest";
	private String pass = "";
	/** Creates a new instance. */
	public OmeroProStack(String u, String p, String h, int P)
	{
		user = u;
		pass = p;
		host = h;
		PORT = P;
	}
	/** 
	 * Attempts to log in.
	 *  
	 * @param lc The login credentials.
	 */
	private void login(LoginCredentials lc)
	{
		int index = ServicesFactory.getInstance().login(lc);
		switch (index) {
			case ServicesFactory.NAME_FAILURE_INDEX:
				System.err.println("User name is not valid.");
				System.out.println("ERROR User name is not valid.");
				System.exit(1);
				return;
			case ServicesFactory.PASSWORD_FAILURE_INDEX:
				System.err.println("Password is not valid.");
				System.out.println("ERROR Password is not valid.");
				System.exit(1);
				return;
			case ServicesFactory.DNS_INDEX:
				System.err.println("Failed to log onto OMERO.\n" +
		                "Please check the server address or try again later.");
				System.out.println("ERROR Failed to log onto OMERO.");
				System.exit(1);
				return;
			case ServicesFactory.CONNECTION_INDEX:
				System.err.println("Failed to log onto OMERO.\n" +
		                "Please check the port or try again later.");
				System.out.println("ERROR Failed to log onto OMERO.");
				System.exit(1);
				return;
			case ServicesFactory.PERMISSION_INDEX:
				System.err.println("Failed to log onto OMERO.\n" +
		                "Please check the username and/or password\n " +
		                "or try again later.");
				System.out.println("ERROR Failed to log onto OMERO.");
				System.exit(1);
				return;
			case ServicesFactory.SUCCESS_INDEX:
				System.err.println("login successful" );
		}
	}
	
	/** 
	 * Closes the application. 
	 * 
	 * @param cancel Pass <code>true</code> to cancel any on-going
	 * 				 connection's attempt, <code>false</code> otherwise.
	 */
	public void logout(boolean cancel)
	{
		if ( cancel ) {
			ServicesFactory.getInstance().exitApplication();
			System.err.println("logout successful" );
		}
	}

/*	public void download(String ProjectName, String DasName, String ImgName, String outname_blob, String outname_stats)*/
	public void download(String ProjectName, String DasName, String ImgName, String outname_blob)
	{
		if (ServicesFactory.getInstance().isConnected()) {
			DataService ds = ServicesFactory.getInstance().getDataService();
			Collection projects;
			try {
				projects = ds.loadProjects();
				Iterator iter_pr = projects.iterator();
//				System.out.println("Projects = " + projects.size());
				while (iter_pr.hasNext()) {
					ProjectData object_pr;
					object_pr = (ProjectData) iter_pr.next();
//					System.out.println("Path:" + object_pr.getName());
					if ( object_pr.getName().equals(ProjectName) ) {
						Set datasets = object_pr.getDatasets();
						Iterator iter_das = datasets.iterator();
//						System.out.println("Datasets = " + datasets.size());
						while (iter_das.hasNext()) {
							DatasetData object_das;
							object_das = (DatasetData) iter_das.next();
//							System.out.println("Path:" + object_das.getName());
							if ( object_das.getName().equals(DasName) ) {
								long id = object_das.getId();
								Collection images = ds.loadImages(id);
								Iterator iter_img = images.iterator();
//								System.out.println("images = " + images.size());
								DataObject tmp;
								ImageData child;
								Class klass = object_das.getClass();
								while (iter_img.hasNext()) {
									tmp = (DataObject) iter_img.next();
									if ( tmp.getClass().equals(klass) && tmp.getId() == id) {
										if ( tmp instanceof DatasetData) {
											Set images_child = ((DatasetData) tmp).getImages();
											Iterator iter_img_child = images_child.iterator();
//											System.out.println("images child = " + images_child.size());
											DataObject tmp_child;
											ImageData child_child;
											while (iter_img_child.hasNext()) {
												tmp_child = (DataObject) iter_img_child.next();
//												System.out.println("Path:" + tmp_child.getId() + ";" );
												if (tmp_child instanceof ImageData) {
													child_child = (ImageData) tmp_child;
													System.out.println("Path:" + object_pr.getName() + "/" + object_das.getName() + "/" + child_child.getName() + " = not requested");
													if ( child_child.getName().equals(ImgName) ) {
														System.out.println("Path:" + object_pr.getName() + "/" + object_das.getName() + "/" + child_child.getName() + " = requested");
														try {
/*															long pixelsID = child_child.getDefaultPixels().getId();
															ImageObject img = ds.getImage(pixelsID);
															int sizeZ = img.getSizeZ();
															int sizeC = img.getSizeC();
															int sizeT = img.getSizeT();
															int sizeX = img.getSizeX();
															int sizeY = img.getSizeY();
															BufferedOutputStream stats_outputstream = new BufferedOutputStream(new FileOutputStream(outname_stats));
															String stats_str = new String(Integer.toString(sizeZ) + "\n" + Integer.toString(sizeC) + "\n" + Integer.toString(sizeT) + "\n" + Integer.toString(sizeX) + "\n" + Integer.toString(sizeY) + "\n");
															stats_outputstream.write(stats_str.getBytes());
															stats_outputstream.close();
															byte[] array;
															BufferedOutputStream blob_outputstream = new BufferedOutputStream(new FileOutputStream(outname_blob));
															double min, max;
															for (int z = 0; z < sizeZ; z++) {
																for (int c = 0; c < sizeC; c++) {
																	min = img.getGlobalMin(c);
																	max = img.getGlobalMax(c);
																	for (int t = 0; t < sizeT; t++) {
																		array = ds.getPlane(pixelsID, z, c, t);
//																		for (int a = 0; a < array.length; a++) {
																			blob_outputstream.write(array);
//																		}
																	}
																}
															}
															blob_outputstream.close();*/
															File f = new File(outname_blob);
															f = (File) ds.exportImageAsOMETiff( child_child.getId(), f);
														} catch (Exception e) {
															System.err.println("An error occured while downloading the image.");
															System.err.println(e.getMessage());
														}
													}
												}
											}
										}
									}
								}
//								System.out.println("No more images here;" );
							}
						}
//						System.out.println("No more datasets here;" );
					}
				}
//				System.out.println("No more projects here;" );
			} catch (DSAccessException e) {
				System.err.println("An access error occured while loading the projects.");
				System.err.println(e.getMessage());
			} catch (DSOutOfServiceException e) {
				System.err.println("An out-of-service error occured while loading the projects.");
				System.err.println(e.getMessage());
			}
		} else {
			System.err.println("Failed to download - not connected.\n");
		}
	}

	public void connect()
	{
		LoginCredentials lc = new LoginCredentials(user, pass, host, 0, PORT);
		login(lc);
	}
}

